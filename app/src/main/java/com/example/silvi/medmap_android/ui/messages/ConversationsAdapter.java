package com.example.silvi.medmap_android.ui.messages;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.example.medmap_commons.model.ChatRoom;
import com.example.medmap_commons.model.MessageType;
import com.example.medmap_commons.model.Role;
import com.example.medmap_commons.model.User;
import com.example.medmap_commons.model.UserSession;
import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.ui.base.MedMapApplication;
import com.example.silvi.medmap_android.ui.messages.chatroom.ChatRoomActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * Created by silvi on 05.05.2017.
 */

public class ConversationsAdapter extends RecyclerView.Adapter<ConversationsAdapter.ViewHolder> {

  private final int GET_MESSAGES_UPDATE = 1;

  private List<ChatRoom> chatRooms;
  private MessagesActivity context;


  public ConversationsAdapter(MessagesActivity context, List<ChatRoom> chatRooms) {
    this.chatRooms = chatRooms;
    this.context = context;
  }

  @Override
  public ConversationsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater inflater = null;
    inflater = LayoutInflater.from(context);

    View conversationsView = inflater.inflate(R.layout.item_conversation_details, parent, false);

    return new ConversationsAdapter.ViewHolder(conversationsView);
  }

  @Override
  public void onBindViewHolder(ConversationsAdapter.ViewHolder holder, final int position) {

    final ChatRoom chatRoom = chatRooms.get(position);

    final ImageView userPicture = holder.userPicture;
    TextView userName = holder.userName;
    final TextView lastMessage = holder.lastMessage;
    final TextView dateAndTime = holder.dateAndTime;
    final TextView doctorLabel = holder.doctorLabel;
    final LinearLayout conversationSection = holder.conversationSection;

    for (User user : chatRoom.getParticipants()) {
      if (user.getUserId() != UserSession.getUserId()) {

        if (user.getRole().equals(Role.ROLE_DOCTOR) && UserSession.getUserRole() == Role.ROLE_DOCTOR) {
          doctorLabel.setVisibility(View.VISIBLE);
        } else {
          doctorLabel.setVisibility(View.GONE);
        }
        userName.setText(user.getFirstName() + " " + user.getLastName());

        Drawable userIcon = MedMapApplication.getInstance().getResources().getDrawable(R.drawable.user_icon);
        userIcon.setColorFilter(new LightingColorFilter(Color.LTGRAY, Color.LTGRAY));

        Glide.with(context)
            .load(user.getProfilePicture() != null ? user.getProfilePicture().getPath() : "")
            .asBitmap()
            .placeholder(userIcon)
            .centerCrop()
            .into(new BitmapImageViewTarget(userPicture) {
              @Override
              protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                    RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                circularBitmapDrawable.setCornerRadius(
                    Math.max(resource.getWidth(), resource.getHeight()) / 2.0f);
                userPicture.setImageDrawable(circularBitmapDrawable);
              }
            });

        DatabaseReference mDatabaseReference = FirebaseDatabase.getInstance().getReference(chatRoom.getChatRoomId().toString());
        Query lastQuery = mDatabaseReference.child("messages").orderByKey().limitToLast(1);

        lastQuery.addListenerForSingleValueEvent(new ValueEventListener() {
          @Override
          public void onDataChange(DataSnapshot dataSnapshot) {
            HashMap map = (HashMap) dataSnapshot.getValue();


            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm' - 'dd MMM");

            if (map != null) {
              for (Object value : map.values()) {
                if (((HashMap) value).get("type").toString().toUpperCase().equals(MessageType.TEXT.toString())) {
                  if (((HashMap) value).get("content").toString().length() < 22) {
                    lastMessage.setText(((HashMap) value).get("content").toString());
                  } else {
                    lastMessage.setText(((HashMap) value).get("content").toString().substring(0, 20) + " ..");
                  }
                } else {
                  lastMessage.setText("Image");
                }

                if (((HashMap) value).get("ridden").toString().equals("false") && !((HashMap) value).get("authorId").toString().equals(UserSession.getUserId().toString())) {
                  conversationSection.setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryTransparent));
                } else {
                  conversationSection.setBackgroundColor(context.getResources().getColor(R.color.white));
                }
                Timestamp timestamp = new Timestamp((long) ((HashMap) value).get("sentTime"));
                dateAndTime.setText(simpleDateFormat.format(timestamp));
              }


            }
          }

          @Override
          public void onCancelled(DatabaseError databaseError) {

          }
        });


      }
    }

    holder.itemView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        context.startActivityForResult(ChatRoomActivity.makeIntent(context, chatRoom), GET_MESSAGES_UPDATE);
      }
    });


  }


  @Override
  public int getItemCount() {
    return chatRooms.size();
  }

  public static class ViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.user_picture)
    ImageView userPicture;
    @Bind(R.id.user_name)
    TextView userName;
    @Bind(R.id.last_message_text)
    TextView lastMessage;
    @Bind(R.id.date_and_time)
    TextView dateAndTime;
    @Bind(R.id.doctorLabel)
    TextView doctorLabel;
    @Bind(R.id.conversation_section)
    LinearLayout conversationSection;

    public ViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}