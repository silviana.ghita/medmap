package com.example.silvi.medmap_android.ui.requests;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by silvi on 04.04.2017.
 */

class RequestsPageAdapter extends FragmentPagerAdapter {
  private List<Fragment> fragments;

  public RequestsPageAdapter(FragmentManager fragmentManager, List<Fragment> fragments) {
    super(fragmentManager);
    this.fragments = fragments;
  }

  @Override
  public Fragment getItem(int position) {
    return this.fragments.get(position);
  }

  @Override
  public int getCount() {
    return this.fragments.size();
  }
}