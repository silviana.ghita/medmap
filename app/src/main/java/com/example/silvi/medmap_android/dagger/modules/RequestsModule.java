package com.example.silvi.medmap_android.dagger.modules;

import com.example.medmap_api.service.RequestService;
import com.example.silvi.medmap_android.domain.request.delete.DeleteRequestInteractor;
import com.example.silvi.medmap_android.domain.request.delete.DeleteRequestInteractorImpl;
import com.example.silvi.medmap_android.domain.request.get.GetRequestsInteractor;
import com.example.silvi.medmap_android.domain.request.get.GetRequestsInteractorImpl;
import com.example.silvi.medmap_android.domain.request.update.UpdateRequestInteractor;
import com.example.silvi.medmap_android.domain.request.update.UpdateRequestInteractorImpl;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.MainThread;
import com.example.silvi.medmap_android.ui.requests.RequestsActivity;
import com.example.silvi.medmap_android.ui.requests.RequestsContract;
import com.example.silvi.medmap_android.ui.requests.RequestsPresenter;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;

/**
 * Created by silvi on 25.03.2017.
 */

@Module
public class RequestsModule {

  private RequestsContract.View mView;

  public RequestsModule(RequestsActivity view) {
    this.mView = view;
  }

  @Provides
  public RequestsContract.View provideView() {
    return mView;
  }

  @Provides
  public RequestService provideRequestService(RestAdapter restAdapter) {
    return restAdapter.create(RequestService.class);
  }

  @Provides
  public GetRequestsInteractor provideGetRequestsInteractor(Executor executor, MainThread mainThread, RequestService requestService) {
    return new GetRequestsInteractorImpl(executor, mainThread, requestService);
  }

  @Provides
  public UpdateRequestInteractor provideUpdateRequestInteractor(Executor executor, MainThread mainThread, RequestService requestService) {
    return new UpdateRequestInteractorImpl(executor, mainThread, requestService);
  }

  @Provides
  public DeleteRequestInteractor provideDeleteRequestInteractor(Executor executor, MainThread mainThread, RequestService requestService) {
    return new DeleteRequestInteractorImpl(executor, mainThread, requestService);
  }

  @Provides
  RequestsPresenter providePresenter(RequestsContract.View view, GetRequestsInteractor getRequestsInteractor, UpdateRequestInteractor updateRequestInteractor, DeleteRequestInteractor deleteRequestInteractor) {
    return new RequestsPresenter(view, getRequestsInteractor, updateRequestInteractor, deleteRequestInteractor);
  }
}
