package com.example.silvi.medmap_android.ui.profile;

import com.example.medmap_commons.model.Doctor;
import com.example.medmap_commons.model.Role;
import com.example.medmap_commons.model.User;

import java.io.File;

/**
 * Created by silvi on 12.03.2017.
 */

public interface ProfileContract {


  interface View {


    String getFirstName();

    String getLastName();

    String getEmail();

    String getPhoneNumber();

    String getSpecialization();

    String getStudies();

    String getExperience();

    String getDescription();

    String getBirthDate();

    File getImagePath();

    void initUserFields(User user);

    void initDoctorFields(Doctor doctor);

    void showToast(String text);

  }

  interface Presenter {

    void getProfileDetails(Long userId, Role userRole);

    void onSaveProfileDetailsClicked();

    void onSavePictureClicked();
  }
}
