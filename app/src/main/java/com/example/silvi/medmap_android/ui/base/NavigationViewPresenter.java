package com.example.silvi.medmap_android.ui.base;

import com.example.medmap_commons.model.UserSession;
import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.domain.logout.LogoutInteractor;
import com.example.silvi.medmap_android.preferences.SharedPreferencesHandler;

import javax.inject.Inject;

import retrofit.RetrofitError;

/**
 * Created by silvi on 11.03.2017.
 */

public class NavigationViewPresenter implements LogoutInteractor.Callback {

  private NavigationViewActivity mView;
  private LogoutInteractor logoutInteractor;

  @Inject
  public NavigationViewPresenter(NavigationViewActivity mView, LogoutInteractor logoutInteractor) {
    this.mView = mView;
    this.logoutInteractor = logoutInteractor;
  }

  public void onLogoutClick() {
    logoutInteractor.execute(this);
  }

  @Override
  public void onLogoutSuccess() {
    UserSession.clear();
    SharedPreferencesHandler.clearKeys(MedMapApplication.getInstance());
    mView.goToLogin();
  }

  @Override
  public void onLogoutError(RetrofitError retrofitError) {
    mView.showError(MedMapApplication.getInstance().getString(R.string.logout_problem));
  }


}
