package com.example.silvi.medmap_android.ui.home;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.medmap_commons.model.Cabinet;
import com.example.medmap_commons.model.CabinetsFilter;
import com.example.medmap_commons.model.Role;
import com.example.medmap_commons.model.UserSession;
import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.commons.IntentKeys;
import com.example.silvi.medmap_android.dagger.components.DaggerIHomeComponent;
import com.example.silvi.medmap_android.dagger.components.IAppComponent;
import com.example.silvi.medmap_android.dagger.modules.HomeModule;
import com.example.silvi.medmap_android.dagger.modules.NavigationModule;
import com.example.silvi.medmap_android.firebase.RegistrationIntentService;
import com.example.silvi.medmap_android.ui.base.BasePresenter;
import com.example.silvi.medmap_android.ui.base.FinishActivities;
import com.example.silvi.medmap_android.ui.base.FinishCallback;
import com.example.silvi.medmap_android.ui.base.NavigationViewActivity;
import com.example.silvi.medmap_android.ui.cabinet.CabinetActivity;
import com.example.silvi.medmap_android.ui.cabinet.add.AddCabinetActivity;
import com.example.silvi.medmap_android.ui.cabinet.update.UpdateCabinetActivity;
import com.example.silvi.medmap_android.ui.filters.FiltersActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

import javax.inject.Inject;

import butterknife.OnClick;

public class HomeActivity extends NavigationViewActivity implements OnMapReadyCallback, FinishCallback, GoogleMap.OnInfoWindowClickListener,
    GoogleMap.OnMapClickListener, HomeContract.View {

  private static final int FILTERS = 109;
  private static final int UPDATES = 110;

  private GoogleMap mMap;
  private List<Cabinet> cabinets;
  private List<Cabinet> doctorCabinets;

  @Inject
  HomePresenter mHomePresenter;

  private BroadcastReceiver mBroadcastReceiver;

  private CabinetsFilter mCabinetsFilter;


  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    mHomePresenter.getAllCabinets();
    mCabinetsFilter = new CabinetsFilter();
    if (getIntent().hasExtra(IntentKeys.FILTERS)) {
      mCabinetsFilter = getIntent().getParcelableExtra(IntentKeys.FILTERS);
      mHomePresenter.onFilterChanged(mCabinetsFilter);
    }


    SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
        .findFragmentById(R.id.map);
    mapFragment.getMapAsync(this);


    mBroadcastReceiver = new BroadcastReceiver() {
      @Override
      public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(RegistrationIntentService.FCM_INTENT)) {
          String FCMToken = intent.getStringExtra("FCMToken");
          mHomePresenter.setFCMToken(FCMToken);
        }
      }
    };

    if (UserSession.getUserRole().equals(Role.ROLE_DOCTOR))
      mHomePresenter.getDoctorCabinets(UserSession.getUserId());

    setupToolbarDisplay();
    FinishActivities.addActivity(this);
  }


  @Override
  public void setUpComponent(IAppComponent appComponent) {
    DaggerIHomeComponent.builder()
        .iAppComponent(appComponent)
        .homeModule(new HomeModule(this))
        .navigationModule(new NavigationModule(this))
        .build()
        .inject(this);
  }


  @Override
  protected int getSelfNavigationViewItem() {
    return 0;
  }

  private void setupToolbarDisplay() {
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    final ActionBar actionBar = getSupportActionBar();
    View viewActionBar = getLayoutInflater().inflate(R.layout.actionbar_default, null);
    ActionBar.LayoutParams params = new ActionBar.LayoutParams(
        ActionBar.LayoutParams.WRAP_CONTENT,
        ActionBar.LayoutParams.MATCH_PARENT,
        Gravity.CENTER);
    TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.actionbar_textview);
    textviewTitle.setText(getActionBarTitle());
    Typeface robotoMedium = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");
    textviewTitle.setTypeface(robotoMedium);
    actionBar.setCustomView(viewActionBar, params);
    actionBar.setDisplayShowCustomEnabled(true);
    actionBar.setDisplayShowTitleEnabled(false);
    actionBar.setDisplayHomeAsUpEnabled(true);
    actionBar.setHomeButtonEnabled(false);

    getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_drawer));
  }

  @Override
  protected void onResume() {
    super.onResume();
    IntentFilter filter = new IntentFilter(RegistrationIntentService.FCM_INTENT);
    LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastReceiver, filter);
    setupNavigationView();
    mActionBarToolbar.setNavigationIcon(R.drawable.ic_drawer);
  }

  @Override
  protected int getLayout() {
    return R.layout.activity_home;
  }


  @Nullable
  @Override
  protected BasePresenter getPresenter() {
    return null;
  }

  @Override
  protected String getActionBarTitle() {
    return getResources().getString(R.string.home_screen);
  }


  @Override
  public void onMapReady(GoogleMap googleMap) {
    mMap = googleMap;

    LatLng romania = new LatLng(45.65063922468394, 25.609083250164986);

    CameraPosition cameraPosition = new CameraPosition.Builder()
        .target(romania)
        .zoom(13)
        .build();
    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


    mMap.setOnInfoWindowClickListener(this);
    mMap.setOnMapClickListener(this);


    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
        && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
      return;
    }
    mMap.setMyLocationEnabled(true);
  }

  @Override
  public void forceFinish() {
    this.finish();
  }


  public static Intent makeIntent(Context context) {
    return new Intent(context, HomeActivity.class);
  }


  public static Intent makeIntent(Context context, CabinetsFilter cabinetsFilter) {
    Intent intent = new Intent(context, HomeActivity.class);
    intent.putExtra(IntentKeys.FILTERS, cabinetsFilter);
    return intent;
  }


  @Override
  public void onInfoWindowClick(Marker marker) {
    Double latitude = marker.getPosition().latitude;
    Double longitude = marker.getPosition().longitude;

    for (Cabinet c : cabinets) {
      if (c.getLatitude().equals(latitude) && c.getLongitude().equals(longitude) && c.getDoctor().getUserId().equals(UserSession.getUserId())) {
        startActivityForResult(UpdateCabinetActivity.makeIntent(this, c, mHomePresenter.getFilter()), UPDATES);
      } else if (c.getLatitude().equals(latitude) && c.getLongitude().equals(longitude)) {
        startActivity(CabinetActivity.makeIntent(this, c));
      }
    }
  }


  @Override
  public void onMapClick(LatLng latLng) {
    if (UserSession.getUserRole().equals(Role.ROLE_DOCTOR)) {
      if (doctorCabinets != null && doctorCabinets.size() < 3)
        openConfirmationDialog(latLng);
      else {
        showToast(getResources().getString(R.string.maximum_cabinets_number_reached));
      }
    }
  }

  private void openConfirmationDialog(final LatLng latLng) {
    final Dialog dialog = new Dialog(this, R.style.CustomDialog);

    dialog.setContentView(R.layout.dialog_add_cabinet);

    dialog.findViewById(R.id.yes_button).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dialog.dismiss();
        mMap.addMarker(new MarkerOptions()
            .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons(80, 120, UserSession.getUserId())))
            .position(latLng)
            .title("Cabinet"));
        startActivity(AddCabinetActivity.makeIntent(getApplicationContext(), latLng));
        finish();
      }
    });

    dialog.findViewById(R.id.no_button).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dialog.dismiss();
      }
    });

    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    dialog.show();
  }

  public Bitmap resizeMapIcons(int width, int height, long userId) {
    Bitmap icon = BitmapFactory.decodeResource(getResources(),
        R.drawable.pin);
    Bitmap myIcon = BitmapFactory.decodeResource(getResources(),
        R.drawable.mypin);
    Bitmap resizedBitmap = Bitmap.createScaledBitmap(icon, width, height, false);
    Bitmap myResizedBitmap = Bitmap.createScaledBitmap(myIcon, width, height, false);

    if (userId == UserSession.getUserId())
      return myResizedBitmap;
    else return resizedBitmap;

  }


  @Override
  public void showToast(String text) {
    Toast.makeText(this, text, Toast.LENGTH_LONG).show();
  }

  @Override
  public void initCabinets(List<Cabinet> cabinets) {
    mMap.clear();
    this.cabinets = cabinets;
    addCabinetsToMap();
  }

  @Override
  public void setDoctorCabinets(List<Cabinet> doctorCabinets) {
    this.doctorCabinets = doctorCabinets;
  }

  private void addCabinetsToMap() {
    for (Cabinet cabinet : cabinets) {
      String cabinetSpecialization = cabinet.getSpecialization().replace("_", " ");
      cabinetSpecialization = StringUtils.capitalize(cabinetSpecialization.toLowerCase());

      LatLng romania = new LatLng(cabinet.getLatitude(), cabinet.getLongitude());
      mMap.addMarker(new MarkerOptions()
          .position(romania)
          .title(cabinet.getName())
          .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons(80, 120, cabinet.getDoctor().getUserId())))
          .snippet(cabinetSpecialization));
    }
  }

  @OnClick(R.id.filters_btn)
  void onFiltersClicked() {
    startActivityForResult(FiltersActivity.makeIntent(getApplicationContext(), mHomePresenter.getFilter()), FILTERS);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == FILTERS && resultCode == HomeActivity.RESULT_OK) {
      mCabinetsFilter = data.getParcelableExtra(IntentKeys.FILTERS);
      mHomePresenter.onFilterChanged(mCabinetsFilter);
    } else if (requestCode == UPDATES) {
      mMap.clear();
      mHomePresenter.getAllCabinets();
    }
  }


}
