package com.example.silvi.medmap_android.ui.cabinet;

import com.example.medmap_commons.model.Cabinet;
import com.example.medmap_commons.model.ChatRoom;
import com.example.medmap_commons.model.Image;
import com.example.medmap_commons.model.Request;
import com.example.medmap_commons.model.Role;

import java.util.List;

/**
 * Created by silvi on 26.02.2017.
 */

public interface CabinetContract {

  interface View {

    void setDoctorEmail(String email);

    void setDoctorPhoneNumber(String phoneNumber);

    void setDoctorExperience(String experience);

    void setDoctorSpecialization(String specialization);

    void setDoctorBirthDate(String birthDate);

    void setDoctorStudies(String studies);

    void showToast(String text);

    void setCabinetPhotos(List<Image> photos);

    void setDoctorPhoto(Image profilePicture);

    void setUserRequests(List<Request> requests);

    void startChatRoomActivity(ChatRoom chatRoom);

    void initOtherCabinets(List<Cabinet> cabinets);

    void checkEmptyFields();


  }

  interface Presenter {
    void getDoctorDetails(Long userId, Role userRole);

    void onSendRequestClicked(Cabinet cabinet);

    void getUserRequests();

    void openChatRoom(Long otherUserId);

    void getOtherCabinets(Long doctorId);
  }
}
