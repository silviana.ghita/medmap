package com.example.silvi.medmap_android.ui.filters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.medmap_commons.model.CabinetsFilter;
import com.example.medmap_commons.model.User;
import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.commons.IntentKeys;
import com.example.silvi.medmap_android.dagger.components.DaggerIFiltersComponent;
import com.example.silvi.medmap_android.dagger.components.IAppComponent;
import com.example.silvi.medmap_android.dagger.modules.FiltersModule;
import com.example.silvi.medmap_android.ui.base.BaseActivity;
import com.example.silvi.medmap_android.ui.base.BasePresenter;
import com.example.silvi.medmap_android.util.Specializations;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by silvi on 26.04.2017.
 */

public class FiltersActivity extends BaseActivity implements FiltersContract.View {

  @Bind(R.id.specializations_group)
  LinearLayout mSpecializationsGroup;
  @Bind(R.id.doctors_group)
  LinearLayout mDoctorsGroup;

  private List<String> selectedSpecializations = new ArrayList<>();
  private List<User> selectedDoctors = new ArrayList<>();

  private CabinetsFilter mCabinetsFilter;


  @Inject
  FiltersPresenter mPresenter;

  private List<User> mDoctorUsers;


  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mCabinetsFilter = getIntent().getParcelableExtra(IntentKeys.FILTERS);

    setArrayToDoctorList(mCabinetsFilter.getDoctors(), selectedDoctors);
    setArrayToStringList(mCabinetsFilter.getSpecializations(), selectedSpecializations);

    mPresenter.getDoctorUsers();
    renderSpecializations();

    setupToolbarDisplay();
  }

  private void setupToolbarDisplay() {
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    final ActionBar actionBar = getSupportActionBar();
    View viewActionBar = getLayoutInflater().inflate(R.layout.actionbar_default, null);
    ActionBar.LayoutParams params = new ActionBar.LayoutParams(
        ActionBar.LayoutParams.WRAP_CONTENT,
        ActionBar.LayoutParams.MATCH_PARENT,
        Gravity.CENTER);
    TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.actionbar_textview);
    textviewTitle.setText(getActionBarTitle());
    Typeface robotoMedium = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");
    textviewTitle.setTypeface(robotoMedium);
    actionBar.setCustomView(viewActionBar, params);
    actionBar.setDisplayShowCustomEnabled(true);
    actionBar.setDisplayShowTitleEnabled(false);
    actionBar.setDisplayHomeAsUpEnabled(true);

    getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_chevron_left_white));

  }


  public static Intent makeIntent(Context context, CabinetsFilter filter) {
    Intent intent = new Intent(context, FiltersActivity.class);
    intent.putExtra(IntentKeys.FILTERS, filter);
    return intent;
  }

  @Override
  protected int getLayout() {
    return R.layout.activity_filters;
  }

  @Override
  public void setUpComponent(IAppComponent appComponent) {
    DaggerIFiltersComponent.builder()
        .iAppComponent(appComponent)
        .filtersModule(new FiltersModule(this))
        .build()
        .inject(this);
  }

  @Nullable
  @Override
  protected BasePresenter getPresenter() {
    return null;
  }

  @Override
  protected String getActionBarTitle() {
    return getResources().getString(R.string.filters);
  }

  @OnClick(R.id.apply_filters_button)
  void onApplyFiltersClick() {
    applyFilters();
  }

  @OnClick(R.id.reset_btn)
  void onResetButtonClick() {
    selectedDoctors.clear();
    selectedSpecializations.clear();
    unselectCheckBox(mDoctorsGroup);
    unselectCheckBox(mSpecializationsGroup);
  }

  private void unselectCheckBox(ViewGroup parent) {
    for (int i = 0; i < parent.getChildCount(); i++) {
      View child = parent.getChildAt(i);
      if (child instanceof CheckBox) {
        ((CheckBox) child).setChecked(false);
      } else if (child instanceof TextView) {
        if (child.getTag() != null && child.getTag().equals("counter")) {
          ((TextView) child).setText("");
        }
      } else if (child instanceof ViewGroup) {
        unselectCheckBox((ViewGroup) child);
      }
    }
  }

  private void applyFilters() {
    buildFilters();
    setActivityResult();
    finish();
  }

  private void buildFilters() {
    mCabinetsFilter = new CabinetsFilter();

    mCabinetsFilter.setDoctors(getArrayFromDoctorsList(selectedDoctors));
    mCabinetsFilter.setSpecializations(getArrayFromStringList(selectedSpecializations));
  }

  private void setActivityResult() {
    Intent intent = new Intent();
    intent.putExtra(IntentKeys.FILTERS, mCabinetsFilter);
    setResult(RESULT_OK, intent);
  }

  @Override
  public void setDoctorUsers(List<User> doctorUsers) {
    mDoctorUsers = doctorUsers;

    renderDoctors(doctorUsers);
  }

  private void renderDoctors(List<User> doctorUsers) {

    mDoctorsGroup.removeAllViews();
    int row = 1;

    for (final User doctor : doctorUsers) {

      LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      final View view = inflater.inflate(R.layout.item_filter, null, false);
      CheckBox filterCheckbox = (CheckBox) view.findViewById(R.id.filter_checkbox);
      View separator = view.findViewById(R.id.separator);
      final TextView checkbox = (TextView) view.findViewById(R.id.checkbox);
      final String doctorFullName = doctor.getLastName() + " " + doctor.getFirstName();

      filterCheckbox.setText(doctor.getLastName() + " " + doctor.getFirstName());
      for (User selectedDoctor : selectedDoctors) {
        String selectedDoctorFullName = selectedDoctor.getLastName() + " " + selectedDoctor.getFirstName();
        if (selectedDoctorFullName.equals(doctorFullName)) {
          filterCheckbox.setChecked(true);
          checkbox.setText(getResources().getString(R.string.icon_check_circle));
          checkbox.setTextColor(getResources().getColor(R.color.colorPrimary));
        }
      }

      filterCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
          if (isChecked) {
            selectedDoctors.add(doctor);
            checkbox.setText(getResources().getString(R.string.icon_check_circle));
            checkbox.setTextColor(getResources().getColor(R.color.colorPrimary));
          } else {
            for (Iterator<User> iter = selectedDoctors.listIterator(); iter.hasNext(); ) {
              User next = iter.next();
              String nextFullName = next.getLastName() + " " + next.getFirstName();
              if (nextFullName.equals(doctorFullName)) {
                iter.remove();
              }
            }
            checkbox.setText(getResources().getString(R.string.icon_circle));
            checkbox.setTextColor(getResources().getColor(R.color.edit_text_hint_color));
          }
        }
      });

      if (row == doctorUsers.size()) {
        separator.setBackgroundColor(ContextCompat.getColor(this, R.color.edit_text_color));
      }

      row++;
      mDoctorsGroup.addView(view);
    }
  }

  private void renderSpecializations() {

    mSpecializationsGroup.removeAllViews();
    int row = 1;

    for (final Specializations specialization : Specializations.values()) {

      LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      final View view = inflater.inflate(R.layout.item_filter, null, false);
      CheckBox filterCheckbox = (CheckBox) view.findViewById(R.id.filter_checkbox);
      View separator = view.findViewById(R.id.separator);
      final TextView checkbox = (TextView) view.findViewById(R.id.checkbox);

      String text = String.valueOf(specialization.name().toLowerCase().charAt(0)).toUpperCase() + specialization.name().toLowerCase().subSequence(1, specialization.name().length());
      filterCheckbox.setText(text);
      for (String selectedSpecialization : selectedSpecializations) {
        if (selectedSpecialization.equals(specialization.toString())) {
          filterCheckbox.setChecked(true);
          checkbox.setText(getResources().getString(R.string.icon_check_circle));
          checkbox.setTextColor(getResources().getColor(R.color.colorPrimary));
        }
      }

      filterCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
          if (isChecked) {
            selectedSpecializations.add(specialization.toString());
            checkbox.setText(getResources().getString(R.string.icon_check_circle));
            checkbox.setTextColor(getResources().getColor(R.color.colorPrimary));
          } else {
            for (Iterator<String> iter = selectedSpecializations.listIterator(); iter.hasNext(); ) {
              String next = iter.next();
              if (next.equals(specialization.toString())) {
                iter.remove();
              }
            }
            checkbox.setText(getResources().getString(R.string.icon_circle));
            checkbox.setTextColor(getResources().getColor(R.color.edit_text_hint_color));
          }
        }
      });

      if (row == Specializations.values().length) {
        separator.setBackgroundColor(ContextCompat.getColor(this, R.color.edit_text_color));
      }

      row++;
      mSpecializationsGroup.addView(view);
    }


  }

  private String[] getArrayFromStringList(List<String> objects) {
    if (objects == null)
      return new String[0];

    String[] array = new String[objects.size()];
    for (int i = 0; i < objects.size(); i++) {
      array[i] = objects.get(i);
    }
    return array;
  }

  private User[] getArrayFromDoctorsList(List<User> objects) {
    if (objects == null)
      return new User[0];

    User[] array = new User[objects.size()];
    for (int i = 0; i < objects.size(); i++) {
      array[i] = objects.get(i);
    }
    return array;
  }

  private void setArrayToDoctorList(User[] array, List<User> objects) {
    if (array == null)
      return;

    for (User user : array) {
      User object;
      object = user;
      objects.add(object);
    }
  }

  private void setArrayToStringList(String[] array, List<String> objects) {
    if (array == null)
      return;

    for (String string : array) {
      String object;
      object = string;
      objects.add(object);
    }
  }

}
