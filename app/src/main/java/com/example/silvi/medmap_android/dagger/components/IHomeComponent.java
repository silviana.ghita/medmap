package com.example.silvi.medmap_android.dagger.components;

import com.example.medmap_api.service.CabinetService;
import com.example.medmap_api.service.ProfileService;
import com.example.silvi.medmap_android.dagger.ActivityScope;
import com.example.silvi.medmap_android.dagger.modules.HomeModule;
import com.example.silvi.medmap_android.dagger.modules.NavigationModule;
import com.example.silvi.medmap_android.domain.cabinet.get.GetAllCabinetsInteractor;
import com.example.silvi.medmap_android.domain.cabinet.get.other.GetOtherCabinetsInteractor;
import com.example.silvi.medmap_android.domain.firebase.FCMTokenInteractor;
import com.example.silvi.medmap_android.ui.home.HomeActivity;
import com.example.silvi.medmap_android.ui.home.HomeContract;
import com.example.silvi.medmap_android.ui.home.HomePresenter;

import dagger.Component;

/**
 * Created by silvi on 16.03.2017.
 */

@ActivityScope
@Component(
    dependencies = IAppComponent.class,
    modules = {
        HomeModule.class,
        NavigationModule.class
    }
)
public interface IHomeComponent extends IAppComponent {

  void inject(HomeActivity homeActivity);

  HomeContract.View getView();

  CabinetService getCabinetService();

  ProfileService getProfileService();

  FCMTokenInteractor getFCMTokenInteractor();

  GetAllCabinetsInteractor getAllCabinetsInteractor();

  GetOtherCabinetsInteractor getOtherCabinetsInteractor();

  HomePresenter getHomePresenter();
}
