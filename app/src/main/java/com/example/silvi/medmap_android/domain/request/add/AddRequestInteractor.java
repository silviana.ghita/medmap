package com.example.silvi.medmap_android.domain.request.add;

import com.example.medmap_commons.model.Request;
import com.example.medmap_commons.model.RestError;

/**
 * Created by silvi on 19.03.2017.
 */

public interface AddRequestInteractor {

  interface Callback {

    void onAddRequestSuccess();

    void onAddRequestError(RestError restError);
  }

  void execute(Callback callback, Request request);
}
