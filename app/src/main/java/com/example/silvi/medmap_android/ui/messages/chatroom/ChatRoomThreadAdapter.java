package com.example.silvi.medmap_android.ui.messages.chatroom;


import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.medmap_commons.model.Message;
import com.example.medmap_commons.model.MessageType;
import com.example.medmap_commons.model.UserSession;
import com.example.silvi.medmap_android.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by silvi on 02.05.2017.
 */

public class ChatRoomThreadAdapter extends FirebaseRecyclerAdapter {

  private final static int VIEW_TYPE_ME = 0;
  private final static int VIEW_TYPE_OTHER = 1;

  Dialog dialog;
  private Activity activity;
  DatabaseReference reference;


  public ChatRoomThreadAdapter(Activity activity, DatabaseReference reference) {
    super(Message.class, 0, ViewHolder.class, reference);
    this.activity = activity;
    this.reference = reference;
    dialog = new Dialog(activity, R.style.CustomDialog);
  }


  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    ViewGroup view;
    if (viewType == VIEW_TYPE_ME) {
      view = (ViewGroup) LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_mine, parent, false);
    } else {
      view = (ViewGroup) LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_other, parent, false);
    }
    try {
      Constructor<ViewHolder> constructor = ViewHolder.class.getConstructor(View.class);
      return constructor.newInstance(view);
    } catch (NoSuchMethodException e) {
      throw new RuntimeException(e);
    } catch (InvocationTargetException e) {
      throw new RuntimeException(e);
    } catch (InstantiationException e) {
      throw new RuntimeException(e);
    } catch (IllegalAccessException e) {
      throw new RuntimeException(e);
    }
  }


  @Override
  protected void populateViewHolder(RecyclerView.ViewHolder viewHolder, final Object model, int position) {
    Timestamp timestamp = new Timestamp(((Message) model).getSentTime());
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm' - 'dd MMM");

    if (((Message) model).getType().equals(MessageType.TEXT)) {

      ((ViewHolder) viewHolder).messageSection.setVisibility(View.VISIBLE);
      ((ViewHolder) viewHolder).messageImage.setVisibility(View.GONE);

      ((ViewHolder) viewHolder).messageContent.setText(((Message) model).getContent());
      ((ViewHolder) viewHolder).messageDate.setText(simpleDateFormat.format(timestamp));
    } else {

      ((ViewHolder) viewHolder).messageSection.setVisibility(View.GONE);
      ((ViewHolder) viewHolder).messageImage.setVisibility(View.VISIBLE);
      Glide
          .with(activity)
          .load(((Message) model).getContent())
          .centerCrop()
          .crossFade()
          .into(((ViewHolder) viewHolder).messageImage);


      ((ViewHolder) viewHolder).messageImage.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          openImageDialog(((Message) model).getContent());
        }
      });
    }
    ((Message) model).setRidden(true);

    reference.addValueEventListener(new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot) {

        HashMap<String, Object> messages = (HashMap<String, Object>) dataSnapshot.getValue();
        Iterator it = messages.entrySet().iterator();
        while (it.hasNext()) {
          Map.Entry pair = (Map.Entry) it.next();

          HashMap<String, Object> messageDetails = (HashMap<String, Object>) pair.getValue();
          Iterator it2 = messageDetails.entrySet().iterator();
          while (it2.hasNext()) {
            Map.Entry detailsPair = (Map.Entry) it2.next();
            if (detailsPair.getKey().equals("authorId")) {
              if (!detailsPair.getValue().equals(UserSession.getUserId())) {
                reference.child(pair.getKey().toString()).child("ridden").setValue(true);
              }
            }
            it2.remove();
          }

          it.remove();
        }
      }

      @Override
      public void onCancelled(DatabaseError databaseError) {

      }
    });
  }

  private void openImageDialog(String imageUrl) {


    dialog.setContentView(R.layout.dialog_expanded_image);

    ImageView imageView = (ImageView) dialog.findViewById(R.id.expanded_image);

    Glide
        .with(activity)
        .load(imageUrl)
        .centerCrop()
        .crossFade()
        .into(imageView);

    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    dialog.show();
  }

  @Override
  public int getItemViewType(int position) {
    Long authorID = ((Message) getItem(position)).getAuthorId();
    if (authorID.equals(UserSession.getUserId()))
      return VIEW_TYPE_ME;
    return VIEW_TYPE_OTHER;
  }

  public static class ViewHolder extends RecyclerView.ViewHolder {

    TextView messageContent;
    TextView messageDate;
    LinearLayout messageSection;
    ImageView messageImage;

    public ViewHolder(View v) {
      super(v);
      messageContent = (TextView) v.findViewById(R.id.message_content);
      messageDate = (TextView) v.findViewById(R.id.message_date);
      messageSection = (LinearLayout) v.findViewById(R.id.message_section);
      messageImage = (ImageView) v.findViewById(R.id.message_image);
    }
  }
}
