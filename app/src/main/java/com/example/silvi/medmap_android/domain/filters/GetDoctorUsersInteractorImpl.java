package com.example.silvi.medmap_android.domain.filters;

import com.example.medmap_api.service.ProfileService;
import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.model.User;
import com.example.medmap_commons.util.PrintLog;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.Interactor;
import com.example.silvi.medmap_android.executor.MainThread;

import java.util.List;

import javax.inject.Inject;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by silvi on 26.04.2017.
 */

public class GetDoctorUsersInteractorImpl implements Interactor, GetDoctorUsersInteractor {

  private final Executor mExecutor;
  private final MainThread mMainThread;
  private final ProfileService mProfileService;

  private Callback mCallback;

  @Inject
  public GetDoctorUsersInteractorImpl(Executor executor, MainThread mainThread, ProfileService profileService) {
    this.mProfileService = profileService;
    this.mExecutor = executor;
    this.mMainThread = mainThread;
  }

  @Override
  public void execute(Callback callback) {
    if (null == callback) {
      throw new IllegalArgumentException(
          "Callback can't be null, the client of this interactor needs to get the response " +
              "in the callback");
    }
    this.mCallback = callback;
    this.mExecutor.run(this);
  }

  @Override
  public void run() {
    mProfileService.getDoctorUsers(new retrofit.Callback<List<User>>() {
      @Override
      public void success(List<User> users, Response response) {
        notifySuccess(users);
      }

      @Override
      public void failure(RetrofitError error) {
        RestError restError = null;
        try {
          restError = (RestError) error.getBodyAs(RestError.class);
        } catch (Exception e) {
          PrintLog.printStackTrace(getClass().getSimpleName(), e);
        }
        notifyError(restError);
      }
    });

  }

  private void notifySuccess(final List<User> doctorUsers) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onGetDoctorUsersSuccess(doctorUsers);
      }
    });
  }

  private void notifyError(final RestError restError) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onGetDoctorUsersError(restError);
      }
    });
  }
}
