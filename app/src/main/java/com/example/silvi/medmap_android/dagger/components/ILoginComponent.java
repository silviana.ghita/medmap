package com.example.silvi.medmap_android.dagger.components;

import com.example.medmap_api.service.AuthService;
import com.example.silvi.medmap_android.dagger.ActivityScope;
import com.example.silvi.medmap_android.dagger.modules.LoginModule;
import com.example.silvi.medmap_android.domain.login.LoginInteractor;
import com.example.silvi.medmap_android.ui.auth.login.LoginActivity;
import com.example.silvi.medmap_android.ui.auth.login.LoginContract;
import com.example.silvi.medmap_android.ui.auth.login.LoginPresenter;

import dagger.Component;

/**
 * Created by silvi on 08.03.2017.
 */

@ActivityScope
@Component(
    dependencies = IAppComponent.class,
    modules = LoginModule.class
)
public interface ILoginComponent extends IAppComponent {

  void inject(LoginActivity loginActivity);

  LoginContract.View getView();

  AuthService getAuthService();

  LoginInteractor getLoginInteractor();

  LoginPresenter getLoginPresenter();
}
