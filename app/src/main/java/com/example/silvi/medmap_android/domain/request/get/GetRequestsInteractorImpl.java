package com.example.silvi.medmap_android.domain.request.get;

import com.example.medmap_api.service.RequestService;
import com.example.medmap_commons.model.Request;
import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.util.PrintLog;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.Interactor;
import com.example.silvi.medmap_android.executor.MainThread;

import java.util.List;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by silvi on 25.03.2017.
 */

public class GetRequestsInteractorImpl implements Interactor, GetRequestsInteractor {

  private final Executor mExecutor;
  private final MainThread mMainThread;
  private final RequestService mRequestService;

  private Callback mCallback;

  public GetRequestsInteractorImpl(Executor executor, MainThread mainThread, RequestService requestService) {
    mExecutor = executor;
    mMainThread = mainThread;
    mRequestService = requestService;
  }

  @Override
  public void execute(Callback callback) {
    if (null == callback) {
      throw new IllegalArgumentException(
          "Callback can't be null, the client of this interactor needs to get the response " +
              "in the callback");
    }
    this.mCallback = callback;
    this.mExecutor.run(this);
  }

  @Override
  public void run() {
    mRequestService.getRequests(new retrofit.Callback<List<Request>>() {

      @Override
      public void success(List<Request> requests, Response response) {
        notifySuccess(requests);
      }

      @Override
      public void failure(RetrofitError error) {
        RestError restError = null;
        try {
          restError = (RestError) error.getBodyAs(RestError.class);
        } catch (Exception e) {
          PrintLog.printStackTrace(getClass().getSimpleName(), e);
        }
        notifyError(restError);

      }
    });
  }

  private void notifySuccess(final List<Request> requests) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onGetDoctorRequestsSuccess(requests);
      }
    });
  }

  private void notifyError(final RestError restError) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onGetDoctorRequestsError(restError);
      }
    });
  }

}
