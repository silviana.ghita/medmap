package com.example.silvi.medmap_android.ui.auth.login;

import com.example.medmap_commons.model.LoggedInUser;
import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.model.UserSession;
import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.domain.login.LoginInteractor;
import com.example.silvi.medmap_android.firebase.FirebaseHandler;
import com.example.silvi.medmap_android.logic.UserLogic;
import com.example.silvi.medmap_android.ui.base.MedMapApplication;
import com.squareup.okhttp.Credentials;

import javax.inject.Inject;

/**
 * Created by silvi on 23.02.2017.
 */

public class LoginPresenter implements LoginContract.Presenter, LoginInteractor.Callback {

  private LoginContract.View mView;
  private LoginInteractor loginInteractor;

  @Inject
  public LoginPresenter(LoginContract.View mView, LoginInteractor loginInteractor) {
    this.mView = mView;
    this.loginInteractor = loginInteractor;
  }


  @Override
  public void onLoginClick() {

    String email = mView.getEmail();
    String password = mView.getPassword();
    String authorization = Credentials.basic(email, password);

    loginInteractor.execute(this, authorization);
  }


  @Override
  public void onLoginSuccess(LoggedInUser loggedInUser) {

    UserSession.setUserEmail(loggedInUser.getEmail());
    UserSession.setXAuthToken(loggedInUser.getXAuthToken());
    UserSession.setUserId(loggedInUser.getUserId());
    UserSession.setUserRole(loggedInUser.getRole());
    UserSession.setUserFullName(loggedInUser.getFirstName() + " " + loggedInUser.getLastName());
    UserSession.setUserId(loggedInUser.getUserId());
    new UserLogic().saveUserSession();
    FirebaseHandler.startFCMService();

    mView.startHomeActivity();
  }

  @Override
  public void onLoginError(RestError restError) {
    mView.showToast(MedMapApplication.getInstance().getString(R.string.incorrect_data));
  }

}
