package com.example.silvi.medmap_android.ui.cabinet.update;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.medmap_commons.model.Cabinet;
import com.example.medmap_commons.model.CabinetsFilter;
import com.example.medmap_commons.model.Category;
import com.example.medmap_commons.model.Image;
import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.adapters.NamedEntityAdapter;
import com.example.silvi.medmap_android.commons.IntentKeys;
import com.example.silvi.medmap_android.dagger.components.DaggerIUpdateCabinetComponent;
import com.example.silvi.medmap_android.dagger.components.IAppComponent;
import com.example.silvi.medmap_android.dagger.modules.UpdateCabinetModule;
import com.example.silvi.medmap_android.ui.CustomPhotoGalleryActivity;
import com.example.silvi.medmap_android.ui.appointments.add.AddApointmentActivity;
import com.example.silvi.medmap_android.ui.base.BaseActivity;
import com.example.silvi.medmap_android.ui.base.BasePresenter;
import com.example.silvi.medmap_android.ui.base.FinishActivities;
import com.example.silvi.medmap_android.ui.base.FinishCallback;
import com.example.silvi.medmap_android.ui.base.MedMapApplication;
import com.example.silvi.medmap_android.util.Specializations;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by silvi on 16.03.2017.
 */

public class UpdateCabinetActivity extends BaseActivity implements UpdateCabinetContract.View, FinishCallback {

  private final int PICK_IMAGE_MULTIPLE = 1;

  @Bind(R.id.cabinet_name_input)
  EditText mCabinetName;
  @Bind(R.id.description_input)
  EditText mDescription;
  @Bind(R.id.specialization_layout)
  LinearLayout mSpecializationLayout;
  @Bind(R.id.category_layout)
  LinearLayout categoryLayout;
  @Bind(R.id.specialization_spinner)
  Spinner mSpecializationSpinner;
  @Bind(R.id.category_spinner)
  Spinner mCategorySpinner;
  @Bind(R.id.confirmButton)
  Button confirmButton;
  @Bind(R.id.number_of_selected_photos)
  TextView selectedPhotos;
  @Bind(R.id.horizontal_recycler_view)
  RecyclerView mCabinetImagesRecyclerView;

  @Inject
  UpdateCabinetPresenter mPresenter;

  private NamedEntityAdapter mSpecialzationsAdapter;
  private NamedEntityAdapter mCategoriesAdapter;
  HorizontalPhotosAdapter mHorizontalAdapter;
  private Cabinet cabinet;
  private ArrayList<String> imagesPathList;
  private List<Image> imagesToAdd;
  private boolean[] thumbnailsSelection = null;
  private List<Image> mInitialImageList;



  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    cabinet = getIntent().getParcelableExtra(IntentKeys.CABINET);


    mInitialImageList = new ArrayList<>();
    mInitialImageList.addAll(cabinet.getImages());


    FinishActivities.addActivity(this);
    setupToolbarDisplay();

    initLayout();
    setKeyboardVisibility();
    selectedPhotos.setText(MedMapApplication.getInstance().getResources()
        .getString(R.string.selected_photos, 0));

    mHorizontalAdapter = new HorizontalPhotosAdapter(this, cabinet.getImages());
    mCabinetImagesRecyclerView.setAdapter(mHorizontalAdapter);
    mHorizontalAdapter.notifyDataSetChanged();

    LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
    mCabinetImagesRecyclerView.setLayoutManager(horizontalLayoutManagaer);
    mCabinetImagesRecyclerView.setAdapter(mHorizontalAdapter);
  }

  private void initLayout() {
    mCabinetName.setText(cabinet.getName());
    mDescription.setText(cabinet.getDescription());

    initSpecializationSpinner();
    int specializationPos = getSpinnerSelection(mSpecialzationsAdapter.getItems(), cabinet.getSpecialization());
    mSpecializationSpinner.setSelection(specializationPos);

    initCategorySpinner();
    int categoryPos = getSpinnerSelection(mCategoriesAdapter.getItems(), cabinet.getCategory());
    mCategorySpinner.setSelection(categoryPos);
  }

  private void setKeyboardVisibility() {
    final View activityRootView = findViewById(R.id.parent);
    DisplayMetrics metrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(metrics);
    final int height = metrics.heightPixels;
    activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
      @Override
      public void onGlobalLayout() {
        int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight();
        if (heightDiff > (height / 4)) { // if more than 1/4 of the screen, its probably a keyboard...
          confirmButton.setVisibility(View.INVISIBLE);
        } else {
          confirmButton.setVisibility(View.VISIBLE);
        }
      }
    });
  }

  private void initSpecializationSpinner() {
    Specializations[] specializations = Specializations.values();
    List<String> specializationList = new ArrayList<>();
    for (int i = 0; i < specializations.length; i++)
      specializationList.add(specializations[i].toString());

    mSpecialzationsAdapter = new NamedEntityAdapter(this, R.layout.simple_spinner_item, specializationList);
    mSpecializationSpinner.setAdapter(mSpecialzationsAdapter);
  }

  private void initCategorySpinner() {
    Category[] categories = Category.values();
    List<String> categoriesList = new ArrayList<>();
    for (int i = 0; i < categories.length; i++)
      categoriesList.add(categories[i].toString());

    mCategoriesAdapter = new NamedEntityAdapter(this, R.layout.simple_spinner_item, categoriesList);
    mCategorySpinner.setAdapter(mCategoriesAdapter);
  }


  private void setupToolbarDisplay() {
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    final ActionBar actionBar = getSupportActionBar();
    View viewActionBar = getLayoutInflater().inflate(R.layout.actionbar_default, null);
    ActionBar.LayoutParams params = new ActionBar.LayoutParams(
        ActionBar.LayoutParams.WRAP_CONTENT,
        ActionBar.LayoutParams.MATCH_PARENT,
        Gravity.CENTER);
    TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.actionbar_textview);
    textviewTitle.setText(getActionBarTitle());
    Typeface robotoMedium = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");
    textviewTitle.setTypeface(robotoMedium);
    actionBar.setCustomView(viewActionBar, params);
    actionBar.setDisplayShowCustomEnabled(true);
    actionBar.setDisplayShowTitleEnabled(false);
    actionBar.setDisplayHomeAsUpEnabled(true);
    actionBar.setHomeButtonEnabled(false);

    getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_chevron_left_white));

  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (resultCode == RESULT_OK) {
      if (requestCode == PICK_IMAGE_MULTIPLE) {
        imagesPathList = new ArrayList<String>();
        imagesToAdd = new ArrayList<Image>();
        String[] imagesPath = data.getStringExtra("data").split("\\|");
        thumbnailsSelection = data.getBooleanArrayExtra("thumbnailsSelection");
        for (int i = 0; i < imagesPath.length; i++) {
          imagesPathList.add(imagesPath[i]);
          Image image = new Image();
          image.setPath(imagesPath[i]);
          imagesToAdd.add(image);
        }
        selectedPhotos.setText(MedMapApplication.getInstance().getResources()
            .getString(R.string.selected_photos, imagesPath.length));
      }
    }
    mHorizontalAdapter.addImagesToList(imagesToAdd);
    mHorizontalAdapter.notifyDataSetChanged();
  }

  @OnClick(R.id.add_external_appointment_btn)
  void onAddExternalAppointmentBtnClick() {
    startActivity(AddApointmentActivity.makeIntent(getApplicationContext(), cabinet, true));
  }


  @OnClick(R.id.add_photos_btn)
  void onAddPhotosClick() {
    startActivityForResult(CustomPhotoGalleryActivity.makeIntent(this, thumbnailsSelection), PICK_IMAGE_MULTIPLE);
  }

  @OnClick(R.id.confirmButton)
  void onConfirmButtonClick() {
    boolean isDeleted;
    List<Image> imagesToDelete = new ArrayList<>();
    for (Image image : mInitialImageList) {
      isDeleted = true;
      for (Image currentImage : cabinet.getImages()) {
        if (image.getImageId().equals(currentImage.getImageId())) {
          isDeleted = false;
          break;
        }
      }
      if (isDeleted)
        imagesToDelete.add(image);
    }
    if (imagesToDelete.size() > 0) {
      mPresenter.deleteCabinetPictures(cabinet.getId(), imagesToDelete);
    }

    if (imagesPathList != null) {
      if (imagesPathList.size() > 0) {
        boolean isNewImageDeleted;
        for (int i = 0; i < imagesPathList.size(); i++) {
          isNewImageDeleted = true;
          for (Image currentImage : cabinet.getImages()) {
            if (imagesPathList.get(i).equals(currentImage.getPath()))
              isNewImageDeleted = false;
          }
          if (isNewImageDeleted) {
            imagesPathList.remove(i);
            i--;
          }
        }
        mPresenter.onSavePicturesClicked();
      }
    }
    mPresenter.onSaveButtonClicked();
    finish();
  }



  @Override
  protected int getLayout() {
    return R.layout.activity_update_cabinet;
  }

  @Override
  public void setUpComponent(IAppComponent appComponent) {
    DaggerIUpdateCabinetComponent.builder()
        .iAppComponent(appComponent)
        .updateCabinetModule(new UpdateCabinetModule(this))
        .build()
        .inject(this);
  }

  @Nullable
  @Override
  protected BasePresenter getPresenter() {
    return null;
  }

  @Override
  protected String getActionBarTitle() {
    return getResources().getString(R.string.update_cabinet);
  }

  public Long getCabinetId() {
    return cabinet.getId();
  }

  public String getCabinetName() {
    return mCabinetName.getText().toString();
  }

  public String getDescription() {
    return mDescription.getText().toString();
  }

  public String getSpecialization() {
    return mSpecializationSpinner.getSelectedItem().toString();
  }

  public String getCategory() {
    return mCategorySpinner.getSelectedItem().toString();
  }


  @Override
  public void forceFinish() {
    this.finish();
  }

  public static Intent makeIntent(Context context, Cabinet cabinet, CabinetsFilter cabinetsFilter) {
    Intent intent = new Intent(context, UpdateCabinetActivity.class);

    intent.putExtra(IntentKeys.CABINET, cabinet);
    intent.putExtra(IntentKeys.FILTERS, cabinetsFilter);
    return intent;
  }

  private int getSpinnerSelection(List<String> items, String entity) {
    if (entity == null)
      return 0;

    int i = 0;
    for (String item : items) {
      if (item != null &&
          item.equals(entity))
        return i;
      i++;
    }
    return 0;
  }

  @Override
  public void showToast(String text) {
    Toast.makeText(this, text, Toast.LENGTH_LONG).show();
  }

  @Override
  public List<File> getImagesPath() {
    List<File> imagesPaths = new ArrayList<>();
    for (String path : imagesPathList) {
      File file = new File(path);
      imagesPaths.add(file);
    }
    return imagesPaths;
  }

  @Override
  public void deleteCabinetPicture(String imagePath, Integer position, Image image) {
    cabinet.getImages().remove(image);
    mHorizontalAdapter.notifyItemRemoved(position);
    mHorizontalAdapter.notifyDataSetChanged();

  }

}
