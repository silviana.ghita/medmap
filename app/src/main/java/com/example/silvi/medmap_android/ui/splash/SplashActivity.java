package com.example.silvi.medmap_android.ui.splash;

/**
 * Created by silvi on 11.05.2017.
 */

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.view.Window;
import android.view.WindowManager;

import com.daimajia.androidanimations.library.Techniques;
import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.ui.auth.login.LoginActivity;
import com.viksaa.sssplash.lib.activity.AwesomeSplash;
import com.viksaa.sssplash.lib.cnst.Flags;
import com.viksaa.sssplash.lib.model.ConfigSplash;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * @author Octa on 5/10/2016.
 */
public class SplashActivity extends AwesomeSplash {

  //DO NOT OVERRIDE onCreate()!

  @Override
  public void initSplash(ConfigSplash configSplash) {
    setupStatusBarColor();


    //Customize Circular Reveal
    configSplash.setBackgroundColor(R.color.lightGrayBackgroundColor);
    configSplash.setAnimCircularRevealDuration(1250); //int ms
    configSplash.setRevealFlagX(Flags.REVEAL_RIGHT);  //or Flags.REVEAL_LEFT
    configSplash.setRevealFlagY(Flags.REVEAL_BOTTOM); //or Flags.REVEAL_TOP

    //Customize Logo
    configSplash.setLogoSplash(R.drawable.medmap_logo_splash);
    configSplash.setAnimLogoSplashDuration(1000);//int ms
    configSplash.setAnimLogoSplashTechnique(randomAnimation());
    configSplash.setOriginalWidth(100);

    //Customize Title
    configSplash.setTitleSplash("");
    configSplash.setTitleTextColor(R.color.colorAccent);
    configSplash.setTitleTextSize(24f); //float value
    configSplash.setAnimTitleDuration(1250);
    configSplash.setAnimTitleTechnique(Techniques.FlipInX);


    int PERMISSION_ALL = 1;
    String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION};

    if (!hasPermissions(this, PERMISSIONS)) {
      ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
    }
  }

  public static boolean hasPermissions(Context context, String... permissions) {
    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
      for (String permission : permissions) {
        if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
          return false;
        }
      }
    }
    return true;
  }


  @Override
  public void animationsFinished() {
    goToLogin();
  }


  private void goToLogin() {
    startActivity(LoginActivity.makeIntent(this));
    finish();
  }

  private final List<Techniques> VALUES = Collections.unmodifiableList(Arrays.asList(Techniques.Landing, Techniques.FlipInX,
      Techniques.Flash, Techniques.Pulse, Techniques.RubberBand, Techniques.Shake, Techniques.Swing,
      Techniques.Wobble, Techniques.Bounce, Techniques.Tada, Techniques.StandUp, Techniques.Wave, Techniques.RollIn,
      Techniques.BounceIn, Techniques.BounceInDown, Techniques.BounceInLeft, Techniques.BounceInRight, Techniques.BounceInUp,
      Techniques.FadeIn, Techniques.FadeInDown, Techniques.FadeInLeft, Techniques.FadeInRight, Techniques.FadeInUp,
      Techniques.RotateIn, Techniques.RotateInDownLeft, Techniques.RotateInDownRight, Techniques.RotateInUpLeft, Techniques.RotateInUpLeft,
      Techniques.SlideInDown, Techniques.SlideInLeft, Techniques.SlideInRight, Techniques.SlideInUp,
      Techniques.ZoomIn, Techniques.ZoomInDown, Techniques.ZoomInLeft, Techniques.ZoomInRight));
  private final int SIZE = VALUES.size();
  private final Random RANDOM = new Random();

  public Techniques randomAnimation() {
    return VALUES.get(RANDOM.nextInt(SIZE));
  }

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  private void setupStatusBarColor() {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
      return;
    Window window = getWindow();
    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
    window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
    int color;
    Resources res = getResources();
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      color = res.getColor(getStatusBarColor(), null);
    } else {
      color = res.getColor(getStatusBarColor());
    }
    window.setStatusBarColor(color);
  }

  private int getStatusBarColor() {
    return R.color.colorPrimary;
  }

}
