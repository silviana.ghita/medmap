package com.example.silvi.medmap_android.logic;

import com.example.medmap_commons.model.UserSession;
import com.example.silvi.medmap_android.preferences.SharedPreferencesHandler;
import com.example.silvi.medmap_android.ui.base.MedMapApplication;

/**
 * Created by silvi on 11.03.2017.
 */

public class UserLogic {
  public void startUserSession() {
    UserSession.setXAuthToken(getXAuthToken());
    UserSession.setUserEmail(getUserEmail());
  }

  public void saveUserSession() {
    saveXAuthToken();
    saveUserEmail();
  }

  public String getXAuthToken() {
    return SharedPreferencesHandler.getAuthToken(MedMapApplication.getInstance());
  }

  public void saveXAuthToken() {
    SharedPreferencesHandler.setAuthToken(MedMapApplication.getInstance(), UserSession.getXAuthToken());
  }

  public String getUserEmail() {
    return SharedPreferencesHandler.getUserEmail(MedMapApplication.getInstance());
  }

  public void saveUserEmail() {
    SharedPreferencesHandler.setUserEmail(MedMapApplication.getInstance(), UserSession.getUserEmail());
  }
}
