package com.example.silvi.medmap_android.ui.messages;

import com.example.medmap_commons.model.ChatRoom;

import java.util.List;

/**
 * Created by silvi on 01.05.2017.
 */

public interface MessagesContract {

  interface View {

    void setChatRooms(List<ChatRoom> chatRooms);

  }

  interface Presenter {

    void getChatRooms();

  }
}
