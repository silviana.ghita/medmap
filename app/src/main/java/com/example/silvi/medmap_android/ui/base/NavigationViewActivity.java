package com.example.silvi.medmap_android.ui.base;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.dagger.components.DaggerINavigationComponent;
import com.example.silvi.medmap_android.dagger.modules.NavigationModule;
import com.example.silvi.medmap_android.ui.appointments.AppointmentsActivity;
import com.example.silvi.medmap_android.ui.appointments.today.TodayActivity;
import com.example.silvi.medmap_android.ui.auth.login.LoginActivity;
import com.example.silvi.medmap_android.ui.messages.MessagesActivity;
import com.example.silvi.medmap_android.ui.profile.ProfileActivity;
import com.example.silvi.medmap_android.ui.requests.RequestsActivity;
import com.example.silvi.medmap_android.util.UIUtils;

import javax.inject.Inject;

/**
 * Created by silvi on 22.02.2017.
 */

public abstract class NavigationViewActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

  protected static final int NAVIGATION_VIEW_ITEM_PROFILE = R.id.navItemProfile;
  protected static final int NAVIGATION_VIEW_ITEM_APPOINTMENTS = R.id.navItemAppointments;
  protected static final int NAVIGATION_VIEW_ITEM_MESSAGES = R.id.navItemMessages;
  protected static final int NAVIGATION_VIEW_ITEM_REQUESTS = R.id.navItemRequests;
  protected static final int NAVIGATION_VIEW_ITEM_LOGOUT = R.id.navItemLogout;
  protected static final int NAVIGATION_VIEW_ITEM_TODAY = R.id.navItemToday;


  private DrawerLayout mDrawerLayout;


  @Inject
  NavigationViewPresenter mPresenter;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    overridePendingTransition(0, 0);
    setupNavigationView();

    DaggerINavigationComponent.builder()
        .iAppComponent(MedMapApplication.getInstance().getComponent())
        .navigationModule(new NavigationModule(this))
        .build()
        .inject(this);


  }


  @Override
  public void setContentView(int layoutResID) {
    super.setContentView(R.layout.navigation_view_template);
    getLayoutInflater().inflate(layoutResID, (ViewGroup) findViewById(R.id.main_content));
  }

  @Override
  public void setContentView(View view) {
    super.setContentView(R.layout.navigation_view_template);
    ((ViewGroup) findViewById(R.id.main_content)).addView(view);
  }

  protected void setupNavigationView() {

    mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
    NavigationView navigationView = (NavigationView) findViewById(R.id.navView);
    if (null == navigationView || mDrawerLayout == null) {
      return;
    }

    setupDrawerIcon();
    navigationView.setCheckedItem(getSelfNavigationViewItem());
    navigationView.setNavigationItemSelectedListener(NavigationViewActivity.this);


    LayoutInflater inflater = this.getLayoutInflater();
    View dialogView = inflater.inflate(R.layout.drawer_header, null);
  }

  @Override
  public boolean onNavigationItemSelected(MenuItem menuItem) {
    Intent intent = getLaunchIntent(menuItem.getItemId());
    if (null != intent) {
      startActivity(intent);
    }

    return true;
  }

  private Intent getLaunchIntent(int id) {
    if (id == NAVIGATION_VIEW_ITEM_PROFILE) {
      return ProfileActivity.makeIntent(NavigationViewActivity.this);
    } else if (id == NAVIGATION_VIEW_ITEM_TODAY) {
      return TodayActivity.makeIntent(NavigationViewActivity.this);
    } else if (id == NAVIGATION_VIEW_ITEM_APPOINTMENTS) {
      return AppointmentsActivity.makeIntent(NavigationViewActivity.this);
    } else if (id == NAVIGATION_VIEW_ITEM_REQUESTS) {
      return RequestsActivity.makeIntent(NavigationViewActivity.this);
    } else if (id == NAVIGATION_VIEW_ITEM_MESSAGES) {
      return MessagesActivity.makeIntent(NavigationViewActivity.this);
    } else if (id == NAVIGATION_VIEW_ITEM_LOGOUT) {
      toggleDrawer();
      mPresenter.onLogoutClick();
    }
    return null;
  }

  public void goToLogin() {
    startActivity(LoginActivity.makeIntent(NavigationViewActivity.this));
  }

  public void showError(String text) {
    Toast.makeText(this, text, Toast.LENGTH_LONG).show();
  }

  protected boolean isNavDrawerOpen() {
    return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(GravityCompat.START);
  }

  protected void setupDrawerIcon() {
    mActionBarToolbar.setNavigationIcon(R.drawable.ic_drawer);
    mActionBarToolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        openNavDrawer();
      }
    });
  }

  protected void toggleDrawer() {
    if (isNavDrawerOpen()) {
      closeNavDrawer();
    } else {
      openNavDrawer();
    }
  }

  protected void openNavDrawer() {
    UIUtils.hideKeyboard(this);
    if (mDrawerLayout != null) {
      mDrawerLayout.openDrawer(GravityCompat.START);
    }
  }

  protected void closeNavDrawer() {
    if (mDrawerLayout != null) {
      mDrawerLayout.closeDrawers();
    }
  }


  @Override
  public void onBackPressed() {
    if (isNavDrawerOpen()) {
      closeNavDrawer();
    } else {
      super.onBackPressed();
    }
  }

  @Override
  public boolean onKeyDown(int keycode, KeyEvent e) {
    switch (keycode) {
      case KeyEvent.KEYCODE_MENU:
        toggleDrawer();
        return true;
    }
    return super.onKeyDown(keycode, e);
  }

  protected abstract int getSelfNavigationViewItem();


}
