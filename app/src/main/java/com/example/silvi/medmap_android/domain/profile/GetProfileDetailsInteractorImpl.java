package com.example.silvi.medmap_android.domain.profile;

import com.example.medmap_api.service.ProfileService;
import com.example.medmap_commons.model.Doctor;
import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.model.Role;
import com.example.medmap_commons.model.User;
import com.example.medmap_commons.util.PrintLog;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.Interactor;
import com.example.silvi.medmap_android.executor.MainThread;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by silvi on 13.03.2017.
 */

public class GetProfileDetailsInteractorImpl implements GetProfileDetailsInteractor, Interactor {

  private final Executor mExecutor;
  private final MainThread mMainThread;
  private final ProfileService mProfileService;


  private GetProfileDetailsInteractor.Callback mCallback;
  private Long userId;
  private Role userRole;

  public GetProfileDetailsInteractorImpl(Executor executor, MainThread mainThread, ProfileService profileService) {
    mExecutor = executor;
    mMainThread = mainThread;
    mProfileService = profileService;
  }

  @Override
  public void execute(Callback callback, Long userId, Role userRole) {
    if (null == callback) {
      throw new IllegalArgumentException(
          "Callback can't be null, the client of this interactor needs to get the response " +
              "in the callback");
    }
    this.mCallback = callback;
    this.userId = userId;
    this.userRole = userRole;
    this.mExecutor.run(this);
  }

  @Override
  public void run() {

    if (userRole.name().equals("ROLE_USER")) {

      mProfileService.getUserDetails(userId, new retrofit.Callback<User>() {

        @Override
        public void success(User user, Response response) {
          notifyUserSuccess(user);
        }

        @Override
        public void failure(RetrofitError error) {
          RestError restError = null;
          try {
            restError = (RestError) error.getBodyAs(RestError.class);
          } catch (Exception e) {
            PrintLog.printStackTrace(getClass().getSimpleName(), e);
          }
          notifyError(restError);
        }

      });
    } else {
      mProfileService.getDoctorDetails(userId, new retrofit.Callback<Doctor>() {
        @Override
        public void success(Doctor doctor, Response response) {
          notifyDoctorSuccess(doctor);
        }

        @Override
        public void failure(RetrofitError error) {
          RestError restError = null;
          try {
            restError = (RestError) error.getBodyAs(RestError.class);
          } catch (Exception e) {
            PrintLog.printStackTrace(getClass().getSimpleName(), e);
          }
          notifyError(restError);

        }
      });

    }
  }

  private void notifyUserSuccess(final User user) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onGetUserProfileDetailsSuccess(user);
      }
    });
  }

  private void notifyDoctorSuccess(final Doctor doctor) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onGetDoctorProfileDetailsSuccess(doctor);
      }
    });
  }

  private void notifyError(final RestError restError) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onGetProfileDetailsError(restError);
      }
    });
  }
}
