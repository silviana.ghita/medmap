package com.example.silvi.medmap_android.domain.appointment.add;

import com.example.medmap_commons.model.Appointment;
import com.example.medmap_commons.model.RestError;

/**
 * Created by silvi on 10.04.2017.
 */

public interface AddAppointmentInteractor {

  interface Callback {
    void onAddAppointmentSuccess();

    void onAddAppointmentError(RestError restError);
  }

  void execute(Callback callback, Appointment appointment);
}
