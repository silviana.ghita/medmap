package com.example.silvi.medmap_android.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by silvi on 22.02.2017.
 */

public class EmailFormatValidator {
  private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
      + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

  private Pattern pattern;

  public EmailFormatValidator() {
    pattern = Pattern.compile(EMAIL_PATTERN);
  }

  public boolean validate(final String email) {
    Matcher matcher = pattern.matcher(email);
    return matcher.matches();
  }
}
