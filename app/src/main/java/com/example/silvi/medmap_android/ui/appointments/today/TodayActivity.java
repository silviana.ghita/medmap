package com.example.silvi.medmap_android.ui.appointments.today;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.medmap_commons.model.Appointment;
import com.example.medmap_commons.model.Role;
import com.example.medmap_commons.model.UserSession;
import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.adapters.NamedEntityAdapter;
import com.example.silvi.medmap_android.dagger.components.DaggerITodayComponent;
import com.example.silvi.medmap_android.dagger.components.IAppComponent;
import com.example.silvi.medmap_android.dagger.modules.NavigationModule;
import com.example.silvi.medmap_android.dagger.modules.TodayModule;
import com.example.silvi.medmap_android.ui.base.BasePresenter;
import com.example.silvi.medmap_android.ui.base.FinishActivities;
import com.example.silvi.medmap_android.ui.base.FinishCallback;
import com.example.silvi.medmap_android.ui.base.NavigationViewActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by silvi on 13.04.2017.
 */

public class TodayActivity extends NavigationViewActivity implements TodayContract.View, FinishCallback {

  @Bind(R.id.recyclerView)
  RecyclerView mRecyclerView;
  @Bind(R.id.btnDelayProgram)
  Button mDelayProgramButton;
  @Bind(R.id.no_upcoming_appointments)
  TextView mNoUpcomingAppointments;

  @Inject
  public TodayPresenter mPresenter;

  List<Appointment> mAppointments;
  UpcomingDoctorAppointmentsAdapter mUpcomingDoctorAppointmentsAdapter;
  UpcomingUserAppointmentsAdapter mUpcomingUserAppointmentsAdapter;


  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);


    mPresenter.getTodaysAppointments();

    setupToolbarDisplay();
    FinishActivities.addActivity(this);
  }

  private void setupToolbarDisplay() {
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    final ActionBar actionBar = getSupportActionBar();
    View viewActionBar = getLayoutInflater().inflate(R.layout.actionbar_default, null);
    ActionBar.LayoutParams params = new ActionBar.LayoutParams(
        ActionBar.LayoutParams.WRAP_CONTENT,
        ActionBar.LayoutParams.MATCH_PARENT,
        Gravity.CENTER);
    TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.actionbar_textview);
    textviewTitle.setText(getActionBarTitle());
    Typeface robotoMedium = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");
    textviewTitle.setTypeface(robotoMedium);
    actionBar.setCustomView(viewActionBar, params);
    actionBar.setDisplayShowCustomEnabled(true);
    actionBar.setDisplayShowTitleEnabled(false);
    actionBar.setDisplayHomeAsUpEnabled(true);
    actionBar.setHomeButtonEnabled(false);

    getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_chevron_left_white));
  }

  public static Intent makeIntent(Context context) {
    return new Intent(context, TodayActivity.class);
  }


  @Override
  protected int getLayout() {
    return R.layout.activity_today;
  }

  @Override
  public void setUpComponent(IAppComponent appComponent) {
    DaggerITodayComponent.builder()
        .iAppComponent(appComponent)
        .todayModule(new TodayModule(this))
        .navigationModule(new NavigationModule(this))
        .build()
        .inject(this);
  }

  private void openConfirmationDialog() {
    final Dialog dialog = new Dialog(this, R.style.CustomDialog);

    dialog.setContentView(R.layout.dialog_delay_program);

    List<String> durations = new ArrayList<>();
    durations.add("0.5");
    durations.add("1");
    final Spinner spinner = (Spinner) dialog.findViewById(R.id.delay_duration_spinner);
    NamedEntityAdapter<String> delayDurationsAdapter = new NamedEntityAdapter(this, R.layout.delay_spinner_item, durations);
    spinner.setAdapter(delayDurationsAdapter);


    dialog.findViewById(R.id.cancel_button).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dialog.dismiss();

      }
    });

    dialog.findViewById(R.id.apply_button).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        String delay = spinner.getSelectedItem().toString();
        mPresenter.onAddDelayClicked(delay, mAppointments);
        dialog.dismiss();
      }
    });

    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    dialog.show();
  }

  @Nullable
  @Override
  protected BasePresenter getPresenter() {
    return null;
  }

  @Override
  protected String getActionBarTitle() {
    return getResources().getString(R.string.today);
  }

  @Override
  protected int getSelfNavigationViewItem() {
    return 0;
  }

  @Override
  public void forceFinish() {
    this.finish();
  }

  @Override
  public void initTodaysAppointments(List<Appointment> appointmentList) {

    this.mAppointments = appointmentList;
    if (mAppointments.size() > 0) {
      setDelayButtonVisibility();
      populateList();
      mRecyclerView.setVisibility(View.VISIBLE);
      mNoUpcomingAppointments.setVisibility(View.GONE);
    } else {
      mRecyclerView.setVisibility(View.GONE);
      mNoUpcomingAppointments.setVisibility(View.VISIBLE);
    }

  }

  private void populateList() {

    if (UserSession.getUserRole().equals(Role.ROLE_USER)) {
      mUpcomingUserAppointmentsAdapter = new UpcomingUserAppointmentsAdapter(this, mAppointments);
      mRecyclerView.setAdapter(mUpcomingUserAppointmentsAdapter);
    } else {
      mUpcomingDoctorAppointmentsAdapter = new UpcomingDoctorAppointmentsAdapter(this, mAppointments);
      mRecyclerView.setAdapter(mUpcomingDoctorAppointmentsAdapter);
    }

    LinearLayoutManager layoutManager = new LinearLayoutManager(this);
    mRecyclerView.setLayoutManager(layoutManager);
    mRecyclerView.setItemAnimator(new DefaultItemAnimator());
  }

  private void setDelayButtonVisibility() {
    if (UserSession.getUserRole().equals(Role.ROLE_USER)) {
      mDelayProgramButton.setVisibility(View.GONE);
    } else if (UserSession.getUserRole().equals(Role.ROLE_DOCTOR)) {
      if (mAppointments != null && mAppointments.size() != 0)
        if (mAppointments.get(0).getDelay() == null || mAppointments.get(0).getDelay().isEmpty())
          mDelayProgramButton.setVisibility(View.VISIBLE);
    }
  }

  @Override
  public void hideDelayButton() {
    mDelayProgramButton.setVisibility(View.GONE);
  }

  @OnClick(R.id.btnDelayProgram)
  void onDelayButtonClick() {
    openConfirmationDialog();
  }
}
