package com.example.silvi.medmap_android.domain.chat.chatroom;

import com.example.medmap_commons.model.ChatRoom;
import com.example.medmap_commons.model.RestError;

/**
 * Created by silvi on 02.05.2017.
 */

public interface OpenChatRoomInteractor {

  interface Callback {

    void onOpenChatRoomSuccess(ChatRoom chatRoom);

    void onOpenChatRoomError(RestError restError);
  }

  void execute(Callback callback, Long otherUserId);
}
