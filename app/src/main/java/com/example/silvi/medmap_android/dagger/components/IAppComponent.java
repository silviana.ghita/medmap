package com.example.silvi.medmap_android.dagger.components;

import com.example.medmap_api.endpoint.ApiRequestInterceptor;
import com.example.silvi.medmap_android.dagger.modules.AppModule;
import com.example.silvi.medmap_android.dagger.modules.ExecutorModule;
import com.example.silvi.medmap_android.ui.base.MedMapApplication;

import javax.inject.Singleton;

import dagger.Component;
import retrofit.RestAdapter;

/**
 * Created by silvi on 08.03.2017.
 */

@Singleton
@Component(
    modules = {
        AppModule.class,
        ExecutorModule.class
    }
)
public interface IAppComponent extends IExecutorComponent {

  MedMapApplication getApp();

  RestAdapter getRestAdapter();

  ApiRequestInterceptor getApiRequestInterceptor();
}
