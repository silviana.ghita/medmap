package com.example.silvi.medmap_android.ui.requests;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.medmap_commons.model.Request;
import com.example.medmap_commons.model.State;
import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.dagger.components.DaggerIRequestsComponent;
import com.example.silvi.medmap_android.dagger.components.IAppComponent;
import com.example.silvi.medmap_android.dagger.modules.NavigationModule;
import com.example.silvi.medmap_android.dagger.modules.RequestsModule;
import com.example.silvi.medmap_android.ui.base.BasePresenter;
import com.example.silvi.medmap_android.ui.base.FinishActivities;
import com.example.silvi.medmap_android.ui.base.FinishCallback;
import com.example.silvi.medmap_android.ui.base.NavigationViewActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by silvi on 25.03.2017.
 */

public class RequestsActivity extends NavigationViewActivity implements FinishCallback, RequestsContract.View {

  @Bind(R.id.declined_requests_button)
  TextView declinedRequestsButton;
  @Bind(R.id.accepted_requests_button)
  TextView acceptedRequestsButton;
  @Bind(R.id.pending_requests_button)
  TextView pendingRequestsButton;
  @Bind(R.id.viewPager)
  ViewPager viewPager;

  RequestsPageAdapter pageAdapter;

  RequestsFragment mDeclinedFragment;
  RequestsFragment mAcceptedFragment;

  ArrayList<Request> pendingRequests;
  ArrayList<Request> acceptedRequests;
  ArrayList<Request> declinedRequests;

  List<Request> mRequests;

  @Inject
  RequestsPresenter mPresenter;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    mPresenter.getRequests();


    setupToolbarDisplay();
    FinishActivities.addActivity(this);
  }


  private List<Fragment> getFragments() {
    List<Fragment> fragmentsList = new ArrayList<>();

    pendingRequests = new ArrayList<>();
    acceptedRequests = new ArrayList<>();
    declinedRequests = new ArrayList<>();
    for (Request request : mRequests) {
      if (request.getState().equals(State.PENDING))
        pendingRequests.add(request);
      else if (request.getState().equals(State.ACCEPTED))
        acceptedRequests.add(request);
      else
        declinedRequests.add(request);
    }

    mDeclinedFragment = (RequestsFragment) RequestsFragment.newInstance(declinedRequests);
    mAcceptedFragment = (RequestsFragment) RequestsFragment.newInstance(acceptedRequests);

    fragmentsList.add(mDeclinedFragment);
    fragmentsList.add(RequestsFragment.newInstance(pendingRequests));
    fragmentsList.add(mAcceptedFragment);

    return fragmentsList;
  }

  private void setupToolbarDisplay() {
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    final ActionBar actionBar = getSupportActionBar();
    View viewActionBar = getLayoutInflater().inflate(R.layout.actionbar_default, null);
    ActionBar.LayoutParams params = new ActionBar.LayoutParams(
        ActionBar.LayoutParams.WRAP_CONTENT,
        ActionBar.LayoutParams.MATCH_PARENT,
        Gravity.CENTER);
    TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.actionbar_textview);
    textviewTitle.setText(getActionBarTitle());
    Typeface robotoMedium = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");
    textviewTitle.setTypeface(robotoMedium);
    actionBar.setCustomView(viewActionBar, params);
    actionBar.setDisplayShowCustomEnabled(true);
    actionBar.setDisplayShowTitleEnabled(false);
    actionBar.setDisplayHomeAsUpEnabled(true);
    actionBar.setHomeButtonEnabled(false);

    getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_chevron_left_white));
  }


  @Override
  protected int getLayout() {
    return R.layout.activity_requests;
  }

  @Override
  public void setUpComponent(IAppComponent appComponent) {
    DaggerIRequestsComponent.builder()
        .iAppComponent(appComponent)
        .requestsModule(new RequestsModule(this))
        .navigationModule(new NavigationModule(this))
        .build()
        .inject(this);
  }

  public static Intent makeIntent(Context context) {
    return new Intent(context, RequestsActivity.class);
  }

  @Nullable
  @Override
  protected BasePresenter getPresenter() {
    return null;
  }

  @Override
  protected String getActionBarTitle() {
    return getResources().getString(R.string.my_requests);
  }

  @Override
  protected int getSelfNavigationViewItem() {
    return 0;
  }

  @Override
  public void forceFinish() {
    this.finish();
  }


  @Override
  public void showToast(String text) {
    Toast.makeText(this, text, Toast.LENGTH_LONG).show();
  }

  @Override
  public void onRemoveRequestClicked(Request request) {
    mPresenter.deleteRequest(request);
  }

  @Override
  public void setRequests(List<Request> requests) {
    mRequests = requests;

    List<Fragment> fragments = getFragments();
    pageAdapter = new RequestsPageAdapter(getSupportFragmentManager(), fragments);
    viewPager.setAdapter(pageAdapter);
    viewPager.setCurrentItem(1);

    viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
      @Override
      public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (viewPager.getCurrentItem() == 1) {
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            pendingRequestsButton.setBackground(getResources().getDrawable(R.drawable.button_bg_rounded_white));
            declinedRequestsButton.setBackground(null);
            acceptedRequestsButton.setBackground(null);
          }
        } else if (viewPager.getCurrentItem() == 0) {
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            pendingRequestsButton.setBackground(null);
            declinedRequestsButton.setBackground(getResources().getDrawable(R.drawable.button_bg_rounded_white));
            acceptedRequestsButton.setBackground(null);
          }
        } else {
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            pendingRequestsButton.setBackground(null);
            declinedRequestsButton.setBackground(null);
            acceptedRequestsButton.setBackground(getResources().getDrawable(R.drawable.button_bg_rounded_white));
          }
        }
      }

      @Override
      public void onPageSelected(int position) {

      }

      @Override
      public void onPageScrollStateChanged(int state) {

      }
    });


  }

  @Override
  public void onAcceptButtonClick(Request request) {
    mPresenter.acceptRequest(request);
    mAcceptedFragment.requestList.add(0, request);
    mAcceptedFragment.receivedRequestsAdapter.notifyDataSetChanged();
  }

  @Override
  public void onDeclineButtonClick(Request request) {
    mPresenter.declineRequest(request);
    mDeclinedFragment.requestList.add(0, request);
    mDeclinedFragment.receivedRequestsAdapter.notifyDataSetChanged();
  }

  @OnClick(R.id.declined_requests_button)
  void onDeclinedRequestsButtonClick() {
    viewPager.setCurrentItem(0);
  }

  @OnClick(R.id.pending_requests_button)
  void onPendingRequestsButtonClick() {
    viewPager.setCurrentItem(1);
  }

  @OnClick(R.id.accepted_requests_button)
  void onAcceptedRequestsButtonClick() {
    viewPager.setCurrentItem(2);
  }


}
