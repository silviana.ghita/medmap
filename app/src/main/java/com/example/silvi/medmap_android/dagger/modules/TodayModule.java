package com.example.silvi.medmap_android.dagger.modules;

import com.example.medmap_api.service.AppointmentService;
import com.example.silvi.medmap_android.domain.appointment.today.GetTodaysAppointmentsInteractor;
import com.example.silvi.medmap_android.domain.appointment.today.GetTodaysAppointmentsInteractorImpl;
import com.example.silvi.medmap_android.domain.appointment.today.delay.AddDelayInteractor;
import com.example.silvi.medmap_android.domain.appointment.today.delay.AddDelayInteractorImpl;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.MainThread;
import com.example.silvi.medmap_android.ui.appointments.today.TodayActivity;
import com.example.silvi.medmap_android.ui.appointments.today.TodayContract;
import com.example.silvi.medmap_android.ui.appointments.today.TodayPresenter;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;

/**
 * Created by silvi on 13.04.2017.
 */

@Module
public class TodayModule {

  private TodayContract.View mView;

  public TodayModule(TodayActivity view) {
    this.mView = view;
  }

  @Provides
  public TodayContract.View provideView() {
    return mView;
  }

  @Provides
  public AppointmentService provideAppointmentService(RestAdapter restAdapter) {
    return restAdapter.create(AppointmentService.class);
  }

  @Provides
  public GetTodaysAppointmentsInteractor provideGetTodaysAppointmentsInteractor(Executor executor, MainThread mainThread, AppointmentService appointmentService) {
    return new GetTodaysAppointmentsInteractorImpl(executor, mainThread, appointmentService);
  }

  @Provides
  public AddDelayInteractor provideAddDelayInteractor(Executor executor, MainThread mainThread, AppointmentService appointmentService) {
    return new AddDelayInteractorImpl(executor, mainThread, appointmentService);
  }


  @Provides
  TodayPresenter providePresenter(TodayContract.View view, GetTodaysAppointmentsInteractor getTodaysAppointmentsInteractor, AddDelayInteractor addDelayInteractor) {
    return new TodayPresenter(view, getTodaysAppointmentsInteractor, addDelayInteractor);
  }
}

