package com.example.silvi.medmap_android.domain.appointment.get;

import com.example.medmap_commons.model.Appointment;
import com.example.medmap_commons.model.RestError;

import java.util.List;

/**
 * Created by silvi on 12.04.2017.
 */

public interface GetAppointmentsInteractor {

  interface Callback {

    void onGetAppointmentsSuccess(List<Appointment> appointments);

    void onGetAppointmentsError(RestError restError);

  }

  void execute(Callback callback);
}
