package com.example.silvi.medmap_android.domain.appointment.update;

import com.example.medmap_api.service.AppointmentService;
import com.example.medmap_commons.model.Appointment;
import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.util.PrintLog;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.Interactor;
import com.example.silvi.medmap_android.executor.MainThread;

import javax.inject.Inject;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by silvi on 12.04.2017.
 */

public class UpdateAppointmentStateInteractorImpl implements Interactor, UpdateAppointmentStateInteractor {

  private final Executor mExecutor;
  private final MainThread mMainThread;
  private final AppointmentService mAppointmentService;

  private Callback mCallback;
  private Appointment mAppointment;

  @Inject
  public UpdateAppointmentStateInteractorImpl(Executor executor, MainThread mainThread, AppointmentService appointmentService) {
    this.mAppointmentService = appointmentService;
    this.mExecutor = executor;
    this.mMainThread = mainThread;
  }

  @Override
  public void execute(Callback callback, Appointment appointment) {
    if (null == callback) {
      throw new IllegalArgumentException(
          "Callback can't be null, the client of this interactor needs to get the response " +
              "in the callback");
    }
    this.mCallback = callback;
    this.mAppointment = appointment;
    this.mExecutor.run(this);
  }

  @Override
  public void run() {
    mAppointmentService.updateAppointmentState(mAppointment, new retrofit.Callback<Object>() {
      @Override
      public void success(Object o, Response response) {
        notifySuccess();
      }

      @Override
      public void failure(RetrofitError error) {
        RestError restError = null;
        try {
          restError = (RestError) error.getBodyAs(RestError.class);
        } catch (Exception e) {
          PrintLog.printStackTrace(getClass().getSimpleName(), e);
        }
        notifyError(restError);
      }
    });

  }

  private void notifySuccess() {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onUpdateAppointmentStateSuccess();
      }
    });
  }

  private void notifyError(final RestError restError) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onUpdateAppointmentStateError(restError);
      }
    });
  }
}
