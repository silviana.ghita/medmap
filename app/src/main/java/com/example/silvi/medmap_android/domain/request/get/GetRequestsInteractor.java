package com.example.silvi.medmap_android.domain.request.get;

import com.example.medmap_commons.model.Request;
import com.example.medmap_commons.model.RestError;

import java.util.List;

/**
 * Created by silvi on 25.03.2017.
 */

public interface GetRequestsInteractor {

  interface Callback {

    void onGetDoctorRequestsSuccess(List<Request> requests);

    void onGetDoctorRequestsError(RestError restError);
  }

  void execute(Callback callback);
}
