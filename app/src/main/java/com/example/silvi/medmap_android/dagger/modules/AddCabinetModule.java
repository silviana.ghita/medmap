package com.example.silvi.medmap_android.dagger.modules;

import com.example.medmap_api.service.CabinetService;
import com.example.medmap_api.service.ImageService;
import com.example.silvi.medmap_android.domain.cabinet.add.AddCabinetInteractor;
import com.example.silvi.medmap_android.domain.cabinet.add.AddCabinetInteractorImpl;
import com.example.silvi.medmap_android.domain.images.upload.UploadPictureInteractor;
import com.example.silvi.medmap_android.domain.images.upload.UploadPictureInteractorImpl;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.MainThread;
import com.example.silvi.medmap_android.ui.cabinet.add.AddCabinetContract;
import com.example.silvi.medmap_android.ui.cabinet.add.AddCabinetPresenter;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;

/**
 * Created by silvi on 14.03.2017.
 */

@Module
public class AddCabinetModule {

  private AddCabinetContract.View view;

  public AddCabinetModule(AddCabinetContract.View view) {
    this.view = view;
  }

  @Provides
  public AddCabinetContract.View getView() {
    return view;
  }

  @Provides
  public CabinetService provideCabinetService(RestAdapter restAdapter) {
    return restAdapter.create(CabinetService.class);
  }

  @Provides
  public ImageService provideImaheService(RestAdapter restAdapter) {
    return restAdapter.create(ImageService.class);
  }

  @Provides
  public AddCabinetInteractor provideAddCabinetInteractor(Executor executor,
                                                          MainThread mainThread,
                                                          CabinetService cabinetService) {
    return new AddCabinetInteractorImpl(executor, mainThread, cabinetService);
  }

  @Provides
  public UploadPictureInteractor provideUploadPictureInteractor(Executor executor,
                                                                MainThread mainThread,
                                                                ImageService imageService) {
    return new UploadPictureInteractorImpl(executor, mainThread, imageService);
  }

  @Provides
  public AddCabinetPresenter providePresenter(AddCabinetContract.View view, AddCabinetInteractor interactor, UploadPictureInteractor uploadPictureInteractor) {
    return new AddCabinetPresenter(view, interactor, uploadPictureInteractor);
  }
}
