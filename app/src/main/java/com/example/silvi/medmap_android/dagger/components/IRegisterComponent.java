package com.example.silvi.medmap_android.dagger.components;

import com.example.medmap_api.service.AuthService;
import com.example.silvi.medmap_android.dagger.ActivityScope;
import com.example.silvi.medmap_android.dagger.modules.RegisterModule;
import com.example.silvi.medmap_android.domain.register.RegisterDoctorInteractor;
import com.example.silvi.medmap_android.domain.register.RegisterUserInteractor;
import com.example.silvi.medmap_android.ui.auth.register.RegisterActivity;
import com.example.silvi.medmap_android.ui.auth.register.RegisterContract;
import com.example.silvi.medmap_android.ui.auth.register.RegisterPresenter;

import dagger.Component;

/**
 * Created by silvi on 08.03.2017.
 */

@ActivityScope
@Component(
    dependencies = IAppComponent.class,
    modules = RegisterModule.class
)
public interface IRegisterComponent extends IAppComponent {

  void inject(RegisterActivity registerActivity);

  RegisterContract.View getView();

  AuthService getAuthService();

  RegisterUserInteractor getRegisterUserInteractor();

  RegisterDoctorInteractor getRegisterDoctorInteractor();

  RegisterPresenter getRegisterPresenter();
}
