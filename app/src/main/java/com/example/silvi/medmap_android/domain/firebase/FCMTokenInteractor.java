package com.example.silvi.medmap_android.domain.firebase;

import com.example.medmap_commons.model.RestError;

/**
 * Created by silvi on 22.04.2017.
 */

public interface FCMTokenInteractor {

  interface Callback {
    void onSetTokenSuccess();

    void onSetTokenError(RestError restError);
  }

  void execute(Callback callback, String token);
}
