package com.example.silvi.medmap_android.util;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

/**
 * Created by silvi on 21.03.2017.
 */

public class PhotoRepository {


  private ContentResolver mContentResolver;

  public PhotoRepository(Context context) {
    mContentResolver = context.getApplicationContext().getContentResolver();
  }

  public String getImagePath(Uri imageUri) {
    String imagePath = null;
    Cursor cursor = getImagesCursor(imageUri);
    if (cursor != null && cursor.moveToFirst()) {
      int dataColumnIndex = cursor.getColumnIndex(MediaStore.MediaColumns.DATA);
      imagePath = cursor.getString(dataColumnIndex);
    }
    if (cursor != null) cursor.close();
    return imagePath;
  }

  private Cursor getImagesCursor(Uri imageUri) {
    String[] projection = {MediaStore.MediaColumns.DATA};
    return mContentResolver.query(imageUri, projection, null, null, null);
  }
}
