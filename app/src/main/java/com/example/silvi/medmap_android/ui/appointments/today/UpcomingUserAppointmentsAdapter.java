package com.example.silvi.medmap_android.ui.appointments.today;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.example.medmap_commons.model.Appointment;
import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.ui.base.MedMapApplication;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by silvi on 17.04.2017.
 */

public class UpcomingUserAppointmentsAdapter extends RecyclerView.Adapter<UpcomingUserAppointmentsAdapter.ViewHolder> {

  private Context context;
  private List<Appointment> appointments;

  public UpcomingUserAppointmentsAdapter(Context context, List<Appointment> appointments) {
    this.appointments = appointments;
    this.context = context;
  }


  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater inflater = null;
    inflater = LayoutInflater.from(context);

    View upcomingUserAppointments = inflater.inflate(R.layout.line_item_sent_appointment, parent, false);

    return new ViewHolder(upcomingUserAppointments);
  }

  @Override
  public void onBindViewHolder(ViewHolder holder, int position) {
    final Appointment appointment = appointments.get(position);

    ImageView cabinetPhotoImageView = holder.coverPhotoImageView;
    TextView doctorName = holder.doctorName;
    final ImageView doctorPhotoImageView = holder.doctorPhotoImageView;
    TextView appointmentStatus = holder.appointmentStatus;
    TextView appointmentStartDate = holder.appointmentStartDate;
    TextView appointmentStartTime = holder.appointmentStartTime;
    TextView appointmentTypeName = holder.appointmentTypeName;
    TextView appointmentDuration = holder.appointmentDuration;
    TextView appointmentPrice = holder.appointmentPrice;
    Button btnContactDoctor = holder.buttonContactDoctor;
    Button btnCancelAppointment = holder.buttonCancelAppointment;
    TextView appointmentDelay = holder.appointmentDelay;
    LinearLayout delaySection = holder.delaySection;


    Drawable userIcon = MedMapApplication.getInstance().getResources().getDrawable(R.drawable.user_icon);
    userIcon.setColorFilter(new LightingColorFilter(Color.LTGRAY, Color.LTGRAY));


    Glide.with(context)
        .load(appointment.getCabinet().getDoctor().getProfilePicture() != null ? appointment.getCabinet().getDoctor().getProfilePicture().getPath() : "")
        .asBitmap()
        .placeholder(userIcon)
        .centerCrop()
        .into(new BitmapImageViewTarget(doctorPhotoImageView) {
          @Override
          protected void setResource(Bitmap resource) {
            RoundedBitmapDrawable circularBitmapDrawable =
                RoundedBitmapDrawableFactory.create(context.getResources(), resource);
            circularBitmapDrawable.setCornerRadius(
                Math.max(resource.getWidth(), resource.getHeight()) / 2.0f);
            doctorPhotoImageView.setImageDrawable(circularBitmapDrawable);
          }
        });

    Glide
        .with(context)
        .load(appointment.getCabinet().getImages().get(0).getPath())
        .centerCrop()
        .crossFade()
        .into(cabinetPhotoImageView);

    doctorName.setText(appointment.getCabinet().getDoctor().getLastName() + " " + appointment.getCabinet().getDoctor().getFirstName());
    appointmentStatus.setText(appointment.getState());
    appointmentStartDate.setText(appointment.getDate());
    appointmentStartTime.setText(appointment.getHour());
    appointmentTypeName.setText(appointment.getAppointmentType().getType());
    appointmentDuration.setText(appointment.getAppointmentType().getDuration().toString() + " " + MedMapApplication.getInstance().getResources().getString(R.string.hour));
    appointmentPrice.setText(appointment.getAppointmentType().getPrice().toString() + " " + MedMapApplication.getInstance().getResources().getString(R.string.ron));

    if (appointment.getDelay() != null) {
      delaySection.setVisibility(View.VISIBLE);
      appointmentDelay.setText(appointment.getDelay() + " " + MedMapApplication.getInstance().getResources().getString(R.string.hour));
    } else {
      delaySection.setVisibility(View.GONE);
    }
    btnCancelAppointment.setVisibility(View.GONE);
    btnContactDoctor.setVisibility(View.GONE);

  }

  @Override
  public int getItemCount() {
    return appointments.size();
  }

  public static class ViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.coverPhotoImgView)
    ImageView coverPhotoImageView;
    @Bind(R.id.doctorNameTView)
    TextView doctorName;
    @Bind(R.id.doctorPhotoImgView)
    ImageView doctorPhotoImageView;
    @Bind(R.id.appointmentStatusTView)
    TextView appointmentStatus;
    @Bind(R.id.appointment_start_date)
    TextView appointmentStartDate;
    @Bind(R.id.appointment_start_time)
    TextView appointmentStartTime;
    @Bind(R.id.appointment_type_name)
    TextView appointmentTypeName;
    @Bind(R.id.appointment_duration)
    TextView appointmentDuration;
    @Bind(R.id.appointment_price)
    TextView appointmentPrice;
    @Bind(R.id.btnContactDoctor)
    Button buttonContactDoctor;
    @Bind(R.id.btnCancelAppointment)
    Button buttonCancelAppointment;
    @Bind(R.id.appointment_delay)
    TextView appointmentDelay;
    @Bind(R.id.delay_section)
    LinearLayout delaySection;


    public ViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
