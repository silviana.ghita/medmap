package com.example.silvi.medmap_android.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.commons.IntentKeys;
import com.example.silvi.medmap_android.dagger.components.IAppComponent;
import com.example.silvi.medmap_android.ui.base.BaseActivity;
import com.example.silvi.medmap_android.ui.base.BasePresenter;

/**
 * Created by silvi on 22.03.2017.
 */

public class CustomPhotoGalleryActivity extends BaseActivity {

  private GridView grdImages;
  private Button btnSelect;

  private ImageAdapter imageAdapter;
  private String[] arrPath;
  private boolean[] thumbnailsselection;
  private int ids[];
  private int count;


  /**
   * Overrides methods
   */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.custom_gallery);
    grdImages = (GridView) findViewById(R.id.grdImages);
    btnSelect = (Button) findViewById(R.id.btnSelect);

    final String[] columns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID};
    final String orderBy = MediaStore.Images.Media._ID;
    @SuppressWarnings("deprecation")
    Cursor imagecursor = managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null, null, orderBy);
    int image_column_index = imagecursor.getColumnIndex(MediaStore.Images.Media._ID);
    this.count = imagecursor.getCount();
    this.arrPath = new String[this.count];
    ids = new int[count];

    if (getIntent().hasExtra(IntentKeys.THUMBNAILS_SELECTION)) {
      thumbnailsselection = getIntent().getBooleanArrayExtra(IntentKeys.THUMBNAILS_SELECTION);
      if (thumbnailsselection != null) {
        for (int i = 0; i < thumbnailsselection.length; i++)
          if (thumbnailsselection[i])
            grdImages.setSelected(true);
      } else {
        this.thumbnailsselection = new boolean[this.count];
      }
    }

    for (int i = 0; i < this.count; i++) {
      imagecursor.moveToPosition(i);
      ids[i] = imagecursor.getInt(image_column_index);
      int dataColumnIndex = imagecursor.getColumnIndex(MediaStore.Images.Media.DATA);
      arrPath[i] = imagecursor.getString(dataColumnIndex);
    }

    imageAdapter = new ImageAdapter();
    grdImages.setAdapter(imageAdapter);
    imagecursor.close();


    btnSelect.setOnClickListener(new View.OnClickListener() {

      public void onClick(View v) {
        final int len = thumbnailsselection.length;
        int cnt = 0;
        String selectImages = "";
        for (int i = 0; i < len; i++) {
          if (thumbnailsselection[i]) {
            cnt++;
            selectImages = selectImages + arrPath[i] + "|";
          }
        }
        if (cnt == 0) {
          Toast.makeText(getApplicationContext(), "Please select at least one image", Toast.LENGTH_LONG).show();
        } else {

          Log.d("SelectedImages", selectImages);
          Intent i = new Intent();
          i.putExtra("data", selectImages);
          i.putExtra("thumbnailsSelection", thumbnailsselection);
          setResult(Activity.RESULT_OK, i);
          finish();
        }
      }
    });

    setupToolbarDisplay();
  }

  public static Intent makeIntent(Context context, final boolean[] thumbnailsSelection) {
    Intent intent = new Intent(context, CustomPhotoGalleryActivity.class);
    intent.putExtra(IntentKeys.THUMBNAILS_SELECTION, thumbnailsSelection);
    return intent;
  }

  private void setupToolbarDisplay() {
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    final ActionBar actionBar = getSupportActionBar();
    View viewActionBar = getLayoutInflater().inflate(R.layout.actionbar_default, null);
    ActionBar.LayoutParams params = new ActionBar.LayoutParams(
        ActionBar.LayoutParams.WRAP_CONTENT,
        ActionBar.LayoutParams.MATCH_PARENT,
        Gravity.CENTER);
    TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.actionbar_textview);
    textviewTitle.setText(getActionBarTitle());
    Typeface robotoMedium = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");
    textviewTitle.setTypeface(robotoMedium);
    actionBar.setCustomView(viewActionBar, params);
    actionBar.setDisplayShowCustomEnabled(true);
    actionBar.setDisplayShowTitleEnabled(false);
    actionBar.setDisplayHomeAsUpEnabled(true);

    getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_chevron_left_white));

    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        finish();

      }
    });

  }

  @Override
  protected int getLayout() {
    return R.layout.custom_gallery;
  }

  @Override
  public void setUpComponent(IAppComponent appComponent) {

  }

  @Nullable
  @Override
  protected BasePresenter getPresenter() {
    return null;
  }

  @Override
  protected String getActionBarTitle() {
    return getResources().getString(R.string.upload_photos);
  }


  /**
   * Class method
   */

  /**
   * This method used to set bitmap.
   *
   * @param iv represented ImageView
   * @param id represented id
   */

  private void setBitmap(final ImageView iv, final int id) {

    new AsyncTask<Void, Void, Bitmap>() {

      @Override
      protected Bitmap doInBackground(Void... params) {
        return MediaStore.Images.Thumbnails.getThumbnail(getApplicationContext().getContentResolver(), id, MediaStore.Images.Thumbnails.MINI_KIND, null);
      }

      @Override
      protected void onPostExecute(Bitmap result) {
        super.onPostExecute(result);
        iv.setImageBitmap(result);
      }
    }.execute();
  }


  /**
   * List adapter
   *
   * @author tasol
   */

  public class ImageAdapter extends BaseAdapter {
    private LayoutInflater mInflater;

    public ImageAdapter() {
      mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
      return count;
    }

    public Object getItem(int position) {
      return position;
    }

    public long getItemId(int position) {
      return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
      final ViewHolder holder;
      if (convertView == null) {
        holder = new ViewHolder();
        convertView = mInflater.inflate(R.layout.custom_gallery_item, null);
        holder.imgThumb = (ImageView) convertView.findViewById(R.id.imgThumb);
        holder.chkImage = (CheckBox) convertView.findViewById(R.id.checkImage);

        convertView.setTag(holder);
      } else {
        holder = (ViewHolder) convertView.getTag();
      }
      holder.chkImage.setId(position);
      holder.imgThumb.setId(position);
      holder.chkImage.setOnClickListener(new View.OnClickListener() {

        public void onClick(View v) {
          CheckBox cb = (CheckBox) v;
          int id = cb.getId();
          if (thumbnailsselection[id]) {
            cb.setChecked(false);
            thumbnailsselection[id] = false;
          } else {
            cb.setChecked(true);
            thumbnailsselection[id] = true;
          }
        }
      });
      holder.imgThumb.setOnClickListener(new View.OnClickListener() {

        public void onClick(View v) {
          int id = holder.chkImage.getId();
          if (thumbnailsselection[id]) {
            holder.chkImage.setChecked(false);
            thumbnailsselection[id] = false;
          } else {
            holder.chkImage.setChecked(true);
            thumbnailsselection[id] = true;
          }
        }
      });
      try {
        setBitmap(holder.imgThumb, ids[position]);
      } catch (Throwable e) {
      }
      holder.chkImage.setChecked(thumbnailsselection[position]);
      holder.id = position;
      return convertView;
    }
  }


  /**
   * Inner class
   *
   * @author tasol
   */
  class ViewHolder {
    ImageView imgThumb;
    CheckBox chkImage;
    int id;
  }

}