package com.example.silvi.medmap_android.ui.requests;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.medmap_commons.model.Request;
import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.ui.base.MedMapApplication;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by silvi on 25.03.2017.
 */

public class SentRequestsAdapter extends RecyclerView.Adapter<SentRequestsAdapter.ViewHolder> {

  private RequestsFragment context;
  private List<Request> requests;

  public SentRequestsAdapter(RequestsFragment context, List<Request> requests) {
    this.requests = requests;
    this.context = context;
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(context.getContext());
    View requestsView = inflater.inflate(R.layout.line_item_sent_request, parent, false);

    return new ViewHolder(requestsView);
  }

  @Override
  public void onBindViewHolder(ViewHolder holder, final int position) {
    final Request request = requests.get(position);

    TextView cabinetName = holder.cabinetName;
    TextView doctorName = holder.doctorName;
    TextView requestState = holder.requestState;
    LinearLayout removeButton = holder.removeButton;

    cabinetName.setText(request.getCabinet().getName());
    doctorName.setText(request.getCabinet().getDoctor().getLastName() + " " + request.getCabinet().getDoctor().getFirstName());
    requestState.setText(MedMapApplication.getInstance().getResources().getString(R.string.request_state) + " " + request.getState().toString());

    removeButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        context.onRemoveRequestClick(request, position);
      }
    });


  }

  @Override
  public int getItemCount() {
    return requests.size();
  }

  public static class ViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.cabinet_name)
    TextView cabinetName;
    @Bind(R.id.doctor_name)
    TextView doctorName;
    @Bind(R.id.request_state)
    TextView requestState;
    @Bind(R.id.remove_wrapper)
    LinearLayout removeButton;


    public ViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
