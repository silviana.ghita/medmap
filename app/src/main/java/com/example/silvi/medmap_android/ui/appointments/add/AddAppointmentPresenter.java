package com.example.silvi.medmap_android.ui.appointments.add;

import com.example.medmap_commons.model.Appointment;
import com.example.medmap_commons.model.AppointmentType;
import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.model.State;
import com.example.medmap_commons.model.User;
import com.example.silvi.medmap_android.domain.appointment.add.AddAppointmentInteractor;
import com.example.silvi.medmap_android.domain.appointment.hours.GetAvailableHoursForDateInteractor;
import com.example.silvi.medmap_android.domain.appointment.types.GetAppointmentTypesInteractor;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by silvi on 30.03.2017.
 */

public class AddAppointmentPresenter implements AddAppointmentContract.Presenter, GetAvailableHoursForDateInteractor.Callback, GetAppointmentTypesInteractor.Callback, AddAppointmentInteractor.Callback {

    AddAppointmentContract.View mView;
    GetAvailableHoursForDateInteractor mGetAvailableHoursForDateInteractor;
    GetAppointmentTypesInteractor mGetAppointmentTypesInteractor;
    AddAppointmentInteractor mAddAppointmentInteractor;

    @Inject
    public AddAppointmentPresenter(AddAppointmentContract.View view, GetAvailableHoursForDateInteractor getAvailableHoursForDateInteractor,
                                   GetAppointmentTypesInteractor getAppointmentTypesInteractor, AddAppointmentInteractor addAppointmentInteractor) {
        this.mView = view;
        this.mGetAvailableHoursForDateInteractor = getAvailableHoursForDateInteractor;
        this.mGetAppointmentTypesInteractor = getAppointmentTypesInteractor;
        this.mAddAppointmentInteractor = addAppointmentInteractor;
    }


    @Override
    public void getAvailableHoursForDate(String date, Double duration, long cabinetId) {
        mGetAvailableHoursForDateInteractor.execute(this, date, duration, cabinetId);
    }

    @Override
    public void getAppointmentTypes() {
        mGetAppointmentTypesInteractor.execute(this);
    }

    @Override
    public void addAppointment(boolean isExternal) {

        if (checkValidity(isExternal)) {
            Appointment appointment = new Appointment();
            appointment.setCabinet(mView.getCabinet())
                    .setAppointmentType(mView.getSelectedAppointmentType())
                    .setComment(mView.getComment())
                    .setDate(mView.getDate())
                    .setHour(mView.getTime())
                    .setState(State.PENDING.toString());
            if (isExternal) {
                User user = new User();

                user.setFirstName(mView.getUserFirstName());
                user.setLastName(mView.getUserLastName());
                user.setPhoneNumber(mView.getUserPhoneNumber());
                appointment.setUser(user);
            }
            appointment.setExternal(isExternal);
            mAddAppointmentInteractor.execute(this, appointment);
        }
    }

    private boolean checkValidity(boolean isExternal) {
        if (mView.getDate() == null || mView.getDate().isEmpty()) {
            mView.showToast("Date must not be null");
            return false;
        }
        if (isExternal) {
            if (mView.getUserFirstName().isEmpty() || mView.getUserFirstName() == null) {
                mView.showToast("First name must not be null");
                return false;
            } else if (mView.getUserLastName().isEmpty() || mView.getUserLastName() == null) {
                mView.showToast("Last name must not be null");
                return false;
            } else if (mView.getUserPhoneNumber().isEmpty() || mView.getUserPhoneNumber() == null) {
                mView.showToast("Phone number must not be null");
                return false;
            }
        }
        return true;
    }


    @Override
    public void onGetAvailableHoursForDateSuccess(List<Double> availableHours) {
        mView.setAvailableHours(availableHours);
    }

    @Override
    public void onGetAvailableHoursForDateError(RestError restError) {

    }

    @Override
    public void onGetAppointmentTypesSuccess(List<AppointmentType> appointmentTypes) {
        mView.setAppointmentTypes(appointmentTypes);
    }

    @Override
    public void onGetAppointmentTypesError(RestError restError) {

    }

    @Override
    public void onAddAppointmentSuccess() {
        mView.appointmentAddedSuccessfully();
    }

    @Override
    public void onAddAppointmentError(RestError restError) {
        mView.showToast(restError.getMessage());
    }
}
