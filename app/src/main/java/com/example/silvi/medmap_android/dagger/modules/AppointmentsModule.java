package com.example.silvi.medmap_android.dagger.modules;

import com.example.medmap_api.service.AppointmentService;
import com.example.medmap_api.service.ChatService;
import com.example.silvi.medmap_android.domain.appointment.delete.DeleteAppointmentInteractor;
import com.example.silvi.medmap_android.domain.appointment.delete.DeleteAppointmentInteractorImpl;
import com.example.silvi.medmap_android.domain.appointment.get.GetAppointmentsInteractor;
import com.example.silvi.medmap_android.domain.appointment.get.GetAppointmentsInteractorImpl;
import com.example.silvi.medmap_android.domain.appointment.update.UpdateAppointmentStateInteractor;
import com.example.silvi.medmap_android.domain.appointment.update.UpdateAppointmentStateInteractorImpl;
import com.example.silvi.medmap_android.domain.chat.chatroom.OpenChatRoomInteractor;
import com.example.silvi.medmap_android.domain.chat.chatroom.OpenChatRoomInteractorImpl;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.MainThread;
import com.example.silvi.medmap_android.ui.appointments.AppointmentsContract;
import com.example.silvi.medmap_android.ui.appointments.AppointmentsPresenter;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;

/**
 * Created by silvi on 12.04.2017.
 */

@Module
public class AppointmentsModule {

  private AppointmentsContract.View view;

  public AppointmentsModule(AppointmentsContract.View view) {
    this.view = view;
  }

  @Provides
  public AppointmentsContract.View getView() {
    return view;
  }

  @Provides
  public AppointmentService proviceAppointmentService(RestAdapter restAdapter) {
    return restAdapter.create(AppointmentService.class);
  }

  @Provides
  public ChatService proviceChatService(RestAdapter restAdapter) {
    return restAdapter.create(ChatService.class);
  }

  @Provides
  public GetAppointmentsInteractor provideGetAppointmentsInteractor(Executor executor,
                                                                    MainThread mainThread,
                                                                    AppointmentService appointmentService) {
    return new GetAppointmentsInteractorImpl(executor, mainThread, appointmentService);
  }

  @Provides
  public UpdateAppointmentStateInteractor provideUpdateAppointmentStateInteractor(Executor executor,
                                                                                  MainThread mainThread,
                                                                                  AppointmentService appointmentService) {
    return new UpdateAppointmentStateInteractorImpl(executor, mainThread, appointmentService);
  }

  @Provides
  public DeleteAppointmentInteractor provideDeleteAppointmentInteractor(Executor executor,
                                                                        MainThread mainThread,
                                                                        AppointmentService appointmentService) {
    return new DeleteAppointmentInteractorImpl(executor, mainThread, appointmentService);
  }

  @Provides
  public OpenChatRoomInteractor provideOpenChatRoomInteractor(Executor executor,
                                                              MainThread mainThread,
                                                              ChatService chatService) {
    return new OpenChatRoomInteractorImpl(executor, mainThread, chatService);
  }


  @Provides
  public AppointmentsPresenter providePresenter(AppointmentsContract.View view, GetAppointmentsInteractor getAppointmentsInteractor
      , DeleteAppointmentInteractor deleteAppointmentInteractor, UpdateAppointmentStateInteractor updateAppointmentStateInteractor,
                                                OpenChatRoomInteractor openChatRoomInteractor) {
    return new AppointmentsPresenter(view, getAppointmentsInteractor, updateAppointmentStateInteractor, deleteAppointmentInteractor, openChatRoomInteractor);
  }

}
