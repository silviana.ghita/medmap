package com.example.silvi.medmap_android.firebase;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Created by silvi on 20.04.2017.
 */

public class RegistrationIntentService extends IntentService {

  /**
   * Creates an IntentService.  Invoked by your subclass's constructor.
   *
   * @param name Used to name the worker thread, important only for debugging.
   */
  private static final String TAG = "RegIntentService";
  public static final String FCM_INTENT = "FCM_INTENT";

  public RegistrationIntentService() {
    super(TAG);
  }

  @Override
  protected void onHandleIntent(Intent intent) {

    FirebaseInstanceId instanceId = FirebaseInstanceId.getInstance();

    try {
      String token = instanceId.getToken();
      Intent intent1 = new Intent(FCM_INTENT);
      intent1.putExtra("FCMToken", token);
      LocalBroadcastManager.getInstance(this).sendBroadcast(intent1);

    } catch (Exception e) {
      Log.d(TAG, "Failed to fetch token");
    }

  }
}
