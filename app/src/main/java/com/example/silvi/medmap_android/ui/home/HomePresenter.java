package com.example.silvi.medmap_android.ui.home;

import com.example.medmap_commons.model.Cabinet;
import com.example.medmap_commons.model.CabinetsFilter;
import com.example.medmap_commons.model.RestError;
import com.example.silvi.medmap_android.domain.cabinet.get.GetAllCabinetsInteractor;
import com.example.silvi.medmap_android.domain.cabinet.get.other.GetOtherCabinetsInteractor;
import com.example.silvi.medmap_android.domain.firebase.FCMTokenInteractor;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by silvi on 24.02.2017.
 */

public class HomePresenter implements HomeContract.Presenter, GetAllCabinetsInteractor.Callback, FCMTokenInteractor.Callback, GetOtherCabinetsInteractor.Callback {

  private HomeContract.View mView;
  private GetAllCabinetsInteractor getAllCabinetsInteractor;
  private GetOtherCabinetsInteractor getOtherCabinetsInteractor;

  private FCMTokenInteractor fcmTokenInteractor;
  private CabinetsFilter filter = new CabinetsFilter();

  @Inject
  public HomePresenter(HomeContract.View mView, GetAllCabinetsInteractor getAllCabinetsInteractor, FCMTokenInteractor fcmTokenInteractor, GetOtherCabinetsInteractor getOtherCabinetsInteractor) {
    this.mView = mView;
    this.getAllCabinetsInteractor = getAllCabinetsInteractor;
    this.fcmTokenInteractor = fcmTokenInteractor;
    this.getOtherCabinetsInteractor = getOtherCabinetsInteractor;
  }

  @Override
  public void getAllCabinets() {
    getAllCabinetsInteractor.execute(this, filter);
  }

  @Override
  public void setFCMToken(String token) {
    fcmTokenInteractor.execute(this, token);
  }

  @Override
  public void getDoctorCabinets(long userId) {
    getOtherCabinetsInteractor.execute(this, userId);
  }

  @Override
  public void onGetAllCabinetsSuccess(List<Cabinet> cabinets) {
    mView.initCabinets(cabinets);
  }

  @Override
  public void onGetAllCabinetsError(RestError restError) {

  }

  @Override
  public void onSetTokenSuccess() {

  }

  @Override
  public void onSetTokenError(RestError restError) {

  }

  public CabinetsFilter getFilter() {
    return filter;
  }

  public void onFilterChanged(CabinetsFilter newFilter) {
    filter.setSpecializations(newFilter.getSpecializations());
    filter.setDoctors(newFilter.getDoctors());
    getAllCabinetsInteractor.execute(this, filter);
  }

  @Override
  public void onGetOtherCabinetsSuccess(List<Cabinet> cabinetList) {
    mView.setDoctorCabinets(cabinetList);
  }

  @Override
  public void onGetOtherCabinetsError(RestError restError) {

  }
}
