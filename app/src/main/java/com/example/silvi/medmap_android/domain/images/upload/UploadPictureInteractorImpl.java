package com.example.silvi.medmap_android.domain.images.upload;

import com.example.medmap_api.service.ImageService;
import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.util.PrintLog;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.Interactor;
import com.example.silvi.medmap_android.executor.MainThread;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by silvi on 20.03.2017.
 */

public class UploadPictureInteractorImpl implements UploadPictureInteractor, Interactor {


  private final Executor mExecutor;
  private final MainThread mMainThread;
  private final ImageService mUploadService;

  private Callback mCallback;
  private File mFile;
  private Long mCabinetId;
  private List<File> mFiles;

  @Inject
  public UploadPictureInteractorImpl(Executor executor, MainThread mainThread, ImageService service) {
    this.mExecutor = executor;
    this.mMainThread = mainThread;
    this.mUploadService = service;
  }

  @Override
  public void execute(Callback callback, File file) {
    if (null == callback) {
      throw new IllegalArgumentException(
          "Callback can't be null, the client of this interactor needs to get the response " +
              "in the callback");
    }
    this.mCallback = callback;
    this.mFile = file;
    this.mExecutor.run(this);
  }

  @Override
  public void executeMultiple(Callback callback, List<File> files, Long cabinetId) {
    if (null == callback) {
      throw new IllegalArgumentException(
          "Callback can't be null, the client of this interactor needs to get the response " +
              "in the callback");
    }
    this.mCallback = callback;
    this.mFiles = files;
    this.mCabinetId = cabinetId;
    this.mExecutor.run(this);
  }

  @Override
  public void run() {
    if (mCabinetId == null) {
      TypedFile typedFile = new TypedFile("multipart/form-data", mFile);
      mUploadService.uploadProfilePicture(typedFile, new retrofit.Callback<Object>() {
        @Override
        public void success(Object o, Response response) {
          notifySuccess();
        }

        @Override
        public void failure(RetrofitError error) {
          RestError restError = null;
          try {
            restError = (RestError) error.getBodyAs(RestError.class);
          } catch (Exception e) {
            PrintLog.printStackTrace(getClass().getSimpleName(), e);
          }
          notifyError(restError);
        }
      });
    } else {
      for (File file : mFiles) {
        TypedFile typedFile = new TypedFile("multipart/form-data", file);
        mUploadService.uploadCabinetPicture(typedFile, mCabinetId, new retrofit.Callback<Object>() {
          @Override
          public void success(Object o, Response response) {
            notifySuccess();
          }

          @Override
          public void failure(RetrofitError error) {
            RestError restError = null;
            try {
              restError = (RestError) error.getBodyAs(RestError.class);
            } catch (Exception e) {
              PrintLog.printStackTrace(getClass().getSimpleName(), e);
            }
            notifyError(restError);
          }
        });
      }

    }
  }

  private void notifySuccess() {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onPictureUploadSuccess();
      }
    });
  }

  private void notifyError(final RestError restError) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onPictureUploadFailed(restError);
      }
    });
  }
}
