package com.example.silvi.medmap_android.ui.appointments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by silvi on 11.04.2017.
 */

public class AppointmentsPageAdapter extends FragmentPagerAdapter {

  private List<Fragment> fragments;

  public AppointmentsPageAdapter(FragmentManager fragmentManager, List<Fragment> fragments) {
    super(fragmentManager);
    this.fragments = fragments;
  }

  @Override
  public Fragment getItem(int position) {
    return this.fragments.get(position);
  }

  @Override
  public int getCount() {
    return this.fragments.size();
  }
}
