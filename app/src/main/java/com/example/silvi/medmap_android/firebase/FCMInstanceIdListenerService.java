package com.example.silvi.medmap_android.firebase;

import android.content.Intent;

import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by silvi on 20.04.2017.
 */

public class FCMInstanceIdListenerService extends FirebaseInstanceIdService {

  @Override
  public void onTokenRefresh() {
    Intent intent = new Intent(this, RegistrationIntentService.class);
    startService(intent);
  }
}
