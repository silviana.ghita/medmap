package com.example.silvi.medmap_android.ui.cabinet.update;

import com.example.medmap_commons.model.Image;

import java.io.File;
import java.util.List;

/**
 * Created by silvi on 16.03.2017.
 */

public interface UpdateCabinetContract {

  interface View {

    String getCabinetName();

    String getDescription();

    String getSpecialization();

    String getCategory();

    Long getCabinetId();

    void showToast(String text);

    List<File> getImagesPath();

    void deleteCabinetPicture(String imagePath, Integer position, Image image);

    void forceFinish();


  }

  interface Presenter {

    void onSaveButtonClicked();

    void onSavePicturesClicked();

    void deleteCabinetPictures(Long cabinetId, List<Image> imagesToDelete);
  }
}
