package com.example.silvi.medmap_android.ui.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.example.silvi.medmap_android.manager.AssetsManager;

/**
 * Created by silvi on 24.02.2017.
 */

public class TypefaceTextView extends TextView {

  public TypefaceTextView(Context context) {
    super(context);
  }

  public TypefaceTextView(Context context, Typeface typeface) {
    super(context);
    if (!isInEditMode()) {
      setTypeface(typeface);
    }
  }

  public TypefaceTextView(Context context, AttributeSet attrs) {
    super(context, attrs);
    if (!isInEditMode()) {
      setTypeface(AssetsManager.getTypeface(attrs, context));
    }
  }

  public TypefaceTextView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    if (!isInEditMode()) {
      setTypeface(AssetsManager.getTypeface(attrs, context));
    }
  }
}
