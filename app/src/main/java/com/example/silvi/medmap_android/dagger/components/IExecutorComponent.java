package com.example.silvi.medmap_android.dagger.components;

import com.example.silvi.medmap_android.dagger.modules.ExecutorModule;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.MainThread;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by silvi on 08.03.2017.
 */

@Singleton
@Component(
    modules = ExecutorModule.class
)
public interface IExecutorComponent {

  Executor getExecutor();

  MainThread getMainThread();

}
