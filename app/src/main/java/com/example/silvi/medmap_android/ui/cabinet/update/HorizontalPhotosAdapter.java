package com.example.silvi.medmap_android.ui.cabinet.update;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.medmap_commons.model.Image;
import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.ui.cabinet.add.AddCabinetActivity;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by silvi on 01.04.2017.
 */

public class HorizontalPhotosAdapter extends RecyclerView.Adapter<HorizontalPhotosAdapter.ViewHolder> {

  private Context context;
  private List<Image> cabinetImages;


  public HorizontalPhotosAdapter(Context context, List<Image> cabinetImages) {
    this.context = context;
    this.cabinetImages = cabinetImages;
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(context);
    View deleteImagesView = inflater.inflate(R.layout.item_delete_picture, parent, false);

    return new ViewHolder(deleteImagesView);

  }

  @Override
  public void onBindViewHolder(final ViewHolder holder, final int position) {
    final Image image = cabinetImages.get(position);

    final ImageView cabinetImage = holder.cabinetImage;
    TextView deletePhotoButton = holder.deletePhotoButton;

    Glide
        .with(context)
        .load(cabinetImages.get(position).getPath())
        .centerCrop()
        .crossFade()
        .into(cabinetImage);

    deletePhotoButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        openConfirmationDialog(position, image);
      }
    });

  }

  private void openConfirmationDialog(final int position, final Image image) {
    final Dialog dialog = new Dialog(context, R.style.CustomDialog);

    dialog.setContentView(R.layout.dialog_delete_cabinet_picture);

    dialog.findViewById(R.id.yes_button).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dialog.dismiss();
        if (context instanceof UpdateCabinetActivity)
          ((UpdateCabinetActivity) context).deleteCabinetPicture(cabinetImages.get(position).getPath(), position, image);
        else if (context instanceof AddCabinetActivity)
          ((AddCabinetActivity) context).deleteCabinetPicture(cabinetImages.get(position).getPath(), position, image);
      }
    });

    dialog.findViewById(R.id.no_button).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dialog.dismiss();
      }
    });

    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    dialog.show();
  }

  public void setCabinetImages(List<Image> images) {
    this.cabinetImages = images;
  }

  @Override
  public int getItemCount() {
    return cabinetImages.size();
  }

  public void addImagesToList(List<Image> images) {
    boolean isAlreadyAdded;
    if (images != null) {
      for (Image image : images) {
        isAlreadyAdded = false;
        for (Image cabinetImage : cabinetImages) {
          if (image.getPath().equals(cabinetImage.getPath()))
            isAlreadyAdded = true;
        }
        if (!isAlreadyAdded)
          cabinetImages.add(image);
      }
    }
  }

  public static class ViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.horizontal_item_view_image)
    ImageView cabinetImage;
    @Bind(R.id.delete_photo_button)
    TextView deletePhotoButton;

    public ViewHolder(View view) {
      super(view);
      ButterKnife.bind(this, view);
    }
  }
}