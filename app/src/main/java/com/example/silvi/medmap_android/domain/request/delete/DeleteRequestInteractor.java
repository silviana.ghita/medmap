package com.example.silvi.medmap_android.domain.request.delete;

import com.example.medmap_commons.model.Request;
import com.example.medmap_commons.model.RestError;

/**
 * Created by silvi on 27.03.2017.
 */

public interface DeleteRequestInteractor {

  interface Callback {

    void onDeleteRequestSuccess();

    void onDeleteRequestError(RestError restError);

  }

  void execute(Callback callback, Request request);
}
