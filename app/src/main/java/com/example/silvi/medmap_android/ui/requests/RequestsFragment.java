package com.example.silvi.medmap_android.ui.requests;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.medmap_commons.model.Request;
import com.example.medmap_commons.model.Role;
import com.example.medmap_commons.model.UserSession;
import com.example.silvi.medmap_android.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by silvi on 04.04.2017.
 */

public class RequestsFragment extends Fragment {
  public static final String REQUEST_LIST = "REQUEST_LIST";

  List<Request> requestList;
  SentRequestsAdapter sentRequestsAdapter;
  ReceivedRequestsAdapter receivedRequestsAdapter;

  TextView emptyCase;
  RecyclerView recyclerView;


  public static final Fragment newInstance(ArrayList<Request> requestList) {
    RequestsFragment f = new RequestsFragment();
    Bundle bdl = new Bundle(1);
    bdl.putParcelableArrayList(REQUEST_LIST, requestList);
    f.setArguments(bdl);
    return f;
  }

  public void onRemoveRequestClick(Request request, int position) {
    requestList.remove(request);
    sentRequestsAdapter.notifyItemRemoved(position);
    verifyEmptyCase();
    ((RequestsActivity) getActivity()).onRemoveRequestClicked(request);
  }

  public void onAcceptRequestClick(Request request, int position) {
    requestList.remove(position);
    receivedRequestsAdapter.notifyItemRemoved(position);
    receivedRequestsAdapter.notifyDataSetChanged();
    verifyEmptyCase();
    ((RequestsActivity) getActivity()).onAcceptButtonClick(request);
  }

  public void onDeclineRequestClick(Request request, int position) {
    requestList.remove(position);
    receivedRequestsAdapter.notifyItemRemoved(position);
    receivedRequestsAdapter.notifyDataSetChanged();
    verifyEmptyCase();
    ((RequestsActivity) getActivity()).onDeclineButtonClick(request);
  }

  public void verifyEmptyCase() {
    if (requestList.size() == 0) {
      emptyCase.setVisibility(View.VISIBLE);
      recyclerView.setVisibility(View.GONE);
    }
  }


  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    requestList = new ArrayList<>();
    requestList = getArguments().getParcelableArrayList(REQUEST_LIST);
    View v = inflater.inflate(R.layout.fragment_requests, container, false);
    emptyCase = (TextView) v.findViewById(R.id.empty_state);
    recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);


    recyclerView.setHasFixedSize(true);
    recyclerView.setItemAnimator(new DefaultItemAnimator());
    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

    if (UserSession.getUserRole().equals(Role.ROLE_DOCTOR)) {
      receivedRequestsAdapter = new ReceivedRequestsAdapter(this, requestList);
    }

    if (requestList != null && requestList.size() > 0) {
      emptyCase.setVisibility(View.GONE);
      recyclerView.setVisibility(View.VISIBLE);
      if (UserSession.getUserRole().equals(Role.ROLE_DOCTOR)) {
        recyclerView.setAdapter(receivedRequestsAdapter);
      } else {
        sentRequestsAdapter = new SentRequestsAdapter(this, requestList);
        recyclerView.setAdapter(sentRequestsAdapter);
      }
    } else {
      emptyCase.setVisibility(View.VISIBLE);
      recyclerView.setVisibility(View.GONE);
    }

    return v;
  }

}
