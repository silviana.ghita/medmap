package com.example.silvi.medmap_android.domain.appointment.types;

import com.example.medmap_commons.model.AppointmentType;
import com.example.medmap_commons.model.RestError;

import java.util.List;

/**
 * Created by silvi on 07.04.2017.
 */

public interface GetAppointmentTypesInteractor {

  interface Callback {

    void onGetAppointmentTypesSuccess(List<AppointmentType> appointmentTypes);

    void onGetAppointmentTypesError(RestError restError);

  }

  void execute(Callback callback);
}
