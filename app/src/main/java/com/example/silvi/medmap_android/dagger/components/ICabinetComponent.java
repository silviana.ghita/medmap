package com.example.silvi.medmap_android.dagger.components;

import com.example.medmap_api.service.CabinetService;
import com.example.medmap_api.service.ChatService;
import com.example.medmap_api.service.ProfileService;
import com.example.medmap_api.service.RequestService;
import com.example.silvi.medmap_android.dagger.ActivityScope;
import com.example.silvi.medmap_android.dagger.modules.CabinetModule;
import com.example.silvi.medmap_android.domain.cabinet.get.other.GetOtherCabinetsInteractor;
import com.example.silvi.medmap_android.domain.chat.chatroom.OpenChatRoomInteractor;
import com.example.silvi.medmap_android.domain.profile.GetProfileDetailsInteractor;
import com.example.silvi.medmap_android.domain.request.add.AddRequestInteractor;
import com.example.silvi.medmap_android.domain.request.get.GetRequestsInteractor;
import com.example.silvi.medmap_android.ui.cabinet.CabinetActivity;
import com.example.silvi.medmap_android.ui.cabinet.CabinetContract;
import com.example.silvi.medmap_android.ui.cabinet.CabinetPresenter;

import dagger.Component;

/**
 * Created by silvi on 17.03.2017.
 */

@ActivityScope
@Component(
    dependencies = IAppComponent.class,
    modules = CabinetModule.class
)
public interface ICabinetComponent extends IAppComponent {

  void inject(CabinetActivity CabinetActivity);

  CabinetContract.View getView();

  ProfileService getProfileService();

  RequestService getRequestService();

  ChatService getChatService();

  CabinetService getCabinetService();

  GetProfileDetailsInteractor getProfileDetailsInteractor();

  AddRequestInteractor getAddRequestInteractor();

  GetRequestsInteractor getRequestsInteractor();

  OpenChatRoomInteractor getOpenChatRoomInteractor();

  GetOtherCabinetsInteractor getOtherCabinetsInteractor();

  CabinetPresenter getCabinetPresenter();
}
