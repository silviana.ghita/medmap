package com.example.silvi.medmap_android.dagger.components;

import com.example.medmap_api.service.AppointmentService;
import com.example.silvi.medmap_android.dagger.ActivityScope;
import com.example.silvi.medmap_android.dagger.modules.NavigationModule;
import com.example.silvi.medmap_android.dagger.modules.TodayModule;
import com.example.silvi.medmap_android.domain.appointment.today.GetTodaysAppointmentsInteractor;
import com.example.silvi.medmap_android.domain.appointment.today.delay.AddDelayInteractor;
import com.example.silvi.medmap_android.ui.appointments.today.TodayActivity;
import com.example.silvi.medmap_android.ui.appointments.today.TodayContract;
import com.example.silvi.medmap_android.ui.appointments.today.TodayPresenter;

import dagger.Component;

/**
 * Created by silvi on 13.04.2017.
 */

@ActivityScope
@Component(
    dependencies = IAppComponent.class,
    modules = {
        TodayModule.class,
        NavigationModule.class
    }
)
public interface ITodayComponent {

  void inject(TodayActivity requestsActivity);

  TodayContract.View getView();

  AppointmentService getAppointmentService();

  GetTodaysAppointmentsInteractor getTodaysAppointmentsInteractor();

  AddDelayInteractor getAddDelayInteractor();

  TodayPresenter getTodayPresenter();
}
