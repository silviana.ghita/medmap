package com.example.silvi.medmap_android.domain.cabinet.get.other;

import com.example.medmap_commons.model.Cabinet;
import com.example.medmap_commons.model.RestError;

import java.util.List;

/**
 * Created by silvi on 21.05.2017.
 */

public interface GetOtherCabinetsInteractor {

  interface Callback {
    void onGetOtherCabinetsSuccess(List<Cabinet> cabinetList);

    void onGetOtherCabinetsError(RestError restError);
  }

  void execute(Callback callback, Long doctorId);
}
