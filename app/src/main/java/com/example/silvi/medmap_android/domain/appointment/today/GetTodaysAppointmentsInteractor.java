package com.example.silvi.medmap_android.domain.appointment.today;

import com.example.medmap_commons.model.Appointment;
import com.example.medmap_commons.model.RestError;

import java.util.List;

/**
 * Created by silvi on 13.04.2017.
 */

public interface GetTodaysAppointmentsInteractor {


  interface Callback {

    void onGetTodaysAppointmentsSuccess(List<Appointment> appointments);

    void onGetTodaysAppointmentsError(RestError restError);

  }

  void execute(Callback callback);
}
