package com.example.silvi.medmap_android.dagger.modules;

import com.example.medmap_api.service.AuthService;
import com.example.silvi.medmap_android.domain.register.RegisterDoctorInteractor;
import com.example.silvi.medmap_android.domain.register.RegisterDoctorInteractorImpl;
import com.example.silvi.medmap_android.domain.register.RegisterUserInteractor;
import com.example.silvi.medmap_android.domain.register.RegisterUserInteractorImpl;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.MainThread;
import com.example.silvi.medmap_android.ui.auth.register.RegisterActivity;
import com.example.silvi.medmap_android.ui.auth.register.RegisterContract;
import com.example.silvi.medmap_android.ui.auth.register.RegisterPresenter;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;

/**
 * Created by silvi on 08.03.2017.
 */

@Module
public class RegisterModule {

  private RegisterContract.View mView;

  public RegisterModule(RegisterActivity view) {
    this.mView = view;
  }

  @Provides
  public RegisterContract.View provideView() {
    return mView;
  }

  @Provides
  public AuthService provideAuthService(RestAdapter restAdapter) {
    return restAdapter.create(AuthService.class);
  }

  @Provides
  public RegisterUserInteractor provideRegisterUserInteractor(Executor executor, MainThread mainThread, AuthService authService) {
    return new RegisterUserInteractorImpl(executor, mainThread, authService);
  }

  @Provides
  public RegisterDoctorInteractor provideRegisterDoctorInteractor(Executor executor, MainThread mainThread, AuthService authService) {
    return new RegisterDoctorInteractorImpl(executor, mainThread, authService);
  }

  @Provides
  RegisterPresenter providePresenter(RegisterContract.View view, RegisterUserInteractor registerUserInteractor, RegisterDoctorInteractor registerDoctorInteractor) {
    return new RegisterPresenter(view, registerUserInteractor, registerDoctorInteractor);
  }
}
