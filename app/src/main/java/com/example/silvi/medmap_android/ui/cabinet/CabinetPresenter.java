package com.example.silvi.medmap_android.ui.cabinet;

import com.example.medmap_commons.model.Cabinet;
import com.example.medmap_commons.model.ChatRoom;
import com.example.medmap_commons.model.Doctor;
import com.example.medmap_commons.model.Request;
import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.model.Role;
import com.example.medmap_commons.model.State;
import com.example.medmap_commons.model.User;
import com.example.silvi.medmap_android.domain.cabinet.get.other.GetOtherCabinetsInteractor;
import com.example.silvi.medmap_android.domain.chat.chatroom.OpenChatRoomInteractor;
import com.example.silvi.medmap_android.domain.profile.GetProfileDetailsInteractor;
import com.example.silvi.medmap_android.domain.request.add.AddRequestInteractor;
import com.example.silvi.medmap_android.domain.request.get.GetRequestsInteractor;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by silvi on 26.02.2017.
 */

public class CabinetPresenter implements CabinetContract.Presenter, GetProfileDetailsInteractor.Callback
    , AddRequestInteractor.Callback, GetRequestsInteractor.Callback, OpenChatRoomInteractor.Callback
    , GetOtherCabinetsInteractor.Callback {


  CabinetContract.View mView;
  GetProfileDetailsInteractor mGetProfileDetailsInteractor;
  AddRequestInteractor mAddRequestInteractor;
  GetRequestsInteractor mGetRequestsInteractor;
  OpenChatRoomInteractor mOpenChatRoomInteractor;
  GetOtherCabinetsInteractor mGetOtherCabinetsInteractor;

  @Inject
  public CabinetPresenter(CabinetContract.View view, GetProfileDetailsInteractor getProfileDetailsInteractor,
                          AddRequestInteractor addRequestInteractor, GetRequestsInteractor getRequestsInteractor,
                          OpenChatRoomInteractor openChatRoomInteractor, GetOtherCabinetsInteractor getOtherCabinetsInteractor) {
    this.mView = view;
    this.mGetProfileDetailsInteractor = getProfileDetailsInteractor;
    this.mAddRequestInteractor = addRequestInteractor;
    this.mGetRequestsInteractor = getRequestsInteractor;
    this.mOpenChatRoomInteractor = openChatRoomInteractor;
    this.mGetOtherCabinetsInteractor = getOtherCabinetsInteractor;
  }

  @Override
  public void getDoctorDetails(Long userId, Role userRole) {
    mGetProfileDetailsInteractor.execute(this, userId, userRole);
  }

  @Override
  public void getUserRequests() {
    mGetRequestsInteractor.execute(this);
  }

  @Override
  public void openChatRoom(Long otherUserId) {
    mOpenChatRoomInteractor.execute(this, otherUserId);
  }

  @Override
  public void getOtherCabinets(Long doctorId) {
    mGetOtherCabinetsInteractor.execute(this, doctorId);
  }

  @Override
  public void onSendRequestClicked(Cabinet cabinet) {
    Request request = new Request();
    request.setCabinet(cabinet);
    request.setState(State.PENDING);
    mAddRequestInteractor.execute(this, request);
  }


  @Override
  public void onGetUserProfileDetailsSuccess(User user) {

  }

  @Override
  public void onGetDoctorProfileDetailsSuccess(Doctor doctor) {
    mView.setDoctorEmail(doctor.getUser().getEmail());
    mView.setDoctorPhoneNumber(doctor.getUser().getPhoneNumber());
    mView.setDoctorExperience(doctor.getExperience());
    mView.setDoctorSpecialization(doctor.getSpecialization());
    mView.setDoctorStudies(doctor.getStudies());
    mView.setDoctorBirthDate(doctor.getUser().getBirthDate());
    mView.checkEmptyFields();
  }

  @Override
  public void onGetProfileDetailsError(RestError restError) {

  }


  @Override
  public void onAddRequestSuccess() {

  }

  @Override
  public void onAddRequestError(RestError restError) {
    mView.showToast(restError.getMessage());
  }

  @Override
  public void onGetDoctorRequestsSuccess(List<Request> requests) {
    mView.setUserRequests(requests);
  }

  @Override
  public void onGetDoctorRequestsError(RestError restError) {

  }

  @Override
  public void onOpenChatRoomSuccess(ChatRoom chatRoom) {
    mView.startChatRoomActivity(chatRoom);
  }

  @Override
  public void onOpenChatRoomError(RestError restError) {

  }

  @Override
  public void onGetOtherCabinetsSuccess(List<Cabinet> cabinetList) {
    mView.initOtherCabinets(cabinetList);
  }

  @Override
  public void onGetOtherCabinetsError(RestError restError) {

  }
}
