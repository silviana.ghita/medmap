package com.example.silvi.medmap_android.domain.firebase;

import com.example.medmap_api.service.ProfileService;
import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.util.PrintLog;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.Interactor;
import com.example.silvi.medmap_android.executor.MainThread;

import java.util.Collections;
import java.util.Map;

import javax.inject.Inject;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by silvi on 22.04.2017.
 */

public class FCMTokenInteractorImpl implements FCMTokenInteractor, Interactor {

  private final Executor mExecutor;
  private final MainThread mMainThread;
  private final ProfileService mProfileService;

  private Callback mCallback;
  private String mToken;

  @Inject
  public FCMTokenInteractorImpl(Executor executor, MainThread mainThread, ProfileService service) {
    this.mExecutor = executor;
    this.mMainThread = mainThread;
    this.mProfileService = service;
  }

  @Override
  public void execute(Callback callback, String token) {
    if (null == callback) {
      throw new IllegalArgumentException(
          "Callback can't be null, the client of this interactor needs to get the response " +
              "in the callback");
    }
    this.mCallback = callback;
    this.mToken = token;
    this.mExecutor.run(this);
  }

  @Override
  public void run() {
    Map<String, String> notofyToken = Collections.singletonMap("notifyToken", mToken);
    mProfileService.setFCMToken(notofyToken, new retrofit.Callback<Object>() {
      @Override
      public void success(Object o, Response response) {
        notifySuccess();
      }

      @Override
      public void failure(RetrofitError error) {
        RestError restError = null;
        try {
          restError = (RestError) error.getBodyAs(RestError.class);
        } catch (Exception e) {
          PrintLog.printStackTrace(getClass().getSimpleName(), e);
        }
        notifyError(restError);
      }
    });
  }

  private void notifySuccess() {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onSetTokenSuccess();
      }
    });
  }

  private void notifyError(final RestError restError) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onSetTokenError(restError);
      }
    });
  }

}
