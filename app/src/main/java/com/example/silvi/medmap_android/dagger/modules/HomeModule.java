package com.example.silvi.medmap_android.dagger.modules;

import com.example.medmap_api.service.CabinetService;
import com.example.medmap_api.service.ProfileService;
import com.example.silvi.medmap_android.domain.cabinet.get.GetAllCabinetsInteractor;
import com.example.silvi.medmap_android.domain.cabinet.get.GetAllCabinetsInteractorImpl;
import com.example.silvi.medmap_android.domain.cabinet.get.other.GetOtherCabinetsInteractor;
import com.example.silvi.medmap_android.domain.cabinet.get.other.GetOtherCabinetsInteractorImpl;
import com.example.silvi.medmap_android.domain.firebase.FCMTokenInteractor;
import com.example.silvi.medmap_android.domain.firebase.FCMTokenInteractorImpl;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.MainThread;
import com.example.silvi.medmap_android.ui.home.HomeContract;
import com.example.silvi.medmap_android.ui.home.HomePresenter;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;

/**
 * Created by silvi on 16.03.2017.
 */

@Module
public class HomeModule {

  private HomeContract.View view;

  public HomeModule(HomeContract.View view) {
    this.view = view;
  }

  @Provides
  public HomeContract.View getView() {
    return view;
  }

  @Provides
  public CabinetService provideCabinetService(RestAdapter restAdapter) {
    return restAdapter.create(CabinetService.class);
  }

  @Provides
  public ProfileService provideProfileService(RestAdapter restAdapter) {
    return restAdapter.create(ProfileService.class);
  }


  @Provides
  public GetAllCabinetsInteractor provideGetAllCabinetsInteractor(Executor executor,
                                                                  MainThread mainThread,
                                                                  CabinetService cabinetService) {
    return new GetAllCabinetsInteractorImpl(executor, mainThread, cabinetService);
  }

  @Provides
  public FCMTokenInteractor provideFCMTokenInteractor(Executor executor,
                                                      MainThread mainThread,
                                                      ProfileService profileService) {
    return new FCMTokenInteractorImpl(executor, mainThread, profileService);
  }

  @Provides
  public GetOtherCabinetsInteractor provideGetOtherCabinetsInteractor(Executor executor,
                                                                      MainThread mainThread,
                                                                      CabinetService cabinetService) {
    return new GetOtherCabinetsInteractorImpl(executor, mainThread, cabinetService);
  }


  @Provides
  public HomePresenter providePresenter(HomeContract.View view, GetAllCabinetsInteractor interactor, FCMTokenInteractor fcmTokenInteractor, GetOtherCabinetsInteractor getOtherCabinetsInteractor) {
    return new HomePresenter(view, interactor, fcmTokenInteractor, getOtherCabinetsInteractor);
  }
}
