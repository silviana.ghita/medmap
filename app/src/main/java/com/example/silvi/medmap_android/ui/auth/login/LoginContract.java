package com.example.silvi.medmap_android.ui.auth.login;

/**
 * Created by silvi on 23.02.2017.
 */

public interface LoginContract {

  interface Presenter {

    void onLoginClick();

  }

  interface View {

    String getPassword();

    String getEmail();

    void showToast(String text);

    void openUserTypeDialog();

    void startHomeActivity();
  }
}
