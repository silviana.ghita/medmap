package com.example.silvi.medmap_android.domain.appointment.update;

import com.example.medmap_commons.model.Appointment;
import com.example.medmap_commons.model.RestError;

/**
 * Created by silvi on 12.04.2017.
 */

public interface UpdateAppointmentStateInteractor {

  interface Callback {

    void onUpdateAppointmentStateSuccess();

    void onUpdateAppointmentStateError(RestError restError);

  }

  void execute(Callback callback, Appointment appointment);
}
