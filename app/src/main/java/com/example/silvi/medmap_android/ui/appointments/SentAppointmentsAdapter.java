package com.example.silvi.medmap_android.ui.appointments;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.example.medmap_commons.model.Appointment;
import com.example.medmap_commons.model.State;
import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.ui.base.MedMapApplication;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by silvi on 11.04.2017.
 */

public class SentAppointmentsAdapter extends RecyclerView.Adapter<SentAppointmentsAdapter.ViewHolder> {

  private AppointmentsFragment context;
  private List<Appointment> appointments;

  public SentAppointmentsAdapter(AppointmentsFragment context, List<Appointment> appointments) {
    this.appointments = appointments;
    this.context = context;
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater inflater = null;
    inflater = LayoutInflater.from(context.getContext());

    View appointmentsView = inflater.inflate(R.layout.line_item_sent_appointment, parent, false);

    return new ViewHolder(appointmentsView);
  }

  @Override
  public void onBindViewHolder(ViewHolder holder, final int position) {

    final Appointment appointment = appointments.get(position);

    ImageView cabinetPhotoImageView = holder.coverPhotoImageView;
    TextView doctorName = holder.doctorName;
    final ImageView doctorPhotoImageView = holder.doctorPhotoImageView;
    TextView appointmentStatus = holder.appointmentStatus;
    TextView appointmentStartDate = holder.appointmentStartDate;
    TextView appointmentStartTime = holder.appointmentStartTime;
    TextView appointmentTypeName = holder.appointmentTypeName;
    TextView appointmentDuration = holder.appointmentDuration;
    TextView appointmentPrice = holder.appointmentPrice;
    Button btnContactDoctor = holder.buttonContactDoctor;
    Button btnCancelAppointment = holder.buttonCancelAppointment;
    LinearLayout pastAppointmentsLayout = holder.pastAppointmentsLayout;
    TextView pastAppointmentsText = holder.pastAppointmentsText;

    if (position == context.getFirstCompletedAppointmentPosition() && !appointment.getState().equals(State.PENDING.toString())) {
      pastAppointmentsLayout.setVisibility(View.VISIBLE);
      if (appointment.getState().equals(State.ACCEPTED.toString()))
        pastAppointmentsText.setText(MedMapApplication.getInstance().getResources().getString(R.string.completed_appointments));
      else if (appointment.getState().equals(State.DECLINED.toString()))
        pastAppointmentsText.setText(MedMapApplication.getInstance().getResources().getString(R.string.past_appointments));
    } else {
      pastAppointmentsLayout.setVisibility(View.GONE);
    }

    Drawable userIcon = MedMapApplication.getInstance().getResources().getDrawable(R.drawable.user_icon);
    userIcon.setColorFilter(new LightingColorFilter(Color.LTGRAY, Color.LTGRAY));


    Glide.with(context)
        .load(appointment.getCabinet().getDoctor().getProfilePicture() != null ? appointment.getCabinet().getDoctor().getProfilePicture().getPath() : "")
        .asBitmap()
        .centerCrop()
        .placeholder(userIcon)
        .into(new BitmapImageViewTarget(doctorPhotoImageView) {
          @Override
          protected void setResource(Bitmap resource) {
            RoundedBitmapDrawable circularBitmapDrawable =
                RoundedBitmapDrawableFactory.create(context.getResources(), resource);
            circularBitmapDrawable.setCornerRadius(
                Math.max(resource.getWidth(), resource.getHeight()) / 2.0f);
            doctorPhotoImageView.setImageDrawable(circularBitmapDrawable);
          }
        });

    Glide
        .with(context)
        .load(appointment.getCabinet().getImages().get(0).getPath())
        .centerCrop()
        .crossFade()
        .into(cabinetPhotoImageView);

    doctorName.setText(appointment.getCabinet().getDoctor().getLastName() + " " + appointment.getCabinet().getDoctor().getFirstName());
    appointmentStatus.setText(appointment.getState());
    appointmentStartDate.setText(appointment.getDate());
    appointmentStartTime.setText(appointment.getHour());
    appointmentTypeName.setText(appointment.getAppointmentType().getType());
    appointmentDuration.setText(appointment.getAppointmentType().getDuration().toString() + " " + MedMapApplication.getInstance().getResources().getString(R.string.hour));
    appointmentPrice.setText(appointment.getAppointmentType().getPrice().toString() + " " + MedMapApplication.getInstance().getResources().getString(R.string.ron));

    btnContactDoctor.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        context.openChatRoom(appointment.getCabinet().getDoctor().getUserId());
      }
    });

    btnCancelAppointment.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        context.onDeleteAppointmentClick(appointment, position);
      }
    });

  }

  @Override
  public int getItemCount() {
    return appointments.size();
  }

  public static class ViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.coverPhotoImgView)
    ImageView coverPhotoImageView;
    @Bind(R.id.doctorNameTView)
    TextView doctorName;
    @Bind(R.id.doctorPhotoImgView)
    ImageView doctorPhotoImageView;
    @Bind(R.id.appointmentStatusTView)
    TextView appointmentStatus;
    @Bind(R.id.appointment_start_date)
    TextView appointmentStartDate;
    @Bind(R.id.appointment_start_time)
    TextView appointmentStartTime;
    @Bind(R.id.appointment_type_name)
    TextView appointmentTypeName;
    @Bind(R.id.appointment_duration)
    TextView appointmentDuration;
    @Bind(R.id.appointment_price)
    TextView appointmentPrice;
    @Bind(R.id.btnContactDoctor)
    Button buttonContactDoctor;
    @Bind(R.id.btnCancelAppointment)
    Button buttonCancelAppointment;
    @Bind(R.id.past_appointments_layout)
    LinearLayout pastAppointmentsLayout;
    @Bind(R.id.past_appointments_text)
    TextView pastAppointmentsText;


    public ViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
