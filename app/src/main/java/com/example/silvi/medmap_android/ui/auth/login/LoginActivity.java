package com.example.silvi.medmap_android.ui.auth.login;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.dagger.components.DaggerILoginComponent;
import com.example.silvi.medmap_android.dagger.components.IAppComponent;
import com.example.silvi.medmap_android.dagger.modules.LoginModule;
import com.example.silvi.medmap_android.preferences.SharedPreferencesHandler;
import com.example.silvi.medmap_android.ui.auth.register.RegisterActivity;
import com.example.silvi.medmap_android.ui.base.BaseActivity;
import com.example.silvi.medmap_android.ui.base.BasePresenter;
import com.example.silvi.medmap_android.ui.base.FinishActivities;
import com.example.silvi.medmap_android.ui.base.FinishCallback;
import com.example.silvi.medmap_android.ui.home.HomeActivity;
import com.example.silvi.medmap_android.util.ScreenType;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by silvi on 23.02.2017.
 */

public class LoginActivity extends BaseActivity implements LoginContract.View, FinishCallback {

  @Bind(R.id.email_input)
  EditText mEmailInput;
  @Bind(R.id.password_input)
  EditText mPasswordInput;

  @Inject
  LoginPresenter mLoginPresenter;


  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setupEvents();

    FinishActivities.addActivity(this);

  }


  private void setupEvents() {
    mEmailInput.setOnEditorActionListener(new EditorActionListener(mPasswordInput));
    EditorActionListener editorAction = new EditorActionListener();
    editorAction.setListener(new ButtonKeyListener() {
      @Override
      public void onDoneClicked() {
        mLoginPresenter.onLoginClick();
      }
    });
    mPasswordInput.setOnEditorActionListener(editorAction);
  }

  @OnClick(R.id.forgot_password_button)
  void onForgotPasswordClick() {

  }

  @OnClick(R.id.login_button)
  void onLoginClick() {
    mLoginPresenter.onLoginClick();
  }

  @OnClick(R.id.sign_up_button)
  void onSignUpClick() {
    openUserTypeDialog();
  }


  @Override
  protected int getLayout() {
    return R.layout.activity_login;
  }

  @Override
  public void setUpComponent(IAppComponent appComponent) {
    DaggerILoginComponent.builder()
        .iAppComponent(appComponent)
        .loginModule(new LoginModule(this))
        .build()
        .inject(this);
  }

  @Nullable
  @Override
  protected BasePresenter getPresenter() {
    return null;
  }

  @Override
  protected String getActionBarTitle() {
    return null;
  }


  @Override
  public String getPassword() {
    return String.valueOf(mPasswordInput.getText());
  }

  @Override
  public String getEmail() {
    return String.valueOf(mEmailInput.getText());
  }

  @Override
  public void showToast(String text) {
    Toast.makeText(this, text, Toast.LENGTH_LONG).show();
  }

  @Override
  public void openUserTypeDialog() {
    final Dialog dialog = new Dialog(this, R.style.CustomDialog);
    dialog.setContentView(R.layout.dialog_select_user_type);

    dialog.findViewById(R.id.doctor_button).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(RegisterActivity.makeIntent(getApplicationContext(), ScreenType.DOCTOR_REGISTER));
        dialog.dismiss();
        finish();
      }
    });

    dialog.findViewById(R.id.pacient_button).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(RegisterActivity.makeIntent(getApplicationContext(), ScreenType.PACIENT_REGISTER));
        dialog.dismiss();
        finish();
      }
    });

    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    dialog.show();
  }

  @Override
  public void startHomeActivity() {
    getAppComponent().getApiRequestInterceptor().setAuthToken(SharedPreferencesHandler.getAuthToken(this));
    finish();
    startActivity(HomeActivity.makeIntent(getApplicationContext()));
  }

  @Override
  public void forceFinish() {
    this.finish();
  }

  public static Intent makeIntent(Context context) {
    return new Intent(context, LoginActivity.class);
  }
}
