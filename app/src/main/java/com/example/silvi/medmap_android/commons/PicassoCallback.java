package com.example.silvi.medmap_android.commons;

/**
 * Created by silvi on 24.03.2017.
 */

import android.widget.ViewAnimator;

import com.squareup.picasso.Callback;

public class PicassoCallback implements Callback {

  private ViewAnimator animator;
  private int position;

  public PicassoCallback(ViewAnimator animator, int position) {
    this.animator = animator;
    this.position = position;
  }

  @Override
  public void onSuccess() {
    if (animator != null && position > -1 && position < animator.getChildCount()) {
      animator.setDisplayedChild(position);
    }
  }

  @Override
  public void onError() {
    //
  }
}
