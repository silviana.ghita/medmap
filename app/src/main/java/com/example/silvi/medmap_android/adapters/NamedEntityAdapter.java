package com.example.silvi.medmap_android.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by silvi on 10.03.2017.
 */

public class NamedEntityAdapter<T> extends ArrayAdapter<T> implements SpinnerAdapter {
  private Activity activity;
  private List<T> list_bsl;
  private int textViewResourceId;

  public NamedEntityAdapter(Activity activity, int textViewResourceId, List<T> list_bsl) {
    super(activity, textViewResourceId, list_bsl);
    this.activity = activity;
    this.list_bsl = list_bsl;
    this.textViewResourceId = textViewResourceId;
  }

  @Override
  public int getCount() {
    return list_bsl.size();
  }

  @Override
  public T getItem(int position) {
    return list_bsl.get(position);
  }

  @Override
  public long getItemId(int position) {
    return list_bsl.get(position).hashCode();
  }

  public List<T> getItems() {
    return list_bsl;
  }

  @Override
  public View getDropDownView(int position, View convertView, ViewGroup parent) {
    return getCustomView(position, convertView, parent);
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    return getCustomView(position, convertView, parent);
  }

  public View getCustomView(int position, View convertView, ViewGroup parent) {

    View spinView;
    if (convertView == null) {
      LayoutInflater inflater = activity.getLayoutInflater();
      spinView = inflater.inflate(textViewResourceId, parent, false);
    } else {
      spinView = convertView;
    }
    TextView t1 = (TextView) spinView.findViewById(android.R.id.text1);
    T item = list_bsl.get(position);

    if (item instanceof String)
      t1.setText(((String) item));
    return spinView;
  }
}

