package com.example.silvi.medmap_android.ui.messages;

import com.example.medmap_commons.model.ChatRoom;
import com.example.medmap_commons.model.RestError;
import com.example.silvi.medmap_android.domain.chat.GetChatRoomsInteractor;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by silvi on 01.05.2017.
 */

public class MessagesPresenter implements MessagesContract.Presenter, GetChatRoomsInteractor.Callback {

  MessagesContract.View mView;
  GetChatRoomsInteractor mGetChatRoomsInteractor;

  @Inject
  public MessagesPresenter(MessagesContract.View view, GetChatRoomsInteractor getChatRoomsInteractor) {
    this.mView = view;
    this.mGetChatRoomsInteractor = getChatRoomsInteractor;
  }

  @Override
  public void getChatRooms() {
    mGetChatRoomsInteractor.execute(this);
  }

  @Override
  public void onGetChatRoomsSuccess(List<ChatRoom> chatRooms) {
    mView.setChatRooms(chatRooms);
  }

  @Override
  public void onGetChatRoomsError(RestError restError) {

  }


}
