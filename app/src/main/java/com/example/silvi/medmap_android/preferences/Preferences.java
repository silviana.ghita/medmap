package com.example.silvi.medmap_android.preferences;

/**
 * Created by silvi on 22.02.2017.
 */

public interface Preferences {

  interface PrefNames {
    String PREFS_NAME_TAG = "MedMapPrefs";
  }

  interface PrefKeys {

    String PREF_X_AUTH_TOKEN = "sessionToken";
    String PREF_USER_EMAIL = "userEmail";
    String PREF_USER_ID = "userId";
    String PREF_USER_FULL_NAME = "userFullName";
    String PREF_APPLICATION_VISIBLE = "applicationVisible";
    String PREF_APPLICATION_FOREGROUND = "applicationForeground";


    String PREF_NOTIFICATIONS = "notifications";

  }
}
