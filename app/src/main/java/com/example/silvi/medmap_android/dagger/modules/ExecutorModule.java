package com.example.silvi.medmap_android.dagger.modules;

import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.MainThread;
import com.example.silvi.medmap_android.executor.MainThreadImpl;
import com.example.silvi.medmap_android.executor.ThreadExecutor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by silvi on 08.03.2017.
 */

@Module
public final class ExecutorModule {

  @Provides
  @Singleton
  Executor provideExecutor() {
    return new ThreadExecutor();
  }

  @Provides
  @Singleton
  MainThread provideMainThread() {
    return new MainThreadImpl();
  }

}
