package com.example.silvi.medmap_android.domain.logout;

import retrofit.RetrofitError;

/**
 * Created by silvi on 11.03.2017.
 */

public interface LogoutInteractor {

  interface Callback {

    void onLogoutSuccess();

    void onLogoutError(RetrofitError retrofitError);

  }

  void execute(Callback callback);
}
