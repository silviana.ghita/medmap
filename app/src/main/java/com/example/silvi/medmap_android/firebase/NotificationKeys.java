package com.example.silvi.medmap_android.firebase;

/**
 * Created by silvi on 20.04.2017.
 */

public enum NotificationKeys {
  DOCTOR_FULL_NAME("doctorFullName"),
  APPOINTMENT_DATE("appointmentDate"),
  APPOINTMENT_HOUR("appointmentHour"),
  APPOINTMENT_DELAY("delay"),
  CABINET_NAME("cabinetName"),
  USER_FULL_NAME("userFullName"),
  STATE("state");


  private final String key;

  NotificationKeys(String key) {
    this.key = key;
  }

  @Override
  public String toString() {
    return key;
  }
}
