package com.example.silvi.medmap_android.domain.cabinet.update;

import com.example.medmap_api.service.CabinetService;
import com.example.medmap_commons.model.Cabinet;
import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.util.PrintLog;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.Interactor;
import com.example.silvi.medmap_android.executor.MainThread;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by silvi on 17.03.2017.
 */

public class UpdateCabinetInteractorImpl implements UpdateCabinetInteractor, Interactor {


  private final Executor mExecutor;
  private final MainThread mMainThread;
  private final CabinetService mCabinetService;

  public UpdateCabinetInteractorImpl(Executor executor, MainThread mainThread, CabinetService cabinetService) {
    mExecutor = executor;
    mMainThread = mainThread;
    mCabinetService = cabinetService;
  }

  private Callback mCallback;
  private Cabinet mCabinet;

  @Override
  public void execute(Callback callback, Cabinet cabinet) {
    if (null == callback) {
      throw new IllegalArgumentException(
          "Callback can't be null, the client of this interactor needs to get the response " +
              "in the callback");
    }
    this.mCallback = callback;
    this.mCabinet = cabinet;
    this.mExecutor.run(this);
  }

  @Override
  public void run() {
    mCabinetService.updateCabinet(mCabinet, new retrofit.Callback<Cabinet>() {
      @Override
      public void success(Cabinet cabinet, Response response) {
        notifySuccess();
      }

      @Override
      public void failure(RetrofitError error) {
        RestError restError = null;
        try {
          restError = (RestError) error.getBodyAs(RestError.class);
        } catch (Exception e) {
          PrintLog.printStackTrace(getClass().getSimpleName(), e);
        }
        notifyError(restError);

      }
    });
  }

  private void notifySuccess() {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onUpdateCabinetSuccess();
      }
    });
  }

  private void notifyError(final RestError restError) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onUpdateCabinetError(restError);
      }
    });
  }
}
