package com.example.silvi.medmap_android.domain.images.upload;

import com.example.medmap_commons.model.RestError;

import java.io.File;
import java.util.List;

/**
 * Created by silvi on 20.03.2017.
 */

public interface UploadPictureInteractor {

  interface Callback {

    void onPictureUploadSuccess();

    void onPictureUploadFailed(RestError restError);
  }

  void execute(Callback callback, File path);

  void executeMultiple(Callback callback, List<File> paths, Long cabinetId);
}
