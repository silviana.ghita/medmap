package com.example.silvi.medmap_android.manager;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.example.silvi.medmap_android.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by silvi on 24.02.2017.
 */

public class AssetsManager {
  private static final String TAG = AssetsManager.class.getSimpleName();

  /**
   * Caches typefaces based on their file path and name, so that they don't have to be created
   * every time when they are referenced.
   */
  private static Map<String, Typeface> mTypefaces;

  public static Typeface getTypeface(String typefaceAssetPath, Context context) {
    if (typefaceAssetPath == null || context == null) {
      return null;
    }

    return getCachedTypeface(context.getAssets(), typefaceAssetPath);
  }

  public static Typeface getTypeface(AttributeSet attrs, Context context) {
    if (attrs == null || context == null) {
      return null;
    }

    final String typefaceAssetPath = getTypefaceAssetPath(attrs, context);

    if (typefaceAssetPath == null) {
      return null;
    }

    return getCachedTypeface(context.getAssets(), typefaceAssetPath);
  }

  public static String getTypefaceAssetPath(AttributeSet attrs, Context context) {
    final TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.TypefaceWidget);

    if (array == null) {
      return null;
    }

    final String typefaceAssetPath = array.getString(R.styleable.TypefaceWidget_customTypeface);
    array.recycle();
    return typefaceAssetPath;
  }

  public static Typeface getCachedTypeface(AssetManager assetManager, String typefaceAssetPath) {
    Typeface typeface;

    if (mTypefaces == null) {
      mTypefaces = new HashMap<>();
    }

    if (mTypefaces.containsKey(typefaceAssetPath)) {
      typeface = mTypefaces.get(typefaceAssetPath);
    } else {
      typeface = Typeface.createFromAsset(assetManager, typefaceAssetPath);
      mTypefaces.put(typefaceAssetPath, typeface);
    }

    return typeface;
  }
}
