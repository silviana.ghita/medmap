package com.example.silvi.medmap_android.ui.auth.register;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.adapters.NamedEntityAdapter;
import com.example.silvi.medmap_android.commons.IntentKeys;
import com.example.silvi.medmap_android.dagger.components.DaggerIRegisterComponent;
import com.example.silvi.medmap_android.dagger.components.IAppComponent;
import com.example.silvi.medmap_android.dagger.modules.RegisterModule;
import com.example.silvi.medmap_android.ui.auth.login.LoginActivity;
import com.example.silvi.medmap_android.ui.base.BaseActivity;
import com.example.silvi.medmap_android.ui.base.BasePresenter;
import com.example.silvi.medmap_android.ui.base.FinishActivities;
import com.example.silvi.medmap_android.ui.base.FinishCallback;
import com.example.silvi.medmap_android.util.ScreenType;
import com.example.silvi.medmap_android.util.Specializations;
import com.example.silvi.medmap_android.util.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by silvi on 24.02.2017.
 */

public class RegisterActivity extends BaseActivity implements RegisterContract.View, FinishCallback {

  @Inject
  RegisterPresenter mRegisterPresenter;

  @Bind(R.id.first_name_input)
  EditText mFirstName;
  @Bind(R.id.last_name_input)
  EditText mLastName;
  @Bind(R.id.email_input)
  EditText mEmail;
  @Bind(R.id.phone_number_input)
  EditText mPhoneNumber;
  @Bind(R.id.specialization_spinner)
  Spinner mSpecializationSpinner;
  @Bind(R.id.password_input)
  EditText mPassword;
  @Bind(R.id.retype_input)
  EditText mRetype;
  @Bind(R.id.specialization_layout)
  LinearLayout mSpecializationLayout;

  private String screenType;
  private NamedEntityAdapter mSpecialzationsAdapter;

  ;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    if (getIntent().hasExtra(IntentKeys.REGISTER_SCREEN_TYPE)) {
      screenType = getIntent().getStringExtra(IntentKeys.REGISTER_SCREEN_TYPE);
      if (screenType.equals("PACIENT_REGISTER")) {
        mSpecializationLayout.setVisibility(View.GONE);
        setupPacientSignUpEvents();
      } else {
        mPhoneNumber.setVisibility(View.VISIBLE);
        mSpecializationLayout.setVisibility(View.VISIBLE);

        Specializations[] specializations = Specializations.values();
        List<String> specializationList = new ArrayList<>();
        for (int i = 0; i < specializations.length; i++)
          specializationList.add(specializations[i].toString());

        mSpecialzationsAdapter = new NamedEntityAdapter(this, R.layout.simple_spinner_item, specializationList);
        mSpecializationSpinner.setAdapter(mSpecialzationsAdapter);

        setupDoctorSignUpEvents();
      }
    }

    FinishActivities.addActivity(this);

    setupToolbarDisplay();

  }

  private void setupToolbarDisplay() {
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    final ActionBar actionBar = getSupportActionBar();
    View viewActionBar = getLayoutInflater().inflate(R.layout.actionbar_default, null);
    ActionBar.LayoutParams params = new ActionBar.LayoutParams(
        ActionBar.LayoutParams.WRAP_CONTENT,
        ActionBar.LayoutParams.MATCH_PARENT,
        Gravity.CENTER);
    TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.actionbar_textview);
    textviewTitle.setText(getActionBarTitle());
    Typeface robotoMedium = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");
    textviewTitle.setTypeface(robotoMedium);
    actionBar.setCustomView(viewActionBar, params);
    actionBar.setDisplayShowCustomEnabled(true);
    actionBar.setDisplayShowTitleEnabled(false);
    actionBar.setDisplayHomeAsUpEnabled(true);
    actionBar.setHomeButtonEnabled(false);

    getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_chevron_left_white));


    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        finish();
        startActivity(LoginActivity.makeIntent(getApplicationContext()));
      }
    });
  }

  private void setupDoctorSignUpEvents() {
    mFirstName.setOnEditorActionListener(new EditorActionListener(mLastName));
    mLastName.setOnEditorActionListener(new EditorActionListener(mEmail));
    mEmail.setOnEditorActionListener(new EditorActionListener(mPhoneNumber));
    mPhoneNumber.setOnEditorActionListener(new EditorActionListener(mPassword));
    mPassword.setOnEditorActionListener(new EditorActionListener(mRetype));

    EditorActionListener editorAction = new EditorActionListener();
    editorAction.setListener(new ButtonKeyListener() {
      @Override
      public void onDoneClicked() {
        mRegisterPresenter.onRegisterClick(screenType);
      }
    });
    mRetype.setOnEditorActionListener(editorAction);
  }


  private void setupPacientSignUpEvents() {
    mFirstName.setOnEditorActionListener(new EditorActionListener(mLastName));
    mLastName.setOnEditorActionListener(new EditorActionListener(mEmail));
    mEmail.setOnEditorActionListener(new EditorActionListener(mPassword));
    mPassword.setOnEditorActionListener(new EditorActionListener(mRetype));

    EditorActionListener editorAction = new EditorActionListener();
    editorAction.setListener(new ButtonKeyListener() {
      @Override
      public void onDoneClicked() {
        mRegisterPresenter.onRegisterClick(screenType);
      }
    });
    mRetype.setOnEditorActionListener(editorAction);
  }

  public static Intent makeIntent(Context context, ScreenType screenType) {
    Intent intent = new Intent(context, RegisterActivity.class);
    String type = screenType.name();
    intent.putExtra(IntentKeys.REGISTER_SCREEN_TYPE, type);
    return intent;
  }


  @OnClick(R.id.register_button)
  void onSignUpClick() {
    mRegisterPresenter.onRegisterClick(screenType);
  }

  @Override
  protected int getLayout() {
    return R.layout.activity_register;
  }

  @Override
  public void setUpComponent(IAppComponent appComponent) {
    DaggerIRegisterComponent.builder()
        .iAppComponent(appComponent)
        .registerModule(new RegisterModule(this))
        .build()
        .inject(this);
  }

  @Nullable
  @Override
  protected BasePresenter getPresenter() {
    return null;
  }

  @Override
  protected String getActionBarTitle() {
    return getResources().getString(R.string.register_screen);
  }

  @Override
  public void forceFinish() {
    this.finish();
  }

  @Override
  public String getFirstName() {
    return mFirstName.getText().toString();
  }

  @Override
  public String getLastName() {
    return mLastName.getText().toString();
  }

  @Override
  public String getEmail() {
    return mEmail.getText().toString();
  }

  @Override
  public String getPhoneNumber() {
    return mPhoneNumber.getText().toString();
  }

  @Override
  public String getSpecialization() {
    return mSpecializationSpinner.getSelectedItem().toString();
  }

  @Override
  public String getPassword() {
    return mPassword.getText().toString();
  }

  @Override
  public String getRetype() {
    return mRetype.getText().toString();
  }

  @Override
  public void displayRegisterSuccessDialog() {
    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setMessage(getResources().getString(R.string.register_success_dialog_message));
    builder.setCancelable(false);
    builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
      @Override
      public void onCancel(DialogInterface dialogInterface) {
        dialogInterface.dismiss();
      }
    });
    builder.setNeutralButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();

      }
    });
    builder.show();
  }

  @Override
  public void showError(String error) {
    ToastUtils.showError(null != error ? error : "Error", this);
  }

  @Override
  public void startNextActivity() {
    finish();
    startActivity(LoginActivity.makeIntent(getApplicationContext()));
  }
}
