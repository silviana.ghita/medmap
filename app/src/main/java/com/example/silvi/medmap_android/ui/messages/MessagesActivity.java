package com.example.silvi.medmap_android.ui.messages;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.example.medmap_commons.model.ChatRoom;
import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.dagger.components.DaggerIMessagesComponent;
import com.example.silvi.medmap_android.dagger.components.IAppComponent;
import com.example.silvi.medmap_android.dagger.modules.MessagesModule;
import com.example.silvi.medmap_android.ui.base.BaseActivity;
import com.example.silvi.medmap_android.ui.base.BasePresenter;
import com.example.silvi.medmap_android.ui.base.FinishActivities;
import com.example.silvi.medmap_android.ui.base.FinishCallback;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;

/**
 * Created by silvi on 01.05.2017.
 */

public class MessagesActivity extends BaseActivity implements MessagesContract.View, FinishCallback {

  private final int GET_MESSAGES_UPDATE = 1;

  @Bind(R.id.conversations)
  RecyclerView mConversations;
  @Bind(R.id.no_conversations_text)
  TextView mNoConversationsText;

  @Inject
  MessagesPresenter mPresenter;

  private List<ChatRoom> mChatRooms;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    mChatRooms = new ArrayList<>();
    mPresenter.getChatRooms();

    FinishActivities.addActivity(this);
    setupToolbarDisplay();
  }

  private void setupToolbarDisplay() {
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    final ActionBar actionBar = getSupportActionBar();
    View viewActionBar = getLayoutInflater().inflate(R.layout.actionbar_default, null);
    ActionBar.LayoutParams params = new ActionBar.LayoutParams(
        ActionBar.LayoutParams.WRAP_CONTENT,
        ActionBar.LayoutParams.MATCH_PARENT,
        Gravity.CENTER);
    TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.actionbar_textview);
    textviewTitle.setText(getActionBarTitle());
    Typeface robotoMedium = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");
    textviewTitle.setTypeface(robotoMedium);
    actionBar.setCustomView(viewActionBar, params);
    actionBar.setDisplayShowCustomEnabled(true);
    actionBar.setDisplayShowTitleEnabled(false);
    actionBar.setDisplayHomeAsUpEnabled(true);
    actionBar.setHomeButtonEnabled(false);

    getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_chevron_left_white));

  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == GET_MESSAGES_UPDATE) {
      mConversations.removeAllViews();
      mPresenter.getChatRooms();
    }
  }

  @Override
  protected int getLayout() {
    return R.layout.activity_messages;
  }

  @Override
  public void setUpComponent(IAppComponent appComponent) {
    DaggerIMessagesComponent.builder()
        .iAppComponent(appComponent)
        .messagesModule(new MessagesModule(this))
        .build()
        .inject(this);
  }

  @Nullable
  @Override
  protected BasePresenter getPresenter() {
    return null;
  }

  @Override
  protected String getActionBarTitle() {
    return getResources().getString(R.string.messages);
  }

  @Override
  public void forceFinish() {
    this.finish();
  }

  @Override
  public void setChatRooms(List<ChatRoom> chatRooms) {
    this.mChatRooms = chatRooms;

    if (chatRooms.size() > 0) {
      LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
      mConversations.setLayoutManager(linearLayoutManager);
      ConversationsAdapter adapter = new ConversationsAdapter(this, chatRooms);
      mConversations.setAdapter(adapter);
      mConversations.setVisibility(View.VISIBLE);
      mNoConversationsText.setVisibility(View.GONE);
    } else {
      mConversations.setVisibility(View.GONE);
      mNoConversationsText.setVisibility(View.VISIBLE);
    }

  }


  public static Intent makeIntent(Context context) {
    Intent intent = new Intent(context, MessagesActivity.class);

    return intent;
  }
}
