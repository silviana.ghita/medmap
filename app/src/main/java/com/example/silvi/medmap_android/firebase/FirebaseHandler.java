package com.example.silvi.medmap_android.firebase;

import android.content.Intent;
import android.util.Log;

import com.example.silvi.medmap_android.ui.base.MedMapApplication;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;

/**
 * Created by silvi on 20.04.2017.
 */

public class FirebaseHandler {

  private FirebaseHandler() {
  }

  public static void startFCMService() {
    Intent intent = new Intent(MedMapApplication.getInstance(), RegistrationIntentService.class);
    MedMapApplication.getInstance().startService(intent);
  }

  public static void deleteInstance() {
    new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          FirebaseInstanceId.getInstance().deleteInstanceId();
          Log.d("Firebase", "Instance Deleted");
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }).start();
  }

}
