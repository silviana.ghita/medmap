package com.example.silvi.medmap_android.domain.register;

import com.example.medmap_commons.model.LoggedInUser;
import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.model.User;

/**
 * Created by silvi on 07.03.2017.
 */

public interface RegisterUserInteractor {

  interface Callback {
    void onRegisterSuccess(LoggedInUser loggedInUser);

    void onRegisterError(RestError restError);
  }

  void execute(Callback callback, User user);
}