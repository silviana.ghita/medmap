package com.example.silvi.medmap_android.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by silvi on 19.03.2017.
 */
public class SlidingTabStrip extends LinearLayout {

  SlidingTabStrip(Context context) {
    this(context, null);
  }

  SlidingTabStrip(Context context, AttributeSet attrs) {
    super(context, attrs);
    setWillNotDraw(false);
  }

  public void onViewPagerPageChanged(int position, float positionOffset) {
    invalidate();
  }
}