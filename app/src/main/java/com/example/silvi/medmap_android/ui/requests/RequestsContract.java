package com.example.silvi.medmap_android.ui.requests;

import com.example.medmap_commons.model.Request;

import java.util.List;

/**
 * Created by silvi on 25.03.2017.
 */

public interface RequestsContract {

  interface View {

    void setRequests(List<Request> requests);

    void onAcceptButtonClick(Request request);

    void onDeclineButtonClick(Request request);

    void showToast(String message);

    void onRemoveRequestClicked(Request request);
  }

  interface Presenter {

    void getRequests();

    void acceptRequest(Request request);

    void declineRequest(Request request);

    void deleteRequest(Request request);
  }
}
