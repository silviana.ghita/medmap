package com.example.silvi.medmap_android.ui.cabinet.add;

import com.example.medmap_commons.model.Image;
import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.util.List;

/**
 * Created by silvi on 14.03.2017.
 */

public interface AddCabinetContract {

  interface View {

    String getCabinetName();

    String getDescription();

    String getSpecialization();

    String getCategory();

    LatLng getLatLng();

    void showToast(String text);

    void closeActivity();

    void deleteCabinetPicture(String imagePath, Integer position, Image image);

    List<File> getImagesPath();
  }

  interface Presenter {

    void onConfirmButtonClicked();

  }
}
