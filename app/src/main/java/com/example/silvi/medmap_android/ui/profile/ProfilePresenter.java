package com.example.silvi.medmap_android.ui.profile;

import com.example.medmap_commons.model.Doctor;
import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.model.Role;
import com.example.medmap_commons.model.User;
import com.example.medmap_commons.model.UserSession;
import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.domain.images.upload.UploadPictureInteractor;
import com.example.silvi.medmap_android.domain.profile.GetProfileDetailsInteractor;
import com.example.silvi.medmap_android.domain.profile.SaveProfileDetailsInteractor;
import com.example.silvi.medmap_android.ui.base.MedMapApplication;
import com.example.silvi.medmap_android.util.EmailFormatValidator;

import javax.inject.Inject;

/**
 * Created by silvi on 12.03.2017.
 */

public class ProfilePresenter implements ProfileContract.Presenter, GetProfileDetailsInteractor.Callback, SaveProfileDetailsInteractor.Callback, UploadPictureInteractor.Callback {

  private ProfileContract.View mView;
  private GetProfileDetailsInteractor mGetProfileDetailsInteractor;
  private SaveProfileDetailsInteractor mSaveProfileDetailsInteractor;
  private UploadPictureInteractor mUploadPictureInteractor;

  @Inject
  public ProfilePresenter(ProfileContract.View view, GetProfileDetailsInteractor getProfileDetailsInteractor, SaveProfileDetailsInteractor saveProfileDetailsInteractor, UploadPictureInteractor uploadIdDocumentInteractor) {
    this.mView = view;
    this.mGetProfileDetailsInteractor = getProfileDetailsInteractor;
    this.mSaveProfileDetailsInteractor = saveProfileDetailsInteractor;
    this.mUploadPictureInteractor = uploadIdDocumentInteractor;
  }


  @Override
  public void getProfileDetails(Long userId, Role userRole) {
    mGetProfileDetailsInteractor.execute(this, userId, userRole);
  }

  @Override
  public void onSaveProfileDetailsClicked() {
    if (isFormValid()) {
      User user = new User();
      Doctor doctor = new Doctor();
      if (UserSession.getUserRole().name().equals("ROLE_USER")) {
        user.setLastName(mView.getLastName());
        user.setFirstName(mView.getFirstName());
        user.setEmail(mView.getEmail());
        user.setPhoneNumber(mView.getPhoneNumber());
        user.setUserId(UserSession.getUserId());
        user.setBirthDate(mView.getBirthDate());
      } else {
        doctor.setLastName(mView.getLastName());
        doctor.setFirstName(mView.getFirstName());
        doctor.setEmail(mView.getEmail());
        doctor.setPhoneNumber(mView.getPhoneNumber());
        doctor.setSpecialization(mView.getSpecialization());
        doctor.setStudies(mView.getStudies());
        doctor.setDescription(mView.getDescription());
        doctor.setBirthDate(mView.getBirthDate());
        doctor.setExperience(mView.getExperience());
      }
      mSaveProfileDetailsInteractor.execute(this, user, doctor);
    }
  }

  @Override
  public void onSavePictureClicked() {
    mUploadPictureInteractor.execute(this, mView.getImagePath());
  }

  @Override
  public void onGetUserProfileDetailsSuccess(User user) {
    mView.initUserFields(user);
  }

  @Override
  public void onGetDoctorProfileDetailsSuccess(Doctor doctor) {
    mView.initDoctorFields(doctor);
  }

  @Override
  public void onGetProfileDetailsError(RestError restError) {

  }

  @Override
  public void onUserSaveProfileDetailsSuccess(User user) {
    mView.showToast(MedMapApplication.getInstance().getString(R.string.profile_updated_successfully));
  }

  @Override
  public void onDoctorSaveProfileDetailsSuccess(Doctor doctor) {
    mView.showToast(MedMapApplication.getInstance().getString(R.string.profile_updated_successfully));
  }

  @Override
  public void onSaveProfileDetailsError(RestError restError) {
    // mView.showToast(MedMapApplication.getInstance().getString(R.string.error_during_update));
  }

  public boolean isFormValid() {

    EmailFormatValidator emailValidator = new EmailFormatValidator();

    if (mView.getEmail().isEmpty() || mView.getEmail() == null) {
      mView.showToast(MedMapApplication.getInstance().getString(R.string.email_null));
      return false;
    } else if (mView.getFirstName() == null || mView.getFirstName().isEmpty()) {
      mView.showToast(MedMapApplication.getInstance().getString(R.string.first_name_null));
      return false;
    } else if (mView.getLastName() == null || mView.getLastName().isEmpty()) {
      mView.showToast(MedMapApplication.getInstance().getString(R.string.last_name_null));
      return false;
    } else if (!emailValidator.validate(mView.getEmail())) {
      mView.showToast(MedMapApplication.getInstance().getString(R.string.incorrect_email_format));
      return false;
    }

    if (UserSession.getUserRole().name().equals("ROLE_DOCTOR")) {
      if (mView.getSpecialization() == null || mView.getSpecialization().isEmpty()) {
        mView.showToast(MedMapApplication.getInstance().getString(R.string.specialization_null));
        return false;
      }
    }
    return true;
  }

  @Override
  public void onPictureUploadSuccess() {

  }

  @Override
  public void onPictureUploadFailed(RestError restError) {

  }
}
