package com.example.silvi.medmap_android.domain.appointment.delete;

import com.example.medmap_commons.model.Appointment;
import com.example.medmap_commons.model.RestError;

/**
 * Created by silvi on 12.04.2017.
 */

public interface DeleteAppointmentInteractor {

  interface Callback {

    void onDeleteAppointmentSuccess();

    void onDeleteAppointmentError(RestError restError);

  }

  void execute(Callback callback, Appointment appointment);
}
