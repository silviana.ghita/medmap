package com.example.silvi.medmap_android.util;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import com.example.silvi.medmap_android.preferences.SharedPreferencesHandler;
import com.example.silvi.medmap_android.ui.base.MedMapApplication;

/**
 * Created by silvi on 21.04.2017.
 */

public class LifeCycleHandler implements Application.ActivityLifecycleCallbacks {

  private int started = 0;
  private int resumed = 0;
  private int stopped = 0;
  private int paused = 0;

  @Override
  public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

  }

  @Override
  public void onActivityStarted(Activity activity) {
    started++;
    isApplicationVisible();
  }

  @Override
  public void onActivityResumed(Activity activity) {
    resumed++;
    isApplicationInForeground();
  }

  @Override
  public void onActivityPaused(Activity activity) {
    paused++;
    isApplicationInForeground();
  }

  @Override
  public void onActivityStopped(Activity activity) {
    stopped++;
    isApplicationVisible();
  }

  @Override
  public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

  }

  @Override
  public void onActivityDestroyed(Activity activity) {

  }

  private void isApplicationVisible() {
    SharedPreferencesHandler.setIsApplicatonVisible(MedMapApplication.getInstance().getApplicationContext(), started > stopped);
  }

  private void isApplicationInForeground() {
    SharedPreferencesHandler.setIsApplicationInForeground(MedMapApplication.getInstance().getApplicationContext(), resumed > paused);
  }
}
