package com.example.silvi.medmap_android.dagger.components;

import com.example.medmap_api.service.AppointmentService;
import com.example.silvi.medmap_android.dagger.ActivityScope;
import com.example.silvi.medmap_android.dagger.modules.AppointmentsModule;
import com.example.silvi.medmap_android.dagger.modules.NavigationModule;
import com.example.silvi.medmap_android.domain.appointment.delete.DeleteAppointmentInteractor;
import com.example.silvi.medmap_android.domain.appointment.get.GetAppointmentsInteractor;
import com.example.silvi.medmap_android.domain.appointment.update.UpdateAppointmentStateInteractor;
import com.example.silvi.medmap_android.domain.chat.chatroom.OpenChatRoomInteractor;
import com.example.silvi.medmap_android.ui.appointments.AppointmentsActivity;
import com.example.silvi.medmap_android.ui.appointments.AppointmentsContract;
import com.example.silvi.medmap_android.ui.appointments.AppointmentsPresenter;

import dagger.Component;

/**
 * Created by silvi on 12.04.2017.
 */

@ActivityScope
@Component(
    dependencies = IAppComponent.class,
    modules = {AppointmentsModule.class,
        NavigationModule.class
    }
)
public interface IAppointmentsComponent extends IAppComponent {

  void inject(AppointmentsActivity apointmentsActivity);

  AppointmentsContract.View getView();

  AppointmentService getAppointmentService();

  GetAppointmentsInteractor getAppointmentsInteractor();

  UpdateAppointmentStateInteractor getUpdateAppointmentStateInteractor();

  DeleteAppointmentInteractor getDeleteAppointmentInteractor();

  OpenChatRoomInteractor getOpenChatRoomInteractor();

  AppointmentsPresenter getAppointmentsPresenter();
}
