package com.example.silvi.medmap_android.ui.base;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by silvi on 22.02.2017.
 */

public class FinishActivities {
  private static ArrayList<FinishCallback> finishCallbacks = new ArrayList<>();

  public static void addActivity(FinishCallback listener) {
    finishCallbacks.add(listener);
  }

  public static void removeActivity(FinishCallback listener) {
    finishCallbacks.remove(listener);
  }

  public static void clearAllActivities() {
    for (FinishCallback callback : finishCallbacks) {
      if (callback != null) {
        callback.forceFinish();
      }
    }
    finishCallbacks.clear();
  }

  public static void clearAllBookingActivities() {
    Iterator<FinishCallback> i = finishCallbacks.iterator();
    while (i.hasNext()) {
      FinishCallback callback = i.next();
      if (callback != null) {
        callback.forceFinish();
      }
      // Do something
      i.remove();
    }
  }

  public static void clearAllSignInActivities() {
    Iterator<FinishCallback> i = finishCallbacks.iterator();
    while (i.hasNext()) {
      FinishCallback callback = i.next();
      if (callback != null) {
        callback.forceFinish();
      }
      // Do something
      i.remove();
    }
  }

}
