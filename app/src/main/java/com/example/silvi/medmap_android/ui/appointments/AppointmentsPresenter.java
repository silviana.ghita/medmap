package com.example.silvi.medmap_android.ui.appointments;

import com.example.medmap_commons.model.Appointment;
import com.example.medmap_commons.model.ChatRoom;
import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.model.State;
import com.example.silvi.medmap_android.domain.appointment.delete.DeleteAppointmentInteractor;
import com.example.silvi.medmap_android.domain.appointment.get.GetAppointmentsInteractor;
import com.example.silvi.medmap_android.domain.appointment.update.UpdateAppointmentStateInteractor;
import com.example.silvi.medmap_android.domain.chat.chatroom.OpenChatRoomInteractor;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by silvi on 11.04.2017.
 */

public class AppointmentsPresenter implements AppointmentsContract.Presenter, GetAppointmentsInteractor.Callback, UpdateAppointmentStateInteractor.Callback, DeleteAppointmentInteractor.Callback
    , OpenChatRoomInteractor.Callback {

  AppointmentsContract.View mView;
  GetAppointmentsInteractor mGetAppointmentsInteractor;
  UpdateAppointmentStateInteractor mUpdateAppointmentStateInteractor;
  DeleteAppointmentInteractor mDeleteAppointmentInteractor;
  OpenChatRoomInteractor mOpenChatRoomInteractor;

  @Inject
  public AppointmentsPresenter(AppointmentsContract.View view, GetAppointmentsInteractor getAppointmentsInteractor,
                               UpdateAppointmentStateInteractor updateAppointmentStateInteractor,
                               DeleteAppointmentInteractor deleteAppointmentInteractor,
                               OpenChatRoomInteractor openChatRoomInteractor) {
    this.mView = view;
    this.mGetAppointmentsInteractor = getAppointmentsInteractor;
    this.mUpdateAppointmentStateInteractor = updateAppointmentStateInteractor;
    this.mDeleteAppointmentInteractor = deleteAppointmentInteractor;
    this.mOpenChatRoomInteractor = openChatRoomInteractor;
  }

  @Override
  public void getAppointments() {
    mGetAppointmentsInteractor.execute(this);
  }

  @Override
  public void acceptAppointment(Appointment appointment) {
    appointment.setState(State.ACCEPTED.toString());
    mUpdateAppointmentStateInteractor.execute(this, appointment);
  }

  @Override
  public void declineAppointment(Appointment appointment) {
    appointment.setState(State.DECLINED.toString());
    mUpdateAppointmentStateInteractor.execute(this, appointment);
  }

  @Override
  public void deleteAppointment(Appointment appointment) {
    mDeleteAppointmentInteractor.execute(this, appointment);
  }

  @Override
  public void openChatRoom(long doctorId) {
    mOpenChatRoomInteractor.execute(this, doctorId);
  }

  @Override
  public void onGetAppointmentsSuccess(List<Appointment> appointments) {
      mView.setAppointments(appointments);
  }

  @Override
  public void onGetAppointmentsError(RestError restError) {

  }

  @Override
  public void onDeleteAppointmentSuccess() {

  }

  @Override
  public void onDeleteAppointmentError(RestError restError) {

  }

  @Override
  public void onUpdateAppointmentStateSuccess() {

  }

  @Override
  public void onUpdateAppointmentStateError(RestError restError) {

  }

  @Override
  public void onOpenChatRoomSuccess(ChatRoom chatRoom) {
    mView.startChatRoomActivity(chatRoom);
  }

  @Override
  public void onOpenChatRoomError(RestError restError) {

  }
}
