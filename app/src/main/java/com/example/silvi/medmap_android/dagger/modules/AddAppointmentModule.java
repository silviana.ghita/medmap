package com.example.silvi.medmap_android.dagger.modules;

import com.example.medmap_api.service.AppointmentService;
import com.example.silvi.medmap_android.domain.appointment.add.AddAppointmentInteractor;
import com.example.silvi.medmap_android.domain.appointment.add.AddAppointmentInteractorImpl;
import com.example.silvi.medmap_android.domain.appointment.hours.GetAvailableHoursForDateInteractor;
import com.example.silvi.medmap_android.domain.appointment.hours.GetAvailableHoursForDateInteractorImpl;
import com.example.silvi.medmap_android.domain.appointment.types.GetAppointmentTypesInteractor;
import com.example.silvi.medmap_android.domain.appointment.types.GetAppointmentTypesInteractorImpl;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.MainThread;
import com.example.silvi.medmap_android.ui.appointments.add.AddAppointmentContract;
import com.example.silvi.medmap_android.ui.appointments.add.AddAppointmentPresenter;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;

/**
 * Created by silvi on 07.04.2017.
 */

@Module
public class AddAppointmentModule {

  private AddAppointmentContract.View view;

  public AddAppointmentModule(AddAppointmentContract.View view) {
    this.view = view;
  }

  @Provides
  public AddAppointmentContract.View getView() {
    return view;
  }

  @Provides
  public AppointmentService proviceAppointmentService(RestAdapter restAdapter) {
    return restAdapter.create(AppointmentService.class);
  }

  @Provides
  public GetAvailableHoursForDateInteractor provideGetAvailableHoursForDateInteractor(Executor executor,
                                                                                      MainThread mainThread,
                                                                                      AppointmentService appointmentService) {
    return new GetAvailableHoursForDateInteractorImpl(executor, mainThread, appointmentService);
  }


  @Provides
  public GetAppointmentTypesInteractor provideGetAppointmentTypesInteractor(Executor executor,
                                                                            MainThread mainThread,
                                                                            AppointmentService appointmentService) {
    return new GetAppointmentTypesInteractorImpl(executor, mainThread, appointmentService);
  }

  @Provides
  public AddAppointmentInteractor provideAddAppointmentInteractor(Executor executor,
                                                                  MainThread mainThread,
                                                                  AppointmentService appointmentService) {
    return new AddAppointmentInteractorImpl(executor, mainThread, appointmentService);
  }


  @Provides
  public AddAppointmentPresenter providePresenter(AddAppointmentContract.View view, GetAvailableHoursForDateInteractor interactor,
                                                  GetAppointmentTypesInteractor getAppointmentTypesInteractor, AddAppointmentInteractor addAppointmentInteractor) {
    return new AddAppointmentPresenter(view, interactor, getAppointmentTypesInteractor, addAppointmentInteractor);
  }
}
