package com.example.silvi.medmap_android.domain.cabinet.update;

import com.example.medmap_commons.model.Cabinet;
import com.example.medmap_commons.model.RestError;

/**
 * Created by silvi on 17.03.2017.
 */

public interface UpdateCabinetInteractor {

  interface Callback {

    void onUpdateCabinetSuccess();

    void onUpdateCabinetError(RestError restError);
  }

  void execute(Callback callback, Cabinet cabinet);
}
