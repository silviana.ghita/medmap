package com.example.silvi.medmap_android.ui.base;

/**
 * Created by silvi on 22.02.2017.
 */

public abstract class BasePresenter {

  public abstract void onResume();
}
