package com.example.silvi.medmap_android.domain.logout;

import com.example.medmap_api.service.AuthService;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.Interactor;
import com.example.silvi.medmap_android.executor.MainThread;

import javax.inject.Inject;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by silvi on 11.03.2017.
 */

public class LogoutInteractorImpl implements LogoutInteractor, Interactor {

  private final Executor mExecutor;
  private final MainThread mMainThread;
  private final AuthService mAuthService;

  private Callback mCallback;

  @Inject
  public LogoutInteractorImpl(Executor executor, MainThread mainThread, AuthService authService) {
    this.mAuthService = authService;
    this.mExecutor = executor;
    this.mMainThread = mainThread;
  }

  @Override
  public void execute(Callback callback) {
    if (null == callback) {
      throw new IllegalArgumentException(
          "Callback can't be null, the client of this interactor needs to get the response " +
              "in the callback");
    }
    this.mCallback = callback;
    this.mExecutor.run(this);
  }

  @Override
  public void run() {
    mAuthService.logout(new retrofit.Callback<Object>() {
      @Override
      public void success(Object o, Response response) {
        notifyLogoutSuccess();
      }

      @Override
      public void failure(RetrofitError error) {
        notifyError(error);
      }
    });
  }

  private void notifyLogoutSuccess() {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onLogoutSuccess();
      }
    });
  }

  private void notifyError(final RetrofitError retrofitError) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onLogoutError(retrofitError);
      }
    });
  }
}
