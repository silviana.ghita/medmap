package com.example.silvi.medmap_android.dagger.components;

import com.example.medmap_api.service.ProfileService;
import com.example.silvi.medmap_android.dagger.ActivityScope;
import com.example.silvi.medmap_android.dagger.modules.FiltersModule;
import com.example.silvi.medmap_android.domain.filters.GetDoctorUsersInteractor;
import com.example.silvi.medmap_android.ui.filters.FiltersActivity;
import com.example.silvi.medmap_android.ui.filters.FiltersContract;
import com.example.silvi.medmap_android.ui.filters.FiltersPresenter;

import dagger.Component;

/**
 * Created by silvi on 26.04.2017.
 */

@ActivityScope
@Component(
    dependencies = IAppComponent.class,
    modules = {
        FiltersModule.class,
    }
)
public interface IFiltersComponent extends IAppComponent {

  void inject(FiltersActivity filtersActivity);

  FiltersContract.View getView();

  ProfileService getProfileService();

  GetDoctorUsersInteractor getDoctorUsersInteractor();

  FiltersPresenter getFiltersPresenter();
}
