package com.example.silvi.medmap_android.ui.profile;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.medmap_commons.model.Doctor;
import com.example.medmap_commons.model.Role;
import com.example.medmap_commons.model.User;
import com.example.medmap_commons.model.UserSession;
import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.adapters.NamedEntityAdapter;
import com.example.silvi.medmap_android.dagger.components.DaggerIProfileComponent;
import com.example.silvi.medmap_android.dagger.components.IAppComponent;
import com.example.silvi.medmap_android.dagger.modules.ProfileModule;
import com.example.silvi.medmap_android.ui.base.BaseActivity;
import com.example.silvi.medmap_android.ui.base.BasePresenter;
import com.example.silvi.medmap_android.ui.base.FinishActivities;
import com.example.silvi.medmap_android.ui.base.FinishCallback;
import com.example.silvi.medmap_android.util.PhotoRepository;
import com.example.silvi.medmap_android.util.Specializations;
import com.example.silvi.medmap_android.util.ToastUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by silvi on 12.03.2017.
 */

public class ProfileActivity extends BaseActivity implements ProfileContract.View, FinishCallback {

  private static final int REQUEST_PICK_IMAGE = 101;

  @Bind(R.id.first_name_input)
  EditText mFirstName;
  @Bind(R.id.last_name_input)
  EditText mLastName;
  @Bind(R.id.email_input)
  EditText mEmail;
  @Bind(R.id.phone_number_input)
  EditText mPhoneNumber;
  @Bind(R.id.specialization_spinner)
  Spinner mSpecializationSpinner;
  @Bind(R.id.specialization_layout)
  LinearLayout mSpecializationLayout;
  @Bind(R.id.studies_input)
  EditText mStudies;
  @Bind(R.id.experience_input)
  EditText mExperience;
  @Bind(R.id.description_input)
  EditText mDescription;
  @Bind(R.id.btnSave)
  Button saveButton;
  @Bind(R.id.image_user_profile)
  ImageView userImage;
  @Bind(R.id.birth_date)
  TextView mBirthDate;

  @Inject
  ProfilePresenter mProfilePresenter;
  private NamedEntityAdapter mSpecialzationsAdapter;

  private PhotoRepository mPhotoRepository;
  private File selectedImageFile;
  private DatePickerDialog datePickerDialog;


  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    if (UserSession.getUserRole().equals(Role.ROLE_USER)) {
      mSpecializationLayout.setVisibility(View.GONE);
      mStudies.setVisibility(View.GONE);
      mExperience.setVisibility(View.GONE);
      mDescription.setVisibility(View.GONE);
    } else {
      mSpecializationLayout.setVisibility(View.VISIBLE);
      mStudies.setVisibility(View.VISIBLE);
      mExperience.setVisibility(View.VISIBLE);
      mDescription.setVisibility(View.VISIBLE);
    }


    mProfilePresenter.getProfileDetails(UserSession.getUserId(), UserSession.getUserRole());


    setupToolbarDisplay();
    setKeyboardVisibility();
    initSpecializationSpinner();

    FinishActivities.addActivity(this);

    mPhotoRepository = new PhotoRepository(getApplicationContext());


    initDatePicker();
  }

  private void setKeyboardVisibility() {
    final View activityRootView = findViewById(R.id.parent);
    DisplayMetrics metrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(metrics);
    final int height = metrics.heightPixels;
    activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
      @Override
      public void onGlobalLayout() {
        int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight();
        if (heightDiff > (height / 4)) { // if more than 1/4 of the screen, its probably a keyboard...
          saveButton.setVisibility(View.INVISIBLE);
        } else {
          saveButton.setVisibility(View.VISIBLE);
        }
      }
    });
  }

  private void initSpecializationSpinner() {
    Specializations[] specializations = Specializations.values();
    List<String> specializationList = new ArrayList<>();
    for (int i = 0; i < specializations.length; i++)
      specializationList.add(specializations[i].toString());

    mSpecialzationsAdapter = new NamedEntityAdapter(this, R.layout.simple_spinner_item, specializationList);
    mSpecializationSpinner.setAdapter(mSpecialzationsAdapter);
  }

  private void setupToolbarDisplay() {
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    final ActionBar actionBar = getSupportActionBar();
    View viewActionBar = getLayoutInflater().inflate(R.layout.actionbar_default, null);
    ActionBar.LayoutParams params = new ActionBar.LayoutParams(
        ActionBar.LayoutParams.WRAP_CONTENT,
        ActionBar.LayoutParams.MATCH_PARENT,
        Gravity.CENTER);
    TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.actionbar_textview);
    textviewTitle.setText(getActionBarTitle());
    Typeface robotoMedium = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");
    textviewTitle.setTypeface(robotoMedium);
    actionBar.setCustomView(viewActionBar, params);
    actionBar.setDisplayShowCustomEnabled(true);
    actionBar.setDisplayShowTitleEnabled(false);
    actionBar.setDisplayHomeAsUpEnabled(true);

    getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_chevron_left_white));

  }

  private void initDatePicker() {
    final Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

      @Override
      public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, monthOfYear);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        clearCalendarTime(myCalendar);

        Calendar today = Calendar.getInstance();
        clearCalendarTime(today);

        if (!myCalendar.equals(today) && myCalendar.after(today)) {
          showError(getResources().getString(R.string.dates_in_future));
        } else {
          updateLabel(myCalendar);
        }
      }
    };

    datePickerDialog = new DatePickerDialog(ProfileActivity.this, date, myCalendar
        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
  }

  private void updateLabel(Calendar calendar) {

    String myFormat = "dd MMM yyyy";
    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());

    mBirthDate.setText(sdf.format(calendar.getTime()));
  }

  private Calendar clearCalendarTime(Calendar calendar) {
    calendar.clear(Calendar.HOUR);
    calendar.clear(Calendar.MINUTE);
    calendar.clear(Calendar.SECOND);
    calendar.clear(Calendar.MILLISECOND);

    return calendar;
  }

  public void showError(String error) {
    ToastUtils.showError(error, ProfileActivity.this);
  }

  @OnClick(R.id.image_user_profile)
  public void selectPhoto(View view) {
    startActivityForResult(createImagePickerIntent(), REQUEST_PICK_IMAGE);
  }

  @OnClick(R.id.birth_date)
  void onBirthDateClick() {
    if (datePickerDialog != null)
      datePickerDialog.show();
  }

  private Intent createImagePickerIntent() {
    Intent intent = new Intent(Intent.ACTION_PICK);
    intent.setType("image/*");
    return intent;
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == REQUEST_PICK_IMAGE && resultCode == RESULT_OK && data != null) {
      onImagePicked(data.getData());
      displayImage(data.getData());
    } else {
      super.onActivityResult(requestCode, resultCode, data);
    }
  }

  private void displayImage(Uri selectedImageUri) {
    Glide
        .with(this)
        .load(selectedImageUri)
        .centerCrop()
        .crossFade()
        .into(userImage);
  }


  private void onImagePicked(Uri imageUri) {
    String text = imageUri.toString() + "\n\n" + imageUri.getPath() + "\n\n File Path: ";

    String imagePath = mPhotoRepository.getImagePath(imageUri);

    File imageFile = new File(imagePath);
    if (imageFile.exists()) {
      text += imageFile.getAbsolutePath();
    } else {
      text += "This file doesn't exist!";
    }

    selectedImageFile = imageFile;
  }

  @OnClick(R.id.btnSave)
  void onSaveClick() {
    mProfilePresenter.onSavePictureClicked();
    mProfilePresenter.onSaveProfileDetailsClicked();
  }

  @Override
  protected int getLayout() {
    return R.layout.activity_profile;
  }

  @Override
  public void setUpComponent(IAppComponent appComponent) {
    DaggerIProfileComponent.builder()
        .iAppComponent(appComponent)
        .profileModule(new ProfileModule(this))
        .build()
        .inject(this);
  }

  @Nullable
  @Override
  protected BasePresenter getPresenter() {
    return null;
  }

  @Override
  protected String getActionBarTitle() {
    return getResources().getString(R.string.profile_details);
  }

  @Override
  public void forceFinish() {
    this.finish();
  }

  @Override
  public String getFirstName() {
    return mFirstName.getText().toString();
  }

  @Override
  public String getLastName() {
    return mLastName.getText().toString();
  }

  @Override
  public String getEmail() {
    return mEmail.getText().toString();
  }

  @Override
  public String getPhoneNumber() {
    return mPhoneNumber.getText().toString();
  }

  @Override
  public String getSpecialization() {
    return mSpecializationSpinner.getSelectedItem().toString();
  }

  @Override
  public String getStudies() {
    return mStudies.getText().toString();
  }

  @Override
  public String getExperience() {
    return mExperience.getText().toString();
  }

  @Override
  public String getDescription() {
    return mDescription.getText().toString();
  }

  @Override
  public File getImagePath() {
    return selectedImageFile;
  }

  @Override
  public String getBirthDate() {
    return mBirthDate.getText().toString();
  }


  @Override
  public void initUserFields(User user) {
    mFirstName.setText(user.getFirstName());
    mLastName.setText(user.getLastName());
    mPhoneNumber.setText(user.getPhoneNumber());
    mEmail.setText(user.getEmail());
    mBirthDate.setText(user.getBirthDate());


    if (user.getProfilePicture() != null) {
      String path = user.getProfilePicture().getPath();
      setUserImage(path);
    }

  }

  @Override
  public void initDoctorFields(Doctor doctor) {
    mFirstName.setText(doctor.getUser().getFirstName());
    mLastName.setText(doctor.getUser().getLastName());
    mPhoneNumber.setText(doctor.getUser().getPhoneNumber());
    mEmail.setText(doctor.getUser().getEmail());
    mStudies.setText(doctor.getStudies());
    mExperience.setText(doctor.getExperience());
    mDescription.setText(doctor.getDescription());
    mBirthDate.setText(doctor.getUser().getBirthDate());

    int pos = getSpecializationSpinnerSelection(mSpecialzationsAdapter.getItems(), doctor.getSpecialization());
    mSpecializationSpinner.setSelection(pos);

    if (doctor.getUser().getProfilePicture() != null) {
      String path = doctor.getUser().getProfilePicture().getPath();
      setUserImage(path);
    }

  }

  private void setUserImage(String path) {
    Glide
        .with(this)
        .load(path)
        .centerCrop()
        .crossFade()
        .into(userImage);

  }

  public static Intent makeIntent(Context context) {
    return new Intent(context, ProfileActivity.class);
  }

  private int getSpecializationSpinnerSelection(List<String> items, String entity) {
    if (entity == null)
      return 0;

    int i = 0;
    for (String item : items) {
      if (item != null &&
          item.equals(entity))
        return i;
      i++;
    }
    return 0;
  }

  @Override
  public void showToast(String text) {
    Toast.makeText(this, text, Toast.LENGTH_LONG).show();
  }
}
