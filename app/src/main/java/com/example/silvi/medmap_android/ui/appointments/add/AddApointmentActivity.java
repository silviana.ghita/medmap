package com.example.silvi.medmap_android.ui.appointments.add;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.medmap_commons.model.AppointmentType;
import com.example.medmap_commons.model.Cabinet;
import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.adapters.NamedEntityAdapter;
import com.example.silvi.medmap_android.commons.IntentKeys;
import com.example.silvi.medmap_android.dagger.components.DaggerIAddAppointmentComponent;
import com.example.silvi.medmap_android.dagger.components.IAppComponent;
import com.example.silvi.medmap_android.dagger.modules.AddAppointmentModule;
import com.example.silvi.medmap_android.ui.base.BaseActivity;
import com.example.silvi.medmap_android.ui.base.BasePresenter;
import com.example.silvi.medmap_android.ui.base.FinishActivities;
import com.example.silvi.medmap_android.ui.base.FinishCallback;
import com.example.silvi.medmap_android.util.ToastUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by silvi on 30.03.2017.
 */

public class AddApointmentActivity extends BaseActivity implements FinishCallback, AddAppointmentContract.View {

  @Bind(R.id.calendar_picker)
  RelativeLayout mCalendarPicker;
  @Bind(R.id.calendar_show_date)
  TextView mCalendarShowDateText;
  @Bind(R.id.start_time_picker)
  RelativeLayout mStartTimePicker;
  @Bind(R.id.start_time_spinner)
  Spinner mStartTimeSpinner;
  @Bind(R.id.appointment_type_picker)
  RelativeLayout mAppointmentTypePicker;
  @Bind(R.id.appointment_type_spinner)
  Spinner mAppointmentTypeSpinner;
  @Bind(R.id.comment_edit_text)
  EditText mCommentEditText;
  @Bind(R.id.price_input)
  TextView mPriceInput;
  @Bind(R.id.duration_input)
  TextView mDurationInput;
  @Bind(R.id.confirmButton)
  Button mConfirmButton;
  @Bind(R.id.bottom_margin_layout)
  View mBottomMarginLayout;
  @Bind(R.id.scrollView)
  ScrollView mScrollView;
  @Bind(R.id.bottom_group)
  LinearLayout mBottomGroup;
  @Bind(R.id.start_time_separator)
  View mStartTimeSeparator;

  @Bind(R.id.comment_section)
  LinearLayout mCommentSection;
  @Bind(R.id.section_new_pacient)
  LinearLayout mNewPacientSection;

  @Bind(R.id.first_name_input)
  EditText mFirstName;
  @Bind(R.id.last_name_input)
  EditText mLastName;
  @Bind(R.id.phone_number_input)
  EditText mPhoneNumber;

  @Inject
  AddAppointmentPresenter mPresenter;

  private DatePickerDialog datePickerDialog;
  private List<String> mAvailableHours;
  private NamedEntityAdapter mAvailableHoursAdapter;
  private NamedEntityAdapter mAppointmentTypesAdapter;
  private List<AppointmentType> mAppointmentTypes;
  private List<String> mAppointmentTypeNames;

  private Cabinet mCabinet;
  private AppointmentType selectedAppointmentType;
  private String appointmentDate;
  private String appointmentTime;

  private boolean mIsExternal = false;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    mCabinet = getIntent().getParcelableExtra(IntentKeys.CABINET);
    mIsExternal = getIntent().getBooleanExtra(IntentKeys.IS_EXTERNAL, false);

    if (!mIsExternal) {
      mCommentSection.setVisibility(View.VISIBLE);
      mNewPacientSection.setVisibility(View.GONE);
    } else {
      mCommentSection.setVisibility(View.GONE);
      mNewPacientSection.setVisibility(View.VISIBLE);
    }

    FinishActivities.addActivity(this);
    setupToolbarDisplay();
    setKeyboardVisibility();
    setListenersForCommentsEditText();
    initDatePicker();

    mPresenter.getAppointmentTypes();


  }

  private void initDatePicker() {
    final Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

      @Override
      public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, monthOfYear);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        clearCalendarTime(myCalendar);

        Calendar today = Calendar.getInstance();
        clearCalendarTime(today);

        if (!myCalendar.equals(today) && myCalendar.before(today)) {
          showError(getResources().getString(R.string.dates_in_past));
        } else if ((myCalendar.get(Calendar.YEAR) > (today.get(Calendar.YEAR) + 1)) || (
            (myCalendar.get(Calendar.YEAR) == (today.get(Calendar.YEAR) + 1) &&
                myCalendar.get(Calendar.MONTH) > today.get(Calendar.MONTH)))) {
          showError(getResources().getString(R.string.max_1_year_in_the_future));
        } else {
          updateLabel(myCalendar);
        }
      }
    };

    datePickerDialog = new DatePickerDialog(AddApointmentActivity.this, date, myCalendar
        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
  }

  private void updateLabel(Calendar calendar) {

    String myFormat = "dd MMM yyyy";
    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());

    mCalendarShowDateText.setText(sdf.format(calendar.getTime()));

    int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
    int month = calendar.get(Calendar.MONTH);
    int year = calendar.get(Calendar.YEAR);
    appointmentDate = (dayOfMonth < 10 ? "0" : "") + dayOfMonth + "-" +
        (month + 1 < 10 ? "0" : "") + (month + 1) + "-" + year;

    mPresenter.getAvailableHoursForDate(appointmentDate, selectedAppointmentType.getDuration(), mCabinet.getId());
  }


  private Calendar clearCalendarTime(Calendar calendar) {
    calendar.clear(Calendar.HOUR);
    calendar.clear(Calendar.MINUTE);
    calendar.clear(Calendar.SECOND);
    calendar.clear(Calendar.MILLISECOND);

    return calendar;
  }

  public void showError(String error) {
    ToastUtils.showError(error, AddApointmentActivity.this);
  }

  void setListenersForCommentsEditText() {
    mCommentEditText.setOnTouchListener(new View.OnTouchListener() {
      @Override
      public boolean onTouch(View v, MotionEvent event) {
        Point childOffset = new Point();
        mBottomMarginLayout.setVisibility(View.VISIBLE);
        getDeepChildOffset(mScrollView, mBottomMarginLayout.getParent(), mBottomMarginLayout, childOffset);
        mScrollView.smoothScrollTo(0, childOffset.y);
        return false;
      }
    });

    mCommentEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
      @Override
      public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        boolean handled = false;
        if (actionId == EditorInfo.IME_ACTION_DONE) {
          mBottomMarginLayout.setVisibility(View.GONE);
          InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
          imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
          handled = true;
        }
        return handled;
      }
    });
  }

  private void setKeyboardVisibility() {
    final View activityRootView = findViewById(R.id.parent);
    DisplayMetrics metrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(metrics);
    final int height = metrics.heightPixels;
    activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
      @Override
      public void onGlobalLayout() {
        int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight();
        if (heightDiff > (height / 4)) { // if more than 1/4 of the screen, its probably a keyboard...
          mBottomGroup.setVisibility(View.INVISIBLE);
        } else {
          mBottomGroup.setVisibility(View.VISIBLE);
        }
      }
    });
  }

  private void setupToolbarDisplay() {
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    final ActionBar actionBar = getSupportActionBar();
    View viewActionBar = getLayoutInflater().inflate(R.layout.actionbar_default, null);
    ActionBar.LayoutParams params = new ActionBar.LayoutParams(
        ActionBar.LayoutParams.WRAP_CONTENT,
        ActionBar.LayoutParams.MATCH_PARENT,
        Gravity.CENTER);
    TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.actionbar_textview);
    textviewTitle.setText(getActionBarTitle());
    Typeface robotoMedium = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");
    textviewTitle.setTypeface(robotoMedium);
    actionBar.setCustomView(viewActionBar, params);
    actionBar.setDisplayShowCustomEnabled(true);
    actionBar.setDisplayShowTitleEnabled(false);
    actionBar.setDisplayHomeAsUpEnabled(true);
    actionBar.setHomeButtonEnabled(false);

    getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_chevron_left_white));


    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        finish();
      }
    });
  }

  public static Intent makeIntent(Context context, Cabinet cabinet, boolean isExternal) {
    Intent intent = new Intent(context, AddApointmentActivity.class);
    intent.putExtra(IntentKeys.CABINET, cabinet);
    intent.putExtra(IntentKeys.IS_EXTERNAL, isExternal);
    return intent;
  }


  @OnClick(R.id.confirmButton)
  void onConfirmButtonClick() {
    mPresenter.addAppointment(mIsExternal);
  }

  @OnClick(R.id.calendar_picker)
  void onCalendarPickerClick() {
    if (datePickerDialog != null)
      datePickerDialog.show();
  }

  @OnClick(R.id.start_time_picker)
  void onStartTimePickerClick() {

  }

  @OnClick(R.id.appointment_type_picker)
  void onAppointmentTypePickerClick() {

  }


  @Override
  public void forceFinish() {
    this.finish();
  }


  @Override
  protected int getLayout() {
    return R.layout.activity_add_appointment;
  }

  @Override
  public void setUpComponent(IAppComponent appComponent) {
    DaggerIAddAppointmentComponent.builder()
        .iAppComponent(appComponent)
        .addAppointmentModule(new AddAppointmentModule(this))
        .build()
        .inject(this);
  }

  @Nullable
  @Override
  protected BasePresenter getPresenter() {
    return null;
  }


  @Override
  protected String getActionBarTitle() {
    return getResources().getString(R.string.add_appointment_page);
  }

  @Override
  public Cabinet getCabinet() {
    return mCabinet;
  }

  @Override
  public String getComment() {
    return mCommentEditText.getText().toString();
  }

  @Override
  public AppointmentType getSelectedAppointmentType() {
    return selectedAppointmentType;
  }

  @Override
  public String getDate() {
    return appointmentDate;
  }

  @Override
  public String getTime() {
    return appointmentTime;
  }

  @Override
  public String getUserFirstName() {
    return mFirstName.getText().toString();
  }

  @Override
  public String getUserLastName() {
    return mLastName.getText().toString();
  }


  @Override
  public String getUserPhoneNumber() {
    return mPhoneNumber.getText().toString();
  }

  @Override
  public void showToast(String text) {
    Toast.makeText(this, text, Toast.LENGTH_LONG).show();
  }

  @Override
  public void appointmentAddedSuccessfully() {
    showToast(getResources().getString(R.string.appointment_sent_to_doctor));
    finish();
  }


  private void getDeepChildOffset(final ViewGroup mainParent, final ViewParent parent, final View child, final Point accumulatedOffset) {
    ViewGroup parentGroup = (ViewGroup) parent;

    accumulatedOffset.x += child.getLeft();
    accumulatedOffset.y += child.getTop();
    if (parentGroup.equals(mainParent)) {
      return;
    }
    getDeepChildOffset(mainParent, parentGroup.getParent(), parentGroup, accumulatedOffset);
  }

  @Override
  public void setAvailableHours(List<Double> availableHours) {
    mAvailableHours = new ArrayList<>();
    String hourString;
    for (double hour : availableHours) {
      if ((int) hour == hour) {
        if ((int) hour < 10) {
          hourString = "0" + (int) hour + ":" + "00";
        } else {
          hourString = (int) hour + ":" + "00";
        }
      } else {
        if ((int) hour < 10) {
          hourString = "0" + (int) hour + ":" + "30";
        } else {
          hourString = (int) hour + ":" + "30";
        }
      }
      mAvailableHours.add(hourString);
    }

    initAvailableHoursSpinner();
  }

  private void initAvailableHoursSpinner() {
    mAvailableHoursAdapter = new NamedEntityAdapter(this, R.layout.simple_spinner_item_with_big_text, mAvailableHours);
    mStartTimeSpinner.setAdapter(mAvailableHoursAdapter);

    mStartTimePicker.setVisibility(View.VISIBLE);
    mStartTimeSeparator.setVisibility(View.VISIBLE);

    mStartTimeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        appointmentTime = (String) mStartTimeSpinner.getSelectedItem();
      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });
  }

  @Override
  public void setAppointmentTypes(List<AppointmentType> appointmentTypes) {
    mAppointmentTypes = new ArrayList<>();
    mAppointmentTypes = appointmentTypes;
    mAppointmentTypeNames = new ArrayList<>();
    for (AppointmentType appointmentType : mAppointmentTypes) {
      mAppointmentTypeNames.add(appointmentType.getType());
    }
    initAppointmentTypesSpinner();

  }

  private void initAppointmentTypesSpinner() {

    mAppointmentTypesAdapter = new NamedEntityAdapter(this, R.layout.simple_spinner_item_with_big_text, mAppointmentTypeNames);
    mAppointmentTypeSpinner.setAdapter(mAppointmentTypesAdapter);

    mAppointmentTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

      @Override
      public void onItemSelected(AdapterView<?> arg0, View arg1,
                                 int pos, long arg3) {
        String selectedAppointmentTypeName = (String) mAppointmentTypeSpinner.getSelectedItem();

        for (AppointmentType appointmentType : mAppointmentTypes)
          if (selectedAppointmentTypeName.equals(appointmentType.getType())) {
            selectedAppointmentType = appointmentType;
          }
        mPriceInput.setText(selectedAppointmentType.getPrice().toString() + " " + getResources().getString(R.string.ron));
        mDurationInput.setText(selectedAppointmentType.getDuration().toString() + " " + getResources().getString(R.string.hour));

        if (appointmentDate != null)
          mPresenter.getAvailableHoursForDate(appointmentDate, selectedAppointmentType.getDuration(), mCabinet.getId());

      }

      @Override
      public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub

      }
    });
  }
}
