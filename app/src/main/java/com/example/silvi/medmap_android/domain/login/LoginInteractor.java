package com.example.silvi.medmap_android.domain.login;

import com.example.medmap_commons.model.LoggedInUser;
import com.example.medmap_commons.model.RestError;

/**
 * Created by silvi on 08.03.2017.
 */

public interface LoginInteractor {

  interface Callback {
    void onLoginSuccess(LoggedInUser loggedInUser);

    void onLoginError(RestError restError);
  }

  void execute(Callback callback, String authorization);
}
