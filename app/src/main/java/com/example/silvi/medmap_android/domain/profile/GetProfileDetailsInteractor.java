package com.example.silvi.medmap_android.domain.profile;

import com.example.medmap_commons.model.Doctor;
import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.model.Role;
import com.example.medmap_commons.model.User;

/**
 * Created by silvi on 13.03.2017.
 */

public interface GetProfileDetailsInteractor {

  interface Callback {

    void onGetUserProfileDetailsSuccess(User user);

    void onGetDoctorProfileDetailsSuccess(Doctor doctor);

    void onGetProfileDetailsError(RestError restError);
  }

  void execute(Callback callback, Long userId, Role userRole);
}
