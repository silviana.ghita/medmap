package com.example.silvi.medmap_android.ui.filters;

import com.example.medmap_commons.model.User;

import java.util.List;

/**
 * Created by silvi on 26.04.2017.
 */

public interface FiltersContract {

  interface View {

    void setDoctorUsers(List<User> doctorUsers);

  }

  interface Presenter {

    void getDoctorUsers();

  }
}
