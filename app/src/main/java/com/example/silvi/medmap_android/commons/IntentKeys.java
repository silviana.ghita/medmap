package com.example.silvi.medmap_android.commons;

/**
 * Created by silvi on 22.02.2017.
 */

public class IntentKeys {

  private final static String NAMESPACE = "com.example";
  public final static String REGISTER_SCREEN_TYPE = NAMESPACE + ".extra.register_screen_type";
  public final static String LATLNG = NAMESPACE + ".extra.latlng";
  public final static String CABINET = NAMESPACE + ".extra.cabinet";
  public final static String DOCTOR = NAMESPACE + ".extra.doctor";
  public final static String THUMBNAILS_SELECTION = NAMESPACE + ".extra.thumbnails_selection";
  public final static String BUNDLE = NAMESPACE + ".extra.bundle";
  public final static String FILTERS = NAMESPACE + ".extra.filters";
  public final static String CHATROOM = NAMESPACE + ".extra.chatroom";
  public final static String IS_EXTERNAL = NAMESPACE + ".extra.is_external";
}
