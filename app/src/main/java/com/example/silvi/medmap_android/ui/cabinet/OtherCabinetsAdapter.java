package com.example.silvi.medmap_android.ui.cabinet;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.medmap_commons.model.Cabinet;
import com.example.silvi.medmap_android.R;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by silvi on 21.05.2017.
 */

public class OtherCabinetsAdapter extends RecyclerView.Adapter<OtherCabinetsAdapter.ViewHolder> {

  List<Cabinet> cabinets;
  CabinetActivity context;

  public OtherCabinetsAdapter(CabinetActivity context, List<Cabinet> cabinets) {
    this.cabinets = cabinets;
    this.context = context;
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater inflater = null;
    inflater = LayoutInflater.from(context);

    View cabinetsView = inflater.inflate(R.layout.item_other_cabinet, parent, false);

    return new ViewHolder(cabinetsView);
  }

  @Override
  public void onBindViewHolder(ViewHolder holder, int position) {

    final Cabinet cabinet = cabinets.get(position);

    ImageView coverPhoto = holder.coverPhoto;
    TextView cabinetCategory = holder.cabinetCategory;
    TextView cabinetSpecialization = holder.cabinetSpecialization;
    TextView cabinetName = holder.cabinetName;

    Glide.with(context)
        .load(cabinet.getImages().get(0).getPath())
        .asBitmap()
        .centerCrop()
        .into(coverPhoto);

    String category = cabinet.getCategory().replace("_", " ");
    String specialization = cabinet.getSpecialization().replace("_", " ");

    cabinetCategory.setText(StringUtils.capitalize(category.toLowerCase()));
    cabinetSpecialization.setText(StringUtils.capitalize(specialization.toLowerCase()));
    cabinetName.setText(cabinet.getName());

    holder.itemView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        context.finish();
        context.startActivity(CabinetActivity.makeIntent(context, cabinet));
      }
    });


  }

  @Override
  public int getItemCount() {
    return cabinets != null ? cabinets.size() : 0;
  }


  public static class ViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.cover_photo)
    ImageView coverPhoto;
    @Bind(R.id.cabinet_category)
    TextView cabinetCategory;
    @Bind(R.id.cabinet_specialization)
    TextView cabinetSpecialization;
    @Bind(R.id.cabinet_name)
    TextView cabinetName;

    public ViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
