package com.example.silvi.medmap_android.commons;

import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.ui.base.MedMapApplication;

/**
 * Created by silvi on 08.03.2017.
 */

public class AppStatistics {
  private static String appVersion = "";
  private static int appCode = 0;

  public static void init(String appV, int appC) {
    appVersion = appV;
    appCode = appC;
  }

  public static String getAppVersion() {
    return appVersion;
  }

  public static int getAppCode() {
    return appCode;
  }

  public static String getActorId() {
    return String.format(MedMapApplication.getInstance().getResources().getString(R.string.android_app_v), getAppVersion());
  }
}
