package com.example.silvi.medmap_android.ui.appointments.today;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.example.medmap_commons.model.Appointment;
import com.example.medmap_commons.model.User;
import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.ui.base.MedMapApplication;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by silvi on 17.04.2017.
 */

public class UpcomingDoctorAppointmentsAdapter extends RecyclerView.Adapter<UpcomingDoctorAppointmentsAdapter.ViewHolder> {

  Context context;
  List<Appointment> appointments;
  Dialog dialog;

  public UpcomingDoctorAppointmentsAdapter(Context context, List<Appointment> appointments) {
    this.appointments = appointments;
    this.context = context;
    dialog = new Dialog(context, R.style.CustomDialog);
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater inflater = null;
    inflater = LayoutInflater.from(context);

    View upcomingAppointmentsView = inflater.inflate(R.layout.line_item_received_appointment, parent, false);

    return new ViewHolder(upcomingAppointmentsView);
  }

  @Override
  public void onBindViewHolder(ViewHolder holder, int position) {
    final Appointment appointment = appointments.get(position);

    final ImageView pacientPicture = holder.pacientPicture;
    TextView pacientName = holder.pacientName;
    TextView cabinetName = holder.cabinetName;
    TextView acceptedOrDeclinedText = holder.acceptedOrDeclinedText;
    TextView appointmentStartDate = holder.appointmentStartDate;
    TextView appointmentStartTime = holder.appointmentStartTime;
    TextView appointmentTypeName = holder.appointmentTypeName;
    TextView appointmentDuration = holder.appointmentDuration;
    TextView appointmentPrice = holder.appointmentPrice;
    LinearLayout buttonsLayout = holder.buttonsLayout;
    TextView appointmentDelay = holder.appointmentDelay;
    LinearLayout delaySection = holder.delaySection;
    LinearLayout commentarySection = holder.commentarySection;
    TextView appointmentCommentary = holder.appointmentCommentary;
    TextView externalAppointment = holder.externalAppointment;


    Drawable userIcon = MedMapApplication.getInstance().getResources().getDrawable(R.drawable.user_icon);
    userIcon.setColorFilter(new LightingColorFilter(Color.LTGRAY, Color.LTGRAY));


    Glide.with(context)
        .load(appointment.getUser().getProfilePicture() != null ? appointment.getUser().getProfilePicture().getPath() : "")
        .asBitmap()
        .placeholder(userIcon)
        .centerCrop()
        .into(new BitmapImageViewTarget(pacientPicture) {
          @Override
          protected void setResource(Bitmap resource) {
            RoundedBitmapDrawable circularBitmapDrawable =
                RoundedBitmapDrawableFactory.create(context.getResources(), resource);
            circularBitmapDrawable.setCornerRadius(
                Math.max(resource.getWidth(), resource.getHeight()) / 2.0f);
            pacientPicture.setImageDrawable(circularBitmapDrawable);
          }
        });


    pacientName.setText(appointment.getUser().getLastName() + " " + appointment.getUser().getFirstName());
    cabinetName.setText(appointment.getCabinet().getName());
    appointmentStartDate.setText(appointment.getDate());
    appointmentStartTime.setText(appointment.getHour());
    appointmentTypeName.setText(appointment.getAppointmentType().getType());
    appointmentDuration.setText(appointment.getAppointmentType().getDuration().toString() + " " + MedMapApplication.getInstance().getResources().getString(R.string.hour));
    appointmentPrice.setText(appointment.getAppointmentType().getPrice().toString() + " " + MedMapApplication.getInstance().getResources().getString(R.string.ron));
    appointmentCommentary.setText(appointment.getComment());

    if (appointment.getDelay() != null) {
      delaySection.setVisibility(View.VISIBLE);
      appointmentDelay.setText(appointment.getDelay() + " " + MedMapApplication.getInstance().getResources().getString(R.string.hour));
    } else {
      delaySection.setVisibility(View.GONE);
    }

    if (appointment.isExternal()) {
      commentarySection.setVisibility(View.GONE);
      appointmentCommentary.setVisibility(View.GONE);
      externalAppointment.setVisibility(View.VISIBLE);
    } else {
      commentarySection.setVisibility(View.VISIBLE);
      appointmentCommentary.setVisibility(View.VISIBLE);
      externalAppointment.setVisibility(View.GONE);
    }

    buttonsLayout.setVisibility(View.GONE);
    acceptedOrDeclinedText.setVisibility(View.GONE);

    pacientPicture.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        openUserDetailsDialog(appointment.getUser(), appointment.isExternal());
      }
    });
  }

  private void openUserDetailsDialog(User user, boolean isExternal) {


    dialog.setContentView(R.layout.dialog_user_details);

    TextView userName = (TextView) dialog.findViewById(R.id.user_name);
    TextView userBirthDate = (TextView) dialog.findViewById(R.id.user_birth_date);
    TextView userEmail = (TextView) dialog.findViewById(R.id.user_email);
    TextView userPhoneNumber = (TextView) dialog.findViewById(R.id.user_phone_number);
    LinearLayout birthDateSection = (LinearLayout) dialog.findViewById(R.id.birth_date_section);
    LinearLayout emailSection = (LinearLayout) dialog.findViewById(R.id.email_section);

    userName.setText(user.getFirstName() + " " + user.getLastName());
    userBirthDate.setText(user.getBirthDate());
    userEmail.setText(user.getEmail());
    userPhoneNumber.setText(user.getPhoneNumber());

    if (!isExternal) {
      birthDateSection.setVisibility(View.VISIBLE);
      emailSection.setVisibility(View.VISIBLE);
    } else {
      birthDateSection.setVisibility(View.GONE);
      emailSection.setVisibility(View.GONE);
    }


    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    dialog.show();
  }

  @Override
  public int getItemCount() {
    return appointments.size();
  }

  public static class ViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.pacient_picture)
    ImageView pacientPicture;
    @Bind(R.id.pacient_name)
    TextView pacientName;
    @Bind(R.id.cabinet_name)
    TextView cabinetName;
    @Bind(R.id.appointment_start_date)
    TextView appointmentStartDate;
    @Bind(R.id.appointment_start_time)
    TextView appointmentStartTime;
    @Bind(R.id.appointment_type_name)
    TextView appointmentTypeName;
    @Bind(R.id.appointment_duration)
    TextView appointmentDuration;
    @Bind(R.id.appointment_price)
    TextView appointmentPrice;
    @Bind(R.id.accepted_or_declined_text)
    TextView acceptedOrDeclinedText;
    @Bind(R.id.buttons_layout)
    LinearLayout buttonsLayout;
    @Bind(R.id.appointment_delay)
    TextView appointmentDelay;
    @Bind(R.id.delay_section)
    LinearLayout delaySection;
    @Bind(R.id.commentary_section)
    LinearLayout commentarySection;
    @Bind(R.id.commentary)
    TextView appointmentCommentary;
    @Bind(R.id.external_appointment)
    TextView externalAppointment;


    public ViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
