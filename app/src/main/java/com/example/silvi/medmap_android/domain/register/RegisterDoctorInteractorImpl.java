package com.example.silvi.medmap_android.domain.register;

import com.example.medmap_api.service.AuthService;
import com.example.medmap_commons.model.Doctor;
import com.example.medmap_commons.model.LoggedInUser;
import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.util.PrintLog;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.Interactor;
import com.example.silvi.medmap_android.executor.MainThread;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by silvi on 07.03.2017.
 */

public class RegisterDoctorInteractorImpl implements Interactor, RegisterDoctorInteractor {

  private final Executor mExecutor;
  private final MainThread mMainThread;
  private final AuthService mAuthService;


  private Callback mCallback;
  private Doctor mDoctor;

  public RegisterDoctorInteractorImpl(Executor executor, MainThread mainThread, AuthService authService) {
    mExecutor = executor;
    mMainThread = mainThread;
    mAuthService = authService;
  }

  @Override
  public void execute(Callback callback, Doctor doctor) {
    if (null == callback) {
      throw new IllegalArgumentException(
          "Callback can't be null, the client of this interactor needs to get the response " +
              "in the callback");
    }
    this.mCallback = callback;
    this.mDoctor = doctor;
    this.mExecutor.run(this);
  }


  @Override
  public void run() {
    mAuthService.registerDoctor(mDoctor, new retrofit.Callback<LoggedInUser>() {

      @Override
      public void success(LoggedInUser loggedInUser, Response response) {
        notifyRegisterSuccess(loggedInUser);
      }

      @Override
      public void failure(RetrofitError error) {
        RestError restError = null;
        try {
          restError = (RestError) error.getBodyAs(RestError.class);
        } catch (Exception e) {
          PrintLog.printStackTrace(getClass().getSimpleName(), e);
        }
        notifyError(restError);
      }
    });

  }

  private void notifyRegisterSuccess(final LoggedInUser loggedInUser) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onRegisterSuccess(loggedInUser);
      }
    });
  }

  private void notifyError(final RestError restError) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onRegisterError(restError);
      }
    });
  }

}
