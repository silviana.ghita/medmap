package com.example.silvi.medmap_android.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by silvi on 24.03.2017.
 */

public class ListUtils {

  public static boolean isNullOrEmpty(List list) {
    return (null == list || list.isEmpty());
  }

  public static <T> List<List<T>> chopped(List<T> list, final int L) {
    List<List<T>> parts = new ArrayList<>();
    final int N = list.size();
    for (int i = 0; i < N; i += L) {
      parts.add(new ArrayList<>(list.subList(i, Math.min(N, i + L))));
    }
    return parts;
  }
}