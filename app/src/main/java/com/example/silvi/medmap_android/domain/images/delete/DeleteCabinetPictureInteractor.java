package com.example.silvi.medmap_android.domain.images.delete;

import com.example.medmap_commons.model.Image;
import com.example.medmap_commons.model.RestError;

import java.util.List;

/**
 * Created by silvi on 01.04.2017.
 */

public interface DeleteCabinetPictureInteractor {

  interface Callback {
    void onDeleteCabinetPictureSuccess();

    void onDeleteCabinetPictureError(RestError restError);
  }

  void execute(Callback callback, Long cabinetId, List<Image> imagesToDelete);
}
