package com.example.silvi.medmap_android.dagger.modules;

import com.example.medmap_api.service.ChatService;
import com.example.silvi.medmap_android.domain.chat.GetChatRoomsInteractor;
import com.example.silvi.medmap_android.domain.chat.GetChatRoomsInteractorImpl;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.MainThread;
import com.example.silvi.medmap_android.ui.messages.MessagesContract;
import com.example.silvi.medmap_android.ui.messages.MessagesPresenter;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;

/**
 * Created by silvi on 05.05.2017.
 */

@Module
public class MessagesModule {

  private MessagesContract.View view;

  public MessagesModule(MessagesContract.View view) {
    this.view = view;
  }

  @Provides
  MessagesContract.View getView() {
    return view;
  }

  @Provides
  public ChatService provideChatService(RestAdapter restAdapter) {
    return restAdapter.create(ChatService.class);
  }

  @Provides
  public GetChatRoomsInteractor provideGetChatRoomsInteractor(Executor executor,
                                                              MainThread mainThread,
                                                              ChatService chatService) {
    return new GetChatRoomsInteractorImpl(executor, mainThread, chatService);
  }

  @Provides
  public MessagesPresenter providePresenter(MessagesContract.View view, GetChatRoomsInteractor interactor) {
    return new MessagesPresenter(view, interactor);
  }
}
