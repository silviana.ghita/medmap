package com.example.silvi.medmap_android.domain.appointment.hours;

import com.example.medmap_api.service.AppointmentService;
import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.util.PrintLog;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.Interactor;
import com.example.silvi.medmap_android.executor.MainThread;

import java.util.List;

import javax.inject.Inject;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by silvi on 07.04.2017.
 */

public class GetAvailableHoursForDateInteractorImpl implements Interactor, GetAvailableHoursForDateInteractor {

  private final Executor mExecutor;
  private final MainThread mMainThread;
  private final AppointmentService mAppointmentService;

  private Callback mCallback;
  private String mDate;
  private Double mDuration;
  private long mCabinetId;

  @Inject
  public GetAvailableHoursForDateInteractorImpl(Executor executor, MainThread mainThread, AppointmentService appointmentService) {
    this.mAppointmentService = appointmentService;
    this.mExecutor = executor;
    this.mMainThread = mainThread;
  }

  @Override
  public void execute(Callback callback, String date, Double duration, long cabinetId) {
    if (null == callback) {
      throw new IllegalArgumentException(
          "Callback can't be null, the client of this interactor needs to get the response " +
              "in the callback");
    }
    this.mCallback = callback;
    this.mDate = date;
    this.mDuration = duration;
    this.mCabinetId = cabinetId;
    this.mExecutor.run(this);
  }

  @Override
  public void run() {
    mAppointmentService.getAvailableHoursForDate(mDate, mDuration, mCabinetId, new retrofit.Callback<List<Double>>() {
      @Override
      public void success(List<Double> availableHours, Response response) {
        notifySuccess(availableHours);
      }

      @Override
      public void failure(RetrofitError error) {
        RestError restError = null;
        try {
          restError = (RestError) error.getBodyAs(RestError.class);
        } catch (Exception e) {
          PrintLog.printStackTrace(getClass().getSimpleName(), e);
        }
        notifyError(restError);
      }
    });

  }

  private void notifySuccess(final List<Double> availableHours) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onGetAvailableHoursForDateSuccess(availableHours);
      }
    });
  }

  private void notifyError(final RestError restError) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onGetAvailableHoursForDateError(restError);
      }
    });
  }


}
