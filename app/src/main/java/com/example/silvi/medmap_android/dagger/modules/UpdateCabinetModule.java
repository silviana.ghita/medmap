package com.example.silvi.medmap_android.dagger.modules;

import com.example.medmap_api.service.CabinetService;
import com.example.medmap_api.service.ImageService;
import com.example.silvi.medmap_android.domain.cabinet.update.UpdateCabinetInteractor;
import com.example.silvi.medmap_android.domain.cabinet.update.UpdateCabinetInteractorImpl;
import com.example.silvi.medmap_android.domain.images.delete.DeleteCabinetPictureInteractor;
import com.example.silvi.medmap_android.domain.images.delete.DeleteCabinetPictureInteractorImpl;
import com.example.silvi.medmap_android.domain.images.upload.UploadPictureInteractor;
import com.example.silvi.medmap_android.domain.images.upload.UploadPictureInteractorImpl;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.MainThread;
import com.example.silvi.medmap_android.ui.cabinet.update.UpdateCabinetContract;
import com.example.silvi.medmap_android.ui.cabinet.update.UpdateCabinetPresenter;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;

/**
 * Created by silvi on 17.03.2017.
 */

@Module
public class UpdateCabinetModule {
  private UpdateCabinetContract.View view;

  public UpdateCabinetModule(UpdateCabinetContract.View view) {
    this.view = view;
  }

  @Provides
  public UpdateCabinetContract.View getView() {
    return view;
  }

  @Provides
  public CabinetService provideCabinetService(RestAdapter restAdapter) {
    return restAdapter.create(CabinetService.class);
  }

  @Provides
  public ImageService provideImageService(RestAdapter restAdapter) {
    return restAdapter.create(ImageService.class);
  }

  @Provides
  public UpdateCabinetInteractor provideUpdateCabinetInteractor(Executor executor,
                                                                MainThread mainThread,
                                                                CabinetService cabinetService) {
    return new UpdateCabinetInteractorImpl(executor, mainThread, cabinetService);
  }

  @Provides
  public UploadPictureInteractor provideUploadDocumentInteractor(Executor executor,
                                                                 MainThread mainThread,
                                                                 ImageService uploadService) {
    return new UploadPictureInteractorImpl(executor, mainThread, uploadService);
  }

  @Provides
  public DeleteCabinetPictureInteractor provideDeleteCabinetPictureInteractor(Executor executor,
                                                                              MainThread mainThread,
                                                                              ImageService uploadService) {
    return new DeleteCabinetPictureInteractorImpl(executor, mainThread, uploadService);
  }

  @Provides
  public UpdateCabinetPresenter providePresenter(UpdateCabinetContract.View view, UpdateCabinetInteractor interactor,
                                                 UploadPictureInteractor uploadDocumentInteractor, DeleteCabinetPictureInteractor deleteCabinetPictureInteractor) {
    return new UpdateCabinetPresenter(view, interactor, uploadDocumentInteractor, deleteCabinetPictureInteractor);
  }
}
