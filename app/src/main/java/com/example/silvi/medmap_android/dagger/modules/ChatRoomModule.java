package com.example.silvi.medmap_android.dagger.modules;

import com.example.medmap_api.service.ImageService;
import com.example.silvi.medmap_android.domain.images.chat.SavePictureToS3Interactor;
import com.example.silvi.medmap_android.domain.images.chat.SavePictureToS3InteractorImpl;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.MainThread;
import com.example.silvi.medmap_android.ui.messages.chatroom.ChatRoomActivity;
import com.example.silvi.medmap_android.ui.messages.chatroom.ChatRoomContract;
import com.example.silvi.medmap_android.ui.messages.chatroom.ChatRoomPresenter;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;

/**
 * Created by silvi on 10.05.2017.
 */

@Module
public class ChatRoomModule {

  private ChatRoomContract.View mView;

  public ChatRoomModule(ChatRoomActivity view) {
    this.mView = view;
  }

  @Provides
  public ChatRoomContract.View provideView() {
    return mView;
  }

  @Provides
  public ImageService provideImageService(RestAdapter restAdapter) {
    return restAdapter.create(ImageService.class);
  }

  @Provides
  public SavePictureToS3Interactor provideSavePictureToS3Interactor(Executor executor, MainThread mainThread, ImageService imageService) {
    return new SavePictureToS3InteractorImpl(executor, mainThread, imageService);
  }

  @Provides
  ChatRoomPresenter providePresenter(ChatRoomContract.View view, SavePictureToS3Interactor savePictureToS3Interactor) {
    return new ChatRoomPresenter(view, savePictureToS3Interactor);
  }
}
