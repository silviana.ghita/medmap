package com.example.silvi.medmap_android.ui.messages.chatroom;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.medmap_commons.model.ChatRoom;
import com.example.medmap_commons.model.Message;
import com.example.medmap_commons.model.MessageType;
import com.example.medmap_commons.model.User;
import com.example.medmap_commons.model.UserSession;
import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.commons.IntentKeys;
import com.example.silvi.medmap_android.dagger.components.DaggerIChatRoomComponent;
import com.example.silvi.medmap_android.dagger.components.IAppComponent;
import com.example.silvi.medmap_android.dagger.modules.ChatRoomModule;
import com.example.silvi.medmap_android.ui.base.BaseActivity;
import com.example.silvi.medmap_android.ui.base.BasePresenter;
import com.example.silvi.medmap_android.ui.base.FinishActivities;
import com.example.silvi.medmap_android.ui.base.FinishCallback;
import com.example.silvi.medmap_android.util.PhotoRepository;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.File;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by silvi on 01.05.2017.
 */

public class ChatRoomActivity extends BaseActivity implements FinishCallback, ChatRoomContract.View {


  private final static String MESSAGES_CHILD = "messages";
  private static final int REQUEST_PICK_IMAGE = 101;

  @Bind(R.id.recycler_view_chat)
  RecyclerView mRecyclerView;
  @Bind(R.id.edit_text_message)
  EditText mEditText;
  @Bind(R.id.send_button)
  TextView mSendButton;

  private ChatRoom mChatRoom;
  private DatabaseReference mDatabaseReference;
  private ChatRoomThreadAdapter mChatRoomThreadAdapter;
  private LinearLayoutManager mLinearLayoutManager;

  private PhotoRepository mPhotoRepository;
  private File selectedImageFile;


  @Inject
  public ChatRoomPresenter mPresenter;


  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mChatRoom = getIntent().getParcelableExtra(IntentKeys.CHATROOM);

    mLinearLayoutManager = new LinearLayoutManager(this);
    mLinearLayoutManager.setStackFromEnd(true);
    mRecyclerView.setLayoutManager(mLinearLayoutManager);

    mDatabaseReference = FirebaseDatabase.getInstance().getReference(mChatRoom.getChatRoomId().toString());
    setRecyclerAdapter();
    mRecyclerView.setAdapter(mChatRoomThreadAdapter);

    mRecyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
      @Override
      public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
        mRecyclerView.smoothScrollToPosition(bottom);
      }
    });

    FinishActivities.addActivity(this);
    setupToolbarDisplay();
    setTextChangedListener();

    mPhotoRepository = new PhotoRepository(getApplicationContext());
  }

  public void setRecyclerAdapter() {

    mChatRoomThreadAdapter = new ChatRoomThreadAdapter(this, mDatabaseReference.child(MESSAGES_CHILD));

    mChatRoomThreadAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
      @Override
      public void onItemRangeInserted(int positionStart, int itemCount) {
        super.onItemRangeInserted(positionStart, itemCount);
        int friendlyMessageCount = mChatRoomThreadAdapter.getItemCount();
        int lastVisiblePosition = mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
        if (lastVisiblePosition == -1 ||
            (positionStart >= (friendlyMessageCount - 1) && lastVisiblePosition == (positionStart - 1))) {
          mRecyclerView.scrollToPosition(positionStart);
        }
      }
    });

  }


  private void setTextChangedListener() {
    mEditText.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (s.length() > 0)
          mSendButton.setTextColor(getResources().getColor(R.color.colorAccent));
        else
          mSendButton.setTextColor(getResources().getColor(R.color.edit_text_hint_color));
      }

      @Override
      public void afterTextChanged(Editable s) {

      }
    });
  }

  private void setupToolbarDisplay() {
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    final ActionBar actionBar = getSupportActionBar();
    View viewActionBar = getLayoutInflater().inflate(R.layout.actionbar_default, null);
    ActionBar.LayoutParams params = new ActionBar.LayoutParams(
        ActionBar.LayoutParams.WRAP_CONTENT,
        ActionBar.LayoutParams.MATCH_PARENT,
        Gravity.CENTER);
    TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.actionbar_textview);
    for (User user : mChatRoom.getParticipants()) {
      String userFullName = user.getFirstName() + " " + user.getLastName();

      if (!userFullName.equals(UserSession.getUserFullName())) {
        textviewTitle.setText(userFullName);
        break;
      }
    }
    Typeface robotoMedium = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");
    textviewTitle.setTypeface(robotoMedium);
    actionBar.setCustomView(viewActionBar, params);
    actionBar.setDisplayShowCustomEnabled(true);
    actionBar.setDisplayShowTitleEnabled(false);
    actionBar.setDisplayHomeAsUpEnabled(true);
    actionBar.setHomeButtonEnabled(false);

    getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_chevron_left_white));

  }

  public ChatRoomActivity getActivity() {
    return this;
  }

  @OnClick(R.id.send_button)
  void onSendButtonClick() {
    if (!mEditText.getText().toString().isEmpty()) {
      mDatabaseReference.child(MESSAGES_CHILD)
          .push()
          .setValue(new Message()
              .setAuthorId(UserSession.getUserId())
              .setContent(mEditText.getText().toString())
              .setType(MessageType.TEXT)
              .setRidden(false));
      mEditText.setText("");
    }
  }

  @OnClick(R.id.add_photo_button)
  void onAddPhotoClick() {
    startActivityForResult(createImagePickerIntent(), REQUEST_PICK_IMAGE);
  }


  private Intent createImagePickerIntent() {
    Intent intent = new Intent(Intent.ACTION_PICK);
    intent.setType("image/*");
    return intent;
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == REQUEST_PICK_IMAGE && resultCode == RESULT_OK && data != null) {
      onImagePicked(data.getData());
      mPresenter.savePictureToS3();
    } else {
      super.onActivityResult(requestCode, resultCode, data);
    }
  }

  private void onImagePicked(Uri imageUri) {
    String text = imageUri.toString() + "\n\n" + imageUri.getPath() + "\n\n File Path: ";

    String imagePath = mPhotoRepository.getImagePath(imageUri);

    File imageFile = new File(imagePath);
    if (imageFile.exists()) {
      text += imageFile.getAbsolutePath();
    } else {
      text += "This file doesn't exist!";
    }
    selectedImageFile = imageFile;
  }

  @Override
  public File getImagePath() {
    return selectedImageFile;
  }

  @Override
  public void sendImage(String imageUrl) {
    mDatabaseReference.child(MESSAGES_CHILD)
        .push()
        .setValue(new Message()
            .setAuthorId(UserSession.getUserId())
            .setContent(imageUrl)
            .setType(MessageType.IMAGE)
            .setRidden(false));
  }

  @Override
  protected int getLayout() {
    return R.layout.activity_chat_room;
  }

  @Override
  public void setUpComponent(IAppComponent appComponent) {
    DaggerIChatRoomComponent.builder()
        .iAppComponent(appComponent)
        .chatRoomModule(new ChatRoomModule(this))
        .build()
        .inject(this);
  }

  @Nullable
  @Override
  protected BasePresenter getPresenter() {
    return null;
  }

  @Override
  protected String getActionBarTitle() {
    return null;
  }

  @Override
  public void forceFinish() {
    this.finish();
  }

  public static Intent makeIntent(Context context, ChatRoom chatRoom) {
    Intent intent = new Intent(context, ChatRoomActivity.class);

    intent.putExtra(IntentKeys.CHATROOM, chatRoom);

    return intent;
  }
}
