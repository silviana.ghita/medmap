package com.example.silvi.medmap_android.domain.cabinet.add;

import com.example.medmap_commons.model.Cabinet;
import com.example.medmap_commons.model.RestError;

/**
 * Created by silvi on 14.03.2017.
 */

public interface AddCabinetInteractor {

  interface Callback {

    void onAddCabinetSuccess(Cabinet cabinet);

    void onAddCabinetError(RestError restError);
  }

  void execute(Callback callback, Cabinet cabinet);
}
