package com.example.silvi.medmap_android.dagger.components;

import com.example.medmap_api.service.ImageService;
import com.example.medmap_api.service.ProfileService;
import com.example.silvi.medmap_android.dagger.ActivityScope;
import com.example.silvi.medmap_android.dagger.modules.ProfileModule;
import com.example.silvi.medmap_android.domain.images.upload.UploadPictureInteractor;
import com.example.silvi.medmap_android.domain.profile.GetProfileDetailsInteractor;
import com.example.silvi.medmap_android.domain.profile.SaveProfileDetailsInteractor;
import com.example.silvi.medmap_android.ui.profile.ProfileActivity;
import com.example.silvi.medmap_android.ui.profile.ProfileContract;
import com.example.silvi.medmap_android.ui.profile.ProfilePresenter;

import dagger.Component;

/**
 * Created by silvi on 13.03.2017.
 */


@ActivityScope
@Component(
    dependencies = IAppComponent.class,
    modules = ProfileModule.class
)
public interface IProfileComponent extends IAppComponent {

  void inject(ProfileActivity registerActivity);

  ProfileContract.View getView();

  ProfileService getProfileService();

  ImageService getImageService();

  GetProfileDetailsInteractor getProfileDetailsInteractor();

  SaveProfileDetailsInteractor getSaveProfileDetailsInteractor();

  UploadPictureInteractor getUploadPictureInteractor();

  ProfilePresenter getProfilePresenter();
}
