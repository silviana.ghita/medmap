package com.example.silvi.medmap_android.ui.appointments;

import com.example.medmap_commons.model.Appointment;
import com.example.medmap_commons.model.ChatRoom;

import java.util.List;

/**
 * Created by silvi on 11.04.2017.
 */

public interface AppointmentsContract {

  interface View {

    void setAppointments(List<Appointment> appointments);

    void onAcceptButtonClick(Appointment appointment);

    void onDeclineButtonClick(Appointment appointment);

    void onDeleteAppointmentClick(Appointment appointment);

    void openChatRoom(long doctorId);

    void startChatRoomActivity(ChatRoom chatRoom);
  }

  interface Presenter {

    void getAppointments();

    void acceptAppointment(Appointment appointment);

    void declineAppointment(Appointment appointment);

    void deleteAppointment(Appointment appointment);

    void openChatRoom(long doctorId);

  }
}
