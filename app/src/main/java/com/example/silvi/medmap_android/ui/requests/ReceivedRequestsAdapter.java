package com.example.silvi.medmap_android.ui.requests;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.example.medmap_commons.model.Request;
import com.example.medmap_commons.model.State;
import com.example.medmap_commons.model.User;
import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.ui.base.MedMapApplication;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by silvi on 25.03.2017.
 */

public class ReceivedRequestsAdapter extends RecyclerView.Adapter<ReceivedRequestsAdapter.ViewHolder> {

  RequestsFragment context;
  List<Request> requests;
  Dialog dialog;

  public ReceivedRequestsAdapter(RequestsFragment context, List<Request> requests) {
    this.requests = requests;
    this.context = context;
    dialog = new Dialog(context.getContext(), R.style.CustomDialog);
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(context.getContext());
    View requestsView = inflater.inflate(R.layout.line_item_received_request, parent, false);

    return new ViewHolder(requestsView);
  }

  @Override
  public void onBindViewHolder(ViewHolder holder, final int position) {
    final Request request = requests.get(position);

    final ImageView pacientImage = holder.pacientPicture;
    TextView pacientName = holder.pacientName;
    TextView cabinetName = holder.cabinetName;
    TextView acceptButton = holder.acceptButton;
    TextView declineButton = holder.declineButton;
    final TextView acceptedOrDeclinedText = holder.acceptedOrDeclinedText;
    final LinearLayout buttonsLayout = holder.buttonsLayout;

    Drawable userIcon = MedMapApplication.getInstance().getResources().getDrawable(R.drawable.user_icon);
    userIcon.setColorFilter(new LightingColorFilter(Color.LTGRAY, Color.LTGRAY));


    Glide.with(context)
        .load(request.getUser().getProfilePicture() != null ? request.getUser().getProfilePicture().getPath() : "")
        .asBitmap()
        .placeholder(userIcon)
        .centerCrop()
        .into(new BitmapImageViewTarget(pacientImage) {
          @Override
          protected void setResource(Bitmap resource) {
            RoundedBitmapDrawable circularBitmapDrawable =
                RoundedBitmapDrawableFactory.create(context.getResources(), resource);
            circularBitmapDrawable.setCornerRadius(
                Math.max(resource.getWidth(), resource.getHeight()) / 2.0f);
            pacientImage.setImageDrawable(circularBitmapDrawable);
          }
        });


    pacientName.setText(request.getUser().getLastName() + " " + request.getUser().getFirstName());
    cabinetName.setText(MedMapApplication.getInstance().getResources().getString(R.string.request_for) + " " + request.getCabinet().getName());

    if (request.getState().equals(State.ACCEPTED)) {
      buttonsLayout.setVisibility(View.GONE);
      acceptedOrDeclinedText.setVisibility(View.VISIBLE);
      acceptedOrDeclinedText.setText(MedMapApplication.getInstance().getResources().getString(R.string.request_accepted));
    } else if (request.getState().equals(State.DECLINED)) {
      buttonsLayout.setVisibility(View.GONE);
      acceptedOrDeclinedText.setVisibility(View.VISIBLE);
      acceptedOrDeclinedText.setText(MedMapApplication.getInstance().getResources().getString(R.string.request_declined));
    }

    acceptButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        context.onAcceptRequestClick(request, position);
      }
    });
    declineButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        context.onDeclineRequestClick(request, position);
      }
    });

    pacientImage.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        openUserDetailsDialog(request.getUser());
      }
    });

  }

  private void openUserDetailsDialog(User user) {


    dialog.setContentView(R.layout.dialog_user_details);

    TextView userName = (TextView) dialog.findViewById(R.id.user_name);
    TextView userBirthDate = (TextView) dialog.findViewById(R.id.user_birth_date);
    TextView userEmail = (TextView) dialog.findViewById(R.id.user_email);
    TextView userPhoneNumber = (TextView) dialog.findViewById(R.id.user_phone_number);

    userName.setText(user.getFirstName() + " " + user.getLastName());
    userBirthDate.setText(user.getBirthDate());
    userEmail.setText(user.getEmail());
    userPhoneNumber.setText(user.getPhoneNumber());


    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    dialog.show();
  }

  @Override
  public int getItemCount() {
    return requests.size();
  }

  public static class ViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.pacient_picture)
    ImageView pacientPicture;
    @Bind(R.id.pacient_name)
    TextView pacientName;
    @Bind(R.id.cabinet_name)
    TextView cabinetName;
    @Bind(R.id.acccept_button)
    TextView acceptButton;
    @Bind(R.id.decline_button)
    TextView declineButton;
    @Bind(R.id.accepted_or_declined_text)
    TextView acceptedOrDeclinedText;
    @Bind(R.id.buttons_layout)
    LinearLayout buttonsLayout;


    public ViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
