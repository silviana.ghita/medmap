package com.example.silvi.medmap_android.domain.cabinet.get;

import com.example.medmap_api.service.CabinetService;
import com.example.medmap_commons.model.Cabinet;
import com.example.medmap_commons.model.CabinetsFilter;
import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.util.PrintLog;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.Interactor;
import com.example.silvi.medmap_android.executor.MainThread;

import java.util.List;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by silvi on 16.03.2017.
 */

public class GetAllCabinetsInteractorImpl implements GetAllCabinetsInteractor, Interactor {

  private final Executor mExecutor;
  private final MainThread mMainThread;
  private final CabinetService mCabinetService;


  public GetAllCabinetsInteractorImpl(Executor executor, MainThread mainThread, CabinetService cabinetService) {
    mExecutor = executor;
    mMainThread = mainThread;
    mCabinetService = cabinetService;
  }

  private CabinetsFilter mCabinetsFilter;
  private Callback mCallback;

  @Override
  public void execute(Callback callback, CabinetsFilter cabinetsFilter) {
    if (null == callback) {
      throw new IllegalArgumentException(
          "Callback can't be null, the client of this interactor needs to get the response " +
              "in the callback");
    }
    this.mCallback = callback;
    this.mCabinetsFilter = cabinetsFilter;
    this.mExecutor.run(this);
  }

  @Override
  public void run() {
    mCabinetService.getAllCabinets(mCabinetsFilter, new retrofit.Callback<List<Cabinet>>() {
      @Override
      public void success(List<Cabinet> cabinets, Response response) {
        notifySuccess(cabinets);
      }

      @Override
      public void failure(RetrofitError error) {
        RestError restError = null;
        try {
          restError = (RestError) error.getBodyAs(RestError.class);
        } catch (Exception e) {
          PrintLog.printStackTrace(getClass().getSimpleName(), e);
        }
        notifyError(restError);

      }
    });
  }

  private void notifySuccess(final List<Cabinet> cabinets) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onGetAllCabinetsSuccess(cabinets);
      }
    });
  }

  private void notifyError(final RestError restError) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onGetAllCabinetsError(restError);
      }
    });
  }
}
