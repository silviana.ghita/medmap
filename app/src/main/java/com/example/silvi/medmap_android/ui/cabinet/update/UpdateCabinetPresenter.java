package com.example.silvi.medmap_android.ui.cabinet.update;

import com.example.medmap_commons.model.Cabinet;
import com.example.medmap_commons.model.Image;
import com.example.medmap_commons.model.RestError;
import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.domain.cabinet.update.UpdateCabinetInteractor;
import com.example.silvi.medmap_android.domain.images.delete.DeleteCabinetPictureInteractor;
import com.example.silvi.medmap_android.domain.images.upload.UploadPictureInteractor;
import com.example.silvi.medmap_android.ui.base.MedMapApplication;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by silvi on 16.03.2017.
 */

public class UpdateCabinetPresenter implements UpdateCabinetContract.Presenter, UpdateCabinetInteractor.Callback, UploadPictureInteractor.Callback, DeleteCabinetPictureInteractor.Callback {

  UpdateCabinetContract.View mView;
  UpdateCabinetInteractor mUpdateCabinetInteractor;
  UploadPictureInteractor mUploadDocumentInteractor;
  DeleteCabinetPictureInteractor mDeleteCabinetPictureInteractor;

  @Inject
  public UpdateCabinetPresenter(UpdateCabinetContract.View view, UpdateCabinetInteractor updateCabinetInteractor,
                                UploadPictureInteractor uploadDocumentInteractor, DeleteCabinetPictureInteractor deleteCabinetPictureInteractor
  ) {
    this.mView = view;
    this.mUpdateCabinetInteractor = updateCabinetInteractor;
    this.mUploadDocumentInteractor = uploadDocumentInteractor;
    this.mDeleteCabinetPictureInteractor = deleteCabinetPictureInteractor;

  }

  @Override
  public void onSaveButtonClicked() {

    if (isFormValid()) {
      Cabinet cabinet = new Cabinet();
      cabinet.setId(mView.getCabinetId());
      cabinet.setName(mView.getCabinetName());
      cabinet.setDescription(mView.getDescription());
      cabinet.setCategory(mView.getCategory());
      cabinet.setSpecialization(mView.getSpecialization());

      mUpdateCabinetInteractor.execute(this, cabinet);
    }
  }

  @Override
  public void onSavePicturesClicked() {

    mUploadDocumentInteractor.executeMultiple(this, mView.getImagesPath(), mView.getCabinetId());

  }

  @Override
  public void deleteCabinetPictures(Long cabinetId, List<Image> imagesToDelete) {
    mDeleteCabinetPictureInteractor.execute(this, cabinetId, imagesToDelete);
  }


  public boolean isFormValid() {
    if (mView.getCabinetName() == null || mView.getCabinetName().isEmpty()) {
      mView.showToast(MedMapApplication.getInstance().getResources().getString(R.string.cabinet_name_null));
      return false;
    } else if (mView.getDescription() == null || mView.getDescription().isEmpty()) {
      mView.showToast(MedMapApplication.getInstance().getResources().getString(R.string.cabinet_description_null));
      return false;
    }
    return true;
  }


  @Override
  public void onUpdateCabinetSuccess() {
    mView.showToast(MedMapApplication.getInstance().getString(R.string.cabinet_updated_successfully));
  }

  @Override
  public void onUpdateCabinetError(RestError restError) {
    mView.showToast(MedMapApplication.getInstance().getString(R.string.error_during_update_cabinet));
  }

  @Override
  public void onPictureUploadSuccess() {

  }

  @Override
  public void onPictureUploadFailed(RestError restError) {
    mView.showToast(MedMapApplication.getInstance().getString(R.string.error_during_update_cabinet));
  }

  @Override
  public void onDeleteCabinetPictureSuccess() {
  }

  @Override
  public void onDeleteCabinetPictureError(RestError restError) {
    mView.showToast(MedMapApplication.getInstance().getString(R.string.error_during_update_cabinet));
  }


}
