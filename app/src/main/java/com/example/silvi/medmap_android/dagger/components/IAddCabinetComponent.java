package com.example.silvi.medmap_android.dagger.components;

import com.example.medmap_api.service.CabinetService;
import com.example.medmap_api.service.ImageService;
import com.example.silvi.medmap_android.dagger.ActivityScope;
import com.example.silvi.medmap_android.dagger.modules.AddCabinetModule;
import com.example.silvi.medmap_android.domain.cabinet.add.AddCabinetInteractor;
import com.example.silvi.medmap_android.domain.images.upload.UploadPictureInteractor;
import com.example.silvi.medmap_android.ui.cabinet.add.AddCabinetActivity;
import com.example.silvi.medmap_android.ui.cabinet.add.AddCabinetContract;
import com.example.silvi.medmap_android.ui.cabinet.add.AddCabinetPresenter;

import dagger.Component;

/**
 * Created by silvi on 14.03.2017.
 */

@ActivityScope
@Component(
    dependencies = IAppComponent.class,
    modules = AddCabinetModule.class
)
public interface IAddCabinetComponent extends IAppComponent {

  void inject(AddCabinetActivity addCabinetActivity);

  AddCabinetContract.View getView();

  CabinetService getCabinetService();

  ImageService getimageService();

  AddCabinetInteractor getAddCabinetInteractor();

  UploadPictureInteractor getUploadPictureInteractor();

  AddCabinetPresenter getAddCabinetPresenter();
}
