package com.example.silvi.medmap_android.domain.images.chat;

import com.example.medmap_api.service.ImageService;
import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.util.PrintLog;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.Interactor;
import com.example.silvi.medmap_android.executor.MainThread;

import java.io.File;
import java.util.Map;

import javax.inject.Inject;

import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by silvi on 10.05.2017.
 */

public class SavePictureToS3InteractorImpl implements Interactor, SavePictureToS3Interactor {

  private final Executor mExecutor;
  private final MainThread mMainThread;
  private final ImageService mUploadService;

  private Callback mCallback;
  private File mFile;

  @Inject
  public SavePictureToS3InteractorImpl(Executor executor, MainThread mainThread, ImageService service) {
    this.mExecutor = executor;
    this.mMainThread = mainThread;
    this.mUploadService = service;
  }


  @Override
  public void execute(Callback callback, File path) {
    if (null == callback) {
      throw new IllegalArgumentException(
          "Callback can't be null, the client of this interactor needs to get the response " +
              "in the callback");
    }
    this.mCallback = callback;
    this.mFile = path;
    this.mExecutor.run(this);
  }


  @Override
  public void run() {
    TypedFile typedFile = new TypedFile("multipart/form-data", mFile);
    mUploadService.uploadChatPhoto(typedFile, new retrofit.Callback<Map<String, String>>() {
      @Override
      public void success(Map<String, String> s, Response response) {
        notifySuccess(s.get("imagePath"));
      }

      @Override
      public void failure(RetrofitError error) {
        RestError restError = null;
        try {
          restError = (RestError) error.getBodyAs(RestError.class);
        } catch (Exception e) {
          PrintLog.printStackTrace(getClass().getSimpleName(), e);
        }
        notifyError(restError);
      }
    });
  }

  private void notifySuccess(final String s3Path) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onPictureUploadSuccess(s3Path);
      }
    });
  }

  private void notifyError(final RestError restError) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onPictureUploadFailed(restError);
      }
    });
  }

}
