package com.example.silvi.medmap_android.dagger.modules;

import com.example.medmap_api.service.CabinetService;
import com.example.medmap_api.service.ChatService;
import com.example.medmap_api.service.ProfileService;
import com.example.medmap_api.service.RequestService;
import com.example.silvi.medmap_android.domain.cabinet.get.other.GetOtherCabinetsInteractor;
import com.example.silvi.medmap_android.domain.cabinet.get.other.GetOtherCabinetsInteractorImpl;
import com.example.silvi.medmap_android.domain.chat.chatroom.OpenChatRoomInteractor;
import com.example.silvi.medmap_android.domain.chat.chatroom.OpenChatRoomInteractorImpl;
import com.example.silvi.medmap_android.domain.profile.GetProfileDetailsInteractor;
import com.example.silvi.medmap_android.domain.profile.GetProfileDetailsInteractorImpl;
import com.example.silvi.medmap_android.domain.request.add.AddRequestInteractor;
import com.example.silvi.medmap_android.domain.request.add.AddRequestInteractorImpl;
import com.example.silvi.medmap_android.domain.request.get.GetRequestsInteractor;
import com.example.silvi.medmap_android.domain.request.get.GetRequestsInteractorImpl;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.MainThread;
import com.example.silvi.medmap_android.ui.cabinet.CabinetContract;
import com.example.silvi.medmap_android.ui.cabinet.CabinetPresenter;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;

/**
 * Created by silvi on 17.03.2017.
 */

@Module
public class CabinetModule {

  private CabinetContract.View view;

  public CabinetModule(CabinetContract.View view) {
    this.view = view;
  }

  @Provides
  public CabinetContract.View getView() {
    return view;
  }

  @Provides
  public ProfileService provideProfileService(RestAdapter restAdapter) {
    return restAdapter.create(ProfileService.class);
  }

  @Provides
  public RequestService provideRequestService(RestAdapter restAdapter) {
    return restAdapter.create(RequestService.class);
  }

  @Provides
  public ChatService provideChatService(RestAdapter restAdapter) {
    return restAdapter.create(ChatService.class);
  }

  @Provides
  public CabinetService provideCabinetService(RestAdapter restAdapter) {
    return restAdapter.create(CabinetService.class);
  }


  @Provides
  public GetProfileDetailsInteractor provideProfileDetailsInteracyor(Executor executor,
                                                                     MainThread mainThread,
                                                                     ProfileService profileService) {
    return new GetProfileDetailsInteractorImpl(executor, mainThread, profileService);
  }

  @Provides
  public AddRequestInteractor provideAddRequestInteractor(Executor executor,
                                                          MainThread mainThread,
                                                          RequestService requestService) {
    return new AddRequestInteractorImpl(executor, mainThread, requestService);
  }

  @Provides
  public GetRequestsInteractor provideGetRequestsInteractor(Executor executor,
                                                            MainThread mainThread,
                                                            RequestService requestService) {
    return new GetRequestsInteractorImpl(executor, mainThread, requestService);
  }

  @Provides
  public OpenChatRoomInteractor provideOpenChatRoomInteractor(Executor executor,
                                                              MainThread mainThread,
                                                              ChatService chatService) {
    return new OpenChatRoomInteractorImpl(executor, mainThread, chatService);
  }

  @Provides
  public GetOtherCabinetsInteractor provideGetOtherCabinetsInteractor(Executor executor,
                                                                      MainThread mainThread,
                                                                      CabinetService cabinetService) {
    return new GetOtherCabinetsInteractorImpl(executor, mainThread, cabinetService);
  }

  @Provides
  public CabinetPresenter providePresenter(CabinetContract.View view, GetProfileDetailsInteractor interactor,
                                           AddRequestInteractor addRequestInteractor, GetRequestsInteractor getRequestsInteractor,
                                           OpenChatRoomInteractor openChatRoomInteractor, GetOtherCabinetsInteractor getOtherCabinetsInteractor) {
    return new CabinetPresenter(view, interactor, addRequestInteractor, getRequestsInteractor, openChatRoomInteractor, getOtherCabinetsInteractor);
  }
}
