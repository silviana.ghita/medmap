package com.example.silvi.medmap_android.executor;

import android.os.Handler;
import android.os.Looper;

/**
 * Created by silvi on 22.02.2017.
 */

public class MainThreadImpl implements MainThread {

  private Handler mHandler;

  public MainThreadImpl() {
    this.mHandler = new Handler(Looper.getMainLooper());
  }

  @Override
  public void post(Runnable runnable) {
    mHandler.post(runnable);
  }
}
