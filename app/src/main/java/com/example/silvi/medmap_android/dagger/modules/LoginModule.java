package com.example.silvi.medmap_android.dagger.modules;

import com.example.medmap_api.service.AuthService;
import com.example.silvi.medmap_android.domain.login.LoginInteractor;
import com.example.silvi.medmap_android.domain.login.LoginInteractorImpl;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.MainThread;
import com.example.silvi.medmap_android.ui.auth.login.LoginContract;
import com.example.silvi.medmap_android.ui.auth.login.LoginPresenter;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;

/**
 * Created by silvi on 08.03.2017.
 */

@Module
public class LoginModule {

  private LoginContract.View view;

  public LoginModule(LoginContract.View view) {
    this.view = view;
  }

  @Provides
  public LoginContract.View getView() {
    return view;
  }

  @Provides
  public AuthService provideAuthService(RestAdapter restAdapter) {
    return restAdapter.create(AuthService.class);
  }

  @Provides
  public LoginInteractor provideLoginInteractor(Executor executor,
                                                MainThread mainThread,
                                                AuthService authService) {
    return new LoginInteractorImpl(executor, mainThread, authService);
  }


  @Provides
  public LoginPresenter providePresenter(LoginContract.View view, LoginInteractor interactor) {
    return new LoginPresenter(view, interactor);
  }
}
