package com.example.silvi.medmap_android.ui.appointments.today;

import com.example.medmap_commons.model.Appointment;

import java.util.List;

/**
 * Created by silvi on 13.04.2017.
 */

public interface TodayContract {

  interface View {

    void initTodaysAppointments(List<Appointment> appointmentList);

    void hideDelayButton();

  }

  interface Presenter {

    void getTodaysAppointments();

    void onAddDelayClicked(String delay, List<Appointment> appointments);

  }
}
