package com.example.silvi.medmap_android.domain.request.delete;

import com.example.medmap_api.service.RequestService;
import com.example.medmap_commons.model.Request;
import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.util.PrintLog;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.Interactor;
import com.example.silvi.medmap_android.executor.MainThread;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by silvi on 27.03.2017.
 */

public class DeleteRequestInteractorImpl implements Interactor, DeleteRequestInteractor {

  private final Executor mExecutor;
  private final MainThread mMainThread;
  private final RequestService mRequestService;

  private Callback mCallback;
  private Request mRequest;

  public DeleteRequestInteractorImpl(Executor executor, MainThread mainThread, RequestService requestService) {
    mExecutor = executor;
    mMainThread = mainThread;
    mRequestService = requestService;
  }


  @Override
  public void execute(Callback callback, Request request) {

    if (null == callback) {
      throw new IllegalArgumentException(
          "Callback can't be null, the client of this interactor needs to get the response " +
              "in the callback");
    }
    this.mCallback = callback;
    this.mRequest = request;
    this.mExecutor.run(this);
  }

  @Override
  public void run() {
    mRequestService.deleteRequest(mRequest, new retrofit.Callback<Object>() {
      @Override
      public void success(Object o, Response response) {
        notifySuccess();
      }

      @Override
      public void failure(RetrofitError error) {
        RestError restError = null;
        try {
          restError = (RestError) error.getBodyAs(RestError.class);
        } catch (Exception e) {
          PrintLog.printStackTrace(getClass().getSimpleName(), e);
        }
        notifyError(restError);
      }
    });
  }

  private void notifySuccess() {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onDeleteRequestSuccess();
      }
    });
  }

  private void notifyError(final RestError restError) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onDeleteRequestError(restError);
      }
    });
  }

}
