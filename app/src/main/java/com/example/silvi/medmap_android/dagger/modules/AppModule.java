package com.example.silvi.medmap_android.dagger.modules;

import com.example.medmap_api.ApiConstants;
import com.example.medmap_api.endpoint.ApiRequestInterceptor;
import com.example.medmap_commons.adapter.CustomDateTypeAdapter;
import com.example.silvi.medmap_android.ui.base.MedMapApplication;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * Created by silvi on 08.03.2017.
 */

@Module
public class AppModule {

  final MedMapApplication app;

  private ApiRequestInterceptor requestInterceptor;

  public AppModule(MedMapApplication app) {
    this.app = app;
  }

  @Provides
  @Singleton
  MedMapApplication provideApp() {
    return this.app;
  }

  @Provides
  @Singleton
  RestAdapter provideRestAdapter() {
    requestInterceptor = new ApiRequestInterceptor();
    final OkHttpClient okHttpClient = new OkHttpClient();
    okHttpClient.setReadTimeout(60, TimeUnit.SECONDS);
    okHttpClient.setConnectTimeout(60, TimeUnit.SECONDS);
    return new RestAdapter.Builder()
        .setRequestInterceptor(requestInterceptor)
        .setEndpoint(ApiConstants.getUrl())
        .setConverter(new GsonConverter(new GsonBuilder()
            .registerTypeAdapter(Date.class, new CustomDateTypeAdapter())
            .serializeNulls()
            .create()))
        .setClient(new OkClient(okHttpClient))
        .setLogLevel(RestAdapter.LogLevel.FULL)
        .build();
  }

  @Provides
  @Singleton
  ApiRequestInterceptor provideApiRequestInterceptor() {
    return requestInterceptor;
  }

}
