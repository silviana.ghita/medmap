package com.example.silvi.medmap_android.domain.images.chat;

import com.example.medmap_commons.model.RestError;

import java.io.File;

/**
 * Created by silvi on 10.05.2017.
 */

public interface SavePictureToS3Interactor {

  interface Callback {

    void onPictureUploadSuccess(String s3Path);

    void onPictureUploadFailed(RestError restError);
  }

  void execute(Callback callback, File path);
}
