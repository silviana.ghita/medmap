package com.example.silvi.medmap_android.domain.filters;

import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.model.User;

import java.util.List;

/**
 * Created by silvi on 26.04.2017.
 */

public interface GetDoctorUsersInteractor {

  interface Callback {
    void onGetDoctorUsersSuccess(List<User> doctorUsers);

    void onGetDoctorUsersError(RestError restError);
  }

  void execute(Callback callback);
}
