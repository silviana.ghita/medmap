package com.example.silvi.medmap_android.ui.messages.chatroom;

import java.io.File;

/**
 * Created by silvi on 01.05.2017.
 */

public interface ChatRoomContract {

  interface View {

    File getImagePath();

    void sendImage(String imageUrl);
  }

  interface Presenter {

    void savePictureToS3();
  }
}
