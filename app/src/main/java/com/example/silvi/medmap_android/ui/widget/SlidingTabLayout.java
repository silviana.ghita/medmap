package com.example.silvi.medmap_android.ui.widget;

/**
 * Created by silvi on 19.03.2017.
 */

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.silvi.medmap_android.R;


public class SlidingTabLayout extends HorizontalScrollView {

  private static final int TITLE_OFFSET_DIPS = 24;
  private final int TAB_VIEW_PADDING_DIPS;

  private int mTitleOffset;

  private ViewPager mViewPager;
  private ViewPager.OnPageChangeListener mViewPagerPageChangeListener;

  private final SlidingTabStrip mTabStrip;

  public SlidingTabLayout(Context context) {
    this(context, null);
  }

  public SlidingTabLayout(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public SlidingTabLayout(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);

    // Disable the Scroll Bar
    setHorizontalScrollBarEnabled(false);
    // Make sure that the Tab Strips fills this View
    setFillViewport(true);

    TAB_VIEW_PADDING_DIPS = context.getResources()
        .getDimensionPixelOffset(R.dimen.five_dp);

    mTitleOffset = (int) (TITLE_OFFSET_DIPS * getResources().getDisplayMetrics().density);

    mTabStrip = new SlidingTabStrip(context);
    addView(mTabStrip, LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
  }

  /**
   * Sets the associated view pager. Note that the assumption here is that the pager content
   * (number of tabs and tab titles) does not change after this call has been made.
   */
  public void setViewPager(ViewPager viewPager) {
    mTabStrip.removeAllViews();

    mViewPager = viewPager;
    if (viewPager != null) {
      viewPager.setOnPageChangeListener(new InternalViewPagerListener());
      populateTabStrip();
    }
  }

  /**
   * Create a default view to be used for tabs.
   */
  protected ImageView createDefaultTabView(Context context) {
    ImageView imageView = new ImageView(context);
    imageView.setImageResource(R.drawable.cabinet_images_indicator_bg_selector);
    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
        ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    layoutParams.setMargins(TAB_VIEW_PADDING_DIPS, 0, TAB_VIEW_PADDING_DIPS, 0);
    layoutParams.gravity = Gravity.CENTER_VERTICAL;
    imageView.setLayoutParams(layoutParams);
    return imageView;
  }

  public void notifyDataSetChanged() {
    populateTabStrip();
  }

  private void populateTabStrip() {
    final PagerAdapter adapter = mViewPager.getAdapter();
    final View.OnClickListener tabClickListener = new TabClickListener();

    mTabStrip.removeAllViews();

    for (int i = 0; i < adapter.getCount(); i++) {
      View tabView = createDefaultTabView(getContext());
      tabView.setOnClickListener(tabClickListener);
      mTabStrip.addView(tabView);

      if (i == mViewPager.getCurrentItem()) {
        tabView.setSelected(true);
      }
    }
  }

  @Override
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    if (mViewPager != null) {
      scrollToTab(mViewPager.getCurrentItem(), 0);
    }
  }

  private void scrollToTab(int tabIndex, int positionOffset) {
    final int tabStripChildCount = mTabStrip.getChildCount();
    if (tabStripChildCount == 0 || tabIndex < 0 || tabIndex >= tabStripChildCount) {
      return;
    }

    View selectedChild = mTabStrip.getChildAt(tabIndex);
    if (selectedChild != null) {
      int targetScrollX = selectedChild.getLeft() + positionOffset;

      if (tabIndex > 0 || positionOffset > 0) {
        // If we're not at the first child and are mid-scroll, make sure we obey the offset
        targetScrollX -= mTitleOffset;
      }
      scrollTo(targetScrollX, 0);
    }
  }

  private class InternalViewPagerListener implements ViewPager.OnPageChangeListener {
    private int mScrollState;

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
      int tabStripChildCount = mTabStrip.getChildCount();
      if ((tabStripChildCount == 0) || (position < 0) || (position >= tabStripChildCount)) {
        return;
      }

      mTabStrip.onViewPagerPageChanged(position, positionOffset);

      View selectedTitle = mTabStrip.getChildAt(position);
      int extraOffset = (selectedTitle != null)
          ? (int) (positionOffset * selectedTitle.getWidth())
          : 0;
      scrollToTab(position, extraOffset);

      if (mViewPagerPageChangeListener != null) {
        mViewPagerPageChangeListener.onPageScrolled(position, positionOffset,
            positionOffsetPixels);
      }
    }

    @Override
    public void onPageScrollStateChanged(int state) {
      mScrollState = state;

      if (mViewPagerPageChangeListener != null) {
        mViewPagerPageChangeListener.onPageScrollStateChanged(state);
      }
    }

    @Override
    public void onPageSelected(int position) {
      if (mScrollState == ViewPager.SCROLL_STATE_IDLE) {
        mTabStrip.onViewPagerPageChanged(position, 0f);
        scrollToTab(position, 0);
      }

      for (int i = 0; i < mTabStrip.getChildCount(); i++) {
        mTabStrip.getChildAt(i).setSelected(i == position);
      }

      if (mViewPagerPageChangeListener != null) {
        mViewPagerPageChangeListener.onPageSelected(position);
      }
    }
  }

  private class TabClickListener implements View.OnClickListener {
    @Override
    public void onClick(View v) {
      for (int i = 0; i < mTabStrip.getChildCount(); i++) {
        mTabStrip.getChildAt(i).setSelected(false);
        if (v == mTabStrip.getChildAt(i)) {
          mViewPager.setCurrentItem(i);
          v.setSelected(true);
        }
      }
    }
  }
}