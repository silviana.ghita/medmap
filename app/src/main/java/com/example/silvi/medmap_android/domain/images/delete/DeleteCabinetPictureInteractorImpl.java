package com.example.silvi.medmap_android.domain.images.delete;

import com.example.medmap_api.service.ImageService;
import com.example.medmap_commons.model.Image;
import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.util.PrintLog;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.Interactor;
import com.example.silvi.medmap_android.executor.MainThread;

import java.util.List;

import javax.inject.Inject;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by silvi on 01.04.2017.
 */

public class DeleteCabinetPictureInteractorImpl implements Interactor, DeleteCabinetPictureInteractor {

  private final Executor mExecutor;
  private final MainThread mMainThread;
  private final ImageService mImageService;

  private Callback mCallback;
  private List<Image> mImagesToDelete;
  private Long mCabinetId;


  @Inject
  public DeleteCabinetPictureInteractorImpl(Executor executor, MainThread mainThread, ImageService service) {
    this.mExecutor = executor;
    this.mMainThread = mainThread;
    this.mImageService = service;
  }

  @Override
  public void execute(Callback callback, Long cabinetId, List<Image> imagesToDelete) {
    if (null == callback) {
      throw new IllegalArgumentException(
          "Callback can't be null, the client of this interactor needs to get the response " +
              "in the callback");
    }
    this.mCallback = callback;
    this.mImagesToDelete = imagesToDelete;
    this.mCabinetId = cabinetId;
    this.mExecutor.run(this);
  }

  @Override
  public void run() {
    mImageService.deleteCabinetPictures(mImagesToDelete, mCabinetId, new retrofit.Callback<Object>() {
      @Override
      public void success(Object o, Response response) {
        notifySuccess();
      }

      @Override
      public void failure(RetrofitError error) {
        RestError restError = null;
        try {
          restError = (RestError) error.getBodyAs(RestError.class);
        } catch (Exception e) {
          PrintLog.printStackTrace(getClass().getSimpleName(), e);
      }
        notifyError(restError);
      }
    });

  }

  private void notifySuccess() {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onDeleteCabinetPictureSuccess();
      }
    });
  }

  private void notifyError(final RestError restError) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onDeleteCabinetPictureError(restError);
      }
    });
  }
}
