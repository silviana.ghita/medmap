package com.example.silvi.medmap_android.domain.register;

import com.example.medmap_commons.model.Doctor;
import com.example.medmap_commons.model.LoggedInUser;
import com.example.medmap_commons.model.RestError;

/**
 * Created by silvi on 07.03.2017.
 */

public interface RegisterDoctorInteractor {
  interface Callback {
    void onRegisterSuccess(LoggedInUser loggedInUser);

    void onRegisterError(RestError restError);
  }

  void execute(Callback callback, Doctor doctor);
}
