package com.example.silvi.medmap_android.ui.widget;

/**
 * Created by silvi on 27.02.2017.
 */

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import com.example.silvi.medmap_android.manager.AssetsManager;

public class TypefaceButton extends Button {

  public TypefaceButton(Context context) {
    super(context);
  }

  public TypefaceButton(Context context, Typeface typeface) {
    super(context);
    if (!isInEditMode()) {
      setTypeface(typeface);
    }
  }

  public TypefaceButton(Context context, AttributeSet attrs) {
    super(context, attrs);
    if (!isInEditMode()) {
      setTypeface(AssetsManager.getTypeface(attrs, context));
    }
  }

  public TypefaceButton(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    if (!isInEditMode()) {
      setTypeface(AssetsManager.getTypeface(attrs, context));
    }
  }
}