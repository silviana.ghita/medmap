package com.example.silvi.medmap_android.preferences;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by silvi on 22.02.2017.
 */

public class SharedPreferencesHandler {

  private static SharedPreferences getSharedPrefs(Context context) {
    return context.getSharedPreferences(Preferences.PrefNames.PREFS_NAME_TAG, Context.MODE_PRIVATE);
  }

  public static String getAuthToken(Context context) {
    SharedPreferences prefs = getSharedPrefs(context);
    return prefs.getString(Preferences.PrefKeys.PREF_X_AUTH_TOKEN, "");
  }

  public static void setAuthToken(Context context, String authToken) {
    SharedPreferences prefs = getSharedPrefs(context);
    SharedPreferences.Editor editor = prefs.edit();
    editor.putString(Preferences.PrefKeys.PREF_X_AUTH_TOKEN, authToken);
    editor.apply();
  }

  public static String getUserEmail(Context context) {
    SharedPreferences prefs = getSharedPrefs(context);
    return prefs.getString(Preferences.PrefKeys.PREF_USER_EMAIL, "");
  }

  public static void setUserEmail(Context context, String email) {
    SharedPreferences prefs = getSharedPrefs(context);
    SharedPreferences.Editor editor = prefs.edit();
    editor.putString(Preferences.PrefKeys.PREF_USER_EMAIL, email);
    editor.apply();
  }

  public static Long getUserId(Context context) {
    SharedPreferences prefs = getSharedPrefs(context);
    return prefs.getLong(Preferences.PrefKeys.PREF_USER_ID, 0);
  }

  public static void setUserId(Context context, Long id) {
    SharedPreferences prefs = getSharedPrefs(context);
    SharedPreferences.Editor editor = prefs.edit();
    editor.putLong(Preferences.PrefKeys.PREF_USER_ID, id);
    editor.apply();
  }



  public static String getUserFullName(Context context) {
    SharedPreferences prefs = getSharedPrefs(context);
    return prefs.getString(Preferences.PrefKeys.PREF_USER_FULL_NAME, "");
  }

  public static void setUserFullName(Context context, String name) {
    SharedPreferences prefs = getSharedPrefs(context);
    SharedPreferences.Editor editor = prefs.edit();
    editor.putString(Preferences.PrefKeys.PREF_USER_FULL_NAME, name);
    editor.apply();
  }

  public static void setIsApplicatonVisible(Context context, boolean state) {
    SharedPreferences prefs = getSharedPrefs(context);
    prefs.edit().putBoolean(Preferences.PrefKeys.PREF_APPLICATION_VISIBLE, state).apply();
  }

  public static boolean isApplicationVisible(Context context) {
    SharedPreferences prefs = getSharedPrefs(context);
    return prefs.getBoolean(Preferences.PrefKeys.PREF_APPLICATION_VISIBLE, false);
  }

  public static void setIsApplicationInForeground(Context context, boolean state) {
    SharedPreferences prefs = getSharedPrefs(context);
    prefs.edit().putBoolean(Preferences.PrefKeys.PREF_APPLICATION_FOREGROUND, state).apply();
  }


  public static boolean isApplicationInForegound(Context context) {
    SharedPreferences prefs = getSharedPrefs(context);
    return prefs.getBoolean(Preferences.PrefKeys.PREF_APPLICATION_VISIBLE, false);
  }

  public static void clearKeys(Context context) {
    SharedPreferences prefs = getSharedPrefs(context);
    SharedPreferences.Editor editor = prefs.edit();
    editor.clear();
    editor.apply();
  }
}
