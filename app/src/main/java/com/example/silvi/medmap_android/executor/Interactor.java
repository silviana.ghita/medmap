package com.example.silvi.medmap_android.executor;

/**
 * Created by silvi on 22.02.2017.
 */

public interface Interactor {

  void run();
}
