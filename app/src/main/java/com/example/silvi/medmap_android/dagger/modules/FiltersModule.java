package com.example.silvi.medmap_android.dagger.modules;

import com.example.medmap_api.service.ProfileService;
import com.example.silvi.medmap_android.domain.filters.GetDoctorUsersInteractor;
import com.example.silvi.medmap_android.domain.filters.GetDoctorUsersInteractorImpl;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.MainThread;
import com.example.silvi.medmap_android.ui.filters.FiltersContract;
import com.example.silvi.medmap_android.ui.filters.FiltersPresenter;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;

/**
 * Created by silvi on 26.04.2017.
 */

@Module
public class FiltersModule {

  private FiltersContract.View view;

  public FiltersModule(FiltersContract.View view) {
    this.view = view;
  }

  @Provides
  FiltersContract.View getView() {
    return view;
  }

  @Provides
  public ProfileService provideProfileService(RestAdapter restAdapter) {
    return restAdapter.create(ProfileService.class);
  }

  @Provides
  public GetDoctorUsersInteractor provideGetDoctorUsersInteractor(Executor executor,
                                                                  MainThread mainThread,
                                                                  ProfileService profileService) {
    return new GetDoctorUsersInteractorImpl(executor, mainThread, profileService);
  }

  @Provides
  public FiltersPresenter providePresenter(FiltersContract.View view, GetDoctorUsersInteractor interactor) {
    return new FiltersPresenter(view, interactor);
  }


}
