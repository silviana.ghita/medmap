package com.example.silvi.medmap_android.dagger.components;

import com.example.medmap_api.service.ChatService;
import com.example.silvi.medmap_android.dagger.ActivityScope;
import com.example.silvi.medmap_android.dagger.modules.MessagesModule;
import com.example.silvi.medmap_android.domain.chat.GetChatRoomsInteractor;
import com.example.silvi.medmap_android.ui.messages.MessagesActivity;
import com.example.silvi.medmap_android.ui.messages.MessagesContract;
import com.example.silvi.medmap_android.ui.messages.MessagesPresenter;

import dagger.Component;

/**
 * Created by silvi on 05.05.2017.
 */

@ActivityScope
@Component(
    dependencies = IAppComponent.class,
    modules = {
        MessagesModule.class,
    }
)
public interface IMessagesComponent extends IAppComponent {


  void inject(MessagesActivity messagesActivity);

  MessagesContract.View getView();

  ChatService getChatService();

  GetChatRoomsInteractor getChatRoomsInteractor();

  MessagesPresenter getMessagesPresenter();
}