package com.example.silvi.medmap_android.domain.profile;

import com.example.medmap_api.service.ProfileService;
import com.example.medmap_commons.model.Doctor;
import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.model.User;
import com.example.medmap_commons.model.UserSession;
import com.example.medmap_commons.util.PrintLog;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.Interactor;
import com.example.silvi.medmap_android.executor.MainThread;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by silvi on 13.03.2017.
 */

public class SaveProfileDetailsInteractorImpl implements SaveProfileDetailsInteractor, Interactor {

  private final Executor mExecutor;
  private final MainThread mMainThread;
  private final ProfileService mProfileService;

  private SaveProfileDetailsInteractor.Callback mCallback;
  private User mUser;
  private Doctor mDoctor;

  public SaveProfileDetailsInteractorImpl(Executor executor, MainThread mainThread, ProfileService profileService) {
    mExecutor = executor;
    mMainThread = mainThread;
    mProfileService = profileService;
  }

  @Override
  public void execute(Callback callback, User user, Doctor doctor) {
    if (null == callback) {
      throw new IllegalArgumentException(
          "Callback can't be null, the client of this interactor needs to get the response " +
              "in the callback");
    }
    this.mCallback = callback;
    this.mExecutor.run(this);
    this.mUser = user;
    this.mDoctor = doctor;
  }


  @Override
  public void run() {
    if (UserSession.getUserRole().name().equals("ROLE_USER")) {
      mProfileService.updateUser(mUser, new retrofit.Callback<User>() {
        @Override
        public void success(User user, Response response) {
          notifyUserSuccess(user);
        }

        @Override
        public void failure(RetrofitError error) {
          RestError restError = null;
          try {
            restError = (RestError) error.getBodyAs(RestError.class);
          } catch (Exception e) {
            PrintLog.printStackTrace(getClass().getSimpleName(), e);
          }
          notifyError(restError);
        }
      });
    } else {
      mProfileService.updateDoctor(mDoctor, new retrofit.Callback<Doctor>() {
        @Override
        public void success(Doctor doctor, Response response) {
          notifyDoctorSuccess(doctor);
        }

        @Override
        public void failure(RetrofitError error) {
          RestError restError = null;
          try {
            restError = (RestError) error.getBodyAs(RestError.class);
          } catch (Exception e) {
            PrintLog.printStackTrace(getClass().getSimpleName(), e);
          }
          notifyError(restError);

        }
      });
    }
  }

  private void notifyUserSuccess(final User user) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onUserSaveProfileDetailsSuccess(user);
      }
    });
  }

  private void notifyDoctorSuccess(final Doctor doctor) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onDoctorSaveProfileDetailsSuccess(doctor);
      }
    });
  }

  private void notifyError(final RestError restError) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onSaveProfileDetailsError(restError);
      }
    });
  }
}
