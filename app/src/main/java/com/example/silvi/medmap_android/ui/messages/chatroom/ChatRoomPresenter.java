package com.example.silvi.medmap_android.ui.messages.chatroom;

import com.example.medmap_commons.model.RestError;
import com.example.silvi.medmap_android.domain.images.chat.SavePictureToS3Interactor;

import javax.inject.Inject;

/**
 * Created by silvi on 01.05.2017.
 */

public class ChatRoomPresenter implements ChatRoomContract.Presenter, SavePictureToS3Interactor.Callback {

  SavePictureToS3Interactor mSavePictureToS3Interactor;
  ChatRoomContract.View mView;

  @Inject
  public ChatRoomPresenter(ChatRoomContract.View view, SavePictureToS3Interactor savePictureToS3Interactor) {
    this.mView = view;
    this.mSavePictureToS3Interactor = savePictureToS3Interactor;
  }

  public void savePictureToS3() {
    mSavePictureToS3Interactor.execute(this, mView.getImagePath());
  }

  @Override
  public void onPictureUploadSuccess(String url) {
    mView.sendImage(url);
  }

  @Override
  public void onPictureUploadFailed(RestError restError) {

  }
}
