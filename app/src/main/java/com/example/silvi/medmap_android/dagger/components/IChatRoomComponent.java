package com.example.silvi.medmap_android.dagger.components;

import com.example.medmap_api.service.ImageService;
import com.example.silvi.medmap_android.dagger.ActivityScope;
import com.example.silvi.medmap_android.dagger.modules.ChatRoomModule;
import com.example.silvi.medmap_android.domain.images.chat.SavePictureToS3Interactor;
import com.example.silvi.medmap_android.ui.messages.chatroom.ChatRoomActivity;
import com.example.silvi.medmap_android.ui.messages.chatroom.ChatRoomContract;
import com.example.silvi.medmap_android.ui.messages.chatroom.ChatRoomPresenter;

import dagger.Component;

/**
 * Created by silvi on 10.05.2017.
 */

@ActivityScope
@Component(
    dependencies = IAppComponent.class,
    modules = ChatRoomModule.class
)
public interface IChatRoomComponent extends IAppComponent {

  void inject(ChatRoomActivity registerActivity);

  ChatRoomContract.View getView();

  ImageService getImageService();

  SavePictureToS3Interactor getSavePictureToS3Interactor();

  ChatRoomPresenter getChatRoomPresenter();

}
