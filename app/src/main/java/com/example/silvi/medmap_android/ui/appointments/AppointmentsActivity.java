package com.example.silvi.medmap_android.ui.appointments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.medmap_commons.model.Appointment;
import com.example.medmap_commons.model.ChatRoom;
import com.example.medmap_commons.model.State;
import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.dagger.components.DaggerIAppointmentsComponent;
import com.example.silvi.medmap_android.dagger.components.IAppComponent;
import com.example.silvi.medmap_android.dagger.modules.AppointmentsModule;
import com.example.silvi.medmap_android.dagger.modules.NavigationModule;
import com.example.silvi.medmap_android.ui.base.BasePresenter;
import com.example.silvi.medmap_android.ui.base.FinishActivities;
import com.example.silvi.medmap_android.ui.base.FinishCallback;
import com.example.silvi.medmap_android.ui.base.NavigationViewActivity;
import com.example.silvi.medmap_android.ui.messages.chatroom.ChatRoomActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by silvi on 11.04.2017.
 */

public class AppointmentsActivity extends NavigationViewActivity implements FinishCallback, AppointmentsContract.View {

  @Bind(R.id.declined_appointments_button)
  TextView mDeclinedAppointmentsButton;
  @Bind(R.id.accepted_appointments_button)
  TextView mAcceptedAppointmentsButton;
  @Bind(R.id.pending_appointments_button)
  TextView mPendingAppointmentsButton;
  @Bind(R.id.viewPager)
  ViewPager mViewPager;


  AppointmentsPageAdapter mAppointmentsPageAdapter;

  AppointmentsFragment mDeclinedFragment;
  AppointmentsFragment mAcceptedFragment;

  ArrayList<Appointment> pendingAppointments;
  ArrayList<Appointment> acceptedAppointments;
  ArrayList<Appointment> declinedAppointments;

  List<Appointment> mAppointments;

  @Inject
  AppointmentsPresenter mPresenter;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    mPresenter.getAppointments();

    setupToolbarDisplay();
    FinishActivities.addActivity(this);
  }

  private List<Fragment> getFragments() {
    List<Fragment> fragmentsList = new ArrayList<>();

    pendingAppointments = new ArrayList<>();
    acceptedAppointments = new ArrayList<>();
    declinedAppointments = new ArrayList<>();
    for (Appointment appointment : mAppointments) {
      if (appointment.getState().equals(State.PENDING.toString()))
        pendingAppointments.add(appointment);
      else if (appointment.getState().equals(State.ACCEPTED.toString()))
        acceptedAppointments.add(appointment);
      else
        declinedAppointments.add(appointment);
    }

    mDeclinedFragment = (AppointmentsFragment) AppointmentsFragment.newInstance(declinedAppointments);
    mAcceptedFragment = (AppointmentsFragment) AppointmentsFragment.newInstance(acceptedAppointments);

    fragmentsList.add(mDeclinedFragment);
    fragmentsList.add(AppointmentsFragment.newInstance(pendingAppointments));
    fragmentsList.add(mAcceptedFragment);

    return fragmentsList;
  }

  private void setupToolbarDisplay() {
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    final ActionBar actionBar = getSupportActionBar();
    View viewActionBar = getLayoutInflater().inflate(R.layout.actionbar_default, null);
    ActionBar.LayoutParams params = new ActionBar.LayoutParams(
        ActionBar.LayoutParams.WRAP_CONTENT,
        ActionBar.LayoutParams.MATCH_PARENT,
        Gravity.CENTER);
    TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.actionbar_textview);
    textviewTitle.setText(getActionBarTitle());
    Typeface robotoMedium = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");
    textviewTitle.setTypeface(robotoMedium);
    actionBar.setCustomView(viewActionBar, params);
    actionBar.setDisplayShowCustomEnabled(true);
    actionBar.setDisplayShowTitleEnabled(false);
    actionBar.setDisplayHomeAsUpEnabled(true);
    actionBar.setHomeButtonEnabled(false);

    getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_chevron_left_white));
  }

  @Override
  protected int getLayout() {
    return R.layout.activity_appointments;
  }

  @Override
  public void setUpComponent(IAppComponent appComponent) {
    DaggerIAppointmentsComponent.builder()
        .iAppComponent(appComponent)
        .appointmentsModule(new AppointmentsModule(this))
        .navigationModule(new NavigationModule(this))
        .build()
        .inject(this);

  }


  public static Intent makeIntent(Context context) {
    return new Intent(context, AppointmentsActivity.class);
  }

  public void showToast(String text) {
    Toast.makeText(this, text, Toast.LENGTH_LONG).show();
  }

  @Nullable
  @Override
  protected BasePresenter getPresenter() {
    return null;
  }

  @Override
  protected String getActionBarTitle() {
    return getResources().getString(R.string.my_appointments);
  }

  @Override
  protected int getSelfNavigationViewItem() {
    return 0;
  }

  @Override
  public void forceFinish() {
    this.finish();
  }

  @OnClick(R.id.declined_appointments_button)
  void onDeclinedAppointmentsButtonClick() {
    mViewPager.setCurrentItem(0);
  }

  @OnClick(R.id.pending_appointments_button)
  void onPendingAppointmentsButtonClick() {
    mViewPager.setCurrentItem(1);
  }

  @OnClick(R.id.accepted_appointments_button)
  void onAcceptedAppointmentsButtonClick() {
    mViewPager.setCurrentItem(2);
  }

  @Override
  public void setAppointments(List<Appointment> appointments) {

    mAppointments = appointments;

    List<Fragment> fragments = getFragments();
    mAppointmentsPageAdapter = new AppointmentsPageAdapter(getSupportFragmentManager(), fragments);
    mViewPager.setAdapter(mAppointmentsPageAdapter);
    mViewPager.setCurrentItem(1);

    mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
      @Override
      public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (mViewPager.getCurrentItem() == 1) {
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mPendingAppointmentsButton.setBackground(getResources().getDrawable(R.drawable.button_bg_rounded_white));
            mDeclinedAppointmentsButton.setBackground(null);
            mAcceptedAppointmentsButton.setBackground(null);
          }
        } else if (mViewPager.getCurrentItem() == 0) {
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mPendingAppointmentsButton.setBackground(null);
            mDeclinedAppointmentsButton.setBackground(getResources().getDrawable(R.drawable.button_bg_rounded_white));
            mAcceptedAppointmentsButton.setBackground(null);
          }
        } else {
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mPendingAppointmentsButton.setBackground(null);
            mDeclinedAppointmentsButton.setBackground(null);
            mAcceptedAppointmentsButton.setBackground(getResources().getDrawable(R.drawable.button_bg_rounded_white));
          }
        }
      }

      @Override
      public void onPageSelected(int position) {

      }

      @Override
      public void onPageScrollStateChanged(int state) {

      }
    });
  }

  @Override
  public void onAcceptButtonClick(Appointment appointment) {
    mPresenter.acceptAppointment(appointment);
    mAcceptedFragment.appointmentList.add(0, appointment);
    mAcceptedFragment.receivedAppointmentsAdapter.notifyDataSetChanged();
  }

  @Override
  public void onDeclineButtonClick(Appointment appointment) {
    mPresenter.declineAppointment(appointment);
    mDeclinedFragment.appointmentList.add(0, appointment);
    mDeclinedFragment.receivedAppointmentsAdapter.notifyDataSetChanged();
  }

  @Override
  public void onDeleteAppointmentClick(Appointment appointment) {
    mPresenter.deleteAppointment(appointment);
  }

  @Override
  public void openChatRoom(long doctorId) {
    mPresenter.openChatRoom(doctorId);
  }

  @Override
  public void startChatRoomActivity(ChatRoom chatRoom) {
    startActivity(ChatRoomActivity.makeIntent(this, chatRoom));
  }
}
