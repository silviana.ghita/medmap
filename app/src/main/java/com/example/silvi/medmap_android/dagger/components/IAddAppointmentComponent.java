package com.example.silvi.medmap_android.dagger.components;

import com.example.medmap_api.service.AppointmentService;
import com.example.silvi.medmap_android.dagger.ActivityScope;
import com.example.silvi.medmap_android.dagger.modules.AddAppointmentModule;
import com.example.silvi.medmap_android.domain.appointment.add.AddAppointmentInteractor;
import com.example.silvi.medmap_android.domain.appointment.hours.GetAvailableHoursForDateInteractor;
import com.example.silvi.medmap_android.domain.appointment.types.GetAppointmentTypesInteractor;
import com.example.silvi.medmap_android.ui.appointments.add.AddApointmentActivity;
import com.example.silvi.medmap_android.ui.appointments.add.AddAppointmentContract;
import com.example.silvi.medmap_android.ui.appointments.add.AddAppointmentPresenter;

import dagger.Component;

/**
 * Created by silvi on 07.04.2017.
 */
@ActivityScope
@Component(
    dependencies = IAppComponent.class,
    modules = AddAppointmentModule.class
)
public interface IAddAppointmentComponent extends IAppComponent {

  void inject(AddApointmentActivity addApointmentActivity);

  AddAppointmentContract.View getView();

  AppointmentService getAppointmentService();

  GetAvailableHoursForDateInteractor getAvailableHoursForDateInteractor();

  GetAppointmentTypesInteractor getAppointmentTypesInteractor();

  AddAppointmentInteractor getAddAppointmentInteractor();

  AddAppointmentPresenter getAddAppointmentPresenter();
}
