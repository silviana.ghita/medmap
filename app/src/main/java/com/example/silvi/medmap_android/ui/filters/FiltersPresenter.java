package com.example.silvi.medmap_android.ui.filters;

import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.model.User;
import com.example.silvi.medmap_android.domain.filters.GetDoctorUsersInteractor;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by silvi on 26.04.2017.
 */

public class FiltersPresenter implements FiltersContract.Presenter, GetDoctorUsersInteractor.Callback {

  FiltersContract.View mView;
  GetDoctorUsersInteractor mGetDoctorUsersInteractor;

  @Inject
  public FiltersPresenter(FiltersContract.View view, GetDoctorUsersInteractor getDoctorUsersInteractor) {
    this.mView = view;
    this.mGetDoctorUsersInteractor = getDoctorUsersInteractor;
  }

  @Override
  public void getDoctorUsers() {
    mGetDoctorUsersInteractor.execute(this);
  }


  @Override
  public void onGetDoctorUsersSuccess(List<User> doctorUsers) {
    mView.setDoctorUsers(doctorUsers);
  }

  @Override
  public void onGetDoctorUsersError(RestError restError) {

  }


}
