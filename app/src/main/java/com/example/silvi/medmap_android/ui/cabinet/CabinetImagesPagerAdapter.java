package com.example.silvi.medmap_android.ui.cabinet;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.medmap_commons.model.Image;

import java.util.List;

/**
 * Created by silvi on 24.03.2017.
 */

public class CabinetImagesPagerAdapter extends PagerAdapter {

  private Context mContext;
  private List<Image> mImages;

  public CabinetImagesPagerAdapter(Context context, List<Image> images) {
    this.mContext = context;
    this.mImages = images;
  }

  @Override
  public int getCount() {
    return (null != mImages) ? mImages.size() : 0;
  }

  @Override
  public boolean isViewFromObject(View view, Object object) {
    return view == object;
  }

  @Override
  public Object instantiateItem(ViewGroup container, int position) {

    ImageView imageView = new ImageView(mContext);

    ViewGroup.LayoutParams imageParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    imageView.setLayoutParams(imageParams);
    imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

    Glide
        .with(mContext)
        .load(mImages.get(position).getPath())
        .centerCrop()
        .crossFade()
        .into(imageView);

    container.addView(imageView);

    return imageView;
  }

  @Override
  public void destroyItem(ViewGroup container, int position, Object object) {
    container.removeView((ImageView) object);
  }
}
