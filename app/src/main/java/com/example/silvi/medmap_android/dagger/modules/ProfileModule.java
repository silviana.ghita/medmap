package com.example.silvi.medmap_android.dagger.modules;

import com.example.medmap_api.service.ImageService;
import com.example.medmap_api.service.ProfileService;
import com.example.silvi.medmap_android.domain.images.upload.UploadPictureInteractor;
import com.example.silvi.medmap_android.domain.images.upload.UploadPictureInteractorImpl;
import com.example.silvi.medmap_android.domain.profile.GetProfileDetailsInteractor;
import com.example.silvi.medmap_android.domain.profile.GetProfileDetailsInteractorImpl;
import com.example.silvi.medmap_android.domain.profile.SaveProfileDetailsInteractor;
import com.example.silvi.medmap_android.domain.profile.SaveProfileDetailsInteractorImpl;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.MainThread;
import com.example.silvi.medmap_android.ui.profile.ProfileActivity;
import com.example.silvi.medmap_android.ui.profile.ProfileContract;
import com.example.silvi.medmap_android.ui.profile.ProfilePresenter;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;

/**
 * Created by silvi on 13.03.2017.
 */

@Module
public class ProfileModule {

  private ProfileContract.View mView;

  public ProfileModule(ProfileActivity view) {
    this.mView = view;
  }

  @Provides
  public ProfileContract.View provideView() {
    return mView;
  }

  @Provides
  public ProfileService provideProfileService(RestAdapter restAdapter) {
    return restAdapter.create(ProfileService.class);
  }

  @Provides
  public ImageService provideImageService(RestAdapter restAdapter) {
    return restAdapter.create(ImageService.class);
  }

  @Provides
  public GetProfileDetailsInteractor provideGetProfileDetailsInteractor(Executor executor, MainThread mainThread, ProfileService profileService) {
    return new GetProfileDetailsInteractorImpl(executor, mainThread, profileService);
  }

  @Provides
  public SaveProfileDetailsInteractor provideSaveProfileDetailsInteractor(Executor executor, MainThread mainThread, ProfileService profileService) {
    return new SaveProfileDetailsInteractorImpl(executor, mainThread, profileService);
  }

  @Provides
  public UploadPictureInteractor provideUploadIdDocumentInteractor(Executor executor, MainThread mainThread, ImageService imageService) {
    return new UploadPictureInteractorImpl(executor, mainThread, imageService);
  }

  @Provides
  ProfilePresenter providePresenter(ProfileContract.View view, GetProfileDetailsInteractor getProfileDetailsInteractor, SaveProfileDetailsInteractor saveProfileDetailsInteractor, UploadPictureInteractor uploadIdDocumentInteractor) {
    return new ProfilePresenter(view, getProfileDetailsInteractor, saveProfileDetailsInteractor, uploadIdDocumentInteractor);
  }
}
