package com.example.silvi.medmap_android.ui.appointments.today;

import com.example.medmap_commons.model.Appointment;
import com.example.medmap_commons.model.RestError;
import com.example.silvi.medmap_android.domain.appointment.today.GetTodaysAppointmentsInteractor;
import com.example.silvi.medmap_android.domain.appointment.today.delay.AddDelayInteractor;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by silvi on 13.04.2017.
 */

public class TodayPresenter implements TodayContract.Presenter, GetTodaysAppointmentsInteractor.Callback, AddDelayInteractor.Callback {

  TodayContract.View mView;
  GetTodaysAppointmentsInteractor mGetTodaysAppointmentsInteractor;
  AddDelayInteractor mAddDelayInteractor;

  @Inject
  public TodayPresenter(TodayContract.View view, GetTodaysAppointmentsInteractor getTodaysAppointmentsInteractor, AddDelayInteractor addDelayInteractor) {
    this.mView = view;
    this.mGetTodaysAppointmentsInteractor = getTodaysAppointmentsInteractor;
    this.mAddDelayInteractor = addDelayInteractor;
  }

  public void getTodaysAppointments() {
    mGetTodaysAppointmentsInteractor.execute(this);
  }

  @Override
  public void onAddDelayClicked(String delay, List<Appointment> appointments) {
    mAddDelayInteractor.execute(this, delay, appointments);
  }


  @Override
  public void onGetTodaysAppointmentsSuccess(List<Appointment> appointments) {
    mView.initTodaysAppointments(appointments);
  }

  @Override
  public void onGetTodaysAppointmentsError(RestError restError) {

  }

  @Override
  public void onAddDelaySuccess(List<Appointment> appointmentList) {
    mView.hideDelayButton();
    mView.initTodaysAppointments(appointmentList);
  }

  @Override
  public void onAddDelayError(RestError restError) {

  }
}
