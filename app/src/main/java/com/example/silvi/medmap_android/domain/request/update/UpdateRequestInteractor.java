package com.example.silvi.medmap_android.domain.request.update;

import com.example.medmap_commons.model.Request;
import com.example.medmap_commons.model.RestError;

/**
 * Created by silvi on 27.03.2017.
 */

public interface UpdateRequestInteractor {

  interface Callback {

    void onUpdateRequestSuccess();

    void onUpdateRequestError(RestError restError);

  }

  void execute(Callback callback, Request request);
}