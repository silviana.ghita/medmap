package com.example.silvi.medmap_android.ui.widget;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import com.example.silvi.medmap_android.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by silvi on 23.02.2017.
 */

public class TypefaceEditText extends EditText {

  /*
   * Caches typefaces based on their file path and name, so that they don't have to be created
   * every time when they are referenced.
   */
  private static Map<String, Typeface> mTypefaces;

  public TypefaceEditText(final Context context) {
    this(context, null);
  }

  public TypefaceEditText(final Context context, final AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public TypefaceEditText(final Context context, final AttributeSet attrs, final int defStyle) {
    super(context, attrs, defStyle);
    if (!isInEditMode()) {
      if (mTypefaces == null) {
        mTypefaces = new HashMap<>();
      }

      final TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.TypefaceWidget);
      if (array != null) {
        final String typefaceAssetPath = array.getString(
            R.styleable.TypefaceWidget_customTypeface);

        if (typefaceAssetPath != null) {
          Typeface typeface = null;

          if (mTypefaces.containsKey(typefaceAssetPath)) {
            typeface = mTypefaces.get(typefaceAssetPath);
          } else {
            AssetManager assets = context.getAssets();
            typeface = Typeface.createFromAsset(assets, typefaceAssetPath);
            mTypefaces.put(typefaceAssetPath, typeface);
          }

          setTypeface(typeface);
        }
        array.recycle();
      }

      TypefaceEditText.this.setOnTouchListener(new OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
          TypefaceEditText.this.setFocusableInTouchMode(true);
          return false;
        }
      });
    }
  }
}
