package com.example.silvi.medmap_android.ui.appointments.add;

import com.example.medmap_commons.model.AppointmentType;
import com.example.medmap_commons.model.Cabinet;

import java.util.List;

/**
 * Created by silvi on 30.03.2017.
 */

public interface AddAppointmentContract {

  interface View {

    void setAvailableHours(List<Double> availableHours);

    void setAppointmentTypes(List<AppointmentType> appointmentTypes);

    Cabinet getCabinet();

    String getDate();

    String getTime();

    String getUserFirstName();

    String getUserLastName();

    String getUserPhoneNumber();

    AppointmentType getSelectedAppointmentType();

    String getComment();

    void showToast(String text);

    void appointmentAddedSuccessfully();
  }

  interface Presenter {

    void getAvailableHoursForDate(String date, Double duration, long cabinetId);

    void getAppointmentTypes();

    void addAppointment(boolean isExternal);

  }
}
