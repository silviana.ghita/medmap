package com.example.silvi.medmap_android.domain.chat.chatroom;

import com.example.medmap_api.service.ChatService;
import com.example.medmap_commons.model.ChatRoom;
import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.util.PrintLog;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.Interactor;
import com.example.silvi.medmap_android.executor.MainThread;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by silvi on 02.05.2017.
 */

public class OpenChatRoomInteractorImpl implements OpenChatRoomInteractor, Interactor {

  private final Executor mExecutor;
  private final MainThread mMainThread;
  private final ChatService mChatService;


  public OpenChatRoomInteractorImpl(Executor executor, MainThread mainThread, ChatService chatService) {
    mExecutor = executor;
    mMainThread = mainThread;
    mChatService = chatService;
  }

  private Long mOtherUserId;
  private Callback mCallback;

  @Override
  public void execute(Callback callback, Long otherUserId) {
    if (null == callback) {
      throw new IllegalArgumentException(
          "Callback can't be null, the client of this interactor needs to get the response " +
              "in the callback");
    }
    this.mCallback = callback;
    this.mOtherUserId = otherUserId;
    this.mExecutor.run(this);
  }

  @Override
  public void run() {
    mChatService.openChatRoom(mOtherUserId, new retrofit.Callback<ChatRoom>() {
      @Override
      public void success(ChatRoom chatRoom, Response response) {
        notifySuccess(chatRoom);
      }

      @Override
      public void failure(RetrofitError error) {
        RestError restError = null;
        try {
          restError = (RestError) error.getBodyAs(RestError.class);
        } catch (Exception e) {
          PrintLog.printStackTrace(getClass().getSimpleName(), e);
        }
        notifyError(restError);

      }
    });
  }

  private void notifySuccess(final ChatRoom chatRoom) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onOpenChatRoomSuccess(chatRoom);
      }
    });
  }

  private void notifyError(final RestError restError) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onOpenChatRoomError(restError);
      }
    });
  }

}
