package com.example.silvi.medmap_android.ui.base;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.dagger.components.DaggerIAppComponent;
import com.example.silvi.medmap_android.dagger.components.IAppComponent;
import com.example.silvi.medmap_android.dagger.modules.AppModule;
import com.example.silvi.medmap_android.logic.UserLogic;
import com.example.silvi.medmap_android.util.LifeCycleHandler;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by silvi on 22.02.2017.
 */

public class MedMapApplication extends MultiDexApplication {

  private IAppComponent mAppComponent;
  private static MedMapApplication mInstance;

  @Override
  public void onCreate() {
    super.onCreate();
    registerActivityLifecycleCallbacks(new LifeCycleHandler());
    mInstance = this;
    setupFacebook();
    setupGraph();
    new UserLogic().startUserSession();
    CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
        .setDefaultFontPath(getString(R.string.roboto_regular))
        .setFontAttrId(R.attr.fontPath)
        .build()
    );
  }


  public static MedMapApplication get(Context context) {
    return (MedMapApplication) context.getApplicationContext();
  }

  private void setupGraph() {
    mAppComponent = DaggerIAppComponent.builder()
        .appModule(new AppModule(this))
        .build();
  }

  public IAppComponent getComponent() {
    return mAppComponent;
  }

  public static MedMapApplication getInstance() {
    return mInstance;
  }

  public void setupFacebook() {
    Permission[] permissions = new Permission[]{
        Permission.USER_ABOUT_ME,
        Permission.EMAIL
    };
    SimpleFacebookConfiguration configuration = new SimpleFacebookConfiguration.Builder()
        .setAppId(getResources().getString(R.string.facebook_app_id))
        .setNamespace(getResources().getString(R.string.facebook_namespace))
        .setPermissions(permissions)
        .build();
    SimpleFacebook.setConfiguration(configuration);
  }
}
