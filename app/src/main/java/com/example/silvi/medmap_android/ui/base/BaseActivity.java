package com.example.silvi.medmap_android.ui.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Selection;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.dagger.components.IAppComponent;
import com.example.silvi.medmap_android.util.UIUtils;

import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by silvi on 22.02.2017.
 */

public abstract class BaseActivity extends AppCompatActivity {

  private static final int REQUEST_CHECK_SETTINGS = 2;

  protected Toolbar mActionBarToolbar;
  private AlertDialog.Builder connectionDialog;
  private AlertDialog alertDialog;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(getLayout());

    injectDependencies();
    ButterKnife.bind(this);
    setupActionBarToolbar();

  }

  @Override
  protected void onStart() {
    super.onStart();
  }

  @Override
  protected void onStop() {
    super.onStop();
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == REQUEST_CHECK_SETTINGS) {
      switch (resultCode) {
        case Activity.RESULT_OK:
          break;
        case Activity.RESULT_CANCELED:
          break;
      }
    }
    super.onActivityResult(requestCode, resultCode, data);
  }

  @Override
  protected void onResume() {
    super.onResume();
    if (getPresenter() != null)
      getPresenter().onResume();

  }

  protected void setupActionBarToolbar() {
    mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
    if (mActionBarToolbar != null) {
      setSupportActionBar(mActionBarToolbar);
      setActionBarTitle(getActionBarTitle());
      if (getClass() != NavigationViewActivity.class) {
        setDisplayHomeAsUpEnabled();
        UIUtils.disableStatusBarTranslucency(getWindow(), getResources());
      }
    }
  }

  protected void setActionBarTitle(String title) {
    if (getSupportActionBar() != null) {
      getSupportActionBar().setTitle(title);
    }
  }

  protected void setDisplayHomeAsUpEnabled() {
    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
  }


  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        quit();
        return true;
    }
    return false;
  }



  protected void quit() {
    BaseActivity.this.finish();
  }

  /**
   * @return The layout resource that will be inflated as the root view of this activity.
   */
  protected abstract int getLayout();

  private void injectDependencies() {
    setUpComponent(MedMapApplication.get(this).getComponent());
  }

  public abstract void setUpComponent(IAppComponent appComponent);

  protected IAppComponent getAppComponent() {
    return MedMapApplication.get(this).getComponent();
  }

  /**
   * @return The presenter attached to the activity. This must extends from {@link BasePresenter}
   */
  @Nullable
  protected abstract BasePresenter getPresenter();

  @Override
  protected void attachBaseContext(Context newBase) {
    super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
  }

  protected class EditorActionListener implements TextView.OnEditorActionListener {

    private EditText nextElement;
    private ButtonKeyListener listener;

    public EditorActionListener() {
    }

    public EditorActionListener(EditText nextElement) {
      this.nextElement = nextElement;
    }

    public void setListener(ButtonKeyListener listener) {
      this.listener = listener;
    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
      if (actionId == EditorInfo.IME_ACTION_NEXT && nextElement != null) {
        Selection.setSelection(nextElement.getText(), nextElement.getSelectionEnd());
        nextElement.requestFocus();
        return true;
      } else if (actionId == EditorInfo.IME_ACTION_DONE) {
        if (listener != null) {
          listener.onDoneClicked();
        }
      }
      return false;
    }
  }


  protected interface ButtonKeyListener {
    void onDoneClicked();
  }

  protected abstract String getActionBarTitle();


}
