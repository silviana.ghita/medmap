package com.example.silvi.medmap_android.ui.auth.register;

import com.example.medmap_commons.model.Doctor;
import com.example.medmap_commons.model.LoggedInUser;
import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.model.User;
import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.domain.register.RegisterDoctorInteractor;
import com.example.silvi.medmap_android.domain.register.RegisterUserInteractor;
import com.example.silvi.medmap_android.ui.base.MedMapApplication;
import com.example.silvi.medmap_android.util.EmailFormatValidator;

import javax.inject.Inject;

/**
 * Created by silvi on 24.02.2017.
 */

public class RegisterPresenter implements RegisterContract.Presenter, RegisterUserInteractor.Callback, RegisterDoctorInteractor.Callback {

  private RegisterContract.View mView;
  private RegisterUserInteractor mRegisterUserInteractor;
  private RegisterDoctorInteractor mRegisterDoctorInteractor;

  @Inject
  public RegisterPresenter(RegisterContract.View view, RegisterUserInteractor registerUserInteractor, RegisterDoctorInteractor registerDoctorInteractor) {
    this.mView = view;
    this.mRegisterUserInteractor = registerUserInteractor;
    this.mRegisterDoctorInteractor = registerDoctorInteractor;
  }


  @Override
  public void onRegisterClick(String screenType) {
    if (isFormValid(screenType)) {
      if (screenType.equals("PACIENT_REGISTER")) {
        User user = new User();
        user.setEmail(mView.getEmail());
        user.setPassword(mView.getPassword());
        user.setFirstName(mView.getFirstName());
        user.setLastName(mView.getLastName());
        user.setPhoneNumber(mView.getPhoneNumber());
        mRegisterUserInteractor.execute(this, user);
      } else {
        Doctor doctor = new Doctor();
        doctor.setEmail(mView.getEmail());
        doctor.setPhoneNumber(mView.getPhoneNumber());
        doctor.setFirstName(mView.getFirstName());
        doctor.setLastName(mView.getLastName());
        doctor.setSpecialization(mView.getSpecialization());
        doctor.setPassword(mView.getPassword());
        mRegisterDoctorInteractor.execute(this, doctor);
      }
    }
  }

  private boolean isFormValid(String screenType) {

    EmailFormatValidator validator = new EmailFormatValidator();

    if (mView.getFirstName().isEmpty() || mView.getFirstName() == null) {
      mView.showError(MedMapApplication.getInstance().getResources().getString(R.string.complete_all_fields));
      return false;
    } else if (mView.getLastName().isEmpty() || mView.getLastName() == null) {
      mView.showError(MedMapApplication.getInstance().getResources().getString(R.string.complete_all_fields));
      return false;
    } else if (mView.getEmail().isEmpty() || mView.getEmail() == null) {
      mView.showError(MedMapApplication.getInstance().getResources().getString(R.string.complete_all_fields));
      return false;
    } else if (mView.getPhoneNumber().isEmpty() || mView.getPhoneNumber() == null) {
      mView.showError(MedMapApplication.getInstance().getResources().getString(R.string.complete_all_fields));
      return false;
    } else if (mView.getPassword().isEmpty() || mView.getPassword() == null) {
      mView.showError(MedMapApplication.getInstance().getResources().getString(R.string.complete_all_fields));
      return false;
    } else if (mView.getRetype().isEmpty() || mView.getRetype() == null) {
      mView.showError(MedMapApplication.getInstance().getResources().getString(R.string.complete_all_fields));
      return false;
    } else if (!mView.getPassword().equals(mView.getRetype())) {
      mView.showError(MedMapApplication.getInstance().getResources().getString(R.string.password_and_retype_must_be_the_same));
      return false;
    } else if (!validator.validate(mView.getEmail())) {
      mView.showError(MedMapApplication.getInstance().getResources().getString(R.string.incorrect_email_format));
      return false;
    } else if (mView.getPassword().length() < 6) {
      mView.showError(MedMapApplication.getInstance().getResources().getString(R.string.password_is_too_short));
      return false;
    }


    return true;
  }

  @Override
  public void onRegisterSuccess(LoggedInUser loggedInUser) {
    mView.showError(MedMapApplication.getInstance().getResources().getString(R.string.register_success_dialog_message));
    mView.startNextActivity();
  }

  @Override
  public void onRegisterError(RestError restError) {
    mView.showError(MedMapApplication.getInstance().getResources().getString(R.string.email_already_in_use));
  }
}
