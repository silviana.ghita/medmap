package com.example.silvi.medmap_android.ui.cabinet.add;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.medmap_commons.model.Category;
import com.example.medmap_commons.model.Image;
import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.adapters.NamedEntityAdapter;
import com.example.silvi.medmap_android.commons.IntentKeys;
import com.example.silvi.medmap_android.dagger.components.DaggerIAddCabinetComponent;
import com.example.silvi.medmap_android.dagger.components.IAppComponent;
import com.example.silvi.medmap_android.dagger.modules.AddCabinetModule;
import com.example.silvi.medmap_android.ui.CustomPhotoGalleryActivity;
import com.example.silvi.medmap_android.ui.base.BaseActivity;
import com.example.silvi.medmap_android.ui.base.BasePresenter;
import com.example.silvi.medmap_android.ui.base.FinishActivities;
import com.example.silvi.medmap_android.ui.base.FinishCallback;
import com.example.silvi.medmap_android.ui.base.MedMapApplication;
import com.example.silvi.medmap_android.ui.cabinet.update.HorizontalPhotosAdapter;
import com.example.silvi.medmap_android.ui.home.HomeActivity;
import com.example.silvi.medmap_android.util.Specializations;
import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by silvi on 14.03.2017.
 */

public class AddCabinetActivity extends BaseActivity implements AddCabinetContract.View, FinishCallback {

  private final int PICK_IMAGE_MULTIPLE = 1;

  @Bind(R.id.cabinet_name_input)
  EditText mCabinetName;
  @Bind(R.id.description_input)
  EditText mDescription;
  @Bind(R.id.specialization_layout)
  LinearLayout mSpecializationLayout;
  @Bind(R.id.category_layout)
  LinearLayout categoryLayout;
  @Bind(R.id.specialization_spinner)
  Spinner mSpecializationSpinner;
  @Bind(R.id.category_spinner)
  Spinner mCategorySpinner;
  @Bind(R.id.confirmButton)
  Button confirmButton;
  @Bind(R.id.number_of_selected_photos)
  TextView selectedPhotos;
  @Bind(R.id.horizontal_recycler_view)
  RecyclerView mCabinetImagesRecyclerView;

  private LatLng latLng;
  private NamedEntityAdapter mSpecialzationsAdapter;
  private NamedEntityAdapter mCategoriesAdapter;
  HorizontalPhotosAdapter mHorizontalAdapter;

  private boolean[] thumbnailsSelection = null;
  private ArrayList<String> imagesPathList;
  private List<Image> imagesToAdd;
  private List<Image> cabinetImages;



  @Inject
  AddCabinetPresenter mPresenter;


  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    latLng = getIntent().getParcelableExtra(IntentKeys.LATLNG);

    cabinetImages = new ArrayList<>();


    FinishActivities.addActivity(this);
    setupToolbarDisplay();

    initSpecializationSpinner();
    initCategorySpinner();
    setKeyboardVisibility();

    selectedPhotos.setText(MedMapApplication.getInstance().getResources()
        .getString(R.string.selected_photos, 0));

    mHorizontalAdapter = new HorizontalPhotosAdapter(this, cabinetImages);
    mCabinetImagesRecyclerView.setAdapter(mHorizontalAdapter);
    mHorizontalAdapter.notifyDataSetChanged();

    LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
    mCabinetImagesRecyclerView.setLayoutManager(horizontalLayoutManagaer);
    mCabinetImagesRecyclerView.setAdapter(mHorizontalAdapter);

  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (resultCode == RESULT_OK) {
      if (requestCode == PICK_IMAGE_MULTIPLE) {
        imagesPathList = new ArrayList<String>();
        imagesToAdd = new ArrayList<Image>();
        String[] imagesPath = data.getStringExtra("data").split("\\|");
        thumbnailsSelection = data.getBooleanArrayExtra("thumbnailsSelection");
        for (int i = 0; i < imagesPath.length; i++) {
          imagesPathList.add(imagesPath[i]);
          Image image = new Image();
          image.setPath(imagesPath[i]);
          imagesToAdd.add(image);
        }
        selectedPhotos.setText(MedMapApplication.getInstance().getResources()
            .getString(R.string.selected_photos, imagesPath.length));
      }
    }
    mHorizontalAdapter.addImagesToList(imagesToAdd);
    mHorizontalAdapter.notifyDataSetChanged();
  }

  private void setKeyboardVisibility() {
    final View activityRootView = findViewById(R.id.parent);
    DisplayMetrics metrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(metrics);
    final int height = metrics.heightPixels;
    activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
      @Override
      public void onGlobalLayout() {
        int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight();
        if (heightDiff > (height / 4)) { // if more than 1/4 of the screen, its probably a keyboard...
          confirmButton.setVisibility(View.INVISIBLE);
        } else {
          confirmButton.setVisibility(View.VISIBLE);
        }
      }
    });
  }

  private void initSpecializationSpinner() {
    Specializations[] specializations = Specializations.values();
    List<String> specializationList = new ArrayList<>();
    for (int i = 0; i < specializations.length; i++)
      specializationList.add(specializations[i].toString());

    mSpecialzationsAdapter = new NamedEntityAdapter(this, R.layout.simple_spinner_item, specializationList);
    mSpecializationSpinner.setAdapter(mSpecialzationsAdapter);
  }

  private void initCategorySpinner() {
    Category[] categories = Category.values();
    List<String> categoriesList = new ArrayList<>();
    for (int i = 0; i < categories.length; i++)
      categoriesList.add(categories[i].toString());

    mCategoriesAdapter = new NamedEntityAdapter(this, R.layout.simple_spinner_item, categoriesList);
    mCategorySpinner.setAdapter(mCategoriesAdapter);
  }


  private void setupToolbarDisplay() {
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    final ActionBar actionBar = getSupportActionBar();
    View viewActionBar = getLayoutInflater().inflate(R.layout.actionbar_default, null);
    ActionBar.LayoutParams params = new ActionBar.LayoutParams(
        ActionBar.LayoutParams.WRAP_CONTENT,
        ActionBar.LayoutParams.MATCH_PARENT,
        Gravity.CENTER);
    TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.actionbar_textview);
    textviewTitle.setText(getActionBarTitle());
    Typeface robotoMedium = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");
    textviewTitle.setTypeface(robotoMedium);
    actionBar.setCustomView(viewActionBar, params);
    actionBar.setDisplayShowCustomEnabled(true);
    actionBar.setDisplayShowTitleEnabled(false);
    actionBar.setDisplayHomeAsUpEnabled(true);
    actionBar.setHomeButtonEnabled(false);

    getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_chevron_left_white));


    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        finish();
        startActivity(HomeActivity.makeIntent(getApplicationContext()));
      }
    });
  }

  @OnClick(R.id.confirmButton)
  void onConfirmButtonClick() {

    if (imagesPathList != null) {
      if (imagesPathList.size() > 0) {
        boolean isNewImageDeleted;
        for (int i = 0; i < imagesPathList.size(); i++) {
          isNewImageDeleted = true;
          for (Image currentImage : cabinetImages) {
            if (imagesPathList.get(i).equals(currentImage.getPath()))
              isNewImageDeleted = false;
          }
          if (isNewImageDeleted) {
            imagesPathList.remove(i);
            i--;
          }
        }
      }
      mPresenter.onConfirmButtonClicked();
    }
  }

  @OnClick(R.id.add_photos_btn)
  void onAddPhotosClick() {
    startActivityForResult(CustomPhotoGalleryActivity.makeIntent(this, thumbnailsSelection), PICK_IMAGE_MULTIPLE);
  }


  @Override
  protected int getLayout() {
    return R.layout.activity_add_cabinet;
  }

  @Override
  public void setUpComponent(IAppComponent appComponent) {
    DaggerIAddCabinetComponent.builder()
        .iAppComponent(appComponent)
        .addCabinetModule(new AddCabinetModule(this))
        .build()
        .inject(this);

  }

  @Nullable
  @Override
  protected BasePresenter getPresenter() {
    return null;
  }

  @Override
  protected String getActionBarTitle() {
    return getResources().getString(R.string.add_cabinet);
  }

  public static Intent makeIntent(Context context, LatLng latLng) {
    Intent intent = new Intent(context, AddCabinetActivity.class);
    intent.putExtra(IntentKeys.LATLNG, latLng);
    return intent;
  }

  @Override
  public void forceFinish() {
    this.finish();
  }

  @Override
  public String getCabinetName() {
    return mCabinetName.getText().toString();
  }

  @Override
  public String getDescription() {
    return mDescription.getText().toString();
  }

  @Override
  public String getSpecialization() {
    return mSpecializationSpinner.getSelectedItem().toString();
  }

  @Override
  public String getCategory() {
    return mCategorySpinner.getSelectedItem().toString();
  }

  @Override
  public LatLng getLatLng() {
    return latLng;
  }

  @Override
  public void showToast(String text) {
    Toast.makeText(this, text, Toast.LENGTH_LONG).show();
  }

  @Override
  public void closeActivity() {
    startActivity(HomeActivity.makeIntent(this));
    finish();

  }

  @Override
  public void deleteCabinetPicture(String imagePath, Integer position, Image image) {
    cabinetImages.remove(image);
    mHorizontalAdapter.notifyItemRemoved(position);
    mHorizontalAdapter.notifyDataSetChanged();

  }

  @Override
  public List<File> getImagesPath() {
    List<File> imagesPaths = new ArrayList<>();
    for (String path : imagesPathList) {
      File file = new File(path);
      imagesPaths.add(file);
    }
    return imagesPaths;
  }
}
