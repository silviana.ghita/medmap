package com.example.silvi.medmap_android.dagger.components;

import com.example.medmap_api.service.CabinetService;
import com.example.medmap_api.service.ImageService;
import com.example.silvi.medmap_android.dagger.ActivityScope;
import com.example.silvi.medmap_android.dagger.modules.UpdateCabinetModule;
import com.example.silvi.medmap_android.domain.cabinet.update.UpdateCabinetInteractor;
import com.example.silvi.medmap_android.domain.images.delete.DeleteCabinetPictureInteractor;
import com.example.silvi.medmap_android.domain.images.upload.UploadPictureInteractor;
import com.example.silvi.medmap_android.ui.cabinet.update.UpdateCabinetActivity;
import com.example.silvi.medmap_android.ui.cabinet.update.UpdateCabinetContract;
import com.example.silvi.medmap_android.ui.cabinet.update.UpdateCabinetPresenter;

import dagger.Component;

/**
 * Created by silvi on 17.03.2017.
 */

@ActivityScope
@Component(
    dependencies = IAppComponent.class,
    modules = UpdateCabinetModule.class
)
public interface IUpdateCabinetComponent extends IAppComponent {

  void inject(UpdateCabinetActivity updateCabinetActivity);

  UpdateCabinetContract.View getView();

  CabinetService getCabinetService();

  ImageService getImageService();

  UpdateCabinetInteractor getUpdateCabinetInteractor();

  UploadPictureInteractor getUploadDocumentInteractor();

  DeleteCabinetPictureInteractor getDeleteCabinetPictureInteractor();

  UpdateCabinetPresenter getUpdateCabinetPresenter();
}
