package com.example.silvi.medmap_android.ui.requests;

import com.example.medmap_commons.model.Request;
import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.model.State;
import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.domain.request.delete.DeleteRequestInteractor;
import com.example.silvi.medmap_android.domain.request.get.GetRequestsInteractor;
import com.example.silvi.medmap_android.domain.request.update.UpdateRequestInteractor;
import com.example.silvi.medmap_android.ui.base.MedMapApplication;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by silvi on 25.03.2017.
 */

public class RequestsPresenter implements RequestsContract.Presenter, GetRequestsInteractor.Callback, UpdateRequestInteractor.Callback, DeleteRequestInteractor.Callback {

  private RequestsContract.View mView;
  private GetRequestsInteractor mGetRequestsInteractor;
  private UpdateRequestInteractor mUpdateRequestInteractor;
  private DeleteRequestInteractor mDeleteRequestInteractor;

  @Inject
  public RequestsPresenter(RequestsContract.View view, GetRequestsInteractor getRequestsInteractor, UpdateRequestInteractor updateRequestInteractor, DeleteRequestInteractor deleteRequestInteractor) {
    this.mView = view;
    this.mGetRequestsInteractor = getRequestsInteractor;
    this.mUpdateRequestInteractor = updateRequestInteractor;
    this.mDeleteRequestInteractor = deleteRequestInteractor;
  }

  @Override
  public void getRequests() {
    mGetRequestsInteractor.execute(this);
  }

  @Override
  public void acceptRequest(Request request) {
    request.setState(State.ACCEPTED);
    mUpdateRequestInteractor.execute(this, request);
  }

  @Override
  public void declineRequest(Request request) {
    request.setState(State.DECLINED);
    mUpdateRequestInteractor.execute(this, request);
  }

  @Override
  public void deleteRequest(Request request) {
    mDeleteRequestInteractor.execute(this, request);
  }


  @Override
  public void onGetDoctorRequestsSuccess(List<Request> requests) {
      mView.setRequests(requests);

  }

  @Override
  public void onGetDoctorRequestsError(RestError restError) {

  }

  @Override
  public void onUpdateRequestSuccess() {

  }

  @Override
  public void onUpdateRequestError(RestError restError) {

  }

  @Override
  public void onDeleteRequestSuccess() {
    mView.showToast(MedMapApplication.getInstance().getResources().getString(R.string.request_deleted_successfully));
  }

  @Override
  public void onDeleteRequestError(RestError restError) {

  }
}
