package com.example.silvi.medmap_android.ui.appointments;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.medmap_commons.model.Appointment;
import com.example.medmap_commons.model.Role;
import com.example.medmap_commons.model.UserSession;
import com.example.silvi.medmap_android.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by silvi on 11.04.2017.
 */

public class AppointmentsFragment extends Fragment {

  public static final String APPOINTMENTS_LIST = "APPOINTMENTS_LIST";

  List<Appointment> appointmentList;
  SentAppointmentsAdapter sentAppointmentsAdapter;
  ReceivedAppointmentsAdapter receivedAppointmentsAdapter;

  TextView emptyCase;
  RecyclerView recyclerView;


  public void onAcceptAppointmentClick(Appointment appointment, int position) {
    appointmentList.remove(position);
    receivedAppointmentsAdapter.notifyItemRemoved(position);
    receivedAppointmentsAdapter.notifyDataSetChanged();
    verifyEmptyCase();
    ((AppointmentsActivity) getActivity()).onAcceptButtonClick(appointment);
  }

  public void onDeclineAppointmentClick(Appointment appointment, int position) {
    appointmentList.remove(position);
    receivedAppointmentsAdapter.notifyItemRemoved(position);
    receivedAppointmentsAdapter.notifyDataSetChanged();
    verifyEmptyCase();
    ((AppointmentsActivity) getActivity()).onDeclineButtonClick(appointment);
  }

  public void onDeleteAppointmentClick(Appointment appointment, int position) {
    appointmentList.remove(appointment);
    sentAppointmentsAdapter.notifyItemRemoved(position);
    verifyEmptyCase();
    ((AppointmentsActivity) getActivity()).onDeleteAppointmentClick(appointment);
  }

  public void openChatRoom(long doctorId) {
    ((AppointmentsActivity) getActivity()).openChatRoom(doctorId);
  }

  public static final Fragment newInstance(ArrayList<Appointment> appointmentList) {
    AppointmentsFragment f = new AppointmentsFragment();
    Bundle bdl = new Bundle(1);
    bdl.putParcelableArrayList(APPOINTMENTS_LIST, appointmentList);
    f.setArguments(bdl);
    return f;
  }

  public void verifyEmptyCase() {
    if (appointmentList.size() == 0) {
      emptyCase.setVisibility(View.VISIBLE);
      recyclerView.setVisibility(View.GONE);
    }
  }

  public int getFirstCompletedAppointmentPosition() {
    for (int i = 0; i < appointmentList.size(); i++)
      if (parseStringToDate(appointmentList.get(i).getDate()).before(new Date())) {
        return i;
      }
    return -1;
  }


  public Date parseStringToDate(String stringDate) {
    DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
    Date date = null;
    try {
      date = format.parse(stringDate);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return date;
  }



  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    appointmentList = new ArrayList<>();
    appointmentList = getArguments().getParcelableArrayList(APPOINTMENTS_LIST);
    View v = inflater.inflate(R.layout.fragment_requests, container, false);
    emptyCase = (TextView) v.findViewById(R.id.empty_state);
    emptyCase.setText(getResources().getString(R.string.no_appointments_found));
    recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);


    recyclerView.setHasFixedSize(true);
    recyclerView.setItemAnimator(new DefaultItemAnimator());
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    if (UserSession.getUserRole().equals(Role.ROLE_DOCTOR)) {
      receivedAppointmentsAdapter = new ReceivedAppointmentsAdapter(this, appointmentList);
    }

    if (appointmentList != null && appointmentList.size() > 0) {
      emptyCase.setVisibility(View.GONE);
      recyclerView.setVisibility(View.VISIBLE);
      if (UserSession.getUserRole().equals(Role.ROLE_DOCTOR)) {
        recyclerView.setAdapter(receivedAppointmentsAdapter);
      } else {
        sentAppointmentsAdapter = new SentAppointmentsAdapter(this, appointmentList);
        recyclerView.setAdapter(sentAppointmentsAdapter);
      }
    } else {
      emptyCase.setVisibility(View.VISIBLE);
      recyclerView.setVisibility(View.GONE);
    }

    return v;
  }


}
