package com.example.silvi.medmap_android.domain.appointment.today.delay;

import com.example.medmap_api.service.AppointmentService;
import com.example.medmap_commons.model.Appointment;
import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.util.PrintLog;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.Interactor;
import com.example.silvi.medmap_android.executor.MainThread;

import java.util.List;

import javax.inject.Inject;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by silvi on 18.04.2017.
 */

public class AddDelayInteractorImpl implements Interactor, AddDelayInteractor {

  private final Executor mExecutor;
  private final MainThread mMainThread;
  private final AppointmentService mAppointmentService;

  private Callback mCallback;
  private String mDelay;
  private List<Appointment> mAppointments;

  @Inject
  public AddDelayInteractorImpl(Executor executor, MainThread mainThread, AppointmentService appointmentService) {
    this.mAppointmentService = appointmentService;
    this.mExecutor = executor;
    this.mMainThread = mainThread;
  }


  @Override
  public void execute(Callback callback, String delay, List<Appointment> appointmentList) {
    if (null == callback) {
      throw new IllegalArgumentException(
          "Callback can't be null, the client of this interactor needs to get the response " +
              "in the callback");
    }
    this.mCallback = callback;
    this.mDelay = delay;
    this.mAppointments = appointmentList;
    this.mExecutor.run(this);
  }

  @Override
  public void run() {
    mAppointmentService.delayTodayProgram(mAppointments, mDelay, new retrofit.Callback<List<Appointment>>() {
      @Override
      public void success(List<Appointment> appointments, Response response) {
        notifySuccess(appointments);
      }

      @Override
      public void failure(RetrofitError error) {
        RestError restError = null;
        try {
          restError = (RestError) error.getBodyAs(RestError.class);
        } catch (Exception e) {
          PrintLog.printStackTrace(getClass().getSimpleName(), e);
        }
        notifyError(restError);
      }
    });
  }

  private void notifySuccess(final List<Appointment> appointments) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onAddDelaySuccess(appointments);
      }
    });
  }

  private void notifyError(final RestError restError) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onAddDelayError(restError);
      }
    });
  }


}
