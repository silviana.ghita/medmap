package com.example.silvi.medmap_android.domain.chat;

import com.example.medmap_api.service.ChatService;
import com.example.medmap_commons.model.ChatRoom;
import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.util.PrintLog;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.Interactor;
import com.example.silvi.medmap_android.executor.MainThread;

import java.util.List;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by silvi on 05.05.2017.
 */

public class GetChatRoomsInteractorImpl implements Interactor, GetChatRoomsInteractor {


  private final Executor mExecutor;
  private final MainThread mMainThread;
  private final ChatService mChatService;


  public GetChatRoomsInteractorImpl(Executor executor, MainThread mainThread, ChatService chatService) {
    mExecutor = executor;
    mMainThread = mainThread;
    mChatService = chatService;
  }

  private Callback mCallback;

  @Override
  public void execute(Callback callback) {
    if (null == callback) {
      throw new IllegalArgumentException(
          "Callback can't be null, the client of this interactor needs to get the response " +
              "in the callback");
    }
    this.mCallback = callback;
    this.mExecutor.run(this);
  }

  @Override
  public void run() {
    mChatService.getChatRooms(new retrofit.Callback<List<ChatRoom>>() {
      @Override
      public void success(List<ChatRoom> chatRooms, Response response) {
        notifySuccess(chatRooms);
      }

      @Override
      public void failure(RetrofitError error) {
        RestError restError = null;
        try {
          restError = (RestError) error.getBodyAs(RestError.class);
        } catch (Exception e) {
          PrintLog.printStackTrace(getClass().getSimpleName(), e);
        }
        notifyError(restError);

      }

    });
  }

  private void notifySuccess(final List<ChatRoom> chatRooms) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onGetChatRoomsSuccess(chatRooms);
      }
    });
  }

  private void notifyError(final RestError restError) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onGetChatRoomsError(restError);
      }
    });
  }

}
