package com.example.silvi.medmap_android.dagger.components;

import com.example.medmap_api.service.RequestService;
import com.example.silvi.medmap_android.dagger.ActivityScope;
import com.example.silvi.medmap_android.dagger.modules.NavigationModule;
import com.example.silvi.medmap_android.dagger.modules.RequestsModule;
import com.example.silvi.medmap_android.domain.request.delete.DeleteRequestInteractor;
import com.example.silvi.medmap_android.domain.request.get.GetRequestsInteractor;
import com.example.silvi.medmap_android.domain.request.update.UpdateRequestInteractor;
import com.example.silvi.medmap_android.ui.requests.RequestsActivity;
import com.example.silvi.medmap_android.ui.requests.RequestsContract;
import com.example.silvi.medmap_android.ui.requests.RequestsPresenter;

import dagger.Component;

/**
 * Created by silvi on 25.03.2017.
 */

@ActivityScope
@Component(
    dependencies = IAppComponent.class,
    modules = {
        RequestsModule.class,
        NavigationModule.class
    }
)
public interface IRequestsComponent {

  void inject(RequestsActivity requestsActivity);

  RequestsContract.View getView();

  RequestService getRequestService();

  GetRequestsInteractor getRequestsInteractor();

  UpdateRequestInteractor updateRequestInteractor();

  DeleteRequestInteractor deleteRequestInteractor();

  RequestsPresenter getRequestsPresenter();
}
