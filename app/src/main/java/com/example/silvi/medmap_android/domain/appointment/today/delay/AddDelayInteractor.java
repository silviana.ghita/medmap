package com.example.silvi.medmap_android.domain.appointment.today.delay;

import com.example.medmap_commons.model.Appointment;
import com.example.medmap_commons.model.RestError;

import java.util.List;

/**
 * Created by silvi on 18.04.2017.
 */

public interface AddDelayInteractor {

  interface Callback {

    void onAddDelaySuccess(List<Appointment> appointmentList);

    void onAddDelayError(RestError restError);

  }

  void execute(Callback callback, String delay, List<Appointment> appointments);
}
