package com.example.silvi.medmap_android.ui.cabinet;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

import com.bumptech.glide.Glide;
import com.example.medmap_commons.model.Cabinet;
import com.example.medmap_commons.model.ChatRoom;
import com.example.medmap_commons.model.Image;
import com.example.medmap_commons.model.Request;
import com.example.medmap_commons.model.Role;
import com.example.medmap_commons.model.State;
import com.example.medmap_commons.model.UserSession;
import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.commons.IntentKeys;
import com.example.silvi.medmap_android.dagger.components.DaggerICabinetComponent;
import com.example.silvi.medmap_android.dagger.components.IAppComponent;
import com.example.silvi.medmap_android.dagger.modules.CabinetModule;
import com.example.silvi.medmap_android.ui.appointments.add.AddApointmentActivity;
import com.example.silvi.medmap_android.ui.base.BaseActivity;
import com.example.silvi.medmap_android.ui.base.BasePresenter;
import com.example.silvi.medmap_android.ui.base.FinishActivities;
import com.example.silvi.medmap_android.ui.base.FinishCallback;
import com.example.silvi.medmap_android.ui.base.MedMapApplication;
import com.example.silvi.medmap_android.ui.messages.chatroom.ChatRoomActivity;
import com.example.silvi.medmap_android.ui.widget.SlidingTabLayout;
import com.example.silvi.medmap_android.util.ListUtils;
import com.makeramen.roundedimageview.RoundedImageView;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by silvi on 26.02.2017.
 */

public class CabinetActivity extends BaseActivity implements CabinetContract.View, FinishCallback {

  Shader textShader;
  @Bind(R.id.cabinetSpecializationTView)
  TextView mCabinetSpecialization;
  @Bind(R.id.categoryTView)
  TextView mCabinetCategory;
  @Bind(R.id.cabinetDescriptionTView)
  TextView mCabinetDescription;
  @Bind(R.id.studiesTView)
  TextView mDoctorStudies;
  @Bind(R.id.experienceTView)
  TextView mDoctorExperience;
  @Bind(R.id.specializationTView)
  TextView mDoctorSpecialization;
  @Bind(R.id.phoneTView)
  TextView mDoctorPhoneNumber;
  @Bind(R.id.emailTView)
  TextView mDoctorEmail;
  @Bind(R.id.bottom_group)
  RelativeLayout mBottomGroup;
  @Bind(R.id.bottom_margin)
  View bottomMargin;
  @Bind(R.id.continue_button)
  TextView mContinueButton;
  @Bind(R.id.birth_date_tview)
  TextView mBirthDate;
  @Bind(R.id.more_details_button)
  LinearLayout mMoreDetailsButton;
  @Bind(R.id.more_details_arrow)
  TextView mMoreDetailsArrow;
  @Bind(R.id.more_details_text)
  TextView mMoreDetailsText;
  @Bind(R.id.studies_section)
  RelativeLayout mStudiesSection;
  @Bind(R.id.experience_section)
  RelativeLayout mExperienceSection;
  @Bind(R.id.birth_date_section)
  RelativeLayout mBirthDateSection;

  @Bind(R.id.imagesViewPager)
  ViewPager mImagesViewPager;
  @Bind(R.id.slidingTabLayout)
  SlidingTabLayout mSlidingTabLayout;
  @Bind(R.id.doctorNameTView)
  TextView mDoctorName;
  @Bind(R.id.doctorPhotoAnimator)
  ViewAnimator mDoctorPhotoAnimator;
  @Bind(R.id.doctorPhotoImgView)
  RoundedImageView mDoctorPhotoImageView;

  @Bind(R.id.other_cabinets_recycler)
  RecyclerView mOtherCabinetsRecycler;
  @Bind(R.id.other_cabinets_text)
  LinearLayout mOtherCabinetsTextSection;

  Cabinet mCabinet;
  List<Cabinet> mOtherCabinets;

  private boolean isReadMoreClicked = false;

  @Inject
  CabinetPresenter mPresenter;


  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mCabinet = getIntent().getParcelableExtra(IntentKeys.CABINET);
    if (mCabinet != null)
      mPresenter.getDoctorDetails(mCabinet.getDoctor().getUserId(), Role.ROLE_DOCTOR);

    initCabinetDetails();

    mPresenter.getUserRequests();
    mPresenter.getOtherCabinets(mCabinet.getDoctor().getUserId());


    FinishActivities.addActivity(this);
    setupToolbarDisplay();

  }

  private void initCabinetDetails() {
    String category = mCabinet.getCategory().replace("_", " ");
    mCabinetCategory.setText(StringUtils.capitalize(category.toLowerCase()));

    String specialization = mCabinet.getSpecialization().replace("_", " ");
    mCabinetSpecialization.setText(StringUtils.capitalize(specialization.toLowerCase()));

    setDescriptionText();


    if (UserSession.getUserRole().equals(Role.ROLE_DOCTOR)) {
      mBottomGroup.setVisibility(View.GONE);
      bottomMargin.setVisibility(View.GONE);
    } else {
      mBottomGroup.setVisibility(View.VISIBLE);
      bottomMargin.setVisibility(View.VISIBLE);
    }

    mDoctorName.setText(mCabinet.getDoctor().getLastName() + " " + mCabinet.getDoctor().getFirstName());

    if (mCabinet.getImages() != null)
      setCabinetPhotos(mCabinet.getImages());
      setDoctorPhoto(mCabinet.getDoctor().getProfilePicture());
  }

  public void checkEmptyFields() {
    if (mDoctorStudies.getText().equals("")) {
      mStudiesSection.setVisibility(View.GONE);
    } else {
      mStudiesSection.setVisibility(View.VISIBLE);
    }

    if (mDoctorExperience.getText().equals("")) {
      mExperienceSection.setVisibility(View.GONE);
    } else {
      mExperienceSection.setVisibility(View.VISIBLE);
    }

    if (mBirthDate.getText().equals("")) {
      mBirthDateSection.setVisibility(View.GONE);
    } else {
      mBirthDateSection.setVisibility(View.VISIBLE);
    }
  }

  private void setDescriptionText() {

    textShader = new LinearGradient(0, 680, 0, 0,
        new int[]{Color.TRANSPARENT, getResources().getColor(R.color.edit_text_color)},
        new float[]{0, 1}, Shader.TileMode.CLAMP);

    String description = mCabinet.getDescription();
    if (description.length() > 500)
      mCabinetDescription.getPaint().setShader(textShader);
    mCabinetDescription.setText(description);
    mCabinetDescription.post(new Runnable() {
      @Override
      public void run() {
        if (mCabinetDescription.getLineCount() > 10) {
          mMoreDetailsButton.setVisibility(View.VISIBLE);
        }
      }
    });
  }

  @Override
  public void setCabinetPhotos(List<Image> images) {
    if (!ListUtils.isNullOrEmpty(images)) {
      CabinetImagesPagerAdapter pagerAdapter = new CabinetImagesPagerAdapter(this, images);
      mImagesViewPager.setAdapter(pagerAdapter);
      mSlidingTabLayout.setViewPager(mImagesViewPager);
    }
  }

  @Override
  public void setDoctorPhoto(Image profilePicture) {

    Drawable userIcon = MedMapApplication.getInstance().getResources().getDrawable(R.drawable.user_icon);
    userIcon.setColorFilter(new LightingColorFilter(Color.LTGRAY, Color.LTGRAY));


    Glide
        .with(this)
        .load(profilePicture != null ? profilePicture.getPath() : "")
        .centerCrop()
        .crossFade()
        .placeholder(userIcon)
        .into(mDoctorPhotoImageView);
  }

  private void setupToolbarDisplay() {
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    final ActionBar actionBar = getSupportActionBar();
    View viewActionBar = getLayoutInflater().inflate(R.layout.actionbar_default, null);
    ActionBar.LayoutParams params = new ActionBar.LayoutParams(
        ActionBar.LayoutParams.WRAP_CONTENT,
        ActionBar.LayoutParams.MATCH_PARENT,
        Gravity.CENTER);
    TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.actionbar_textview);
    textviewTitle.setText(mCabinet.getName());
    Typeface robotoMedium = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");
    textviewTitle.setTypeface(robotoMedium);
    actionBar.setCustomView(viewActionBar, params);
    actionBar.setDisplayShowCustomEnabled(true);
    actionBar.setDisplayShowTitleEnabled(false);
    actionBar.setDisplayHomeAsUpEnabled(true);
    actionBar.setHomeButtonEnabled(false);

    getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_chevron_left_white));

  }

  @Override
  protected int getLayout() {
    return R.layout.activity_cabinet;
  }

  @Override
  public void setUpComponent(IAppComponent appComponent) {
    DaggerICabinetComponent.builder()
        .iAppComponent(appComponent)
        .cabinetModule(new CabinetModule(this))
        .build()
        .inject(this);
  }

  @Nullable
  @Override
  protected BasePresenter getPresenter() {
    return null;
  }

  @Override
  protected String getActionBarTitle() {
    return "Test cabinet";
  }

  public static Intent makeIntent(Context context, Cabinet cabinet) {
    Intent intent = new Intent(context, CabinetActivity.class);

    intent.putExtra(IntentKeys.CABINET, cabinet);

    return intent;
  }

  @Override
  public void forceFinish() {
    this.finish();
  }

  @Override
  public void showToast(String text) {
    Toast.makeText(this, text, Toast.LENGTH_LONG).show();
  }

  @OnClick(R.id.continue_button)
  void onSendRequestClick() {
    finish();
    mPresenter.onSendRequestClicked(mCabinet);
  }

  @OnClick(R.id.btnContactDoctor)
  void onSendMessageClick() {
    mPresenter.openChatRoom(mCabinet.getDoctor().getUserId());
  }

  @OnClick(R.id.more_details_button)
  public void onReadMoreClicked() {


    if (!isReadMoreClicked) {
      mCabinetDescription.getPaint().setShader(null);
      mCabinetDescription.setMaxLines(Integer.MAX_VALUE);
      mMoreDetailsText.setText(getResources().getString(R.string.show_less));
      mMoreDetailsArrow.setText(getResources().getString(R.string.icon_chevron_up));

      isReadMoreClicked = true;
    } else {
      mCabinetDescription.getPaint().setShader(textShader);
      mCabinetDescription.setMaxLines(10);
      mMoreDetailsText.setText(getResources().getString(R.string.read_more));
      mMoreDetailsArrow.setText(getResources().getString(R.string.icon_chevron_down));

      isReadMoreClicked = false;
    }


  }

  public void setDoctorStudies(String studies) {
    mDoctorStudies.setText(studies);
  }

  @Override
  public void setDoctorBirthDate(String birthDate) {
    mBirthDate.setText(birthDate);
  }

  public void setDoctorExperience(String experience) {
    mDoctorExperience.setText(experience);
  }

  public void setDoctorSpecialization(String specialization) {

    String cabinetSpecialization = specialization.replace("_", " ");
    mDoctorSpecialization.setText(StringUtils.capitalize(cabinetSpecialization.toLowerCase()));
  }

  public void setDoctorPhoneNumber(String phoneNumber) {
    mDoctorPhoneNumber.setText(phoneNumber);
  }

  public void setDoctorEmail(String email) {
    mDoctorEmail.setText(email);
  }

  public void setUserRequests(List<Request> requests) {

    if (requests != null) {
      for (Request request : requests) {
        if (request.getUser().getUserId().equals(UserSession.getUserId()) && request.getCabinet().getId().equals(mCabinet.getId())) {
          if (request.getState().equals(State.ACCEPTED)) {
            mContinueButton.setText(getResources().getString(R.string.make_an_appointment));
            mContinueButton.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                startActivity(AddApointmentActivity.makeIntent(getApplicationContext(), mCabinet, false));
              }
            });
          } else {
            mBottomGroup.setVisibility(View.GONE);
            bottomMargin.setVisibility(View.GONE);
          }
        }
      }
    }
  }

  @Override
  public void startChatRoomActivity(ChatRoom chatRoom) {
    startActivity(ChatRoomActivity.makeIntent(this, chatRoom));
  }

  @Override
  public void initOtherCabinets(List<Cabinet> cabinets) {
    mOtherCabinets = new ArrayList<>();
    for (Cabinet cabinet : cabinets) {
      if (!cabinet.getId().equals(mCabinet.getId()))
        mOtherCabinets.add(cabinet);
    }
    if (mOtherCabinets.size() == 0) {
      mOtherCabinetsTextSection.setVisibility(View.GONE);
    } else {
      mOtherCabinetsTextSection.setVisibility(View.VISIBLE);
    }
    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
    mOtherCabinetsRecycler.setLayoutManager(linearLayoutManager);

    OtherCabinetsAdapter adapter = new OtherCabinetsAdapter(this, mOtherCabinets);
    mOtherCabinetsRecycler.setAdapter(adapter);
  }
}
