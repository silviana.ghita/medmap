package com.example.silvi.medmap_android.ui.cabinet.add;

import com.example.medmap_commons.model.Cabinet;
import com.example.medmap_commons.model.RestError;
import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.domain.cabinet.add.AddCabinetInteractor;
import com.example.silvi.medmap_android.domain.images.upload.UploadPictureInteractor;
import com.example.silvi.medmap_android.ui.base.MedMapApplication;

import javax.inject.Inject;

/**
 * Created by silvi on 14.03.2017.
 */

public class AddCabinetPresenter implements AddCabinetContract.Presenter, AddCabinetInteractor.Callback, UploadPictureInteractor.Callback {

  AddCabinetContract.View mView;
  AddCabinetInteractor mAddCabinetInteractor;
  UploadPictureInteractor mUploadPictureInteractor;

  @Inject
  public AddCabinetPresenter(AddCabinetContract.View view, AddCabinetInteractor addCabinetInteractor, UploadPictureInteractor uploadPictureInteractor) {
    this.mView = view;
    this.mAddCabinetInteractor = addCabinetInteractor;
    this.mUploadPictureInteractor = uploadPictureInteractor;
  }

  @Override
  public void onConfirmButtonClicked() {

    if (isFormValid()) {
      Cabinet cabinet = new Cabinet();
      cabinet.setName(mView.getCabinetName());
      cabinet.setDescription(mView.getDescription());
      cabinet.setSpecialization(mView.getSpecialization());
      cabinet.setCategory(mView.getCategory());
      cabinet.setLatitude(mView.getLatLng().latitude);
      cabinet.setLongitude(mView.getLatLng().longitude);

      mAddCabinetInteractor.execute(this, cabinet);
    }

  }

  @Override
  public void onAddCabinetSuccess(Cabinet cabinet) {
    mView.showToast(MedMapApplication.getInstance().getResources().getString(R.string.cabinet_added_successfully));
    mUploadPictureInteractor.executeMultiple(this, mView.getImagesPath(), cabinet.getId());
    mView.closeActivity();
  }

  @Override
  public void onAddCabinetError(RestError restError) {
    mView.showToast(MedMapApplication.getInstance().getResources().getString(R.string.error_during_add_cabinet));
  }

  public boolean isFormValid() {
    if (mView.getCabinetName() == null || mView.getCabinetName().isEmpty()) {
      mView.showToast(MedMapApplication.getInstance().getResources().getString(R.string.cabinet_name_null));
      return false;
    } else if (mView.getDescription() == null || mView.getDescription().isEmpty()) {
      mView.showToast(MedMapApplication.getInstance().getResources().getString(R.string.cabinet_description_null));
      return false;
    }
    return true;
  }

  @Override
  public void onPictureUploadSuccess() {

  }

  @Override
  public void onPictureUploadFailed(RestError restError) {

  }
}
