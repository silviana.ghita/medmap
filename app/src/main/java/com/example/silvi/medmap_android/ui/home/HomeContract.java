package com.example.silvi.medmap_android.ui.home;

import com.example.medmap_commons.model.Cabinet;

import java.util.List;

/**
 * Created by silvi on 24.02.2017.
 */

public class HomeContract {

  public interface View {

    void showToast(String text);

    void initCabinets(List<Cabinet> cabinets);

    void setDoctorCabinets(List<Cabinet> doctorCabinets);

  }

  interface Presenter {

    void getAllCabinets();

    void setFCMToken(String token);

    void getDoctorCabinets(long userId);

  }
}
