package com.example.silvi.medmap_android.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import com.example.silvi.medmap_android.R;

/**
 * Created by silvi on 24.02.2017.
 */

public class BitmapUtils {

  public static BitmapDrawable getToolbarBitmap(Context context, int resId) {
    Drawable drawable = context.getResources().getDrawable(resId);
    Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
    return new BitmapDrawable(context.getResources(), Bitmap.createScaledBitmap(bitmap,
        context.getResources().getDimensionPixelSize(R.dimen.action_button_height),
        context.getResources().getDimensionPixelSize(R.dimen.action_button_height), true));
  }
}
