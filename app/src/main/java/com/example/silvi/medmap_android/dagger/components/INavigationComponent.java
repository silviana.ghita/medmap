package com.example.silvi.medmap_android.dagger.components;

import com.example.medmap_api.service.AuthService;
import com.example.silvi.medmap_android.dagger.ActivityScope;
import com.example.silvi.medmap_android.dagger.modules.NavigationModule;
import com.example.silvi.medmap_android.domain.logout.LogoutInteractor;
import com.example.silvi.medmap_android.ui.base.NavigationViewActivity;
import com.example.silvi.medmap_android.ui.base.NavigationViewPresenter;

import dagger.Component;

/**
 * Created by silvi on 11.03.2017.
 */

@ActivityScope
@Component(
    dependencies = IAppComponent.class,
    modules = NavigationModule.class
)
public interface INavigationComponent extends IAppComponent {

  void inject(NavigationViewActivity navigationViewActivity);

  NavigationViewActivity getView();

  AuthService getAuthService();

  LogoutInteractor getLogoutInteractor();

  NavigationViewPresenter getNavigationViewPresenter();
}
