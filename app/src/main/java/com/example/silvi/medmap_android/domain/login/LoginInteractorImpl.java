package com.example.silvi.medmap_android.domain.login;

import com.example.medmap_api.service.AuthService;
import com.example.medmap_commons.model.LoggedInUser;
import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.util.PrintLog;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.Interactor;
import com.example.silvi.medmap_android.executor.MainThread;

import javax.inject.Inject;

import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

/**
 * Created by silvi on 08.03.2017.
 */

public class LoginInteractorImpl implements LoginInteractor, Interactor {

  private final Executor mExecutor;
  private final MainThread mMainThread;
  private final AuthService mAuthService;

  @Inject
  public LoginInteractorImpl(Executor executor, MainThread mainThread, AuthService authService) {
    this.mAuthService = authService;
    this.mExecutor = executor;
    this.mMainThread = mainThread;
  }

  private Callback mCallback;
  private String mAuthorization;

  @Override
  public void execute(Callback callback, String authorization) {
    if (null == callback) {
      throw new IllegalArgumentException(
          "Callback can't be null, the client of this interactor needs to get the response " +
              "in the callback");
    }
    this.mCallback = callback;
    this.mAuthorization = authorization;
    this.mExecutor.run(this);
  }

  @Override
  public void run() {
    mAuthService.loginUser(mAuthorization, new retrofit.Callback<LoggedInUser>() {
      @Override
      public void success(LoggedInUser loggedInUser, Response response) {
        String xAuthToken = "";
        for (Header header : response.getHeaders()) {
          if (header.getName().toLowerCase().equals("x-auth-token")) {
            xAuthToken = header.getValue();
            break;
          }
        }
        loggedInUser.setXAuthToken(xAuthToken);
        notifyLoginSuccess(loggedInUser);
      }

      @Override
      public void failure(RetrofitError error) {
        RestError restError = null;
        try {
          restError = (RestError) error.getBodyAs(RestError.class);
        } catch (Exception e) {
          PrintLog.printStackTrace(getClass().getSimpleName(), e);
        }
        notifyError(restError);
      }
    });
  }

  private void notifyLoginSuccess(final LoggedInUser loggedInUser) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onLoginSuccess(loggedInUser);
      }
    });
  }

  private void notifyError(final RestError restError) {
    mMainThread.post(new Runnable() {
      @Override
      public void run() {
        mCallback.onLoginError(restError);
      }
    });
  }
}
