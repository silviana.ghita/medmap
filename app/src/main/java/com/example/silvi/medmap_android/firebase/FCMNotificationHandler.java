package com.example.silvi.medmap_android.firebase;

import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.silvi.medmap_android.R;
import com.example.silvi.medmap_android.preferences.SharedPreferencesHandler;
import com.example.silvi.medmap_android.ui.base.MedMapApplication;
import com.example.silvi.medmap_android.ui.home.HomeActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

/**
 * Created by silvi on 20.04.2017.
 */

public class FCMNotificationHandler extends FirebaseMessagingService {

  Dialog dialog;
  Bundle bundle;

  @Override
  public void onMessageReceived(RemoteMessage remoteMessage) {
    Map<String, String> data = remoteMessage.getData();
    NotificationType type = NotificationType.valueOf(data.get(NotificationType.TYPE.name().toLowerCase()));
    switch (type) {
      case TYPE_MEDMAP_APPOINTMENT_DELAY:
        appointmentDelay(data);
        break;
      case TYPE_MEDMAP_SENT_APPOINTMENT:
        sentAppointment(data);
        break;
      case TYPE_MEDMAP_CHANGED_STATE_APPOINTMENT:
        changedAppointmentState(data);
        break;
      case TYPE_MEDMAP_SENT_REQUEST:
        sentRequest(data);
        break;
      case TYPE_MEDMAP_CHANGED_STATE_REQUEST:
        changedRequestState(data);
        break;
      case TYPE_MEDMAP_ONE_DAY_BEFORE_APPOINTMENT:
        sendAppointmentReminder(data);
        break;
      default:
        break;
    }
  }


  private void appointmentDelay(Map<String, String> data) {

    bundle = new Bundle();
    bundle.putString(NotificationType.TYPE.toString(), data.get(NotificationType.TYPE.name().toLowerCase()));
    bundle.putString(NotificationKeys.CABINET_NAME.toString(), data.get(NotificationKeys.CABINET_NAME.toString()));
    bundle.putString(NotificationKeys.APPOINTMENT_DATE.toString(), data.get(NotificationKeys.APPOINTMENT_DATE.toString()));
    bundle.putString(NotificationKeys.APPOINTMENT_HOUR.toString(), data.get(NotificationKeys.APPOINTMENT_HOUR.toString()));
    bundle.putString(NotificationKeys.APPOINTMENT_DELAY.toString(), data.get(NotificationKeys.APPOINTMENT_DELAY.toString()));
    bundle.putString(NotificationKeys.DOCTOR_FULL_NAME.toString(), data.get(NotificationKeys.DOCTOR_FULL_NAME.toString()));

    if (SharedPreferencesHandler.isApplicationInForegound(this)) {
      Handler handler = new Handler(Looper.getMainLooper());
      handler.post(new Runnable() {
        @Override
        public void run() {

          dialog = new Dialog(MedMapApplication.getInstance().getApplicationContext(), R.style.CustomDialog);
          dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
          dialog.setContentView(R.layout.dialog_appointment_delayed);

          dialog.findViewById(R.id.close_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              dialog.dismiss();
            }
          });

          TextView textView = (TextView) dialog.findViewById(R.id.appointment_delayed_notification_text);


          final String notificationText = getString(R.string.delay_info, bundle.getString(NotificationKeys.CABINET_NAME.toString()),
              bundle.getString(NotificationKeys.DOCTOR_FULL_NAME.toString()), bundle.getString(NotificationKeys.APPOINTMENT_DATE.toString()),
              bundle.getString(NotificationKeys.APPOINTMENT_HOUR.toString()), bundle.getString(NotificationKeys.APPOINTMENT_DELAY.toString()));
          textView.setText(notificationText);

          dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

          dialog.show();
        }
      });

    } else {
      createNotification(bundle);
    }
  }

  private void sentAppointment(Map<String, String> data) {
    bundle = new Bundle();
    bundle.putString(NotificationType.TYPE.toString(), data.get(NotificationType.TYPE.name().toLowerCase()));
    bundle.putString(NotificationKeys.CABINET_NAME.toString(), data.get(NotificationKeys.CABINET_NAME.toString()));
    bundle.putString(NotificationKeys.APPOINTMENT_DATE.toString(), data.get(NotificationKeys.APPOINTMENT_DATE.toString()));
    bundle.putString(NotificationKeys.APPOINTMENT_HOUR.toString(), data.get(NotificationKeys.APPOINTMENT_HOUR.toString()));
    bundle.putString(NotificationKeys.USER_FULL_NAME.toString(), data.get(NotificationKeys.USER_FULL_NAME.toString()));

    if (SharedPreferencesHandler.isApplicationInForegound(this)) {
      Handler handler = new Handler(Looper.getMainLooper());
      handler.post(new Runnable() {
        @Override
        public void run() {

          dialog = new Dialog(MedMapApplication.getInstance().getApplicationContext(), R.style.CustomDialog);
          dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
          dialog.setContentView(R.layout.dialog_appointment_delayed);

          dialog.findViewById(R.id.close_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              dialog.dismiss();
            }
          });

          TextView textView = (TextView) dialog.findViewById(R.id.appointment_delayed_notification_text);


          final String notificationText = getString(R.string.appointment_info, bundle.getString(NotificationKeys.USER_FULL_NAME.toString()),
              bundle.getString(NotificationKeys.CABINET_NAME.toString()), bundle.getString(NotificationKeys.APPOINTMENT_DATE.toString()),
              bundle.getString(NotificationKeys.APPOINTMENT_HOUR.toString()));
          textView.setText(notificationText);

          dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

          dialog.show();
        }
      });

    } else {
      createNotification(bundle);
    }
  }

  private void changedAppointmentState(Map<String, String> data) {
    bundle = new Bundle();
    bundle.putString(NotificationType.TYPE.toString(), data.get(NotificationType.TYPE.name().toLowerCase()));
    bundle.putString(NotificationKeys.CABINET_NAME.toString(), data.get(NotificationKeys.CABINET_NAME.toString()));
    bundle.putString(NotificationKeys.APPOINTMENT_DATE.toString(), data.get(NotificationKeys.APPOINTMENT_DATE.toString()));
    bundle.putString(NotificationKeys.APPOINTMENT_HOUR.toString(), data.get(NotificationKeys.APPOINTMENT_HOUR.toString()));
    bundle.putString(NotificationKeys.DOCTOR_FULL_NAME.toString(), data.get(NotificationKeys.DOCTOR_FULL_NAME.toString()));
    bundle.putString(NotificationKeys.STATE.toString(), data.get(NotificationKeys.STATE.toString()));

    if (SharedPreferencesHandler.isApplicationInForegound(this)) {
      Handler handler = new Handler(Looper.getMainLooper());
      handler.post(new Runnable() {
        @Override
        public void run() {

          dialog = new Dialog(MedMapApplication.getInstance().getApplicationContext(), R.style.CustomDialog);
          dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
          dialog.setContentView(R.layout.dialog_appointment_delayed);

          dialog.findViewById(R.id.close_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              dialog.dismiss();
            }
          });

          TextView textView = (TextView) dialog.findViewById(R.id.appointment_delayed_notification_text);


          final String notificationText = getString(R.string.changed_state_appointment_info, bundle.getString(NotificationKeys.DOCTOR_FULL_NAME.toString()),
              bundle.getString(NotificationKeys.CABINET_NAME.toString()), bundle.getString(NotificationKeys.STATE.toString()), bundle.getString(NotificationKeys.APPOINTMENT_DATE.toString()),
              bundle.getString(NotificationKeys.APPOINTMENT_HOUR.toString()));
          textView.setText(notificationText);

          dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

          dialog.show();
        }
      });

    } else {
      createNotification(bundle);
    }
  }

  private void sentRequest(Map<String, String> data) {
    bundle = new Bundle();
    bundle.putString(NotificationType.TYPE.toString(), data.get(NotificationType.TYPE.name().toLowerCase()));
    bundle.putString(NotificationKeys.CABINET_NAME.toString(), data.get(NotificationKeys.CABINET_NAME.toString()));
    bundle.putString(NotificationKeys.USER_FULL_NAME.toString(), data.get(NotificationKeys.USER_FULL_NAME.toString()));

    if (SharedPreferencesHandler.isApplicationInForegound(this)) {
      Handler handler = new Handler(Looper.getMainLooper());
      handler.post(new Runnable() {
        @Override
        public void run() {

          dialog = new Dialog(MedMapApplication.getInstance().getApplicationContext(), R.style.CustomDialog);
          dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
          dialog.setContentView(R.layout.dialog_appointment_delayed);

          dialog.findViewById(R.id.close_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              dialog.dismiss();
            }
          });

          TextView textView = (TextView) dialog.findViewById(R.id.appointment_delayed_notification_text);


          final String notificationText = getString(R.string.request_info, bundle.getString(NotificationKeys.USER_FULL_NAME.toString()),
              bundle.getString(NotificationKeys.CABINET_NAME.toString()));
          textView.setText(notificationText);

          dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

          dialog.show();
        }
      });

    } else {
      createNotification(bundle);
    }
  }

  private void changedRequestState(Map<String, String> data) {
    bundle = new Bundle();
    bundle.putString(NotificationType.TYPE.toString(), data.get(NotificationType.TYPE.name().toLowerCase()));
    bundle.putString(NotificationKeys.CABINET_NAME.toString(), data.get(NotificationKeys.CABINET_NAME.toString()));
    bundle.putString(NotificationKeys.DOCTOR_FULL_NAME.toString(), data.get(NotificationKeys.DOCTOR_FULL_NAME.toString()));
    bundle.putString(NotificationKeys.STATE.toString(), data.get(NotificationKeys.STATE.toString()));

    if (SharedPreferencesHandler.isApplicationInForegound(this)) {
      Handler handler = new Handler(Looper.getMainLooper());
      handler.post(new Runnable() {
        @Override
        public void run() {

          dialog = new Dialog(MedMapApplication.getInstance().getApplicationContext(), R.style.CustomDialog);
          dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
          dialog.setContentView(R.layout.dialog_appointment_delayed);

          dialog.findViewById(R.id.close_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              dialog.dismiss();
            }
          });

          TextView textView = (TextView) dialog.findViewById(R.id.appointment_delayed_notification_text);

          final String notificationText = getString(R.string.changed_state_request_info, bundle.getString(NotificationKeys.DOCTOR_FULL_NAME.toString()),
              bundle.getString(NotificationKeys.STATE.toString()),
              bundle.getString(NotificationKeys.CABINET_NAME.toString()));
          textView.setText(notificationText);

          dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

          dialog.show();
        }
      });

    } else {
      createNotification(bundle);
    }
  }

  private void sendAppointmentReminder(Map<String, String> data) {
    bundle = new Bundle();
    bundle.putString(NotificationType.TYPE.toString(), data.get(NotificationType.TYPE.name().toLowerCase()));
    bundle.putString(NotificationKeys.CABINET_NAME.toString(), data.get(NotificationKeys.CABINET_NAME.toString()));
    ;
    bundle.putString(NotificationKeys.DOCTOR_FULL_NAME.toString(), data.get(NotificationKeys.DOCTOR_FULL_NAME.toString()));

    if (SharedPreferencesHandler.isApplicationInForegound(this)) {
      Handler handler = new Handler(Looper.getMainLooper());
      handler.post(new Runnable() {
        @Override
        public void run() {

          dialog = new Dialog(MedMapApplication.getInstance().getApplicationContext(), R.style.CustomDialog);
          dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
          dialog.setContentView(R.layout.dialog_appointment_delayed);

          dialog.findViewById(R.id.close_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              dialog.dismiss();
            }
          });

          TextView textView = (TextView) dialog.findViewById(R.id.appointment_delayed_notification_text);


          final String notificationText = getString(R.string.appointment_reminder, bundle.getString(NotificationKeys.CABINET_NAME.toString()),
              bundle.getString(NotificationKeys.DOCTOR_FULL_NAME.toString()));

          textView.setText(notificationText);

          dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

          dialog.show();
        }
      });

    } else {
      createNotification(bundle);
    }
  }

  private void createNotification(Bundle bundle) {
    Intent intent;
    int notification_id;
    NotificationType type = NotificationType.valueOf(bundle.getString(NotificationType.TYPE.name()));
    switch (type) {
      case TYPE_MEDMAP_APPOINTMENT_DELAY:
        notification_id = 1;
        intent = new Intent(MedMapApplication.getInstance(), HomeActivity.class);
        break;
      case TYPE_MEDMAP_SENT_APPOINTMENT:
        notification_id = 2;
        intent = new Intent(MedMapApplication.getInstance(), HomeActivity.class);
        break;
      case TYPE_MEDMAP_CHANGED_STATE_APPOINTMENT:
        notification_id = 3;
        intent = new Intent(MedMapApplication.getInstance(), HomeActivity.class);
        break;
      case TYPE_MEDMAP_SENT_REQUEST:
        notification_id = 4;
        intent = new Intent(MedMapApplication.getInstance(), HomeActivity.class);
        break;
      case TYPE_MEDMAP_CHANGED_STATE_REQUEST:
        notification_id = 5;
        intent = new Intent(MedMapApplication.getInstance(), HomeActivity.class);
        break;
      case TYPE_MEDMAP_ONE_DAY_BEFORE_APPOINTMENT:
        notification_id = 6;
        intent = new Intent(MedMapApplication.getInstance(), HomeActivity.class);
        break;
      default:
        notification_id = 0;
        intent = new Intent(MedMapApplication.getInstance(), HomeActivity.class);
        break;
    }
    intent.putExtras(bundle);
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

    TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(MedMapApplication.getInstance());
    taskStackBuilder.addParentStack(HomeActivity.class);
    taskStackBuilder.addNextIntent(intent);
    PendingIntent pendingIntent = taskStackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);


    String notificationText = null;

    if (notification_id == 1) {
      notificationText = getString(R.string.delay_info, bundle.getString(NotificationKeys.CABINET_NAME.toString()),
          bundle.getString(NotificationKeys.DOCTOR_FULL_NAME.toString()), bundle.getString(NotificationKeys.APPOINTMENT_DATE.toString()),
          bundle.getString(NotificationKeys.APPOINTMENT_HOUR.toString()), bundle.getString(NotificationKeys.APPOINTMENT_DELAY.toString()));
    } else if (notification_id == 2) {
      notificationText = getString(R.string.appointment_info, bundle.getString(NotificationKeys.USER_FULL_NAME.toString()),
          bundle.getString(NotificationKeys.CABINET_NAME.toString()), bundle.getString(NotificationKeys.APPOINTMENT_DATE.toString()),
          bundle.getString(NotificationKeys.APPOINTMENT_HOUR.toString()));
    } else if (notification_id == 3) {
      notificationText = getString(R.string.changed_state_appointment_info, bundle.getString(NotificationKeys.DOCTOR_FULL_NAME.toString()),
          bundle.getString(NotificationKeys.CABINET_NAME.toString()), bundle.getString(NotificationKeys.STATE.toString()), bundle.getString(NotificationKeys.APPOINTMENT_DATE.toString()),
          bundle.getString(NotificationKeys.APPOINTMENT_HOUR.toString()));
    } else if (notification_id == 4) {
      notificationText = getString(R.string.request_info, bundle.getString(NotificationKeys.USER_FULL_NAME.toString()),
          bundle.getString(NotificationKeys.CABINET_NAME.toString()));
    } else if (notification_id == 5) {
      notificationText = getString(R.string.changed_state_request_info, bundle.getString(NotificationKeys.DOCTOR_FULL_NAME.toString()),
          bundle.getString(NotificationKeys.STATE.toString()),
          bundle.getString(NotificationKeys.CABINET_NAME.toString()));
    } else if (notification_id == 6) {
      notificationText = getString(R.string.appointment_reminder, bundle.getString(NotificationKeys.CABINET_NAME.toString()),
          bundle.getString(NotificationKeys.DOCTOR_FULL_NAME.toString()));
    }

    NotificationCompat.Builder notification = new NotificationCompat.Builder(MedMapApplication.getInstance())
        .setPriority(Notification.PRIORITY_MAX)
        .setDefaults(Notification.DEFAULT_ALL)
        .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(),
            R.drawable.medmap_logo_splash))
        .setSmallIcon(R.drawable.medmap_logo_splash)
        .setContentTitle("Medmap")
        .setStyle(new NotificationCompat.BigTextStyle().bigText(notificationText))
        .setContentText(notificationText)
        .setAutoCancel(true)
        .setContentIntent(pendingIntent);
    NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    manager.notify(notification_id, notification.build());
  }
}
