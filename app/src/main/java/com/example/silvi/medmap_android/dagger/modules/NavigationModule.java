package com.example.silvi.medmap_android.dagger.modules;

import com.example.medmap_api.service.AuthService;
import com.example.silvi.medmap_android.domain.logout.LogoutInteractor;
import com.example.silvi.medmap_android.domain.logout.LogoutInteractorImpl;
import com.example.silvi.medmap_android.executor.Executor;
import com.example.silvi.medmap_android.executor.MainThread;
import com.example.silvi.medmap_android.ui.base.NavigationViewActivity;
import com.example.silvi.medmap_android.ui.base.NavigationViewPresenter;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;

/**
 * Created by silvi on 11.03.2017.
 */

@Module
public class NavigationModule {

  private NavigationViewActivity view;

  public NavigationModule(NavigationViewActivity view) {
    this.view = view;
  }

  @Provides
  public NavigationViewActivity getView() {
    return view;
  }

  @Provides
  public AuthService provideAuthService(RestAdapter restAdapter) {
    return restAdapter.create(AuthService.class);
  }

  @Provides
  public LogoutInteractor provideLogoutInteractor(Executor executor,
                                                  MainThread mainThread,
                                                  AuthService authService) {
    return new LogoutInteractorImpl(executor, mainThread, authService);
  }

  @Provides
  public NavigationViewPresenter providePresenter(NavigationViewActivity view, LogoutInteractor interactor) {
    return new NavigationViewPresenter(view, interactor);
  }
}
