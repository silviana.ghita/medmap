package com.example.silvi.medmap_android.domain.cabinet.get;

import com.example.medmap_commons.model.Cabinet;
import com.example.medmap_commons.model.CabinetsFilter;
import com.example.medmap_commons.model.RestError;

import java.util.List;

/**
 * Created by silvi on 16.03.2017.
 */

public interface GetAllCabinetsInteractor {

  interface Callback {

    void onGetAllCabinetsSuccess(List<Cabinet> cabinets);

    void onGetAllCabinetsError(RestError restError);
  }

  void execute(Callback callback, CabinetsFilter filter);
}
