package com.example.silvi.medmap_android.ui.auth.register;

/**
 * Created by silvi on 24.02.2017.
 */

public interface RegisterContract {

  interface View {

    String getFirstName();

    String getLastName();

    String getEmail();

    String getPhoneNumber();

    String getSpecialization();

    String getPassword();

    String getRetype();

    void displayRegisterSuccessDialog();

    void showError(String error);

    void startNextActivity();
  }

  interface Presenter {

    void onRegisterClick(String screenType);

  }
}
