package com.example.silvi.medmap_android.domain.chat;

import com.example.medmap_commons.model.ChatRoom;
import com.example.medmap_commons.model.RestError;

import java.util.List;

/**
 * Created by silvi on 05.05.2017.
 */

public interface GetChatRoomsInteractor {

  interface Callback {

    void onGetChatRoomsSuccess(List<ChatRoom> chatRooms);

    void onGetChatRoomsError(RestError restError);

  }

  void execute(Callback callback);
}
