package com.example.silvi.medmap_android.domain.profile;

import com.example.medmap_commons.model.Doctor;
import com.example.medmap_commons.model.RestError;
import com.example.medmap_commons.model.User;

/**
 * Created by silvi on 13.03.2017.
 */

public interface SaveProfileDetailsInteractor {

  interface Callback {

    void onUserSaveProfileDetailsSuccess(User user);

    void onDoctorSaveProfileDetailsSuccess(Doctor doctor);

    void onSaveProfileDetailsError(RestError restError);
  }

  void execute(Callback callback, User user, Doctor doctor);
}
