package com.example.silvi.medmap_android.util;

/**
 * Created by silvi on 10.03.2017.
 */

public enum Specializations {

  GYNAECOLOGY, RADIOLOGY, DENTISTRY, IMMUNOLOGY, PEDIATRICS, DERMATOLOGY,
  PSYCHIATRY, FAMILY_MEDICINE, NUTRITION, GERIATRICS
}
