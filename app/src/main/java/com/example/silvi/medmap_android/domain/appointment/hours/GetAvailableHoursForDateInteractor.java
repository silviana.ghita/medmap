package com.example.silvi.medmap_android.domain.appointment.hours;

import com.example.medmap_commons.model.RestError;

import java.util.List;

/**
 * Created by silvi on 07.04.2017.
 */

public interface GetAvailableHoursForDateInteractor {

  interface Callback {

    void onGetAvailableHoursForDateSuccess(List<Double> availableHours);

    void onGetAvailableHoursForDateError(RestError restError);

  }

  void execute(Callback callback, String date, Double duration, long cabinetId);
}
