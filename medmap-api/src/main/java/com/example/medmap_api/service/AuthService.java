package com.example.medmap_api.service;

import com.example.medmap_api.ApiConstants;
import com.example.medmap_commons.model.Doctor;
import com.example.medmap_commons.model.LoggedInUser;
import com.example.medmap_commons.model.User;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;


/**
 * Created by silvi on 22.02.2017.
 */

public interface AuthService {

  String ENDPOINT_PREFIX = ApiConstants.API_VERSION;

  @POST(ENDPOINT_PREFIX + "/users")
  void registerUser(@Body User request, Callback<LoggedInUser> callback);

  @POST(ENDPOINT_PREFIX + "/doctors")
  void registerDoctor(@Body Doctor request, Callback<LoggedInUser> callback);

  @GET(ENDPOINT_PREFIX + "/users/current-user")
  void loginUser(@Header("Authorization") String authorization, Callback<LoggedInUser> callback);

  @DELETE(ENDPOINT_PREFIX + "/users/logout")
  void logout(Callback<Object> callback);

}
