package com.example.medmap_api.service;

import com.example.medmap_api.ApiConstants;
import com.example.medmap_commons.model.Image;

import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.mime.TypedFile;

/**
 * Created by silvi on 20.03.2017.
 */

public interface ImageService {

  String ENDPOINT_PREFIX = ApiConstants.API_VERSION;

  @Multipart
  @POST(ENDPOINT_PREFIX + "/pictures")
  void uploadProfilePicture(@Part("photo") TypedFile request, Callback<Object> callback);

  @Multipart
  @POST(ENDPOINT_PREFIX + "/pictures/{cabinetId}")
  void uploadCabinetPicture(@Part("photo") TypedFile request, @Path("cabinetId") Long cabinetId, Callback<Object> callback);

  @Multipart
  @POST(ENDPOINT_PREFIX + "/pictures/chat")
  void uploadChatPhoto(@Part("photo") TypedFile request, Callback<Map<String, String>> callback);

  @POST(ENDPOINT_PREFIX + "/pictures/{cabinetId}/delete")
  void deleteCabinetPictures(@Body List<Image> imagesToDelete, @Path("cabinetId") Long cabinetId, Callback<Object> callback);

  @GET(ENDPOINT_PREFIX + "/pictures/get/{cabinetId}")
  void getCabinetPictures(@Path("cabinetId") Long cabinetId, Callback<List<Image>> callback);


}
