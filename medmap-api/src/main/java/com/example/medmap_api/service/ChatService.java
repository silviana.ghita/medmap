package com.example.medmap_api.service;

import com.example.medmap_api.ApiConstants;
import com.example.medmap_commons.model.ChatRoom;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by silvi on 02.05.2017.
 */

public interface ChatService {

  String ENDPOINT_PREFIX = ApiConstants.API_VERSION;

  @GET(ENDPOINT_PREFIX + "/chat/chatroom/{otherUserId}")
  void openChatRoom(@Path("otherUserId") Long userId, Callback<ChatRoom> callback);

  @GET(ENDPOINT_PREFIX + "/chat/chatrooms")
  void getChatRooms(Callback<List<ChatRoom>> callback);
}
