package com.example.medmap_api.service;

import com.example.medmap_api.ApiConstants;
import com.example.medmap_commons.model.Appointment;
import com.example.medmap_commons.model.AppointmentType;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by silvi on 07.04.2017.
 */

public interface AppointmentService {

  String ENDPOINT_PREFIX = ApiConstants.API_VERSION;

  @GET(ENDPOINT_PREFIX + "/appointments/{date}/{duration}/{cabinetId}")
  void getAvailableHoursForDate(@Path("date") String date, @Path("duration") Double duration, @Path("cabinetId") long cabinetId, Callback<List<Double>> callback);

  @GET(ENDPOINT_PREFIX + "/appointments/types")
  void getAppointmentTypes(Callback<List<AppointmentType>> callback);

  @POST(ENDPOINT_PREFIX + "/appointments")
  void addAppointment(@Body Appointment appointment, Callback<Object> callback);

  @GET(ENDPOINT_PREFIX + "/appointments")
  void getUserAppointments(Callback<List<Appointment>> appointments);

  @GET(ENDPOINT_PREFIX + "/appointments/today")
  void getTodayUserAppointments(Callback<List<Appointment>> appointments);

  @POST(ENDPOINT_PREFIX + "/appointments/today")
  void delayTodayProgram(@Body List<Appointment> appointments, @Query("delay") String delay, Callback<List<Appointment>> callback);

  @POST(ENDPOINT_PREFIX + "/appointments/update")
  void updateAppointmentState(@Body Appointment appointment, Callback<Object> callback);

  @POST(ENDPOINT_PREFIX + "/appointments/delete")
  void deleteAppointment(@Body Appointment appointment, Callback<Object> callback);
}
