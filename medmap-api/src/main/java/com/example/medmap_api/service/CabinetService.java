package com.example.medmap_api.service;

import com.example.medmap_api.ApiConstants;
import com.example.medmap_commons.model.Cabinet;
import com.example.medmap_commons.model.CabinetsFilter;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by silvi on 14.03.2017.
 */

public interface CabinetService {

  String ENDPOINT_PREFIX = ApiConstants.API_VERSION;

  @POST(ENDPOINT_PREFIX + "/cabinets")
  void addNewCabinet(@Body Cabinet request, Callback<Cabinet> callback);

  @POST(ENDPOINT_PREFIX + "/cabinets/filter")
  void getAllCabinets(@Body CabinetsFilter cabinetsFilter, Callback<List<Cabinet>> callback);

  @GET(ENDPOINT_PREFIX + "/cabinets/{doctorId}")
  void getDoctorCabinets(@Path("doctorId") Long doctorId, Callback<List<Cabinet>> callback);

  @POST(ENDPOINT_PREFIX + "/cabinets/update")
  void updateCabinet(@Body Cabinet request, Callback<Cabinet> callback);
}
