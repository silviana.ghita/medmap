package com.example.medmap_api.service;

import com.example.medmap_api.ApiConstants;
import com.example.medmap_commons.model.Doctor;
import com.example.medmap_commons.model.User;

import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by silvi on 13.03.2017.
 */

public interface ProfileService {

  String ENDPOINT_PREFIX = ApiConstants.API_VERSION;

  @GET(ENDPOINT_PREFIX + "/users/{id}")
  void getUserDetails(@Path("id") Long userId, Callback<User> callback);

  @GET(ENDPOINT_PREFIX + "/doctors/{id}")
  void getDoctorDetails(@Path("id") Long userId, Callback<Doctor> callback);

  @POST(ENDPOINT_PREFIX + "/users/update")
  void updateUser(@Body User request, Callback<User> callback);

  @POST(ENDPOINT_PREFIX + "/doctors/update")
  void updateDoctor(@Body Doctor request, Callback<Doctor> callback);

  @POST(ENDPOINT_PREFIX + "/users/notifications")
  void setFCMToken(@Body Map<String, String> token, Callback<Object> callback);

  @GET(ENDPOINT_PREFIX + "/users")
  void getDoctorUsers(Callback<List<User>> callback);
}
