package com.example.medmap_api.service;

import com.example.medmap_api.ApiConstants;
import com.example.medmap_commons.model.Request;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;

/**
 * Created by silvi on 19.03.2017.
 */

public interface RequestService {

  String ENDPOINT_PREFIX = ApiConstants.API_VERSION;

  @POST(ENDPOINT_PREFIX + "/requests")
  void addRequest(@Body Request request, Callback<Request> callback);

  @GET(ENDPOINT_PREFIX + "/requests")
  void getRequests(Callback<List<Request>> callback);

  @POST(ENDPOINT_PREFIX + "/requests/update")
  void updateRequest(@Body Request request, Callback<Request> callback);

  @POST(ENDPOINT_PREFIX + "/requests/delete")
  void deleteRequest(@Body Request request, Callback<Object> callback);
}
