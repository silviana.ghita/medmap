package com.example.medmap_api;

/**
 * Created by silvi on 22.02.2017.
 */

public final class ApiConstants {

  private ApiConstants() {
  }

  private static final String PROD_URL = "http://medmap.eu-central-1.elasticbeanstalk.com";
  private static final String TEST_URL = "http://192.168.0.103:8080";

  private static boolean isProd = true;

  public static final String API_VERSION = "/api/v1";

  public static String getUrl() {
    return isProd ? PROD_URL : TEST_URL;
  }
}


