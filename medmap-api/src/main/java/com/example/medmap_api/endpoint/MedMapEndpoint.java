package com.example.medmap_api.endpoint;

import retrofit.Endpoint;

/**
 * Created by silvi on 08.03.2017.
 */

public class MedMapEndpoint implements Endpoint {

  private String url;

  public void setUrl(String url) {
    this.url = url;
  }

  @Override
  public String getUrl() {
    return url;
  }

  @Override
  public String getName() {
    return "medmap";
  }
}
