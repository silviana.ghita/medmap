package com.example.medmap_api.endpoint;

import android.text.TextUtils;

import java.util.Date;

import retrofit.RequestInterceptor;

/**
 * Created by silvi on 08.03.2017.
 */

public class ApiRequestInterceptor implements RequestInterceptor {

  private String authToken;

  @Override
  public void intercept(RequestFacade requestFacade) {

    if (!TextUtils.isEmpty(authToken)) {
      requestFacade.addHeader("x-auth-token", authToken);
    }

    String timestamp = String.valueOf(new Date().getTime());
    requestFacade.addHeader("timestamp", timestamp);

    requestFacade.addHeader("Accept", "application/json");
  }

  public void setAuthToken(String authToken) {
    this.authToken = authToken;
  }
}
