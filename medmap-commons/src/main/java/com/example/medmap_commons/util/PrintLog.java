package com.example.medmap_commons.util;

import android.util.Log;

/**
 * Created by silvi on 07.03.2017.
 */

public class PrintLog {
  private static final String PREFIX = "MedMap";


  public static void warning(String TAG, String str) {
    if (str.length() > 4000) {
      Log.w(PREFIX, TAG + "  ---> " + str.substring(0, 4000));
      warning(TAG, str.substring(4000));
    } else {
      Log.w(PREFIX, TAG + " ---->" + str);
    }
  }

  public static void error(String TAG, String str) {
//    if (str.length() > 4000) {
//      Log.e(PREFIX, TAG + "  ---> " + str.substring(0, 4000));
//      error(TAG, str.substring(4000));
//    } else {
//      Log.e(PREFIX, TAG + " ---->" + str);
//    }
  }

  public static void info(String TAG, String str) {
//    if (str.length() > 4000) {
//      Log.i(PREFIX, TAG + "  ---> " + str.substring(0, 4000));
//      info(TAG, str.substring(4000));
//    } else {
//      Log.i(PREFIX, TAG + " ---->" + str);
//    }
  }

  public static void printStackTrace(String tag, Exception e) {
//    if (BuildConfig.DEBUG) {
//      android.util.Log.e(tag, e.toString());
//      for (StackTraceElement ste : e.getStackTrace()) {
//        android.util.Log.e(tag, ste.toString());
//      }
//    }
  }
}
