package com.example.medmap_commons.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by silvi on 02.05.2017.
 */

public class ChatRoom implements Parcelable {

  private Long chatRoomId;
  private Long creationDate;
  private String name;
  private List<User> participants;

  public ChatRoom() {

  }

  public ChatRoom(Long chatRoomId, Long creationDate, String name, List<User> participants) {
    this.chatRoomId = chatRoomId;
    this.creationDate = creationDate;
    this.name = name;
    this.participants = participants;
  }

  protected ChatRoom(Parcel in) {
    name = in.readString();
    chatRoomId = in.readLong();
    creationDate = in.readLong();
    participants = in.createTypedArrayList(User.CREATOR);
  }

  public static final Creator<ChatRoom> CREATOR = new Creator<ChatRoom>() {
    @Override
    public ChatRoom createFromParcel(Parcel in) {
      return new ChatRoom(in);
    }

    @Override
    public ChatRoom[] newArray(int size) {
      return new ChatRoom[size];
    }
  };

  public Long getChatRoomId() {
    return chatRoomId;
  }

  public ChatRoom setChatRoomId(Long chatRoomId) {
    this.chatRoomId = chatRoomId;
    return this;
  }

  public Long getCreationDate() {
    return creationDate;
  }

  public ChatRoom setCreationDate(Long creationDate) {
    this.creationDate = creationDate;
    return this;
  }

  public String getName() {
    return name;
  }

  public ChatRoom setName(String name) {
    this.name = name;
    return this;
  }

  public List<User> getParticipants() {
    return participants;
  }

  public ChatRoom setParticipants(List<User> participants) {
    this.participants = participants;
    return this;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(name);
    dest.writeLong(chatRoomId);
    dest.writeLong(creationDate);
    dest.writeTypedList(participants);
  }
}
