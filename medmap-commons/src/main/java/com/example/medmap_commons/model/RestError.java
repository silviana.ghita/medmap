package com.example.medmap_commons.model;

/**
 * Created by silvi on 07.03.2017.
 */

import java.util.ArrayList;
import java.util.List;

public class RestError {

  private String code;
  private String message;
  private List<RestInvalidField> invalidFields = new ArrayList<>();

  public String getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }

  public List<RestInvalidField> getInvalidFields() {
    return invalidFields;
  }
}