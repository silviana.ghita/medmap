package com.example.medmap_commons.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by silvi on 02.05.2017.
 */

public class Message implements Parcelable {
  private String content;
  private long sentTime;
  private Long authorId;
  private MessageType type;
  private boolean isRidden;


  protected Message(Parcel in) {
    authorId = in.readLong();
    content = in.readString();
    sentTime = in.readLong();
    isRidden = in.readByte() != 0;
  }

  public static final Creator<Message> CREATOR = new Creator<Message>() {
    @Override
    public Message createFromParcel(Parcel in) {
      return new Message(in);
    }

    @Override
    public Message[] newArray(int size) {
      return new Message[size];
    }
  };

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeLong(authorId);
    dest.writeString(content);
    dest.writeLong(sentTime);
    dest.writeByte((byte) (isRidden ? 1 : 0));
  }

  public Message() {
    this.sentTime = System.currentTimeMillis();
  }

  public String getContent() {
    return content;
  }

  public Message setContent(String content) {
    this.content = content;
    return this;
  }

  public Long getAuthorId() {
    return authorId;
  }

  public Message setAuthorId(Long authorId) {
    this.authorId = authorId;
    return this;
  }

  public long getSentTime() {
    return sentTime;
  }

  public Message setSentTime(long sentTime) {
    this.sentTime = sentTime;
    return this;
  }

  public MessageType getType() {
    return type;
  }

  public Message setType(MessageType type) {
    this.type = type;
    return this;
  }

  public boolean isRidden() {
    return isRidden;
  }

  public Message setRidden(boolean ridden) {
    isRidden = ridden;
    return this;
  }
}