package com.example.medmap_commons.model;

/**
 * Created by silvi on 19.03.2017.
 */

public enum State {
  PENDING, ACCEPTED, DECLINED, CANCELED;
}
