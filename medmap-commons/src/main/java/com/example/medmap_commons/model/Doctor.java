package com.example.medmap_commons.model;

/**
 * Created by silvi on 07.03.2017.
 */

public class Doctor {

  private String email;
  private String firstName;
  private String lastName;
  private String password;
  private String phoneNumber;
  private String description;
  private String experience;
  private String specialization;
  private String studies;
  private User user;
  private String birthDate;


  public Doctor() {
  }

  public String getEmail() {
    return email;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getPassword() {
    return password;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public String getDescription() {
    return description;
  }

  public String getExperience() {
    return experience;
  }

  public String getSpecialization() {
    return specialization;
  }

  public String getStudies() {
    return studies;
  }

  public User getUser() {
    return user;
  }

  public String getBirthDate() {
    return birthDate;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setSpecialization(String specialization) {
    this.specialization = specialization;
  }

  public void setStudies(String studies) {
    this.studies = studies;
  }

  public void setExperience(String experience) {
    this.experience = experience;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public void setBirthDate(String birthDate) {
    this.birthDate = birthDate;
  }
}
