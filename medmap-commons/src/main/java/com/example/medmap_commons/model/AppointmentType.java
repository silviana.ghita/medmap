package com.example.medmap_commons.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by silvi on 07.04.2017.
 */

public class AppointmentType implements Parcelable {

  private Long id;

  private Double price;

  private Double duration;

  private String type;

  public AppointmentType() {

  }

  public AppointmentType(Long id, Double price, Double duration, String type) {
    this.id = id;
    this.price = price;
    this.duration = duration;
    this.type = type;
  }

  protected AppointmentType(Parcel in) {
    this.id = in.readLong();
    this.price = in.readDouble();
    this.duration = in.readDouble();
    this.type = in.readString();
  }

  public static final Creator<AppointmentType> CREATOR = new Creator<AppointmentType>() {
    @Override
    public AppointmentType createFromParcel(Parcel in) {
      return new AppointmentType(in);
    }

    @Override
    public AppointmentType[] newArray(int size) {
      return new AppointmentType[size];
    }
  };

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public Double getDuration() {
    return duration;
  }

  public void setDuration(Double duration) {
    this.duration = duration;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeLong(id);
    dest.writeDouble(price);
    dest.writeDouble(duration);
    dest.writeString(type);
  }
}
