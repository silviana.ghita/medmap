package com.example.medmap_commons.model;

/**
 * Created by silvi on 13.03.2017.
 */

public enum Role {
  ROLE_DOCTOR,
  ROLE_USER
}
