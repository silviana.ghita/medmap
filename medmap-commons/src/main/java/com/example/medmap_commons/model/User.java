package com.example.medmap_commons.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by silvi on 07.03.2017.
 */

public class User implements Parcelable {

  private Long userId;
  private Role role;
  private String email;
  private String firstName;
  private String lastName;
  private String password;
  private String phoneNumber;
  private String birthDate;
  private Image profilePicture;

  public User() {
  }


  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getEmail() {
    return this.email;
  }

  public String getFirstName() {
    return this.firstName;
  }

  public String getLastName() {
    return this.lastName;
  }

  public String getPassword() {
    return this.password;
  }

  public String getPhoneNumber() {
    return this.phoneNumber;
  }

  public String getBirthDate() {
    return this.birthDate;
  }

  public Role getRole() {
    return role;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public void setRole(Role role) {
    this.role = role;
  }

  public Image getProfilePicture() {
    return profilePicture;
  }

  public void setProfilePicture(Image profilePicture) {
    this.profilePicture = profilePicture;
  }

  public void setBirthDate(String birthDate) {
    this.birthDate = birthDate;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(email);
    dest.writeString(firstName);
    dest.writeString(lastName);
    dest.writeString(password);
    dest.writeString(phoneNumber);
    dest.writeLong(userId);
    dest.writeString(birthDate);
    dest.writeParcelable(profilePicture, flags);

  }


  protected User(Parcel in) {
    email = in.readString();
    firstName = in.readString();
    lastName = in.readString();
    password = in.readString();
    phoneNumber = in.readString();
    userId = in.readLong();
    birthDate = in.readString();
    profilePicture = in.readParcelable(Image.class.getClassLoader());
  }

  public static final Creator<User> CREATOR = new Creator<User>() {
    @Override
    public User createFromParcel(Parcel in) {
      return new User(in);
    }

    @Override
    public User[] newArray(int size) {
      return new User[size];
    }
  };
}
