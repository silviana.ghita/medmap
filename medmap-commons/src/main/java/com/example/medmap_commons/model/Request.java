package com.example.medmap_commons.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by silvi on 19.03.2017.
 */

public class Request implements Parcelable {

  private Long id;
  private State state;
  private User user;
  private Cabinet cabinet;

  public Request() {

  }

  public Request(Long id, State state, User user, Cabinet cabinet) {
    this.id = id;
    this.state = state;
    this.user = user;
    this.cabinet = cabinet;
  }

  protected Request(Parcel in) {
    user = in.readParcelable(User.class.getClassLoader());
    cabinet = in.readParcelable(Cabinet.class.getClassLoader());
    id = in.readLong();
    String readState = in.readString();
    if (readState.equals("PENDING"))
      state = State.PENDING;
    else if (readState.equals("ACCEPTED"))
      state = State.ACCEPTED;
    else if (readState.equals("DECLINED"))
      state = State.DECLINED;
  }

  public static final Creator<Request> CREATOR = new Creator<Request>() {
    @Override
    public Request createFromParcel(Parcel in) {
      return new Request(in);
    }

    @Override
    public Request[] newArray(int size) {
      return new Request[size];
    }
  };

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public State getState() {
    return state;
  }

  public void setState(State state) {
    this.state = state;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Cabinet getCabinet() {
    return cabinet;
  }

  public void setCabinet(Cabinet cabinet) {
    this.cabinet = cabinet;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeParcelable(user, flags);
    dest.writeParcelable(cabinet, flags);
    dest.writeString(state.toString());
    dest.writeLong(id);
  }
}
