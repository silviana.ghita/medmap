package com.example.medmap_commons.model;

/**
 * Created by silvi on 10.05.2017.
 */

public enum MessageType {
  TEXT, IMAGE;
}
