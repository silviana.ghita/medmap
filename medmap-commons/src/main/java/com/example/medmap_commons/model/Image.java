package com.example.medmap_commons.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by silvi on 23.03.2017.
 */

public class Image implements Parcelable {

  private Long imageId;
  private String path;
  private User user;


  public Image() {

  }

  public Image(Long id, String path, User user) {
    this.imageId = id;
    this.path = path;
    this.user = user;
  }

  protected Image(Parcel in) {
    imageId = in.readLong();
    path = in.readString();
    user = in.readParcelable(User.class.getClassLoader());
  }

  public static final Creator<Image> CREATOR = new Creator<Image>() {
    @Override
    public Image createFromParcel(Parcel in) {
      return new Image(in);
    }

    @Override
    public Image[] newArray(int size) {
      return new Image[size];
    }
  };

  public Long getImageId() {
    return imageId;
  }

  public void setImageId(Long id) {
    this.imageId = id;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeLong(imageId);
    dest.writeString(path);
    dest.writeParcelable(user, flags);
  }
}
