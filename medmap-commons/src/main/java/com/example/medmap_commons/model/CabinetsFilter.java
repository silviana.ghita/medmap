package com.example.medmap_commons.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by silvi on 27.04.2017.
 */

public class CabinetsFilter implements Parcelable {

  private String[] specializations;
  private User[] doctors;

  public CabinetsFilter() {
  }


  public String[] getSpecializations() {
    return specializations;
  }

  public void setSpecializations(String[] specializations) {
    this.specializations = specializations;
  }

  public User[] getDoctors() {
    return doctors;
  }

  public void setDoctors(User[] doctors) {
    this.doctors = doctors;
  }

  protected CabinetsFilter(Parcel in) {
    specializations = in.createStringArray();
    doctors = in.createTypedArray(User.CREATOR);
  }

  public static final Creator<CabinetsFilter> CREATOR = new Creator<CabinetsFilter>() {
    @Override
    public CabinetsFilter createFromParcel(Parcel in) {
      return new CabinetsFilter(in);
    }

    @Override
    public CabinetsFilter[] newArray(int size) {
      return new CabinetsFilter[size];
    }
  };

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeStringArray(specializations);
    dest.writeTypedArray(doctors, flags);
  }
}
