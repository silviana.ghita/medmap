package com.example.medmap_commons.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by silvi on 14.03.2017.
 */

public class Cabinet implements Parcelable {

  private Long id;
  private String category;
  private String description;
  private Double latitude;
  private Double longitude;
  private String name;
  private String specialization;
  private User doctor;

  private List<Image> images;

  public Cabinet() {

  }

  public Cabinet(Long id, String category, String description, Double latitude, Double longitude, String name, String specialization, User doctorId) {
    this.id = id;
    this.category = category;
    this.description = description;
    this.latitude = latitude;
    this.longitude = longitude;
    this.name = name;
    this.specialization = specialization;
    this.doctor = doctorId;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Double getLatitude() {
    return latitude;
  }

  public void setLatitude(Double latitude) {
    this.latitude = latitude;
  }

  public Double getLongitude() {
    return longitude;
  }

  public void setLongitude(Double longitude) {
    this.longitude = longitude;
  }

  public String getName() {
    return name;
  }

  public List<Image> getImages() {
    return images;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSpecialization() {
    return specialization;
  }

  public void setSpecialization(String specialization) {
    this.specialization = specialization;
  }

  public User getDoctor() {
    return doctor;
  }

  public void setDoctor(User doctor) {
    this.doctor = doctor;
  }

  public void setImages(List<Image> images) {
    this.images = images;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeLong(this.id);
    dest.writeString(this.category);
    dest.writeString(this.description);
    dest.writeString(this.name);
    dest.writeString(this.specialization);
    dest.writeDouble(this.latitude);
    dest.writeDouble(this.longitude);
    dest.writeParcelable(this.doctor, flags);
    dest.writeTypedList(this.images);

  }

  protected Cabinet(Parcel in) {
    this.id = in.readLong();
    this.category = in.readString();
    this.description = in.readString();
    this.name = in.readString();
    this.specialization = in.readString();
    this.latitude = in.readDouble();
    this.longitude = in.readDouble();
    this.doctor = in.readParcelable(User.class.getClassLoader());
    this.images = in.createTypedArrayList(Image.CREATOR);
    ;
  }

  public static final Creator<Cabinet> CREATOR = new Creator<Cabinet>() {
    @Override
    public Cabinet createFromParcel(Parcel in) {
      return new Cabinet(in);
    }

    @Override
    public Cabinet[] newArray(int size) {
      return new Cabinet[size];
    }
  };


}
