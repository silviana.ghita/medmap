package com.example.medmap_commons.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by silvi on 10.04.2017.
 */

public class Appointment implements Parcelable {

  private Long id;
  private String date;
  private String hour;
  private User user;
  private Cabinet cabinet;
  private String state;
  private String delay;
  private String comment;
  private AppointmentType appointmentType;
  private boolean isExternal;

  public Appointment() {

  }

  public Appointment(Long id, String date, String hour, User user, Cabinet cabinet, String state, String delay, String comment, AppointmentType appointmentType) {
    this.id = id;
    this.date = date;
    this.hour = hour;
    this.user = user;
    this.cabinet = cabinet;
    this.state = state;
    this.delay = delay;
    this.comment = comment;
    this.appointmentType = appointmentType;
  }

  protected Appointment(Parcel in) {
    id = in.readLong();
    date = in.readString();
    hour = in.readString();
    user = in.readParcelable(User.class.getClassLoader());
    cabinet = in.readParcelable(Cabinet.class.getClassLoader());
    state = in.readString();
    delay = in.readString();
    comment = in.readString();
    isExternal = in.readByte() != 0;
    appointmentType = in.readParcelable(AppointmentType.class.getClassLoader());
  }

  public static final Creator<Appointment> CREATOR = new Creator<Appointment>() {
    @Override
    public Appointment createFromParcel(Parcel in) {
      return new Appointment(in);
    }

    @Override
    public Appointment[] newArray(int size) {
      return new Appointment[size];
    }
  };

  public Long getId() {
    return id;
  }

  public Appointment setId(Long id) {
    this.id = id;
    return this;
  }

  public boolean isExternal() {
    return isExternal;
  }

  public Appointment setExternal(boolean external) {
    isExternal = external;
    return this;
  }

  public String getDate() {
    return date;
  }

  public Appointment setDate(String date) {
    this.date = date;
    return this;
  }

  public String getHour() {
    return hour;
  }

  public Appointment setHour(String hour) {
    this.hour = hour;
    return this;
  }

  public User getUser() {
    return user;
  }

  public Appointment setUser(User user) {
    this.user = user;
    return this;
  }

  public Cabinet getCabinet() {
    return cabinet;
  }

  public Appointment setCabinet(Cabinet cabinet) {
    this.cabinet = cabinet;
    return this;
  }

  public String getState() {
    return state;
  }

  public Appointment setState(String state) {
    this.state = state;
    return this;
  }

  public String getDelay() {
    return delay;
  }

  public Appointment setDelay(String delay) {
    this.delay = delay;
    return this;
  }

  public String getComment() {
    return comment;
  }

  public Appointment setComment(String comment) {
    this.comment = comment;
    return this;
  }

  public AppointmentType getAppointmentType() {
    return appointmentType;
  }

  public Appointment setAppointmentType(AppointmentType appointmentType) {
    this.appointmentType = appointmentType;
    return this;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeLong(id);
    dest.writeString(date);
    dest.writeString(hour);
    dest.writeParcelable(user, flags);
    dest.writeParcelable(cabinet, flags);
    dest.writeString(state);
    dest.writeString(delay);
    dest.writeString(comment);
    dest.writeParcelable(appointmentType, flags);
    dest.writeByte((byte) (isExternal ? 1 : 0));
  }
}
