package com.example.medmap_commons.model;

/**
 * Created by silvi on 11.03.2017.
 */

public class UserSession {

  private static String xAuthToken;
  private static String userEmail;
  private static Long userId;
  private static Role userRole;
  private static String userFullName;

  public static String getXAuthToken() {
    return xAuthToken;
  }

  public static void setXAuthToken(String xAuthToken) {
    UserSession.xAuthToken = xAuthToken;
  }

  public static String getUserEmail() {
    return userEmail;
  }

  public static void setUserEmail(String userEmail) {
    UserSession.userEmail = userEmail;
  }

  public static Long getUserId() {
    return userId;
  }

  public static void setUserId(Long userId) {
    UserSession.userId = userId;
  }

  public static Role getUserRole() {
    return userRole;
  }

  public static void setUserRole(Role userRole) {
    UserSession.userRole = userRole;
  }

  public static String getUserFullName() {
    return userFullName;
  }

  public static void setUserFullName(String userFullName) {
    UserSession.userFullName = userFullName;
  }

  public static void clear() {
    xAuthToken = null;
    userEmail = null;
    userId = null;
    userRole = null;
    userFullName = null;
  }


  public static boolean isLogged() {
    return null != xAuthToken && !xAuthToken.isEmpty();
  }
}
