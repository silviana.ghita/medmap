package com.example.medmap_commons.model;

/**
 * Created by silvi on 14.03.2017.
 */

public enum Category {
  FOR_KIDS,
  FOR_ADULTS,
  FOR_ELDERS
}
