package com.example.medmap_commons.model;

/**
 * Created by silvi on 07.03.2017.
 */

public class RestInvalidField {

  private String field;
  private String message;

  public String getField() {
    return field;
  }

  public String getMessage() {
    return message;
  }
}
