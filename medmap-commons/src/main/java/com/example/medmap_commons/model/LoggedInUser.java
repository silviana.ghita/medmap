package com.example.medmap_commons.model;

/**
 * Created by silvi on 07.03.2017.
 */

public class LoggedInUser extends User {

  private String xAuthToken;

  public String getXAuthToken() {
    return xAuthToken;
  }

  public void setXAuthToken(String xAuthToken) {
    this.xAuthToken = xAuthToken;
  }

}
