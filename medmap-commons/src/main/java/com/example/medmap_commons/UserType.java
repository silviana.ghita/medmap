package com.example.medmap_commons;

/**
 * Created by silvi on 22.02.2017.
 */

public enum UserType {
  DOCTOR, PACIENT;

  public boolean isDoctor() {
    return DOCTOR == this;
  }

  public boolean isPacient() {
    return PACIENT == this;
  }
}
